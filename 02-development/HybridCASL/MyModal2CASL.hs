{- Aqui pretende-se implementar o comorphismo de frases escritas em ModalCASL para CASL -}
module Comorphisms.Modal2CASL (Modal2CASL (..)) where

import Logic.Logic
import Logic.Comorphism

import Common.AS_Annotation
import Common.Id
import Common.ProofTree
import qualified Common.Lib.MapSet as MapSet
import qualified Common.Lib.Rel as Rel

-- CASL
import CASL.Logic_CASL
import CASL.Sublogic as SL
import CASL.Sign
import CASL.AS_Basic_CASL
import CASL.Morphism
import CASL.Utils

-- ModalCASL
import Modal.Logic_Modal
import Modal.AS_Modal
import Modal.ModalSign
import Modal.Utils

-- generated function
import Modal.ModalSystems

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Maybe
import Control.Exception (assert)

{-  
Este tipo de dados permite identificar este comorfismo. 
-}
data Modal2CASL = Modal2CASL deriving (Show)

{-
As dependencias funcionais existentes na instanciação de Comorfismos obrigam
a criar uma "Linguagem".

class Show lid => Language lid where
    language_name :: lid -> String
    language_name = show
    description :: lid -> String
    -- default implementation
    description _ = "No description available"
-}
instance Language Modal2CASL where
	language_name Modal2CASL = "Comorphism from ModalCASL to CASL"
	description Modal2CASL = "Developed by João Melo"

{-
Uma modalidade tem mapeamento na Lógica temporal no conceito de estado. 
Sendo que pode ser uma modalidade vazia <> ou []
Ou então de um tipo definido <m> ou [m] em que m é uma modalidade de um tipo definido anteriormente.

Common/Id.hs:
type SIMPLE_ID = Token

CASL/AS_Basic_CASL.hs:
type SORT = Id
-}
data ModalityName = SimpleModality SIMPLE_ID 
                  | SortModality SORT 
                    deriving (Show, Ord, Eq)
          
{-
Agora vou definir alguns sinonimos para ajudar na interpretação da implementação.

CASL/AS_Basic_CASL.hs:
type PRED_NAME = Id
-}

{-
Para permitir "traduzir" frases em lógica Modal para FOL. É necessário que durante a construção
seja mantido um estado com várias definições, para no final ser possivel realizar a referida
tradução.
-}
data ModalityMapEnvironment = MME { 
	-- Assinatura CASL (FOL)
	caslSign :: CASLSign,
	-- Tipo do Universo
    worldSort :: SORT,
    -- Associação entre modalidades e predicados (porque as modalidades serão traduzidas em predicados)
    modalityRelMap :: Map.Map ModalityName PRED_NAME,
    -- Operações flexible
    flexOps :: Map.Map OP_NAME (Set.Set OpType),
    -- Preficados flexible
    flexPreds :: Map.Map PRED_NAME (Set.Set PredType),
{-
Common/AS_Annotation.hs:
type Named s = SenAttr s String
data SenAttr s a = SenAttr
    { senAttr :: a
    , isAxiom :: Bool
    , isDef :: Bool
    , wasTheorem :: Bool
   	-- will be set to True when status of isAxiom
    --     changes from False to True }
    , simpAnno :: Maybe Bool -- for %simp or %nosimp annotations
    , attrOrigin :: Maybe Id
    , sentence :: s } deriving (Eq, Ord, Show)

Formulas já definidas (traduções?)
-}
    relFormulas :: [Named CASLFORMULA] 
}

{-
Inicialmente é necessário transformar uma assinatura Modal no estado intermédio definido.
-}

{-
CASL/Sign.hs:
sortSet :: Sign f e -> Set.Set SORT
sortSet = Rel.keysSet . sortRel

Esta função dá como resultado um conjunto de todos os tipos existentes na assinatura.
-}

getAllSortsInSignature :: MSign -> Set.Set SORT
getAllSortsInSignature = sortSet

{-
Função que cria um tipo novo. Isto é, neste caso, é necessário criar um novo tipo
que dá a noção de universo.

Common/Id.hs:
mkId :: [Token] -> Id
mkId toks = Id toks [] nullRange
-}
newWorldSort :: SORT
newWorldSort = mkId [mkSimpleId "g_World"]

{-

-}
insertSortInOperation :: SORT -> OP_NAME -> Set.Set OpType -> Map.Map OP_NAME (Set.Set OpType) -> Map.Map OP_NAME (Set.Set OpType)
insertSortInOperation = addWorld insertNewArgument

{-
insertNewArgument recebe um tipo s e adiciona a todas as operações em t um novo argumento.

data OpType = OpType {opKind :: OpKind, opArgs :: [SORT], opRes :: SORT}
-}
insertNewArgument :: SORT -> OpType -> OpType
insertNewArgument s t = t { opArgs = s : opArgs t }

{-

addWorld :: (Ord a) => (SORT -> a -> a) -> SORT -> Id -> Set.Set a -> Map.Map OP_NAME (Set.Set a) -> Map.Map OP_NAME (Set.Set a)
addWorld f fws k set = Map.insert (addPlace k) (Set.map (f fws) set)


MapSet.fromMap . Map.foldWithKey (addWorld_OP fws) Map.empty (MapSet.toMap flexibleOps)

translateSignature :: MSign -> ModalityMapEnvironment
translateSignature sign =
   let sorSet = getAllSortsInSignature sign
       fws = freshWorldSort sorSet
       flexOps' = MapSet.fromMap . Map.foldWithKey (addWorld_OP fws)
                                    Map.empty $ MapSet.toMap flexibleOps
       flexPreds' = MapSet.fromMap . addWorldRels True relsTermMod
                    . addWorldRels False relsMod
                    . Map.foldWithKey (addWorld_PRED fws)
                                    Map.empty $ MapSet.toMap flexiblePreds
       rigOps' = rigidOps $ extendedInfo sign
       rigPreds' = rigidPreds $ extendedInfo sign
       flexibleOps = diffOpMapSet (opMap sign) rigOps'
       flexiblePreds = diffMapSet (predMap sign) rigPreds'
       relations = Map.union relsMod relsTermMod
       genRels f = Map.foldWithKey (\ me _ nm -> f me nm) Map.empty
       genModFrms f = Map.foldWithKey f []
       relSymbS me = Id [mkSimpleId "g_R"] [mkId [me]] nullRange
       relSymbT me = Id [mkSimpleId "g_R_t"] [me] nullRange
       relsMod = genRels (\ me nm -> Map.insert (SimpleM me) (relSymbS me) nm)
                         (modies $ extendedInfo sign)
       relsTermMod = genRels (\ me nm ->
                                  Map.insert (SortM me) (relSymbT me) nm)
                             (termModies $ extendedInfo sign)
       relModFrms = genModFrms (\ me frms trFrms -> trFrms ++
                                         transSchemaMFormulas partMME
                                                  fws (relSymbS me) frms)
                               (modies $ extendedInfo sign)
       relTermModFrms = genModFrms (\ me frms trFrms -> trFrms ++
                                         transSchemaMFormulas partMME
                                                  fws (relSymbT me) frms)
                               (termModies $ extendedInfo sign)
       addWorldRels isTermMod rels mp =
              let argSorts rs = if isTermMod
                             then [getModTermSort rs, fws, fws]
                             else [fws, fws] in
               Map.fold (\ rs nm -> Map.insert rs
                                              (Set.singleton $
                                                    PredType $ argSorts rs)
                                              nm)
                        mp rels
       partMME = MME {caslSign =
            (emptySign ())
               { sortRel = Rel.insertKey fws $ sortRel sign
               , opMap = addOpMapSet flexOps' rigOps'
               , assocOps = diffOpMapSet (assocOps sign) flexibleOps
               , predMap = addMapSet flexPreds' rigPreds'},
         worldSort = fws,
         modalityRelMap = relations,
         flexOps = MapSet.toMap flexibleOps,
         flexPreds = MapSet.toMap flexiblePreds,
         relFormulas = []}
      in partMME { relFormulas = relModFrms ++ relTermModFrms}
      
-}