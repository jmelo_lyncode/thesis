module GeneratePatterns where

main :: IO ()
main = mapM_ print genPatterns

toNFrm :: [[Named a]] -> Named a
toNFrm = head . head

{- The perl script will use the following subtitutions:

"inlineAxioms Modal "modality empty\npred p,q:()\n.  =>
    "inlineAxioms Modal \"modality empty\\n"++
    "pred p,q:()\\n"++
    ". ",
"inlineAxioms CASL "sort world\npred rel : world * world\nforall w1 : world\n.  =>
    "inlineAxioms CASL \"sort world \\n"++
    "pred rel : world * world\\n"++
    "forall w1 : world \\n. "

Further it generates a case expression, where the Modalformula forms
the pattern and the CASL formula is embedded in a call to addTerm.

as variables for modal formulas only 'p','q' and 'r' are recognized.

-}

genPatterns :: [(Named ModalFORMULA,Named CASLFORMULA)]
genPatterns = map (\ (x,y) -> (toNFrm x, toNFrm y))
 snip><Patterns
     [([inlineAxioms Modal "modality empty\npred p,q:()\n. [](p=>q) => (([]p) => []q)"],
       [[Nothing]]),
      ([inlineAxioms Modal "modality empty\npred p,q:()\n. [] p => <> p"],
       [inlineAxioms CASL "sort world\npred rel : world * world\nforall w1 : world\n. exists w2 : world . rel(w1,w2) %(Serial_D)%"]),
      ([inlineAxioms Modal "modality empty\npred p,q:()\n. [] p => p"],
       [inlineAxioms CASL "sort world\npred rel : world * world\nforall w1 : world\n. rel(w1,w1) %(Reflexive_M)%"]),
      ([inlineAxioms Modal "modality empty\npred p,q:()\n. [] p => [][] p "],
       [inlineAxioms CASL "sort world\npred rel : world * world\nforall w1 : world\n. forall w2,w3: world . (rel(w1,w2) /\\ rel(w2,w3)) => rel(w1,w3) %(Transitive_4)%"]),
      ([inlineAxioms Modal "modality empty\npred p,q:()\n.  p => []<> p "],
       [inlineAxioms CASL "sort world\npred rel : world * world\nforall w1 : world\n. forall w2:world . rel(w1,w2) => rel(w2,w1) %(Symmetric_B)%"]),
      ([inlineAxioms Modal "modality empty\npred p,q:()\n. <> p => []<> p"],
       [inlineAxioms CASL "sort world\npred rel : world * world\nforall w1 : world\n. forall w2,w3:world . (rel(w1,w2) /\\ rel(w1,w3)) => rel(w2,w3) %(Euclidean_5)%"])
     ]
   snip>/<Patterns
