module Rules
where

import Maybe
import List
import AST
import MyHaskell
import Variables
import Valid
import Predscheme

-- WRITE validRule:
-- Context and conclusions variables
--   A Rule may contain predicate variables (meta-variables), a
-- context meta-variable (PropVar name) and a conclusions meta-variable.
-- The context and conclusions meta-variables may occur in zero or more
-- sequents, but must be distinct from one another.

-- A set of hypotheses is validated by finding first a weakest propositional 
-- constraint to substitute for the context meta-variable such that
-- the conclusions of each hypothesized sequent are valid when the
-- conclusions meta-variable is substituted by True, and second, the
-- a strongest proposition to substitute for the conclusions meta-variable,
-- such that every hypothesized sequent is valid (satisfied under all 
-- valuations of term variables).  The context constraint and the conclusion
-- are then bound for validation of the consequent of the rule.

-- First, fix the interpretations of predicate variables.  Then,
-- to calculate the value-binding representation of the context variables
-- in a hypothetical sequent,
--        Gamma_i, ExplicitProp |- ExplicitConcl, Delta 
-- choose Gamma_i equiv -/ExplicitProp \/ ExplicitConcl.
-- Take Gamma to be the intersection of value-bindings calculated for all
-- hypotheses.  Then, using this value binding for Gamma, calculate
--        Delta_i equiv Gamma /\ ExplicitProp /\ -/ ExplicitConcl
-- Take Delta to be the union of the value-bindings calculated for all
-- hypotheses.

-- When the concludions contain no predicate variable, i.e. the sequent is
--        Gamma_i, ExplicitProp |- ExplicitConcl
-- choose Gamma_i as before.

-- Predicate variables in Rules
--   To test a rule, its predicate variables must be independently instantiated
-- with both weak and strong predicates, UnDef and Strong Univ.
-- (Strong UnDef is interpreted by the empty set of values; Undef is inter-
-- preted by the universe of values---neither can discriminate expressions)
-- Arrow predicates are interpreted by sets of traces, excluding non-monotonic
-- traces.  Univ at an arrow type (t -> s) is the set product ||t|| X ||s||
-- minus the non-monotonic traces.  The application rule applies a trace
-- and also includes _|_ e = _|_ for the weak arrow property.

-- bindContext calculates a set of variable bindings to instantiate a
-- context variable, when given a set of bindings for predicate variables

fold1 :: (a -> a -> a) -> [a] -> a
fold1 g [x] = x
fold1 g (x:xs) = g x (foldr1 g xs)

negating [] = []
negating (Not p:ps) = p : negating ps
negating (PropVar _:ps) = negating ps
negating (p:ps) = Not p : negating ps

regroup [] hs = hs
regroup (PropVar _ : cs) hs = regroup cs hs
regroup (c : cs) hs = regroup cs (c : hs)

ismember x [] = False
ismember x (y:ys) = if x == y then True
                    else ismember x ys

isAssert :: Prop -> Bool
isAssert (Assert _ _ _) = True
isAssert (Not p)        = isAssert p
isAssert (All _ _ p)    = isAssert p
isAssert (Exist _ _ p)  = isAssert p
isAssert _              = False

bindContext :: Name -> Sequent -> PredEnv -> Maybe [Aenv]
bindContext context sequent@(Consequence hypotheses conclusions) pe =
  if not (or (map isAssert hypotheses) && or (map isAssert conclusions)) then
    if not $ ismember (PropVar context) hypotheses then Nothing
    else
       let vars = allFreeVars sequent
           envs = allVs vars []
       in Just (filter (\e -> or (map (\pr -> truthV pr e pe) 
                           (regroup conclusions (negating hypotheses))))
                       envs)
  else -- There are assertions in both the hypotheses and the conclusions;
       -- Hence, terms in the conclusions are dependent on variables
       -- bound in the hypotheses
       -- prunebottoms filters out envs in which a dvar maps to Bottom
       -- denvs is the set of dependent-var envs (its values are all FTraces)
       -- In a given denv, for EACH TRACE ELEMENT, either the domain value
       -- fails to satisfy the domain hypothesis or the codomain value must
       -- satisfy the result hypothesis, or else the entire denv is rejected
    let ivars = foldr propFreeVars [] hypotheses
        domaintypes = map snd ivars
        dvars = 
           let allvars = foldr propFreeVars ivars conclusions in
           arrowTypes domaintypes $
              take (length allvars - length ivars) allvars
        pruneBottoms ns envs = 
          foldr (\n -> filter (and . (map (\(x,v) -> x /= n || v /= Bottom)))) 
                envs ns
        denvs = pruneBottoms (map fst dvars) $ allDepVals ivars dvars
    in Just $
       filter (\denv -> let newEnvs = mkNewEnvs hypotheses pe denv in
                        and (map (\rho -> 
                                     or (map (\pr -> truthV pr rho pe) 
                                             (regroup conclusions [])))
                                 newEnvs))
              denvs
        
consNewElts :: [Prop] -> PredEnv -> (Name,V) -> Aenv -> [Aenv]
consNewElts hypoths pe (n,v) rho =
   map (\r -> (n,fromJust r):rho) $
       filter (\m -> case m of {Just _ -> True; _ -> False}) $
           qualifiedResults v hypoths pe

mkNewEnvs :: [Prop] -> PredEnv -> Aenv -> [Aenv]
mkNewEnvs hypoths pe denv = foldr' (consNewElts hypoths pe) [] denv        

-- qualifiedResults returns a list of Just the codomain elements of
-- a trace value, FT trace, whose argument values satisfy a (list of) hypoths.
qualifiedResults :: V -> [Prop] -> PredEnv -> [Maybe V]
qualifiedResults (FT trace) hypoths pe = 
         concat (map (qualifyResult hypoths pe) trace)
qualifiedResults v _ _ = [Just v]

qualifyResult :: [Prop] -> PredEnv -> (V,V) -> [Maybe V]
qualifyResult [] _ elt = 
   error ("qualifyResult finds empty hypotheses" ++ show elt)
qualifyResult (h:hypoths) pe elt@(x,y) = 
   let errorMessage = "complex proposition " ++ show h ++ " not anticipated" in
   case h of
      PropVar _ -> qualifyResult hypoths pe elt
      Assert (Var _) _ pr -> if satisfies pe (Just x) pr 
                             then qualifiedResults y hypoths pe
                             else [Nothing]
      Assert (ConApp _ args) _ (ConPred n prs) ->
         case args of
            [] -> [Nothing]
            _ -> error errorMessage
      _ -> error errorMessage

dependent :: [Prop] -> [Prop] -> [Prop]
dependent hypoths concs = foldr depends concs hypoth_vars
  where
     hypoth_vars = foldr propFreeVars [] hypoths

     depends :: (Name,Type) -> [Prop] -> [Prop]
     depends nt concs = map (embedAppInAssertion hypoth_vars nt) concs

     embedAppInAssertion bvs nt pr@(Assert e t p) = 
        case freeVars bvs e t of
           [] -> pr
           unboundVars  -> Assert (embedAppInE unboundVars e nt) t p

     embedAppInAssertion bvs nt (Not pr) = Not (embedAppInAssertion bvs nt pr)

     embedAppInAssertion bvs nt (All n t pr) = 
             All n t $ embedAppInAssertion ((n,t):bvs) nt pr

     embedAppInAssertion bvs nt (Exist n t pr) = 
             Exist n t $ embedAppInAssertion ((n,t):bvs) nt pr

     embedAppInAssertion _ _ pr = pr

     embedAppInE uvs var@(Var m) (n,_) = case lookup m uvs of
                                           Nothing -> var
                                           Just _  -> App var (Var n)
     embedAppInE uvs (App e1 e2) nt = 
                         App (embedAppInE uvs e1 nt) (embedAppInE uvs e2 nt)
     embedAppInE uvs (ConApp n args) nt = 
                         ConApp n (map (\e -> embedAppInE uvs e nt) args)
     embedAppInE _ otherExp _ = otherExp


testY = let 
          hypotheses = [PropVar "Gamma", Assert (Var "x") Triv Univ,
                        Assert (Var "w") Triv (Strong Univ)]
          conclusions = [Assert (Var "y") Triv (Strong Univ),PropVar "Delta"]
          context = fromJust $
                    bindContext "Gamma" (Consequence hypotheses conclusions) []
          assertions = regroup hypotheses 
                                 (negating (dependent hypotheses conclusions))
        in (context, assertions,
            map (\e -> map (\pr -> truthV pr e []) assertions) 
                (let explicit = explicitcontext (regroup hypotheses []) in
                 case context of
                    [] -> explicit [] []
                    _  -> concat $ map (\rho -> explicit rho []) context)
                   
           )


testY' = let 
          hypotheses = [PropVar "Gamma", Assert (Var "x") Triv Univ]
          conclusions = [Assert (Var "x") Triv (Strong Univ),PropVar "Delta"]
          context = fromJust $
                    bindContext "Gamma" (Consequence hypotheses conclusions) []
          assertions = regroup hypotheses 
                                 (negating (dependent hypotheses conclusions))
        in (context, assertions,
            map (\e -> map (\pr -> truthV pr e []) assertions) 
                (let explicit = explicitcontext (regroup hypotheses []) in
                 case context of
                    [] -> explicit [] []
                    _  -> concat $ map (\rho -> explicit rho []) context)
           )

testBindContext0 = 
  (bindContext "Gamma"
     (Consequence [PropVar "Gamma", Assert (Var "x") Triv Univ]
                  [Assert (Var "x") Triv (Strong Univ)])
     [])
  == Just []

testBindContext1 = 
  (bindContext "Gamma"
     (Consequence [PropVar "Gamma", Assert (Var "x") Triv (Strong Univ)]
                  [Assert (Var "x") Triv Univ])
     [])
--   == Just [[("x",Void)]]

testBindContext2 =
   bindContext "Gamma"
     (Consequence [PropVar "Gamma", Assert (Var "x") List Univ]
                  [Assert (Var "x") List (Strong (ConPred "Nil" []))])
     []
--   == Just [[("x",Tagged "Nil" [])]]
--   == [[("x",Tagged "Cons" [Bottom,Bottom])]] -- gives False

testBindContext = 
  [bindContext "Gamma"
     (Consequence [PropVar "Gamma"]
                  [Assert (ConApp ("Cons",[Lazy,Lazy]) [Var "y", Var "z"]) 
                          List 
                          (ConPred "Cons" [Univ,(Strong Univ)])]) []
   == Just
      [[("y",Void),("z",Tagged "Cons" [Bottom,Bottom])]
      ,[("y",Bottom),("z",Tagged "Cons" [Bottom,Bottom])]
      ,[("y",Void),("z",Tagged "Nil" [])]
      ,[("y",Bottom),("z",Tagged "Nil" [])]]

  ,bindContext "Gamma"
     (Consequence [PropVar "Gamma"]
                  [Assert (Var "x") List (Strong UnDef)]) []
   == Just []
  ]

testBindContext5 =
  bindContext "Gamma"
     (Consequence [PropVar "Gamma",Assert (Var "y") Triv UnDef]
                  [Assert (Var "x") List (Strong Univ)]) []
--   == Just
--      [[("x",FT[(Void,Tagged "Cons" [Bottom,Bottom])
--               ,(Bottom,Tagged "Cons" [Bottom,Bottom])])]
--      ,[("x",FT[(Void,Tagged "Nil" []),(Bottom,Tagged "Nil" [])])]]



testNoContextVar = 
   bindContext "Gamma"
     (Consequence [Assert (Var "x") Triv Univ] [Assert (Var "x") Triv Univ]) []

testContextVar = 
   bindContext "Gamma"
     (Consequence [PropVar "Gamma",Assert (Var "x") Triv Univ]
                  [Assert (Var "x") Triv Univ]) []


-- in bindConclusion, the codomain can't simply be [Aenv] but must be Eenv,
-- where Eenv = Aenv -> Bool, is a predicate calculated from the sequent.
-- When an is
-- simply 'Var n' then the conclusion differs little from what's already
-- there.  But when it's 'App e1 e2', for instance, the form is substantially
-- different.  To evaluate truth of a conclusion element, the expression is 
-- evaluated in a context that binds names to values, and the result compared
-- with that asserted by the conclusion.

bindConclusion :: Name -> Sequent -> PredEnv -> Eenv
bindConclusion conc_var sequent@(Consequence hypotheses conclusions) pe =
   if not $ ismember (PropVar conc_var) conclusions then (\e -> False)
   else let assertions = regroup hypotheses 
                                 (negating (dependent hypotheses conclusions))
        in (\e -> and $ map (\pr -> truthV pr e pe) assertions) 



testBindConclusion = and
   [let sequent = (Consequence [PropVar "Gamma", Assert (Var "x") Triv Univ]
                               [Assert (Var "x") Triv (Strong Univ),
                                PropVar "Delta"])
        allVals = allVs (allFreeVarS [sequent]) []
    in
      map (bindConclusion "Delta" sequent []) allVals
    == [False,True]   -- delta is satisfied by "x" in {_|_}

  ,let sequent = (Consequence [PropVar "Gamma", Assert (Var "x") List Univ]
                        [Assert (Var "x") List (Strong (ConPred "Nil" [])),
                         PropVar "Delta"])
       allVals = allVs (allFreeVarS [sequent]) []
   in
     map (bindConclusion "Delta" sequent []) allVals 
   == [True,False,True]  -- delta is satisfied by "x" in {Cons(_|_,_|_), _|_}

  ,let sequent = (Consequence [PropVar "Gamma", Assert (Var "x") List Univ]
                        [Assert (Var "x") List (Strong (ConPred "Nil" []))])
       allVals = allVs (allFreeVarS [sequent]) []
   in
     map (bindConclusion "Delta" sequent []) allVals
   == [False,False,False]  -- as there is no propvar in the conclusions
  ]

-- intersectEnvs combines two sets of environments by including all 
-- compatible bindings

intersectenvs :: [Name] -> Aenv -> Aenv -> Aenv
intersectenvs [] _  _  = []
intersectenvs _  [] e2 = e2
intersectenvs _  e1 [] = e1
intersectenvs (n:ns) e1 e2 =
  case lookup n e1 of
    Nothing -> case lookup n e2 of
                 Nothing -> intersectenvs ns e1 e2
                 Just v  -> (n,v):intersectenvs ns e1 e2
    Just v  -> case lookup n e2 of
                 Nothing -> (n,v):intersectenvs ns e1 e2
                 Just v'  -> if v == v' then (n,v):intersectenvs ns e1 e2
                             else intersectenvs ns e1 e2

interEnvs :: [Name] -> [[Aenv]] -> [Aenv]
interEnvs ns es =
--   case filter (/= []) es of
  if [] `elem` es then [[]]
  else
   case es of
     [] -> []
     es'  ->
       fold1 (\e1s e2s -> 
                foldr (++) [] $
                   map (\e -> map (intersectenvs ns e) e1s) e2s) es'

unionenvs :: [Name] -> Aenv -> Aenv -> Aenv
unionenvs [] _  _  = []
unionenvs _  [] e2 = e2
unionenvs _  e1 [] = e1
unionenvs (n:ns) e1 e2 =
  case lookup n e1 of
    Nothing -> unionenvs ns e1 e2
    Just v  -> case lookup n e2 of
                 Nothing -> unionenvs ns e1 e2
                 Just v'  -> if v == v' then (n,v):unionenvs ns e1 e2
                             else unionenvs ns e1 e2

unionEnvs :: [Eenv] -> Eenv
unionEnvs = foldr (\e e' rho -> e rho || e' rho) (\rho -> False)

testZ :: [Aenv]
testZ = 
   interEnvs ["x","y"] $ filterJust $
      map (\s -> bindContext "Gamma" s [("P", Univ),("Q", Univ)])
         [Consequence [PropVar "Gamma"] [Assert (Var "x") List (PredVar "P")],
          Consequence [PropVar "Gamma"] [Assert (Var "y") Triv (PredVar "Q")]]

testZ' = 
   interEnvs ["x","y"] $ filterJust $
      map (\s -> bindContext "Gamma" s [("P", Univ),("Q", Strong Univ)])
         [Consequence [PropVar "Gamma"] [Assert (Var "x") List (PredVar "P")],
          Consequence [PropVar "Gamma"] [Assert (Var "y") List (PredVar "Q")]]

testZ'' = 
   interEnvs ["x","y"] $ filterJust $
      map (\s -> bindContext "Gamma" s [("P", Strong Univ),("Q", Strong Univ)])
         [Consequence [PropVar "Gamma"] [Assert (Var "x") List (PredVar "P")],
          Consequence [PropVar "Gamma"] [Assert (Var "y") List (PredVar "Q")]]

testZ''' = 
   interEnvs ["x","y"] $ filterJust $
      map (\s -> bindContext "Gamma" s [("P", Strong Univ),("Q", Strong Univ)])
         [Consequence [PropVar "Gamma"] [Assert (Var "x") List (PredVar "P")],
          Consequence [PropVar "Gamma"] [Assert (Var "y") List (PredVar "Q")],
          Consequence [Assert (Var "x") List (PredVar "P")] [PropVar "Delta"]]

testZ'''' =  filterJust $
      map (\s -> bindContext "Gamma" s [("P", Strong Univ),("Q", Strong Univ)])
         [Consequence [PropVar "Gamma"] [Assert (Var "x") List (PredVar "P")],
          Consequence [PropVar "Gamma"] [Assert (Var "y") List (PredVar "Q")],
          Consequence [Assert (Var "x") List (PredVar "P")] [PropVar "Delta"]]

-- sequentBindings calculates sets of bindings for the PropVars "Gamma"
-- and "Delta" that occur in the hypotheses and conclusions, respectively,
-- of a set of sequents.

-- The bindings associated with the PropVar "Gamma" in hypotheses
--  are a set of explicit valuations of
-- the term variables that occur in the sequeents, such that the sequents
-- are valid.  (If any sequent is invalid, the set  of bindings is empty.)
-- If no PropVar occurs in the hypotheses of a sequent then it must be valid
-- for all  bindings.
-- These bindings are calculated sequent-by-sequent as the maximal such set
-- of bindings (for the sequent) then intersected over the set of sequents.

-- The bindings associated with a PropVar "Delta" in a conclusion
-- is the set for which all hypotheses are satisfied but which satisfy no
-- explicitly asserted clause of the conclusion.  (This set of bindings
-- may not be necessary at all.)


sequentBindings :: Name -> Name -> PredEnv -> [Sequent] -> ([Aenv],[Aenv])
sequentBindings context conclusion pe ss =
   let vars = map fst (allFreeVarS ss)
       allVals = allVs (allFreeVarS ss) []
       ctxbndgs = 
          interEnvs vars $ filterJust $ map (\s -> bindContext context s pe) ss
       concbndgs = 
          unionEnvs $ 
             map (\s -> bindConclusion conclusion s pe) ss
       ctxbndgs' = filter (/= []) ctxbndgs
   in (ctxbndgs', filter concbndgs ctxbndgs')

-- ERRORS - interference between contextual and non-contextual clauses
--          blocks conclusions bindings in test series B3

testB0 = and
        [sequentBindings "Gamma" "Delta" []
           [Consequence [PropVar "Gamma"] [PropVar "Delta"]]
         == ([],[])

        ,sequentBindings "Gamma" "Delta" []
           [Consequence [PropVar "Gamma"] [Assert (Var "x") List (Strong UnDef)]
           ,Consequence [PropVar "Gamma"] [Assert (Var "y") Triv Univ]]
         == ([],[])  -- because the first sequent is unsatisfiable

        ,sequentBindings "Gamma" "Delta" [("P", Strong Univ),("Q", Univ)]
         [Consequence [PropVar "Gamma"] [Assert (Var "x") List (PredVar "P")],
          Consequence [PropVar "Gamma"] [Assert (Var "x") List (PredVar "Q")]]
         == ([[("x",Tagged "Cons" [Bottom,Bottom])],
              [("x",Tagged "Nil" [])]],[])

        ,sequentBindings "Gamma" "Delta" [("P",Strong Univ),("Q",Strong Univ)]
         [Consequence [PropVar "Gamma"] [Assert (Var "x") List (PredVar "P")],
          Consequence [PropVar "Gamma"] [Assert (Var "y") List (PredVar "Q")]]
         == ([[("x",Tagged "Cons" [Bottom,Bottom]),
               ("y",Tagged "Cons" [Bottom,Bottom])],
              [("x",Tagged "Nil" []),("y",Tagged "Cons" [Bottom,Bottom])],
              [("x",Tagged "Cons" [Bottom,Bottom]),("y",Tagged "Nil" [])],
              [("x",Tagged "Nil" []),("y",Tagged "Nil" [])]], [])

        ,sequentBindings "Gamma" "Delta" [("P", Univ),("Q", Strong Univ)]
         [Consequence [PropVar "Gamma"] [Assert (Var "x") List (PredVar "P")],
          Consequence [] [Assert (Var "x") List (PredVar "Q"),PropVar "Delta"]]
         == ([[("x",Tagged "Cons" [Bottom,Bottom])],
              [("x",Tagged "Nil" [])],
              [("x",Bottom)]],[[("x",Bottom)]])

        ,sequentBindings "Gamma" "Delta" 
         [("P", Univ),("Q", Strong (ConPred "Nil" []))]
         [Consequence [PropVar "Gamma"] [Assert (Var "x") List (PredVar "P")],
          Consequence [] [Assert (Var "x") List (PredVar "Q"),PropVar "Delta"]]
         == ([[("x",Tagged "Cons" [Bottom,Bottom])],
              [("x",Tagged "Nil" [])],
              [("x",Bottom)]],
              [[("x",Tagged "Cons" [Bottom,Bottom])],[("x",Bottom)]])

        ,sequentBindings "Gamma" "Delta" [("P", Univ),("Q", Strong Univ)]
         [Consequence [PropVar "Gamma"] [Assert (Var "x") List (PredVar "P")],
          Consequence [Assert (Var "x") List (PredVar "P")] 
                      [PropVar "Delta"]]
         == ([[("x",Tagged "Cons" [Bottom,Bottom])],
              [("x",Tagged "Nil" [])],
              [("x",Bottom)]],
             [[("x",Tagged "Cons" [Bottom,Bottom])],
              [("x",Tagged "Nil" [])],
              [("x",Bottom)]])
        ]

testB3' = 
   sequentBindings "Gamma" "Delta" [("P", Univ),("Q", Strong Univ)]
         [Consequence [Assert (Var "x") List (PredVar "P")] 
                      [Assert (Var "x") List (PredVar "Q"),PropVar "Delta"]]

testB3'' = 
   sequentBindings "Gamma" "Delta" [("P", Univ),("Q", Strong Univ)]
         [Consequence [PropVar "Gamma"] [Assert (Var "x") List (PredVar "P")],
          Consequence [Assert (Var "x") List (PredVar "P")] 
                      [Assert (Var "x") List (PredVar "Q"),PropVar "Delta"]]

testB3''' = 
   sequentBindings "Gamma" "Delta" [("P", Univ),("Q", UnDef)]
         [Consequence [PropVar "Gamma"] [Assert (Var "x") List (PredVar "P")],
          Consequence [Assert (Var "x") List (PredVar "P")] 
                      [Assert (Var "x") List (PredVar "Q"),PropVar "Delta"]]

testB3'''' = 
   sequentBindings "Gamma" "Delta" [("P", Univ),("Q", UnDef)]
         [Consequence [Assert (Var "x") List (PredVar "P")] 
                      [Assert (Var "x") List (PredVar "Q"),PropVar "Delta"]]

testB4 = sequentBindings "Gamma" "Delta" []
           [Consequence [Assert (Var "x") Triv Univ] [PropVar "Delta"]]

testB4' =sequentBindings "Gamma" "Delta" []
          [Consequence [Assert (Var "x") Triv (Strong Univ)] [PropVar "Delta"]]

bindcontext :: Name -> Sequent -> Aenv -> PredEnv -> [Aenv]
bindcontext context sequent@(Consequence hypotheses conclusions) env pe =
   if not $ ismember (PropVar context) hypotheses then []
   else
      let vars = allFreeVars sequent
          envs = allVs vars env
          envs' = filter (\e -> or (map (\pr -> truthV pr e pe) 
                            (regroup conclusions (negating hypotheses))))
                         envs
      in case envs' of
            [] -> [env]
            _  -> envs'

testbindcontext = 
   bindcontext "Gamma" (Consequence [PropVar "Gamma"]
                                    [Assert (Var "x") List (ConPred "Nil" [])])
               [("x",Bottom)] []
testbindcontext' = 
   bindcontext "Gamma" (Consequence [PropVar "Gamma"]
                                    [Assert (Var "x") List (ConPred "Nil" [])])
               [("x",Tagged "Nil" [])] []

testbindcontext'' = 
   bindcontext "Gamma" (Consequence [PropVar "Gamma"]
                                    [Assert (Var "x") List (ConPred "Nil" [])])
               [("y",Void)] []

testbindcontext''' =
   bindcontext "Gamma" (Consequence [PropVar "Gamma",
                                     Assert (Var "x") List Univ,
                                     Assert (Var "y") Triv Univ]
                                    [Assert (Var "x") List (Strong Univ)])
               [] []

testbindcontext'''' =
   bindcontext "Gamma" (Consequence [PropVar "Gamma",
                                     Assert (Var "x") List Univ]
                                    [Assert (Var "y") Triv Univ,
                                     Assert (Var "x") List (Strong Univ)])
               [] []

assertsUnSat :: Prop -> PredEnv -> Bool
assertsUnSat (Assert (ConApp (_,ls) _) _ (ConPred _ ps)) pe =
   or (map (\(p,l) -> l==Strict && isUnDef p pe) (zip ps ls))
assertsUnSat (Assert _ _ (Strong p)) pe = isUnDef p pe
assertsUnSat (All _ _ prop) pe = assertsUnSat prop pe
assertsUnSat (Exist _ _ prop) pe = assertsUnSat prop pe
assertsUnSat _ _ = False

isUnDef UnDef pe = True
isUnDef (Strong p) pe = isUnDef p pe
isUnDef (PredVar x) pe = lookup x pe == Just UnDef
isUnDef _ _ = False

assertsUnDef (Assert t _ UnDef) _ = case t of {Var _ -> True; _ -> False}
assertsUnDef (Assert t _ (PredVar x)) pe = 
   lookup x pe == Just UnDef && case t of {Var _ -> True; _ -> False}
assertsUnDef _ _ = False

bindexplicitProp :: Prop -> Aenv -> PredEnv -> Bool -> [Aenv]
bindexplicitProp (Not p) e pe b = bindexplicitProp p e pe (not b)
bindexplicitProp p e pe b =
   let vals = allVs (propFreeVars p []) e in
   if assertsUnSat p pe then [[]] 
   else if b then 
      filter (\e' -> truthV p e' pe) vals
   else 
      filter (not . (\e' -> truthV p e' pe && not (assertsUnDef p pe))) vals

explicitcontext :: [Prop] -> Aenv -> PredEnv -> [Aenv]
explicitcontext [] rho pe = [rho]
explicitcontext ps rho pe = 
   let ps' = filter (\p -> case p of
                              PropVar _ -> False
                              _         -> True) ps
   in folde (\p e -> bindexplicitProp p e pe True) rho ps' 

folde :: (a -> b -> [b]) -> b -> [a] -> [b]
folde f e [] = [e]
folde f e (a:as) = foldr (++) [] $ map (\e' -> folde f e' as) (f a e)

unSatisfiable :: Sequent -> PredEnv -> Bool
unSatisfiable (Consequence _ conclusions) pe = 
   or $ map (\pr -> assertsUnSat pr pe) conclusions

validaterule' (Conclude antecedents consequent) pe =
    sequentBindings "Gamma" "Delta" pe antecedents

validaterule :: Rule -> PredEnv -> Bool
validaterule (Conclude antecedents consequent) pe = 
   if or (map (\s -> unSatisfiable s pe) antecedents) 
   then True -- "Unsatisfiable antecedent "
   else
      let (context,_) = sequentBindings "Gamma" "Delta" pe antecedents
      in checkSequent consequent context resultBndg pe
   where
      resultBndg = unionEnvs $ 
                      map (\s -> bindConclusion "Delta" s pe) antecedents

emptyBndg :: Eenv
emptyBndg = (\rho -> False)

checkSequent_ sequent envs concs pe =
   let Consequence hypotheses conclusions = sequent in
   case envs of
     [] -> explicitcontext hypotheses [] pe
     _  -> foldr (++) [] $ map (\e -> explicitcontext hypotheses e pe) envs

checkSequent :: Sequent -> [Aenv] -> Eenv -> PredEnv -> Bool
checkSequent sequent envs concs pe =
   let Consequence hypotheses conclusions = sequent

       envs' = 
        case envs of
         [] -> explicitcontext hypotheses [] pe
         _  -> if hasPropVar hypotheses then
                  foldr (++) [] $ 
                    map (\e -> explicitcontext hypotheses e pe) envs
               else explicitcontext hypotheses [] pe

       checkExplicitConclusions =
          case envs' of
            [] -> 
                error $ show $
                  or (map (\pr -> case pr of
                                    PropVar _ -> False
                                    _ -> if containsvars pr [] then True
                                         else truthV pr [] pe) conclusions)
            [[]] -> 
                error $ show $
                  or (map (\pr -> case pr of
                                    PropVar _ -> False
                                    _ -> if containsvars pr [] then True
                                            -- because hypotheses are unsatis.
                                         else truthV pr [] pe) conclusions)

            _  -> and $             -- for all variable bindings,
                    map (\e -> or $ -- true of at least one conclusion
                      map (\pr -> case pr of
                                    PropVar _ -> False
                                    _ -> truthV pr e pe) 
                          conclusions) envs'
   in
   case conclusions of
     [] -> False   -- an empty list of conclusions is manifestly unsatisfiable
     _  -> 
        if not (hasPropVar conclusions) then checkExplicitConclusions
        else case envs' of
               [] -> True    -- the assumptions are unsatisfiable
                  -- checkExplicitConclusions    -- no support for a PropVar
               [[]] -> True  -- the assumptions are unsatisfiable
               _ -> or $ map (\prop -> case prop of
                                         PropVar _ -> checkProp concs envs'
                                         _ -> checkExplicitConclusions) 
                             conclusions

checkProp :: Eenv -> [Aenv] -> Bool
checkProp conc envs = and $ map conc envs


-- validateSequent uses bindcontext to find a context binding under which
--   the sequent is valid
validateSequent sequent envs concs pe = 
   let (Consequence hypotheses conclusions) = sequent in
   let envs' = foldr (++) [] $
                  map (\e -> bindcontext "Gamma" sequent e pe) envs in
   case envs' of
      [] -> or (map (\pr -> truthV pr [] pe) conclusions)
      _  -> and (map (\e -> or (map (\pr -> truthV pr e pe) conclusions)) envs')

allPredVars :: Sequent -> [Name]
allPredVars (Consequence hypotheses conclusions) =
   let hp = foldr union [] $ map predvars hypotheses in
   foldr union hp $ map predvars conclusions
   where
      predvars :: Prop -> [Name]
      predvars (Assert _ _ pr) = predVars pr []
      predvars (Not p) = predvars p
      predvars (All _ _ p) = predvars p
      predvars (Exist _ _ p) = predvars p
      predvars (PropVar _) = []
      predvars _ = []

      predVars :: Pr -> [Name] -> [Name]
      predVars (PredVar n) ns = if n `elem` ns then ns else (n:ns)
      predVars (ConPred _ prs) ns = foldr predVars ns prs
      predVars (Strong pr) ns = predVars pr ns
      predVars _ ns = ns

-- allPreds calculates predicate variable bindings, instantiating
-- each predicate variable both to Undef and to $Univ.

allPreds :: [Name] -> [PredEnv]
allPreds [] = [[]]
allPreds [n] = [[(n,UnDef)],[(n,Strong Univ)]]
allPreds (n:ns) = let allns = allPreds ns in
                  map (\pe -> (n,UnDef):pe) allns ++
                  map (\pe -> (n,Strong Univ):pe) allns


-- validateRule_ maps validaterule over all bindings of predicate variables

validateRule_ :: Rule -> [Bool]
validateRule_ r@(Conclude antecedents consequent) = 
   let predVars = 
        foldr union (allPredVars consequent) (map allPredVars antecedents)
   in
   case (allPreds predVars) of
      []  -> [validaterule r []]
      pes -> map (validaterule r) pes

validateRule :: Rule -> Bool
validateRule r = and $ validateRule_ r
