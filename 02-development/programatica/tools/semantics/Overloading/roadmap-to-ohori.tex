\documentclass{entcs}
\usepackage{entcsmacro}
\usepackage{epsfig}
\usepackage{graphics,color}
\usepackage{url}

%\newtheorem{defn}{Definition}

\usepackage{proof}
\usepackage{rotcapt}
\usepackage{rotate}
\usepackage{rotating}
\usepackage{diagrams}
\usepackage{epsfig}
\pagestyle{plain}
\makeatother
\thispagestyle{empty}

%\include{globals}
\newcommand{\classpred}[1]{\ensuremath{\mathsf{#1}}}
\newcommand{\Eq}[1]{\ensuremath{\classpred{Eq}\,#1}}
\newcommand{\Bool}{\ensuremath{\mathit{Bool}}}
\newcommand{\dpdp}{\ensuremath{\mathbf{::}}}
\newcommand{\deuxp}{\ensuremath{\mathbf{:}}}
\newcommand{\fa}[1]{\ensuremath{\forall #1.}}
\newcommand{\ra}{\ensuremath{\!\rightarrow\!}}
\newcommand{\fix}[1]{\ensuremath{\mathsf{fix}(#1)}}
\newcommand{\oscheme}[3]{\ensuremath{\fa{#1} #2 \Rightarrow #3}}
\newcommand{\judgement}[4]{\ensuremath{#1\; \mathbf{|} \; #2\; \vdash\; #3\; \mathbf{:}\; #4}}
\newcommand{\judge}[3]{\ensuremath{#1 \vdash\; #2 \mathbf{:}\; #3}}
\newcommand{\heq}{\ensuremath{\mathtt{==}}}

\newcommand{\hequal}[2]{\ensuremath{#1 \heq #2}}
\newcommand{\hand}{\ensuremath{\mbox{\tt \&\&}}}
\newcommand{\hpair}[2]{\ensuremath{( #1,#2)}}
\newcommand{\evid}[2]{\ensuremath{#1\, \vdash\!\!\vdash\, #2}}
\newcommand{\product}[2]{\ensuremath{#1 \! \times \! #2}}

\newcommand{\tuple}[1]{\ensuremath{\langle #1 \rangle}}
\newcommand{\D}[1]{\ensuremath{D_{#1}}}
\newcommand{\Type}{\ensuremath{\textsf{Type}}}
\newcommand{\typeschemes}{\ensuremath{\textsf{TypSch}}}
\newcommand{\CoreML}{\ensuremath{\mbox{Core-ML}}}
\newcommand{\hey}[1]{\ensuremath{\mbox{{\bf\em #1}}}}
\newcommand{\A}{\ensuremath{\mathcal{A}}}
\newcommand{\M}{\ensuremath{\mathcal{M}}}
\newcommand{\letexp}[3]{\ensuremath{\mbox{let }#1=#2\mbox{ in }#3 }}
\newcommand{\coremlJ}[3]{\ensuremath{#1 \rhd #2 : #3 }}

\newenvironment{observe}{\vspace{-\lastskip}\par \addvspace{.6pc
    plus .2pc minus
    .1pc}\begin{remark}\rm}{\end{remark}\par\addvspace{.6pc plus .2pc
    minus .1pc}}
    
\newcommand{\TL}{\ensuremath{T\Lambda}}  
\newcommand{\TLJ}[3]{\ensuremath{#1 \rhd\!\!\!\!\!\cdot\,\,\,\, #2 : #3 }}
\newcommand{\ldbrack}{[\![}
\newcommand{\rdbrack}{]\!]}
\newcommand{\standard}[1]{\ensuremath{\ldbrack #1 \rdbrack}}
\newcommand{\env}[1]{\ensuremath{\mbox{Env}(#1)}}
\newcommand{\trans}[1]{\ensuremath{t\lambda(#1)}}
\newcommand{\TA}[1]{\ensuremath{\mbox{TA}(#1)}}
\newcommand{\dom}[1]{\ensuremath{\mathsf{dom}(#1)}}
\newcommand{\TP}[2]{\ensuremath{\mbox{TP}(#1,#2)}}
\newcommand{\Msem}[1]{\ensuremath{\M\standard{#1}}}
\newcommand{\CMLsem}[1]{\ensuremath{\M\standard{#1}^{ml}}}
\newcommand{\TLsem}[1]{\ensuremath{\standard{#1}}}
\newcommand{\OMLsem}[1]{\ensuremath{\M\standard{#1}^{oml}}}


\newcommand{\code}[1]{\ensuremath{\mbox{{\it #1}}}}
\newcommand{\forget}[1]{}

\pagestyle{empty}

\begin{document}
\def\lastname{}
\begin{frontmatter}
  \title{A Roadmap to Atsushi Ohori's ``{\em A Simple Semantics for ML Polymorphism}''} 
%  \author{Bill Harrison}
%\address{Pacific Software Research Center\\ 
%         OGI School of Science \& Engineering\\
%         Oregon Health \& Science University\\
%         Beaverton, Oregon USA}       
\end{frontmatter}

\section{Introduction}

What follows is an attempt to aid readers of Ohori's ``{\em A Simple Semantics for ML Polymorphism}.'' Why? There are at least two good reasons. Firstly, I believe---and I'm not alone in this---that Ohori's model of ML polymorphism can be extended in a natural way to
Haskell98. The main technical concern is the presence of overloading in Haskell98, but this is not as big a challenge as one might at first believe. Secondly, although Ohori's semantics is sufficiently natural and elegant to lead one to slap one's forehead and exclaim, ``why didn't I think of that?'', his presentation in \cite{Ohori89} is confusing and disorganized. He frequently uses (what are to my mind) standard terminology in non-standard ways and presents his ideas in a somewhat strange order. My intention is to provide the reader with the relevant details in a more logical fashion. My hope is that, with this ``roadmap'' in one hand and Ohori's 1989 paper in the other, that his ideas will be easier to digest and their relevance to Haskell98 made clear.

\subsection{An Illustration.}

The meaning of the ML identity function \((\lambda x.x)\) is:
\[
\{~ \tuple{\tau\ra\tau, \mbox{id}_{\D{\tau}}}
~|~ \tau \in \Type
~\}
\]
\noindent where \(\Type\) is the set of simple types and \(\D{\tau}\) is the meaning of \(\tau\) (a type-frame).

\section{The Language \CoreML}


There are two classes of types in \cite{Ohori89} (Section 1.1). 
\begin{definition}[Types and Type Schemes for \CoreML]\label{coremltypes}
There are simple types 
(ranged over by \(\tau\) and referred to only as {\em types}) and type schemes
(ranged over by \(\rho\)). Type schemes may contain type variables which are  (implicitly)
quantified.
\end{definition}
\[
\begin{array}{lclcl}
\tau & ::= & b ~|~ \tau \rightarrow \tau &~~~~& \mbox{(\Type)}
\\
\rho & ::= & t ~|~ b ~|~ \rho \rightarrow \rho &~~~~& \mbox{(\typeschemes)}
\end{array}
\]


\begin{definition} 
A \hey{type assignment} \A\/ is any mapping from a finite set of variables to \Type.
\end{definition}

\begin{definition} 
A \hey{type assignment scheme} \(\Sigma\) is any mapping from a finite set of variables 
to \typeschemes. It probably makes more sense to call this a ``type-scheme assignment.''
\end{definition}

\begin{definition} A \hey{substitution} is a function \(\theta\) from type variables to 
\typeschemes\/ s.t. \(\theta \,t \neq t\) for only finitely-many type variables \(t\). N.b., substitutions may be extended to maps from \typeschemes\/ to \typeschemes\/ in an obvious manner.
\end{definition}

\begin{definition}\label{coremlrawterms} The \hey{raw terms of \CoreML} are just the untyped \(\lambda\)-calculus. N.b., that (\letexp{x}{e}{e}) is non-recursive.
\[
\begin{array}{lclcl}
e ~ &::=& ~ x~|~ (e~e) ~|~ \lambda x.~e ~|~ \letexp{x}{e}{e}
\end{array}
\]
\end{definition}


\begin{definition}\label{coremltypings} A \hey{typing} is any formula, \coremlJ{\A}{e}{\tau}, derivable in:
\[
\begin{array}{lcl}
\mbox{(var)} &~~& \coremlJ{\A}{x}{\tau},~ \mbox{ if } \A(x) = \tau
\\[1ex]
\mbox{(app)} &~~& \begin{array}{lcl}
                  \infer[]{\coremlJ{\A}{(e_1~e_2)}{\tau_2}}
                          {\coremlJ{\A}{e_1}{\tau_1\ra\tau_2} & \coremlJ{\A}{e_2}{\tau_1}}
                  \end{array}
\\[1ex]
\mbox{(abs)} &~~& \begin{array}{lcl}
                  \infer[]{\coremlJ{\A}{\lambda x. e}{\tau_1\ra\tau_2}}
                          {\coremlJ{\A\{x := \tau_1\}}{e}{\tau_2}}
                  \end{array}
\\[1ex]
\mbox{(let)} &~~& \begin{array}{lcl}
                  \infer[]{\coremlJ{\A}{\letexp{x}{e_2}{e_1}}{\tau}}
                          {\coremlJ{\A}{e_1[e_2/x]}{\tau} & 
                           \coremlJ{\A}{e_2}{\tau'}}
                  \end{array}\end{array}
\]
\end{definition}

\begin{observe}
Observe that the above system only defines type inference for (simple) type assignments
\A. Definition~\ref{typingscheme} below extends this system to type assignment schemes \(\Sigma\).
\end{observe}

\begin{definition} \label{typingscheme}
A formula, \coremlJ{\Sigma}{e}{\rho}, is a \hey{typing scheme} if, for all
ground instances \((\A,\tau)\) of \((\Sigma,\rho)\), \coremlJ{\A}{e}{\tau}\/
is a typing. %In which case, we write \typingscheme{\Sigma}{e}{\rho}.
\end{definition}

\begin{definition}
The \hey{terms of \CoreML} are the set of all typing schemes.
\end{definition}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{The Explicitly-Typed Language \TL}

\begin{definition} The \hey{type language for \TL} is \Type\/ 
(see Definition~\ref{coremltypes}). 
\end{definition}

\begin{definition}
The \hey{pre-terms for \TL} are:
\[
\begin{array}{lcl}
M & ::= & x ~|~ (M~M) ~|~ \lambda x:\tau.\, M
\end{array}
\]
\end{definition}

\begin{definition}\label{TLtypings} The type system for \TL\/ is what one would expect:
\end{definition}
\[
\begin{array}{lcl}
\mbox{(VAR)} &~~& \TLJ{\A}{x}{\tau},~ \mbox{ if } \A(x) = \tau
\\[1ex]
\mbox{(ABS)} &~~& \begin{array}{lcl}
                  \infer[]{\TLJ{\A}{\lambda x:\tau_1. ~M}{\tau_1\ra\tau_2}}
                          {\TLJ{\A\{x := \tau_1\}}{M}{\tau_2}}
                  \end{array}
\\[2ex]
\mbox{(APP)} &~~& \begin{array}{lcl}
                  \infer[]{\TLJ{\A}{(M_1~M_2)}{\tau_2}}
                          {\TLJ{\A}{M_1}{\tau_1\ra\tau_2} & \TLJ{\A}{M_2}{\tau_1}}
                  \end{array}
\end{array}
\]

\subsection{Denotational Semantics of \TL}

Given in terms of {\em frame models} \cite{Gunter} (a.k.a. {\em Henkin Models} 
\cite{MitchellFoundations}).
\[
\begin{array}{lcl}
\standard{\TLJ{\A}{x}{\tau}}\varepsilon & = & \varepsilon~x
\\
\standard{\TLJ{\A}{\lambda x:\tau_1. M}{\tau_1\ra\tau_2}}\varepsilon & = & 
      \begin{array}[t]{lcl}
      \lefteqn{
      \mbox{ the } f\in \D{\tau_1\ra\tau_2} \mbox{ s.t. }
      }
      \\
      ~~~~~
      f \bullet d & = & \standard{M}\varepsilon[x\mapsto d]
      \mbox{ for all } d\in \D{\tau_1}
      \end{array}
\\      
\standard{\TLJ{\A}{(M~N)}{\tau}}\varepsilon & = & 
(\standard{\TLJ{\A}{M}{\tau'\ra\tau}}\varepsilon)
\bullet
(\standard{\TLJ{\A}{N}{\tau}}\varepsilon)
\end{array}
\]
where \(\varepsilon\in\env{\A}\) is an environment compatible with \A:
\[
\varepsilon\in\env{\A} 
~\Leftrightarrow ~
\varepsilon x \in \D{\tau} \mbox{ where }\A x = \tau
\]


\subsubsection{Background on Type-frames Semantics }

If it helps, one can think of a frame model as set-theoretic version of a cartesian closed category. It provides ``objects''
(i.e., the \(\D{\tau}\)) and axioms characterizing functions 
(i.e., representability and extensionality).
\begin{definition}
A \hey{frame} is a pair \(\tuple{\mathcal{D},\bullet}\) where
\begin{enumerate}
\item \(\mathcal{D} = \{ \D{\tau}~|~ \tau \in \Type~\&~ \D{\tau}\neq\emptyset\}\) 

\item \(\bullet\) is a family of operations 
\(\bullet_{\tau_1\tau_2} : \D{(\tau_1\ra\tau_2)} \ra \D{\tau_1} \ra \D{\tau_2}\)
      \begin{enumerate}
      \item \(\phi : \D{\tau_1} \ra \D{\tau_2}\) is {\em representable} if 
            \[\exists\,f\in\D{(\tau_1\ra\tau_2)} \mbox{ s.t. }
            \phi(d) = f\bullet d,~~\forall d\in\D{\tau_1}
            \]
      \item \(\tuple{\mathcal{D},\bullet}\) is {\em extensional} if, for all \(d\in\D{\tau_1}\),
                \(f\bullet d = g \bullet d~ \Rightarrow f=g\)
      \end{enumerate}
\end{enumerate}
\end{definition}


The usual CPO semantics of the typed \(\lambda\)-calculus with recursion 
are also Henkin models  \cite{MitchellFoundations}.
Accordingly, we introduce the following terminology for such frames:
\begin{definition} A \hey{pcpo frame} is a frame \(\tuple{\mathcal{D},\bullet}\)
in which each \(\D{\tau}\in\mathcal{D}\) is a pointed, complete, partial order.
\end{definition}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Relating \CoreML\/ to \TL}


\begin{definition} Introduce a translation, \trans{-}, from derivations of \CoreML\/ typings (i.e., terms with not type variables) to
\TL\/ terms. 
\[
\begin{array}{lcl}
t\lambda\left(
\begin{array}{lcl}\infer[]{\coremlJ{\A}{x}{\tau}}{}\end{array}\right)
&
=
& x
\\[2ex]
t\lambda\left(
\begin{array}{lcl}
\infer[]{\coremlJ{\A}{\lambda x. e}{\tau_1\ra\tau_2}}
                          {\Delta}
\end{array}
\right)
& 
=
&
\lambda x:\tau_1. ~\trans{\Delta}
\\[2ex]
t\lambda\left(
\begin{array}{lcl}
                  \infer[]{\coremlJ{\A}{(e_1~e_2)}{\tau_2}}
                          {\Delta_1 & \Delta_2}
\end{array}
       \right)
& =
& (\trans{\Delta_1}~~\trans{\Delta_2})
\\[2ex]
t\lambda\left(
\begin{array}{lcl}
                  \infer[]{\coremlJ{\A}{\letexp{x}{e_2}{e_1}}{\tau}}
                          {\Delta_1 & \Delta_2}
                  \end{array}
       \right)
& =
& \trans{\Delta_1}
\end{array}
\]
\end{definition}

A consequence of the Theorem 6 in Ohori\cite{Ohori89}, Section 3.2, is the following:

\noindent {\bf Theorem.} Let \(\Delta_1\), and \(\Delta_2\) be derivations of the
same typing \((\coremlJ{\A}{e}{\tau})\), then:
\[
\standard{\TLJ{\A}{\trans{\Delta_1}}{\tau}} = \standard{\TLJ{\A}{\trans{\Delta_1}}{\tau}}
\]
This theorem allows us to define the meanings of \CoreML\/ typings in terms of translations into \TL.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Semantics of \CoreML}

\begin{definition}
The set of \hey{admissible type assignments under \(\Sigma\)} is:
\end{definition}
\[
\TA{\Sigma} = \{ \A~|~ \exists \,\theta. ~\A\uparrow^{\dom{\Sigma}} =~ \theta \circ \Sigma~\}
\]
\noindent where \(\A\uparrow^{\dom{\Sigma}}\) is the restriction of \A\/ to the domain of \(\Sigma\).

\begin{observe}
\(\A \in \TA{\Sigma}\) means intuitively that corresponding bindings in 
\((\A\uparrow^{\dom{\Sigma}})\) have the same ``shape'' as those in \(\Sigma\). For example, let
\[
%\begin{array}{lcl}
\Sigma = (t \mapsto (a\rightarrow Int) ),~~
\A_0(t) = Int \rightarrow Int,~~
\A_1(t) = Char \times Int
%\end{array}
\]
\noindent then
\(\A_0 \in \TA{\Sigma}\) but \(\A_1 \not\in \TA{\Sigma}\) 
\end{observe}

\begin{definition}
For a given type assignment \A,
the set of types associated with the \CoreML\/ term (\coremlJ{\Sigma}{e}{\rho}) is:
\[
\TP{\A}{\coremlJ{\Sigma}{e}{\rho}}
=
\{
\tau \in \Type ~|~
\exists\, \theta~\mbox{s.t.}~
\A\uparrow^{\dom{\Sigma}} = \theta \circ \Sigma
~~\&~~ \tau = \theta \rho 
\}
\]
\end{definition}


\begin{definition}[Semantics of \CoreML] Let \M\/ be any model of \TL\/
and \((\coremlJ{\Sigma}{e}{\rho})\) be any \CoreML\/ term.
Assuming 
\(\A\in\TA{\Sigma}\) and \(\varepsilon \in \env{\A}\),
then 
the following equations define the semantics of \CoreML, \CMLsem{-}:
\end{definition}
\[
\hspace{-20ex}
\begin{array}{llcl}
\mbox{(Terms)}  & \CMLsem{\coremlJ{\Sigma}{e}{\rho}}\,\A\,\varepsilon & = & 
\\
\lefteqn{\hspace{20ex}\{\,(\tau,\CMLsem{\coremlJ{\A}{e}{\tau}}\,\varepsilon)~|~ 
                                \tau\in\TP{\A}{\coremlJ{\Sigma}{e}{\rho}}\,\}}
\\
\mbox{(Typings)}~~~~ & \CMLsem{\coremlJ{\A}{e}{\tau}}\,\varepsilon & = & 
                             \TLsem{\TLJ{\A}{\trans{\Delta}}{\tau}}
                             \\
\lefteqn{\hspace{25ex}\mbox{for some derivation \(\Delta\) of (\coremlJ{\A}{e}{\tau})}}
\end{array}
\]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\newpage

\section{Making ML Polymorphism More Ad-Hoc}

\subsection{Illustrating Overloading without Polymorphic Recursion}

We denote the class constraints as set functions from
\Type\/ to type frames (just as \CoreML\/ terms):
\[
\standard{\mathsf{Eq}} = 
          \{ %\begin{array}{lcl}
             \tuple{\code{Int}\ra\code{Int}\ra\code{Bool},\code{eqInt}},\, %\\
             \tuple{\code{Float}\ra\code{Float}\ra\code{Bool}, \code{eqFloat}},\, %\\
             \ldots 
             %\end{array}
             \}
\]


Now consider what the meaning of the term:
\[
e = (\judgement{\bullet}{\bullet}{\lambda x.~ \hequal{x}{x}}{\Eq{a} \Rightarrow a \ra \Bool})
\]
where ``\(\bullet\)'' refers to empty constraints.

Just as with \CoreML, the meaning of \(e\) will be defined in terms of the set of its ground
instances:
\[
\left\{
\begin{array}{c}
\OMLsem{(\judgement{\bullet}{\bullet}{\lambda x.~ \hequal{x}{x}}{\Eq{\code{Int}} \Rightarrow \code{Int} \ra \Bool})},
\\
%\OMLsem{(\judgement{\bullet}{\bullet}{\lambda x.~ \hequal{x}{x}}{\Eq{\code{Float}} \Rightarrow \code{Float} \ra \Bool})},
%\\
\vdots
\end{array}
\right\}
\]

These, in turn, will be defined in terms of translations into an explicitly-typed, simply-typed language analogous to \TL:
\[\hspace{-5ex}
\left\{
\begin{array}{c}
\Msem{(\judgement{\bullet}
                 {\bullet}
                 {\lambda (x:\code{Int}).~ (x:\code{Int})
                                           (\heq : {\code{Int}\ra\code{Int}\ra\code{Bool}})
                                           (x:\code{Int})}
{\ldots})}
,
\\
%\Msem{(\judgement{\bullet}{\bullet}{\lambda x.~ \hequal{x}{x}}{\Eq{\code{Float}} \Rightarrow \code{Float} \ra \Bool})},
%\\
\vdots
\end{array}
\right\}
\]
Note that there is now sufficient type information at the call site of (\heq)\/ to 
gives its denotation: 
\[
\Msem{(\heq : {\code{Int}\ra\code{Int}\ra\code{Bool}})}\varepsilon = \standard{\Eq}\, (\code{Int}\ra\code{Int}\ra\code{Bool})= \code{eqInt}
\]
Recall that \standard{\Eq}\/ is a set function, so the above application makes sense.
But most importantly, no dictionary-passing translation is needed!

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Overloading with Polymorphic Recursion}

\begin{remark}[Modeling Parametric Polymorphism] Consider the term polymorphic (but not
polymorphic recursive) term \code{length}:
\[
\begin{array}{lcl}
\code{length} &:& [a] \ra \code{Int}
\\
\code{length}~ &=& \lambda l. \mbox{ case } l ~\{ [\,] \ra 0 ~;~ (x:xs) \ra 1 + (\code{length}~xs)\}
\end{array}
\]
\end{remark}

Within Ohori's framework, this should denote the following
set:
\[
\{ \tuple{\tau, \code{len}_\tau}~|~ \tau = [\code{Int}]\ra\code{Int},\,\ldots \}
\]
where each \(\code{len}_\tau\) is defined as:
\[
\code{len}_\tau = 
\code{fix}_\tau\, (\lambda \code{length}.~
             \lambda l. \mbox{ case } l ~\{ [\,] \ra 0 ~;~ (x:xs) \ra 1 + (\code{length}~xs)\})
\]
Note that \(\code{fix}_\tau\) is the least fixed point on the pointed cpo \D{\tau}.


\begin{remark}[Modeling Polymorphic Recursion] Consider now the term 
polymorphic recursive term \code{foo}:
\[
\begin{array}{lcl}
\code{foo} &:& [a] \ra \code{Bool}
\\
\code{foo}~ &=&~ \lambda x. ~(\code{null}~ x) ~ \& \& ~ (\code{foo}~ [x])
\end{array}
\]
\end{remark}

One might be tempted to view this definition as shorthand for the following:
\[
\begin{array}{lclcc}
\code{foo} & = & \code{fix}~(\lambda \code{foo}. \lambda x. ~(\code{null}~ x) ~ 
                \&\& ~ (\code{foo}~ [x]))
&~~~~~~~~~~~~&(\dagger)
\end{array}
\]
But, where does this \code{fix} live? Intuitively, here's the problem: 
if the ``input \code{foo}'' (i.e., ``\(\code{fix}~(\lambda \code{foo}\ldots\)'')
lives in \D{[\tau]}, then the ``output 
\code{foo}'' (i.e., ``\((\code{foo}~ [x])\)'') lives in
\D{[[\tau]]}. The above definition does not make sense in any particular type frame.

So, what to do? Luckily, any frame in which the \D{\tau} are pointed cpos can be extended
to a pointed cpo over the indexed sets \((\Pi\,\tau \!\!\in\! S.\D{\tau})\) used to denote polymorphic terms. This yields a least
\begin{theorem} Let \(\tuple{\mathcal{D},\bullet}\) be a pcpo-frame and \(S\) be a set of ground type expressions. Then,  \((\Pi\,\tau \!\!\in\! S.\D{\tau})\) is a pointed cpo where
\begin{itemize}
\item for any \(f,g\in(\Pi\,\tau \!\!\in\! S.\D{\tau})\), \(f \sqsubseteq g\)  \(\Leftrightarrow\) for all \(\tau\in S\), 
\(f\tau \sqsubseteq_{\D{\tau}} g \tau\), and
\item the bottom element is \(\{\tuple{\tau,\perp_{\D{\tau}}} ~|~ \tau\in S \}\)
\end{itemize}
\end{theorem}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The OML language}



\begin{figure}[h]
\hrule
\vspace{1ex}
\[
\begin{array}{lcccc}
\mbox{{\bf Type expressions:}} &~~& 
                            & &
                               \begin{array}[t]{lclcl}
                               \tau &::=& t           &~~~& \mbox{{\em type variables}} 
                               \\[-1.5ex]
                                    & | & \tau\ra\tau &   & \mbox{{\em function types}}
                               \\[-1.5ex]
                               \rho & ::= & P \Rightarrow \tau &   & \mbox{{\em qualified types}}
                               \\[-1.5ex]
                               \sigma & ::= & \forall T. \rho &   & \mbox{{\em type schemes}}
                               \\
                               \end{array}
\\
\\[-3ex]
\mbox{{\bf Standard rules:}} && 
                           \mbox{(var)} & &
                               \begin{array}{c}
                               \infer[]{
                                       \judgement{P}{\Sigma}{x}{\sigma}
                                       }
                                       {
                                       (x:\sigma) \in \Sigma 
                                       }
                               \end{array}
\\[2ex]
&&
                           \mbox{(\(\rightarrow\! E\))} & ~~~~&
                               \begin{array}{c}
                               \infer[]{
                                       \judgement{P}{\Sigma}{E F}{\tau}
                                       }
                                       {
                                       \judgement{P}{\Sigma}{E}{\tau' \ra \tau}
                                       &
                                       \judgement{P}{\Sigma}{F}{\tau'}
                                       }
                               \end{array}
\\[2ex]
&&
                           \mbox{(\(\rightarrow\! I\))} & ~~~~&
                               \begin{array}{c}
                               \infer[]{
                                       \judgement{P}{\Sigma}{\lambda x.E}{\tau'\ra\tau}
                                       }
                                       {
                                       \judgement{P}{\Sigma_x,x:\tau'}{E}{\tau}
                                       }
                               \end{array}
\\[3ex]
\mbox{{\bf Qualified types:}} && 
                           (\Rightarrow E) 
                           &&
                               \begin{array}{c}
                               \infer[]{
                                       \judgement{P}{\Sigma}{E}{\rho}
                                       }
                                       {
                                       \judgement{P}{\Sigma}{E}{\pi \Rightarrow \rho}
                                       & \evid{P}{\pi}
                                       }
                               \end{array}
\\[2ex]
                           && 
                           (\Rightarrow I) 
                           &&
                               \begin{array}{c}
                               \infer[]{
                                       \judgement{P}{\Sigma}{E}{\pi \Rightarrow \rho}                               
                                       }
                                       {
                                       \judgement{P,\pi}{\Sigma}{E}{\rho}
                                       }
                               \end{array}
\\[3ex]
\mbox{{\bf Polymorphism:}} && 
                           (\forall E) 
                           &&
                               \begin{array}{c}
                               \infer[]{
                                       \judgement{P}{\Sigma}{E}{[\tau/\alpha]\sigma}
                                       }
                                       {
                                       \judgement{P}{\Sigma}{E}{\forall \alpha. \sigma}
                                       }
                               \end{array}
\\[2ex]
                           && 
                           (\forall I) 
                           &&
                               \begin{array}{c}
                               \infer[]{
                                       \judgement{P}{\Sigma}{E}{\forall \alpha. \sigma}
                                       }
                                       {
                                       \judgement{P}{\Sigma}{E}{\sigma}
                                       & \alpha \not\in \mbox{TV}(\Sigma)\cup\mbox{TV}(P)
                                       }
                               \end{array}
\\[3ex]
\mbox{{\bf Local definition:}} 
                           && 
                           \mbox{(let)}
                           &&
                               \begin{array}{c}
                               \infer[]{
                                       \judgement{P,Q}{\Sigma}{\letexp{x}{E}{F}}{\tau}
                                       }
                                       {
                                       \judgement{P}{\Sigma}{E}{\sigma}
                                       & \judgement{Q}{\Sigma_x, x:\sigma}{F}{\tau}
                                       }
                               \end{array}
\end{array}
\]
\caption{Type expresssions and typing rules for OML as they appear in \cite{jones94}.}
\vspace{1ex}
\hrule
\end{figure}



\subsection{Polymorphic Recursion}

%\begin{displaymath}
\begin{definition}[Polymorphic Recursion Rule] From Schwartzbach \cite{schwartzbach95}:
\[
\begin{array}{c}
\infer[(\bar{\alpha}\not\in A)]{
        \judge{A}{\letexp{f}{e_1}{e_2}}{\tau}
        }
        {
        \judge{A,(f:\fa{\bar{\alpha}} \sigma)}{e_1}{\sigma} 
        & \judge{A,(f:\fa{\bar{\alpha}} \sigma)}{e_2}{\tau}
        }
\end{array}
\]        
\end{definition}
%\end{displaymath}

%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%
\subsection{Satisfiability of Class Predicates}

Constructor classes like \classpred{Eq}, \classpred{Ord}, and \classpred{Show}\/
play two related r\^{o}les in the definition of Haskell. They are logical predicates on
\Type

\subsubsection{First Case: Single Classes}




\section{Related Work}
Comment on \cite{Mycroft84,Kfoury93,Henglein93} and also on Schwartzbach's excellent note 
\cite{schwartzbach95}.


\bibliographystyle{plain}
\bibliography{bib-separation,related-work}  


\end{document}

