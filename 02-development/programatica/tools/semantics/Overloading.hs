{-

foo x = (x==x) && (foo [x])

Here, we assume that generalization information is included in the 
AST for overloaded variables in the definition:

("foo",idD) x = (x ("==",idD) x) && (("foo",listD) [x])

where
   idD, listD :: Dict -> Dict

Then, the meaning of the body of this annotated term is:

    mng :: AST -> M Value, 

where M is a monad s.t. M = Env + Dict + Maybe

    mng ((x ("==",idD) x) && (("foo",listD) [x]))
   
    = do { b1 <- mng (x ("==",idD) x)
         ; b2 <- mng (("foo",listD) [x])
         ; return (b1 && b2)
           }

Expanding the subterms

      mng (x ("==",idD) x) 
      = do { d <- rdDict
           ; inDict (idD d) (mng (x == x))
             }
and

      mng (("foo",listD) [x])
      = do { d <- rdDict
           ; inDict (listD d) (mng (foo [x]))
             }



-}
module Overloading where

type Name = String

data Op = Plus | Eq 

data OLam
  = Ident Var
  | App OLam OLam
  | Abs Var OLam
  | Zero | One       --- Overloaded constants
  --- Class operations
  | FromInteger
  | FromInt
  --- Binary operations
  | Bin Op OLam

data Var = Overloaded Name TCtxt | Variable Name deriving (Show,Eq)

data TCtxt = Type String deriving (Show,Eq)

showOLam :: OLam -> String
showOLam Zero = "0"
showOLam One = "1"
showOLam (Ident v) = 
                case v of
                      Overloaded n tc -> n ++ "?"
                      Variable n   -> n
showOLam (Abs x d) = "(\\" ++ show x ++ " -> " ++ showOLam d ++ ")"
showOLam (App d d') = "(" ++ showOLam d ++ " " ++ showOLam d' ++ ")"

instance Show OLam where
  show = showOLam

----------------------------------------------------------------------
-- Values
----------------------------------------------------------------------

data Value = 
--- Ambiguous values
  Num { int :: Value, 
        integer :: Value,
        float :: Value,
        double :: Value }
--- Base values
  | Intval Int
  | Integerval Integer
  | Floatval Float
  | Doubleval Double
  | BoolVal Bool
--- functions
  | Fun (Value -> Value)

type Env = Var -> Value

apply phi1 phi2 = let (Fun f) = phi1 in  f phi2

xEnv :: Env -> Var -> Value -> Env
xEnv rho n phi = tweek rho n phi
    where tweek f x y = \ z -> if x == z then y else f z

----------------------------------------------------
{-         Semantics of OLam                      -}
----------------------------------------------------

mng :: OLam -> Env -> Value

mng (Ident n) rho = rho n

mng (Abs n e) rho = Fun (\ a -> mng e (xEnv rho n $ a))

--mng (App l1 l2) rho = apply (mng l1 rho) (mng l2 rho)

mng Zero rho = Num { int = Intval 0,
                     integer = Integerval 0,
                     float = Floatval 0,
		     double = Doubleval 0 }

mng One rho = Num { int = Intval 1,
                     integer = Integerval 1,
                     float = Floatval 1,
		     double = Doubleval 1 }

mng FromInteger rho = Fun $ \(Integerval i) -> 
                                Num { int = Intval $ fromInteger i,
                                      integer = Integerval i,
                                      float = Floatval $ fromInteger i,
                                      double = Doubleval $ fromInteger i }

mng FromInt rho = Fun $ \(Intval i) -> 
                            Num { int = Intval i,
                                  integer = Integerval $ fromInt i,
                                  float = Floatval $ fromInt i,
                                  double = Doubleval $ fromInt i }

--- An irritating term:
foo  :: (Eq a, Eq [a]) => a -> Bool
foo x = (x==x) && foo [x]

crud :: (Num a, Eq a) => a -> a
crud x = if (x==x) then x+x else x

class Gorp a where
   gorp :: a -> a -> a

bar :: (Eq a, Gorp a) => a -> a
bar x = if (x==x) then (gorp x x) else x

---- Below here is old stuff, which may be useful:
{- Primitive operations -}

primPlusInt :: Int -> Int -> Int
primPlusInt i j = i + j

primPlusInteger :: Integer -> Integer -> Integer
primPlusInteger i j = i + j

primPlusFloat :: Float -> Float -> Float
primPlusFloat i j = i + j

primPlusDouble :: Double -> Double -> Double
primPlusDouble i j = i + j

primEqInt :: Int -> Int -> Bool
primEqInt x y = (x == y)

primEqInteger :: Integer -> Integer -> Bool
primEqInteger x y = (x == y)

primEqFloat :: Float -> Float -> Bool
primEqFloat x y = (x == y)

primEqDouble :: Double -> Double -> Bool
primEqDouble x y = (x == y)
