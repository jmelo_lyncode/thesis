module AST where

type Name = String
data Op = Plus | Mult | IntEq | IntLess deriving Eq
data LS = Lazy | Strict deriving (Eq, Show)

----------------------------------------------------------------------
-- Patterns and Expressions
----------------------------------------------------------------------

data P --- Nested Patterns
  = Pconst Integer          -- { 5 }
  | Pvar Name               -- { x }
  | Ptuple [P]              -- { (p1,p2) }
  | Pcondata Name [P]       -- data T1 = C1 t1 t2; {C1 p1 p1} = e b
  | Pnewdata Name P         -- newtype T2 = C2 t1;  {C2 p1} = e
  | Ptilde P                -- { ~p }
  | Paspat Name P           -- { x@p }
  | Pwildcard               -- { _ }
  deriving Eq
	   
data E --- Haskell Expressions
  = Var Name                -- { x }
  | Const Integer           -- { 5 }
  | App E E                 -- { f x }
  | Abs [P] E               -- { \ p1 p2 -> e }
  | TupleExp [E]            -- { (e1,e2) }
  | ConApp (Name,[LS]) [E]  -- data T1 = C1 t1 t2; p = {C1 e1 e2}
  | NewApp Name E           -- newtype T2 = C2 t1; p = {C2 e1}
  | Seq E E                 -- { seq e1 e2 }
  | Bin Op E E              -- { e1 + e2 }
  | Cond E E E              -- { if e1 then e2 else e3 }
  | Let [D] E               -- { let x=e1;   y=e2 in e3 }
  | Case E [Match]      -- { case e of p -> b where ds ; ... }
  | Undefined
  | Tconst                  -- { True }
  | Fconst                  -- { False }
  | Trivconst               -- { () }
  deriving Eq

type Match  = (P,B,[D])     -- case e of { pat -> body where decs } 
type Clause = ([P],B,[D])   -- f { p1 p2 = body where decs }

data D --- Declarations
  = Fun Name [Clause]       -- f p1 p2 = b where ds
  | Val P B [D]             -- p = b where ds
  deriving Eq

data B -- Bodies
  = Guarded [(E,E)]         -- f p { | e1 = e2 | e3 = e4 } where ds
  | Normal E                -- f p = { e } where ds
  deriving Eq

showB :: B -> String
showB (Normal e) = showE e
showB (Guarded ps) = foldr f "\n" ps
   where f (e1,e2) ans = "| "++(showE e1) ++ " = " ++(showE e2)++ ans

showD :: D -> String
showD (Val p body ds) =  " " ++ showP p ++ "=" ++ (showB body) ++ "\n" ++
                         "   where " ++ (showDs ds)
showD (Fun nm xs) = foldr (++) "" (map f xs)
   where f (ps,body,ds) = 
           nm ++ " " ++(showPs ps) ++ " = " ++(showB body) ++ "\n" ++
           "   where " ++ (showDs ds)
   

showDs dcls = foldr (++) "" (map showD dcls)

sepByCommas = (\ h -> \ t -> if  t /= "" then (h ++ "," ++ t) else h)

showOp :: Op -> String
showOp Plus = "+"
showOp Mult = "*"
showOp IntEq = "=="
showOp IntLess = "<"

showP (Pconst i) = show i
showP (Pvar n) = n
showP (Pcondata n ns) = 
                "(" ++ n ++ " " ++ 
                (foldr (\ s t -> if t/="" then (showP s) ++ " " ++ t
                                     else (showP s)) 
                        "" ns)
                ++ ")"
showP (Pnewdata n x) = "(" ++ n ++ " " ++ (showP x) ++ ")"              
showP (Ptilde p) = "~" ++ (showP p)
showP (Ptuple ps) = "(" ++ foldr sepByCommas "" (map showP ps) ++ ")" 

showPs [] = ""
showPs (p:ps) = (showP p)++" "++(showPs ps)

showE :: E -> String
showE (Const i) = show i
showE Undefined = "undefined"
showE Fconst = "False"
--showE Boom = "Boom"
showE (Var nm) = nm
showE (Abs p d) = "(\\ " ++ (foldr (++) " " (map showP p)) ++ "-> " ++ showE d ++ ")"
showE (App d d') = "(" ++ showE d ++ " " ++ showE d' ++ ")"
showE (Let dcls d) = "let " ++ 
                          foldr (++) "" (map showD dcls)
                            ++ 
                        " in " ++ showE d

showE (Cond b e1 e2) = 
       "if "++showE b++"\n\t then "++show e1++"\n\t else"++showE e2
showE (ConApp (n,[]) []) =  n 

showE (ConApp (n,ls) es) =  "(" ++ n ++ " " ++  
                (foldr (\ (s,annot) t -> 
                            let sa = if (annot == Strict) then "!" else "" 
                            in
                                 if t/="" then sa ++ (showE s) ++ " " ++ t
                                     else sa ++ (showE s)) 
                        "" (zip es ls))
                      ++ ")"
showE (NewApp n x) = "(" ++ n ++ " " ++  (showE x) ++ ")"                     
showE (Bin op d d') = "(" ++ showE d ++ " " ++ showOp op ++ " " ++ 
                               showE d' ++ ")"
showE Tconst = "true"
showE Fconst = "false"
showE (Case e ms) =
      "(case " ++ showE e ++ " of " ++ 
           (foldr (++) "" (map (\ alt -> "[" ++ (showAlt alt) ++ "]") ms)) ++ ")"
   where showAlt (p,b,ds) = show p ++ " -> " ++ show b ++ "\n" ++ showDs ds

showE (TupleExp es) = "(" ++ foldr sepByCommas "" (map showE es) ++ ")" 


instance Show E where
  show = showE

instance Show P where
  show = showP

instance Show B where
  show = showB
