module Patterns where

import AST
import Examples
import Haskell



{-
data Pr -- Predicates
  = Univ                    -- the Universal predicate
  | UnDef                   -- the Undefined predicate
  | ConPred Name [Pr]       -- a pattern predicate
  | Strong Pr               -- a strengthened predicate
  | PredVar Name            -- a predicate variable
  deriving (Eq,Show)

instance Show Pr where 
      show Univ                 = "Univ"
      show UnDef                = "UnDef"
      show (Strong pr)          = "$" ++ show pr
      show (ConPred "List" prs) = sPL prs
         where sPL prs = "["++foldr sepByCommas [] (map show prs)++"]"
      show (ConPred n prs)      = rPL prs 
         where rPL prs = n++"("++foldr sepByCommas [] (map show prs)++")"
      show (PredVar n)          = n
-}



patPred :: P -> [(Name,Pr)] -> Bool -> (Pr,Bool)
patPred (Pvar x) sigma b   = 
      (purify $ lookup x sigma, is_strong (purify $ lookup x sigma))
                 where     
	                 is_strong (Strong _) = True
	                 is_strong _          = False
					 
patPred Pwildcard sigma b       = (Univ,False)
patPred (Ptilde p) sigma b      = patPred p sigma False
patPred (Pcondata n ps) sigma b = 
       let (ConPred "List" preds, s) = map_patPred ps sigma b
	   in
	      if (s || b) then 
		         (Strong (ConPred n preds), True)
		  else   (Univ, False)

map_patPred :: [P] -> [(Name,Pr)] -> Bool -> (Pr,Bool)
map_patPred [] sigma b     = (ConPred "List" [], False)
map_patPred (p:ps) sigma b = (ConPred "List" (pp:pps), s1 || s2)
                   where
                       (pp,s1)                 = patPred p sigma b
                       (ConPred "List" pps,s2) = map_patPred ps sigma b

pat2pred :: P -> [Pr] -> Pr
pat2pred p plist = fst (patPred p (zip (fringe p) plist) True)

-- Universal closure:
dom p = pat2pred p (replicate pl Univ)
  where pl = length $ fringe p

--- These patterns pat0-pat2 correspond to ex0-ex4 in 
--- Figure 4 of the MPC paper.

pat0 = tpat (spat $ Pvar "x") (rpat $ Pvar "y")
pat1 = Ptilde $ tpat (spat $ Pvar "x") (rpat $ Pvar "y")
pat2 = Ptilde $ tpat (spat $ Pvar "x") (Ptilde (rpat $ Pvar "y"))

pat3 = rpat $ Pvar "x"
