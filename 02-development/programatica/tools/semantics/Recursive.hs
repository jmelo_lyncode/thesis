module Recursive
where
import AST
import MyHaskell
import Valid
import Variables
import Rules
import Predscheme
import Maybe
import List

-- The rule for a Gfp formula is:
--
-- actualGfp =
--   Conclude 
--      [Consequence [Assert (Var "t_m") List (PredScheme "H" List (PredVar "X"))]
--                   [Assert (Var "m")   List (PredVar "X")]]
--      (Consequence [Equal List (Var "m") (Var "t_m")]
--                   [Assert (Var "m") List (LimitIntersect
--                                             (PredScheme "H" List Univ))])
--
-- where the form of PredScheme Type p::Pred is
--    q::Pred \/ ConPred (Name,[SL]) [<predicate args>]
-- and the conclusion is to hold for all values of the predicate arg. p and
-- the fixed predicate, q.  Note that PredScheme doesn't negate the
-- predicate argument. We consider both Strict and Lazy constructors;
-- however, the antecedent cannot be discharged if a constructor is strict
-- in the position of the predicate argument.
--
--   The antecedent clause, together with the equality m==t_m requires that
--
--         (Consequence [Assert (Var "m") List (PredScheme "H" List (PredVar "X"))]
--                      [Assert (Var "m") List (LimitIntersect
--                                                (PredScheme "H" List Univ))])
--
-- must be valid for all predicate variables "X".  When predicate variables
-- range only over UnDef and $Univ, the limit of intersections is reached
-- at the first level of application of PredScheme, hence the simplification
-- in the abstracted rule.  Note, however, that this rule is limited to
-- proving properties of coinductively defined lists.
--
-- Why does the abstraction work?  Intuitively, it's because lazily constructed
-- terms are identified after the first level of construction, i.e. their
-- arguments aren't evaluated.
--
-- LOOK AT THE CONTRAPOSITIVE FORM OF THE COINDUCTION THEOREM.



-- The rule for an Lfp formula asserting a property of a function is
--
-- Conclude
--   [Consequence [PropVar "Gamma", Assert (Var "m") (Arrow List List) Univ]
--      [Assert (Var "t_m") (Arrow List List) (Parrow (PredVar "P1")
--                                            (PredScheme "H" List (PredVar "X")))]
--   ,Consequence [PropVar "Gamma", Assert (Var "m") (Arrow List List) 
--                 (Parrow (Disj (PredVar "P1") (PredVar "P2")) (PredVar "X"))]
--      [Assert (Var "t_m") (Arrow List List) (Parrow (PredVar "P2")
--                                            (PredScheme "H" List (PredVar "X")))]
--   ]
--   (Consequence [PropVar "Gamma", Equal (Arrow List List) (Var "m") (Var "t_m")]
--      [Assert (Var "t_m") (Arrow List List) 
--         (Parrow (Disj (PredVar "P1") (PredVar "P2")) 
--              (LimitUnion (PredScheme "H" List UnDef)))])
--
-- A non-functional Lfp rule:

lfpRule0 =
   Conclude
     [Consequence [PropVar "Gamma", Assert (Var "m") List Univ]
       [Assert (Var "t_m") List (PredScheme "H" List Univ)]
     ,Consequence [PropVar "Gamma", Assert (Var "m") List (PredVar "Xi")]
       [Assert (Var "t_m") List (PredScheme "H" List (PredVar "Xi"))]]
     (Consequence [PropVar "Gamma", Equal List (Var "m") (Var "t_m")]
       [Assert (Var "t_m") List 
          (LimitUnion "H" List UnDef)])


-- Here is a strategy for proving this Lfp rule.  Let \mu be a "psuedo
-- value" of type List.  A pseudo value is a placeholder for an actual
-- but unspecified value, a kind of nonstandard value which is appended
-- to our model and which is incomparable to any standard element of the
-- List domain.  All we know about \mu is that \mu (and \mu only) satisfies
-- the predicate \xi in our nonstandard model.
--
-- The first antecedent clause of the Lfp rule determines that Gamma
-- contains bindings, rho_1, such that mE t_m rho_1 \in $(PredScheme "H" List
-- UnDef)
-- The second antecedent clause determines that Gamma also contains bindings
-- rho_2 such that (rho_2 m = \mu /\ mE t_m rho_2 \in
-- $(PredScheme "H" List \xi)) OR (rho_2 m /= \mu).
-- To check the consequent, we use the meta-theory that says if there is a
-- nonstandard model, then there is a standard model as well.  We allow
-- the pseudo value, \mu, to be substituted by any value, as if it were
-- existentially quantified (i.e. we pick the value to substitute) and
-- check that from the environments bound in Gamma, the conclusion
-- of the consequent is satisfiable.  Strategy: pick a value that
-- satisfies H^i[UnDef] for some i and check t_m satisfies H^(i+1)[UnDef]
--
-- If the rule were modified to remove the strong assertions in the
-- antecedents, or if H were allowed to negate \xi, the model should fail.
--
-- Instantiate H with admissible predicate constructors.
-- For type List: K Nil, K Undef, \P -> Nil + Cons Void P, \P -> Cons Void P
-- Also \P -> Nil + Cons Void (Not P) to check soundness of the checker.
