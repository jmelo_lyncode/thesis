module Examples where

import AST

-----------------------------------------------------------
--			some examples
-----------------------------------------------------------

conapp n els =
  let annotations = map snd els
      exps        = map fst els
  in
      ConApp (n,annotations) exps

leftpat = \ x -> Pcondata "L" [Pvar x]
leftrightpat = \ x -> Pcondata "L" $ [Pcondata "R" [Pvar x]]
rpat = \ x -> Pcondata "R" [x]
spat = \ x -> Pcondata "S" [x]
bpat = \ x -> Pcondata "B" [x]
lpat = \x -> Pcondata "L" [x]
epat = \x -> Pcondata "E" [x]
tpat = \p1 p2 -> Pcondata "T" [p1,p2]
lexp = \t -> conapp "L" [(t,Lazy)]
eexp = \t -> conapp "E" [(t,Lazy)]
bexp = \t -> conapp "B" [(t,Lazy)]
rexp = \t -> conapp "R" [(t,Lazy)]
texp = \t1 t2 -> conapp "T" [(t1,Lazy),(t2,Lazy)]
leafexp = conapp "L" []
leftexp = \t -> conapp "L" [(t,Lazy)]
rightexp = \t -> conapp "R" [(t,Lazy)]
pairexp = \ t1 t2 -> TupleExp [t1,t2]
lambda p e = Abs [p] e

acidtest = App (lambda acidpat (Var "x")) rand
      where acidpat = tpat p1 p2
            p1 = Ptilde $ tpat (varpat "u") (varpat "v")
            p2 = varpat "x"
            rand = texp Undefined leafexp


leafpat = Pcondata "L" []
redpat = \ x -> Pcondata "Red" [Pvar x]
greenpat = \ x -> Pcondata "Green" [Pvar x]
blackpat = \ x -> Pcondata "Black" [Pvar x]
blackexp = \t -> conapp "Black" [(t,Lazy)]
redexp = \t -> conapp "Red" [(t,Lazy)]
greenexp = \t -> conapp "Green" [(t,Lazy)]
black = \ x -> conapp "Black" [(x,Lazy)]
red = \ x -> conapp "Red" [(x,Lazy)]
green = \ x -> conapp "Green" [(x,Lazy)]
varpat = \x -> Pvar x
pairpat = \p1 p2 -> Ptuple [p1,p2]

---pat1 = Ptilde $ pairpat (varpat "x") (rpat (varpat "y"))

a1 = lambda (lpat (rpat (lpat $ varpat "x"))) (Var "x")
ap11 = App a1 (lexp (rexp (lexp (Const 1)))) --- Just 1
ap12 = App a1 (lexp (lexp (lexp (Const 1)))) --- Nothing

x2 = lambda (Ptilde (rpat (lpat $ varpat "x"))) (Const 1)
x21 = App x2 (lexp (lexp (Const 1)))

a2 = lambda (lpat (Ptilde (rpat (lpat $ varpat "x")))) (Const 1)
ap21 = App a2 (lexp (rexp (lexp (Const 1)))) --- Just 1
ap22 = App a2 (lexp (lexp (lexp (Const 1)))) --- Just 1

a21 = lambda (lpat (Ptilde (rpat (lpat $ varpat "x")))) (Var "x")
ap211 = App a21 (lexp (rexp (lexp (Const 1)))) --- Just 1
ap212 = App a21 (lexp (lexp (lexp (Const 1)))) --- undefined

a3 = lambda (Ptilde (lpat (rpat (lpat $ varpat "x")))) (Const 1)

a4 = lambda (Ptilde (lpat (rpat (Ptilde (lpat $ varpat "x"))))) (Const 1)

{-****************************************************-}

data Foo = R Foo | B Foo | Leaf

{-****************************************************-}

{-
q61 = Apply (QAL (QTuple [QId,QId]) (QAL (QTuple [QId,QSeq (QCons "R") QId]) 
                      (UncurryM (LamN "x" (LamN "y" (Vbl "x")))))) 
            (Tuple [Cnst 0,Cl "L" [Cnst 17]])
-}

---a7 = lambda (pairpat (varpat "x") (Ptilde (rpat $ varpat "y"))) (Var "x")

-------------------------------------------------------

{-
     Lang:   Haskell       Miranda       ML
 Test------------------------------------------
  e0        1/Just 1   
  e1        UD/UD
  e2        MF/Nothing
  e3        1/Just 1
  e4        MF/Nothing
  e5        MF/Nothing
  e6        UD/UD
  e7        MF/Nothing
  e8        MF/Nothing
  e9        7/Just 7
  e10       UD/UD
  e11       19/Just 19
  e12       19/Just 19
            (hugs/evalHS)
   UD=undefined   MF = Match Failure
-}

--- e0: (\x -> 1) undefined
e0 = App (lambda (varpat "x") (Const 1)) Undefined 
 
--- e1: (\(Red x) -> 1) undefined                
e1 = App (lambda (redpat "x") (Const 1)) Undefined                  

--- e2: (\(Red x) -> 1) (Black 19)
e2 = App (lambda (redpat "x") (Const 1)) (blackexp (Const 19)) 

--- e3: (\(Red x) -> 1) (Red undefined)
e3 = App (lambda (redpat "x") (Const 1)) (redexp Undefined)         

--- e4: (\(Red x) -> 1) (Black undefined)
e4 = App (lambda (redpat "x") (Const 1)) (blackexp Undefined)       

--- e5: (\(Red x) -> x) (Black undefined)
e5 = App (lambda (redpat "x") (Var "x")) (blackexp Undefined)       

--- e6: (\(Red x) -> x) (Red undefined)
e6 = App (lambda (redpat "x") (Var "x")) (redexp Undefined)       

--- e7: (\(Red x,Black y) -> x) (Black 7,Black 19)
e7 = App (lambda (pairpat (redpat "x") (blackpat "y")) (Var "x")) 
                 (pairexp (blackexp (Const 7)) (blackexp (Const 19)))

--- e8: (\(Red x,Black y) -> y) (Black 7,Black 19)
e8 = App (lambda (pairpat (redpat "x") (blackpat "y")) (Var "y")) 
                 (pairexp (blackexp (Const 7)) (blackexp (Const 19)))

--- e9: (\(Red x,Black y) -> x) (Red 7,Black 19)
e9 = App (lambda (pairpat (redpat "x") (blackpat "y")) (Var "x")) 
                 (pairexp (redexp (Const 7)) (blackexp (Const 19)))

--- e10: (\(Red x, Black y) -> 19) (undefined, undefined)
e10 = App (lambda (pairpat (redpat "x") (blackpat "y")) (Const 19)) 
                  (pairexp Undefined Undefined)

--- e11: (\(Red x, Black y) -> 19) (Red undefined, Black undefined)
e11 = App (lambda (pairpat (redpat "x") (blackpat "y")) (Const 19)) 
                  (pairexp (redexp Undefined) (blackexp Undefined))

--- e12: (\( x, y) -> 19) (undefined, undefined)
e12 = App (lambda (Ptuple [varpat "x", varpat "y"]) (Const 19)) 
                  (pairexp Undefined Undefined)


he60 = App (lambda (Ptilde (tpat (epat (varpat "x")) (epat (varpat "y"))))
             leafexp)
             leafexp


{-
--- ce1: case undefined of (Red x) -> 1
ce1 = Case Undefined [mtch (redpat "x") (Const 1)]
--- UD/UD
--- ce2: case (Black 19) of { (Red x) -> 1 }
ce2 = Case (blackexp (Const 19)) [mtch (redpat "x") (Const 1)]
--- MF/undefined

ce3 = Case (redexp Undefined) [mtch (redpat "x") (Const 1)]
ce4 = Case (blackexp Undefined) [mtch (redpat "x") (Const 1)]
ce5 = Case (blackexp Undefined) [mtch (redpat "x") (Var "x")]
ce6 = Case (blackexp (Const 19)) [mtch (redpat "x") (Const 1),
                                  mtch (blackpat "x") (Const 2)]

--- Observe that deep MF causes poisonous failure
ce7 = Case (leftexp (rightexp Undefined)) 
           [mtch (leftpat "x") 
                      (Case (Var "x") [mtch (leftpat "y") (Const 1)]),
            mtch (leftrightpat "x") (Const 2)]

le1 = Let (redpat "x") (Const 1)
le2 = Let (redpat "x") (blackexp (Const 19)) (Const 1)
le3 = Let (redpat "x") (redexp Undefined) (Const 1)
le4 = Let (redpat "x") (blackexp Undefined) (Const 1)
le5 = Let (redpat "x") (blackexp Undefined) (Var "x")

data ArrBee = Rot !Int | Schwarz Int deriving Show

foo1 = let x = Rot undefined in 1
foo2 = let x = Schwarz undefined in 1

data LR = Lt LR | Rt LR | M Int deriving Show

foo3 (Lt (~(Lt x))) = 1

ex  = {- (\(Red x) -> 1) (Black undefined) -}
      App (lambda (redpat "x") (Const 1)) (blackexp Undefined)       

ex1 = {- let (Red x) = (Black undefined) in x -}
      Let (redpat "x") (blackexp Undefined) (Const 1)

ex2 = {- case (Black undefined) of (Red x) -> 1 -}
      Case (blackexp Undefined) [mtch (redpat "x") (Const 1)]
{-
c7 = let c7body = Guarded (Pvar "x") 
                          [(Bin IntEq (Var "x") (Var "z"), (Const 99))]  
              {- where -} [(Pvar "z",(Const 1))]
     in Case (Const 1) [c7body]
-}
-}
g1 = case 1 of
       x | x==z -> 99 
        where z = 1
{-
c8 = let c8body = Guarded 
                          [(Bin IntEq (Var "x") (Var "z"), (Const 99))]
              {- where -} [(Pvar "z",(Const 2))]
     in Case (Const 1) [c8body]
-}

ce6 = Case (blackexp (Const 19)) [unguarded (redpat "x") (Const 1),
                                  unguarded (blackpat "x") (Const 2)]
          where unguarded p e = (p,Normal e,[])

--- should produce failure
c8a = Case (Const 1) [(Pvar "x",
                     Guarded [(Bin IntEq (Var "x") (Var "z"), (Const 99))],
                     [Val (Pvar "z") (Normal $ Const 2) []])]

--- should produce 99
c8b = Case (Const 1) [(Pvar "x",
                     Guarded [(Bin IntEq (Var "x") (Var "z"), (Const 99))],
                     [Val (Pvar "z") (Normal $ Const 1) []])]

g2 = case 1 of
       x | x==z -> 99 
        where z = 2

{-
c9 = 
   let
        guardedbody = Guarded (Pvar "x") 
                              [(Bin IntEq (Var "x") (Var "z"), (Const 99))]
                              [(Pvar "z",(Const 1))]
        normalbody = Normal (Pvar "y")  (Const 101) []
   in
      Case (Const 1) [guardedbody,normalbody]            
-}

g3 = case 1 of 
           x | x==z -> 99
                 where z = 1
           y -> 101

{-
c10 = 
   let
        guardedbody = Guarded (Pvar "x") 
                              [(Bin IntEq (Var "x") (Var "z"), (Const 99))]
                              [(Pvar "z",(Const 2))]
        normalbody = Normal (Pvar "y")  (Const 101) []
   in
      Case (Const 1) [guardedbody,normalbody]            
-}

g4 = case 1 of 
           x | x==z -> 99
                 where z = 2
           y -> 101

-------------------------------------------------------------------
-------------------------------------------------------------------
-------------------------------------------------------------------
--- g5 & g6 are canonical examples of guard-induced weirdness.
--- canon5 and canon6 are their translations into AST.

g5 = case 1 of 
           x | x==z -> (case 1 of w | False -> 33)
                 where z = 1
           y -> 101

g6 = case 1 of 
           x | x==z -> (case 1 of w | True -> 33)
                 where z = 2
           y -> 101

canon5 = 
   let
        casebody    = Case (Const 1) [(Pvar "w",
                                       Guarded [(Fconst, (Const 33))],
                                      [])]
					 
        guardedbody =  ((Pvar "x") ,
		                    Guarded [(Bin IntEq (Var "x") (Var "z"), casebody)],
                    [Val (Pvar "z") (Normal (Const 1)) []])
					
        normalbody  = ((Pvar "y"),  Normal (Const 101), [])		
   in
      Case (Const 1) [guardedbody,normalbody]           



canon6 = 
   let
        casebody    = Case (Const 1) [(Pvar "w",
                                       Guarded [(Fconst, (Const 33))],
                                       [])]
					 
        guardedbody =  ((Pvar "x") ,
		                    Guarded [(Bin IntEq (Var "x") (Var "z"), casebody)],
                    [Val (Pvar "z") (Normal (Const 2)) []])

        normalbody  = ((Pvar "y"),  Normal (Const 101), [])		
   in
      Case (Const 1) [guardedbody,normalbody]          

--- factorial
facDef  = Fun "fac" 
             (([Pvar "x"],
               Normal (Cond (Bin IntEq (Var "x") (Const 0)) 
                        (Const 1)
                        (Bin Mult (Var "x")   
                          (App (Var "fac") 
                               (Bin Plus (Var "x") (Const $ -1))))),
              []):[])

fac = Let [facDef] (App (Var "fac") (Const 3))

facomega = Let [facDef] (App (Var "fac") (Const $ -3))

--- oddeven
oddeven = Let [evenDef,oddDef]  (App (Var "even") (Const 1001))

evenDef = Fun "even" 
             (([Pvar "x"],
               Normal (Cond (Bin IntEq (Var "x") (Const 0))
                         Tconst
                         (App (Var "odd") (Bin Plus (Var "x") (Const $ -1)))),
              []):[])

oddDef  = Fun "odd" 
             (([Pvar "y"],
               Normal (Cond (Bin IntEq (Var "y") (Const 0)) 
                         Fconst
                         (App (Var "even") (Bin Plus (Var "y") (Const $ -1)))),
              []):[])	  
			  
--- factorial + oddeven in various combinations

fodd0 = Let [evenDef,oddDef]
           (Let [facDef] (App (Var "even") (App (Var "fac") (Const 3))))

fodd1 = Let [facDef] 
           (Let [evenDef,oddDef]  (App (Var "even") (App (Var "fac") (Const 3))))

fodd2 = Let [facDef,evenDef,oddDef]  (App (Var "even") (App (Var "fac") (Const 3)))

fodd3 = Let [evenDef,facDef,oddDef]  (App (Var "even") (App (Var "fac") (Const 3)))

fodd4 = Let [evenDef,oddDef,facDef]  (App (Var "even") (App (Var "fac") (Const 3)))

faul   = \t -> conapp "L" [(t,Lazy)]
streng = \t -> conapp "S" [(t,Strict)]

lazyconapp = faul Undefined
strictconapp = streng Undefined



-------------------------------------------------------------------
-------------------------------------------------------------------
-------------------------------------------------------------------

g7 = case 1 of
          x | 1==2 -> undefined
            | 1==3 -> undefined
          _ -> 99

g8 = case Red undefined of
          Red x | 1==2 -> undefined
                | 1==3 -> undefined
          _ -> 99



------------------ Case examples
data RB = Red Int | Black Int deriving Show
l_1 = let (Red x) = undefined in 1
l_4 = let (Red x) = (Black undefined) in 1
l_5 = let (Red x) = (Black undefined) in x

ex1_old = case undefined of 1 -> 99        ---- undefined
ex2_old = case undefined of x -> 99        ---- 99
ex3_old = case undefined of (x,y) -> 99    ---- undefined
ex4_old = case undefined of (Red x) -> 99  ---- undefined

ex5_old = case (R (B Leaf)) of
		  (R x) -> (case x of 
			         (R y) -> 10)
		  (R (B x)) -> 99

ex6_old = case (R (B Leaf)) of
		  (R x) -> (case x of 
			         (B y) -> 10)
		  (R (R x)) -> 99

boom = boom

c0 = case undefined of x -> 1 
c1 = case undefined of (Red x) -> 1
c2 = case (Black 19) of (Red x) -> 1
c3 = case (Red undefined) of (Red x) -> 1
c4 = case (Black undefined) of (Red x) -> 1
c5 = case (Black undefined) of (Red x) -> x

l0 = App (lambda (Pvar "n") (Const 1)) Undefined

re0 = (\x -> 1) undefined
re6 = (\(Red x) -> 1) (Red undefined)
re7 = (\(Red x) -> x) (Red undefined)
rc6 = case (Red undefined) of (Red x) -> 1
rc7 = case (Red undefined) of (Red x) -> x

--------------------------------------------

lm1 = (\ (R (R (R x))) -> 3) (R (R (R Leaf)))
lm2 = (\ (R (R ~(R x))) -> 3) (R (R (B Leaf)))
lm3 = (\ ~(R (R (R x))) -> 3) Leaf

---General question: (\(R ~(R ~(R x)) -> e) === (\(R ~(R (R x)) -> e)?



ht1 = (\ (T ~(T x y) (T u v)) -> if True then L else x) 
                  (T L (T L L))
ht2 = (\ (T ~(T x y) (T u v)) -> if False then L else x) 
                  (T L (T L L))

ht3 = (\ (T ~(T (T x y) (T u v)) L) -> if True then x else L) 
               (T (T (T L L) L) L)

ht4 = (\ (T ~(T (T x y) ~(T u v)) L) -> if True then x else L) 
               (T (T (T L L) L) L)

ht5 = (\ (E ~(T (E x) (E u))) -> x) (E (T (E L) L))

ht6 = (\ (E ~(T (E x) ~(E u))) -> x) (E (T (E L) L))

ht7 = (\ ~(T L ~(E y)) -> y) (T (E L) (E L)) ---- MF

ht8 = (\ ~(T ~L ~(E y)) -> y) (T (E L) (E L)) ---- L

ht9 = (\ ~(T (T L ~L) ~(E y)) -> y) (T (T L (E L)) (E L))


--------------------------------------------------------------
--- (\(Pair (a,b)-> ...) equivalent to (\(Pair a b)->...) ?
--- They seem to be.
--------------------------------------------------------------

data P1 a b = MyPair (a,b)

data P2 a b = Paar a b

{-
p1 = (\ (MyPair (L,L)) -> 1) (MyPair ((E L), (E L)))
p2 = (\ (Paar L L) -> 1) (Paar (E L) (E L))
-}


----------------------------------------------------------------------
{-
evenDef = Fun "even" 
             (([Pvar "x"], Normal(Const 99),[]):[])
oddDef = Fun "odd" 
             (([Pvar "x"], Normal(Const 77),[]):[])

--

oeq = Apply (UncurryM (LamN "even" (LamN "odd" (Cnst 3))))
                (Tuple [Curry (LamN "x" (Cnst 99)),
                        Curry (LamN "x" (Cnst 77))])


foo = Apply (UncurryM (LamN "even" (LamN "odd" (Cnst 3)))) 
            (Tuple [Cnst 8,Cnst 87657])

oeq = Apply (UncurryM (LamN "even" (LamN "odd" (Cnst 3))))
            (Fix (UncurryM (LamN "even" (LamN "odd"  
                (Tuple [Curry (LamN "x" (Cnst 99)),
                        Curry (LamN "x" (Cnst 77))])))))
-}


let0 = Let [Val (varpat "x") (Normal $ (Const 99)) []] (Const 1)
let1 = Let [Val (redpat "x") (Normal $ redexp Undefined) []] (Const 1)
let2 = Let [Val (varpat "x") (Normal $ (Const 99)) []] (Var "x")
let3 = Let [Val (varpat "x") (Normal $ (Const 99)) [],   
            Val (varpat "y") (Normal $ (Const 1)) [] ] (Const 1)
let4 = Let [Val (varpat "x") (Normal $ (Const 99)) [],   
            Val (varpat "y") (Normal $ (Const 1)) [] ] (Bin Plus (Var "x") (Var "y"))

dec = [Val (varpat "x") (Normal $ (Const 99)) [],   
            Val (varpat "y") (Normal $ (Const 1)) [] ]

grunt = Let [Fun "even" [([Pvar "x"],Normal (Const 99),[])],
             Fun "odd"  [([Pvar "y"],Normal (Const 77),[])] ] (Const 3)

{-
(uncurry (\even -> (\odd -> (even 3))) 

fix(uncurry (\even -> (\odd -> 
               (\x -> if x==0 then True else (odd x+-1),
                \x -> if x==0 then False else (even x+-1)))))
-}

fooDef  = Fun "foo" 
             (([Pvar "x"],
               Normal (Cond (Bin IntEq (Var "x") (Const 0)) 
                        (Const 1)
                        (Bin Mult (Var "x")   
                               (Bin Plus (Var "x") (Const $ -1)))),
              []):[])

foo = Let [fooDef] (App (Var "foo") (Const 0))

--(Bin Mult (Var "x") (Const 8))

le1 = Let [Fun "f" [([varpat "x"],Normal (Var "x"),[])]]
          (App (Var "f") (Const 3))



{-

---transHS y2
Quark "R" :> Nu :> (LamN "x" (Cnst 1) :> Eta) :> Nu

---transHS x2
Quark "R" :> Nu :> (Quark "L" :<> (LamN "x" (Cnst 1) :> Eta)) :> Nu



-}


--- For presentation:

y1 = lambda (Ptilde (epat $ varpat "x")) (Var "x")
y11 = App y1 (texp leafexp leafexp)

y2 = lambda (Ptilde (epat $ varpat "x")) (Const 1)
y21 = App y2 (texp leafexp leafexp)

y3 = lambda (Ptilde (epat $ epat (varpat "x"))) (Const 1)
y31 = App y3 (texp leafexp leafexp)

y4 = lambda  (epat $ epat (varpat "x")) (Var "x")
y41 = App y4 (eexp leafexp)
      

a7 = lambda  (Ptilde (tpat (varpat "x") (epat $ varpat "y")))
         (Const 1) ---(Var "x")
ap71 = App a7 $ eexp (texp leafexp (eexp leafexp))
ap72 = App a7 $ eexp (texp leafexp leafexp)

a8 = lambda (epat (Ptilde (tpat (varpat "x") (epat $ varpat "y"))))
         (Var "x")
ap81 = App a8 $ eexp (texp leafexp leafexp)
ap82 = App a8 $ eexp (texp leafexp (eexp leafexp))


a9 = lambda (Ptilde (tpat (varpat "x") (epat $ varpat "y")))
         (Const 1) ---(Var "x")
ap91 = App a9 $ (texp leafexp leafexp)
ap92 = App a9 $ (texp leafexp (eexp leafexp))

b1 = lambda  (tpat (varpat "x") (varpat "y")) (Var "y")
b11 = App b1 $ (texp leafexp (eexp leafexp))
b12 = App b1 $ (texp leafexp leafexp)

b2 = lambda  (pairpat (varpat "x") (varpat "y")) (pairexp (Var "x") (Var "y"))
b21 = App b2 $ (pairexp leafexp (eexp leafexp))
b22 = App b2 $ (pairexp leafexp leafexp)

b3 = App (lambda  (varpat "x") (Var "x")) leafexp

b5 = lambda (pairpat (varpat "x") (epat $ varpat "y")) (Var "x")
bp51 = App b5 $ pairexp leafexp leafexp
bp52 = App b5 $ pairexp leafexp (eexp leafexp)

a5 = lambda (Ptilde (pairpat (varpat "x") (epat $ varpat "y"))) (Var "x")
ap51 = App a5 $ pairexp leafexp (eexp leafexp)
ap52 = App a5 $ pairexp leafexp (eexp leafexp)

a6 = lambda (Ptilde (pairpat (varpat "x") (Ptilde (epat $ varpat "y")))) 
     (Var "x")
ap61 = App a6 $ pairexp leafexp leafexp
ap62 = App a6 $ pairexp leafexp (eexp leafexp)


h22 = App (lambda (tpat (epat (varpat "x")) (Ptilde (epat (varpat "y")))) 
             leafexp)
             (texp (eexp leafexp) leafexp)

h00 = App (lambda (tpat (epat (varpat "x")) (epat (varpat "y")))
             leafexp)
             leafexp

huh0 = App (lambda (Ptilde (pairpat (epat (varpat "x")) (epat (varpat "y"))))
                   leafexp)
           (pairexp leafexp leafexp)

huh1 = App (lambda (pairpat (epat (varpat "x")) (epat (varpat "y")))
                   leafexp)
           (pairexp leafexp leafexp)

huh2 = App (lambda (pairpat (epat (varpat "x")) (Ptilde (epat (varpat "y"))))
                   (Var "x"))
           (pairexp (eexp leafexp) leafexp)

huh3 = App (lambda (Ptilde (pairpat (epat (varpat "x")) (epat (varpat "y"))))
                   (Var "x"))
           (pairexp (eexp leafexp) leafexp)

huh4 = App (App (Abs [(epat $ varpat "x"), leafpat] (Const 1)) leafexp) leafexp
---((\ (T (E x) (E y)) -> L) L

data Tree = T Tree Tree | E Tree | L deriving Show


----------------------------------------------------------


hugs = [(\ (T (E x) (E y)) -> L) L,
        (\ (T (E x) (E y)) -> L) (T L L),
		(\ (T (E x) (E y)) -> L) (T (E L) L),
		(\ (T (E x) (E y)) -> L) (T L (E L)),
		(\ (T (E x) (E y)) -> L) (T (E L) (E L)),
		(\ (T (E x) (E y)) -> x) L,
		(\ (T (E x) (E y)) -> x) (T L L),(\ (T (E x) (E y)) -> x) (T (E L) L),
		(\ (T (E x) (E y)) -> x) (T L (E L)),
		(\ (T (E x) (E y)) -> x) (T (E L) (E L)),
		(\ (T (E x) (E y)) -> y) L,
		(\ (T (E x) (E y)) -> y) (T L L),
		(\ (T (E x) (E y)) -> y) (T (E L) L),
		(\ (T (E x) (E y)) -> y) (T L (E L)),
		(\ (T (E x) (E y)) -> y) (T (E L) (E L)),
		(\ (T (E x) ~(E y)) -> L) L
		,(\ (T (E x) ~(E y)) -> L) (T L L)
		,(\ (T (E x) ~(E y)) -> L) (T (E L) L)
		,(\ (T (E x) ~(E y)) -> L) (T L (E L))
		,(\ (T (E x) ~(E y)) -> L) (T (E L) (E L))
		,(\ (T (E x) ~(E y)) -> x) L,(\ (T (E x) ~(E y)) -> x) (T L L),
		(\ (T (E x) ~(E y)) -> x) (T (E L) L),
		(\ (T (E x) ~(E y)) -> x) (T L (E L)),
		(\ (T (E x) ~(E y)) -> x) (T (E L) (E L)),
		(\ (T (E x) ~(E y)) -> y) L,
		(\ (T (E x) ~(E y)) -> y) (T L L),
		(\ (T (E x) ~(E y)) -> y) (T (E L) L),
		(\ (T (E x) ~(E y)) -> y) (T L (E L)),(\ (T (E x) ~(E y)) -> y) (T (E L) (E L)),
		(\ (T ~(E x) (E y)) -> L) L,
		(\ (T ~(E x) (E y)) -> L) (T L L),
		(\ (T ~(E x) (E y)) -> L) (T (E L) L),
		(\ (T ~(E x) (E y)) -> L) (T L (E L)),
		(\ (T ~(E x) (E y)) -> L) (T (E L) (E L)),
		(\ (T ~(E x) (E y)) -> x) L,
		(\ (T ~(E x) (E y)) -> x) (T L L),
		(\ (T ~(E x) (E y)) -> x) (T (E L) L),
		(\ (T ~(E x) (E y)) -> x) (T L (E L)),
		(\ (T ~(E x) (E y)) -> x) (T (E L) (E L)),
		(\ (T ~(E x) (E y)) -> y) L,
		(\ (T ~(E x) (E y)) -> y) (T L L),
		(\ (T ~(E x) (E y)) -> y) (T (E L) L),
		(\ (T ~(E x) (E y)) -> y) (T L (E L)),
		(\ (T ~(E x) (E y)) -> y) (T (E L) (E L)),
		(\ (T ~(E x) ~(E y)) -> L) L,
		(\ (T ~(E x) ~(E y)) -> L) (T L L),
		(\ (T ~(E x) ~(E y)) -> L) (T (E L) L),
		(\ (T ~(E x) ~(E y)) -> L) (T L (E L)),
		(\ (T ~(E x) ~(E y)) -> L) (T (E L) (E L)),
		(\ (T ~(E x) ~(E y)) -> x) L,
		(\ (T ~(E x) ~(E y)) -> x) (T L L),
		(\ (T ~(E x) ~(E y)) -> x) (T (E L) L),
		(\ (T ~(E x) ~(E y)) -> x) (T L (E L)),
		(\ (T ~(E x) ~(E y)) -> x) (T (E L) (E L)),
		(\ (T ~(E x) ~(E y)) -> y) L,
		(\ (T ~(E x) ~(E y)) -> y) (T L L),
		(\ (T ~(E x) ~(E y)) -> y) (T (E L) L),
		(\ (T ~(E x) ~(E y)) -> y) (T L (E L)),
		(\ (T ~(E x) ~(E y)) -> y) (T (E L) (E L)),
		(\ ~(T (E x) (E y)) -> L) L,
		(\ ~(T (E x) (E y)) -> L) (T L L),
		(\ ~(T (E x) (E y)) -> L) (T (E L) L),
		(\ ~(T (E x) (E y)) -> L) (T L (E L)),
		(\ ~(T (E x) (E y)) -> L) (T (E L) (E L)),
		(\ ~(T (E x) (E y)) -> x) L,
		(\ ~(T (E x) (E y)) -> x) (T L L),
		(\ ~(T (E x) (E y)) -> x) (T (E L) L),
		(\ ~(T (E x) (E y)) -> x) (T L (E L)),
		(\ ~(T (E x) (E y)) -> x) (T (E L) (E L)),
		(\ ~(T (E x) (E y)) -> y) L,
		(\ ~(T (E x) (E y)) -> y) (T L L),
		(\ ~(T (E x) (E y)) -> y) (T (E L) L),
		(\ ~(T (E x) (E y)) -> y) (T L (E L)),
		(\ ~(T (E x) (E y)) -> y) (T (E L) (E L))
		,(\ ~(T (E x) ~(E y)) -> L) L,
		(\ ~(T (E x) ~(E y)) -> L) (T L L),
		(\ ~(T (E x) ~(E y)) -> L) (T (E L) L),
		(\ ~(T (E x) ~(E y)) -> L) (T L (E L)),
		(\ ~(T (E x) ~(E y)) -> L) (T (E L) (E L)),
		(\ ~(T (E x) ~(E y)) -> x) L,
		(\ ~(T (E x) ~(E y)) -> x) (T L L),
		(\ ~(T (E x) ~(E y)) -> x) (T (E L) L),(\ ~(T (E x) ~(E y)) -> x) (T L (E L)),(\ ~(T (E x) ~(E y)) -> x) (T (E L) (E L)),(\ ~(T (E x) ~(E y)) -> y) L,(\ ~(T (E x) ~(E y)) -> y) (T L L),(\ ~(T (E x) ~(E y)) -> y) (T (E L) L),(\ ~(T (E x) ~(E y)) -> y) (T L (E L)),(\ ~(T (E x) ~(E y)) -> y) (T (E L) (E L)),(\ ~(T ~(E x) (E y)) -> L) L,(\ ~(T ~(E x) (E y)) -> L) (T L L),(\ ~(T ~(E x) (E y)) -> L) (T (E L) L),(\ ~(T ~(E x) (E y)) -> L) (T L (E L)),(\ ~(T ~(E x) (E y)) -> L) (T (E L) (E L)),(\ ~(T ~(E x) (E y)) -> x) L,(\ ~(T ~(E x) (E y)) -> x) (T L L),(\ ~(T ~(E x) (E y)) -> x) (T (E L) L),(\ ~(T ~(E x) (E y)) -> x) (T L (E L)),(\ ~(T ~(E x) (E y)) -> x) (T (E L) (E L)),(\ ~(T ~(E x) (E y)) -> y) L,(\ ~(T ~(E x) (E y)) -> y) (T L L),(\ ~(T ~(E x) (E y)) -> y) (T (E L) L),(\ ~(T ~(E x) (E y)) -> y) (T L (E L)),(\ ~(T ~(E x) (E y)) -> y) (T (E L) (E L)),(\ ~(T ~(E x) ~(E y)) -> L) L,(\ ~(T ~(E x) ~(E y)) -> L) (T L L),(\ ~(T ~(E x) ~(E y)) -> L) (T (E L) L),(\ ~(T ~(E x) ~(E y)) -> L) (T L (E L)),(\ ~(T ~(E x) ~(E y)) -> L) (T (E L) (E L)),(\ ~(T ~(E x) ~(E y)) -> x) L,(\ ~(T ~(E x) ~(E y)) -> x) (T L L),
		(\ ~(T ~(E x) ~(E y)) -> x) (T (E L) L),
		(\ ~(T ~(E x) ~(E y)) -> x) (T L (E L)),
		(\ ~(T ~(E x) ~(E y)) -> x) (T (E L) (E L)),
		(\ ~(T ~(E x) ~(E y)) -> y) L,
		(\ ~(T ~(E x) ~(E y)) -> y) (T L L),
		(\ ~(T ~(E x) ~(E y)) -> y) (T (E L) L),
		(\ ~(T ~(E x) ~(E y)) -> y) (T L (E L)),
		(\ ~(T ~(E x) ~(E y)) -> y) (T (E L) (E L))]


p1 = tpat (epat (varpat "x")) (epat (varpat "y"))

p2 = tpat (epat (varpat "x")) (Ptilde (epat (varpat "y")))

p3 = tpat (Ptilde (epat (varpat "x"))) (epat (varpat "y"))

p4 = tpat (Ptilde (epat (varpat "x"))) (Ptilde (epat (varpat "y")))

p5 = Ptilde $ tpat (epat (varpat "x")) (epat (varpat "y"))

p6 = Ptilde $ tpat (epat (varpat "x")) (Ptilde (epat (varpat "y")))

p7 = Ptilde $ tpat (Ptilde (epat (varpat "x"))) (epat (varpat "y"))

p8 = Ptilde $ tpat (Ptilde (epat (varpat "x"))) (Ptilde (epat (varpat "y")))

pats = [p1,p2,p3,p4,p5,p6,p7,p8]

{-
cbody = transHS leafexp
xbody = Vbl "x"
ybody = Vbl "y"

hcl0 = App (Abs [p5] xbod) ar2

arg0 = transHS leafexp
arg1 = transHS $ texp leafexp leafexp
arg2 = transHS $ texp (eexp leafexp) leafexp
arg3 = transHS $ texp leafexp (eexp leafexp) 
arg4 = transHS $ texp (eexp leafexp) (eexp leafexp) 
-}

cbod = leafexp
xbod = Var "x"
ybod = Var "y"

ar0 = leafexp
ar1 = texp leafexp leafexp
ar2 = texp (eexp leafexp) leafexp
ar3 = texp leafexp (eexp leafexp) 
ar4 = texp (eexp leafexp) (eexp leafexp) 


{-

data Hack = H (P,LQ,LQ)

data Hack2 = H2 (P,E,E)

locate x (v:vs) = if (x==v) then 0 else 1 + (locate x vs)

nth 0 (v:vs) = v
nth n (v:vs) = nth (n-1) vs

n i = nth i beispiele
h i = nth i hugs


beispiele = examples pats []
mysamples = samples pats []

examples :: [P] -> [Hack] -> [Hack] 
examples [] acc     = acc
examples (p:ps) acc = examples ps (acc++(oneeach p))
   where oneeach p = argify p cbody ++ argify p xbody ++ argify p ybody
         argify p b = [H(p,b,arg0),H(p,b,arg1),H(p,b,arg2),H(p,b,arg3),H(p,b,arg4)]

samples :: [P] -> [Hack2] -> [Hack2] 
samples [] acc     = acc
samples (p:ps) acc = samples ps (acc++(oneeach p))
   where oneeach p = argify p cbod ++ argify p xbod ++ argify p ybod
         argify p b = [H2(p,b,ar0),H2(p,b,ar1),H2(p,b,ar2),H2(p,b,ar3),H2(p,b,ar4)]

showExamples (H (p,b,a)) = 
        "(\\ "++show p++" -> "++show b++") "++show a++"\n"

showSamples (H2 (p,b,a)) = 
        "(\\ "++show p++" -> "++show b++") "++show a++"\n"

instance Show Hack where
     show = showExamples 

instance Show Hack2 where
     show = showSamples 
-}
