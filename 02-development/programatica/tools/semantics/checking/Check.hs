module Check
where
import List
import AST
import MyHaskell
import Variables
import Valid
import Rules
import Maybe

valid0 = validateRule
  (Conclude [Consequence [PropVar "Gamma"] 
                         [Assert (Var "x") Triv (Strong (PredVar "P"))]]
            (Consequence [PropVar "Gamma"] 
                         [Assert (Var "x") Triv (Strong (PredVar "P"))]))

valid0' = validateRule
  (Conclude [Consequence [PropVar "Gamma"] 
                         [Assert (Var "x") Triv (Strong (PredVar "P"))]]
            (Consequence [PropVar "Gamma"] 
                         [Assert (Var "x") Triv (Strong Univ)]))

valid1 = validateRule
  (Conclude [Consequence [Assert (Var "x") Triv (PredVar "P")] 
                         [PropVar "Delta"]]
            (Consequence [Assert (Var "x") Triv (PredVar "P")] 
                         [PropVar "Delta"]))

valid00 = not $ validaterule  -- consequent asserts a stronger property
  (Conclude [Consequence [PropVar "Gamma"] [Assert (Var "x") (Arrow Triv Triv) 
                             (Parrow (PredVar "P") Univ)]]
            (Consequence [PropVar "Gamma"] [Assert (Var "x") (Arrow Triv Triv) 
                             (Parrow (Strong (PredVar "P")) (PredVar "Q"))]))
            [("P",Univ),("Q",Strong Univ)]

valid9 = validateRule  -- is False
   (Conclude [Consequence [PropVar "Gamma", Assert (Var "x") Triv 
                                                               (PredVar "P")]
                          [Assert (Var "e") Triv (PredVar "Q")]]
             (Consequence [PropVar "Gamma"]
                          [Assert (Abs [Pvar "x"] (Var "e")) (Arrow Triv Triv)
                              (Strong (Parrow (PredVar "P") (PredVar "Q")))]))

valid10 = validateRule
  (Conclude [Consequence [PropVar "Gamma"] 
                         [Assert (Var "e2") Triv (PredVar "P")],
             Consequence [Assert (App Triv (Var "e1") (Var "e2")) Triv 
                                 (PredVar "Q")]
                         [PropVar "Delta"]]
            (Consequence [(PropVar "Gamma"),Assert (Var "e1") (Arrow Triv Triv)
                                           (Strong (Parrow (PredVar "P")
                                                           (PredVar "Q")))]
                         [PropVar "Delta"]))

invalid10 = not $ validateRule
  (Conclude [Consequence [PropVar "Gamma"] [Assert (Var "e2") Triv 
                                                   (PredVar "P")],
             Consequence [Assert (App Triv (Var "e1") (Var "e2")) Triv 
                                 (PredVar "Q")]
                         [PropVar "Delta"]]
            (Consequence [(PropVar "Gamma"),Assert (Var "e1") (Arrow Triv Triv)
                                          (Parrow (PredVar "P") (PredVar "Q"))]
                         [PropVar "Delta"]))

rule11 = 
  (Conclude [Consequence [PropVar "Gamma"] 
                         [Assert (Var "e1") (Arrow Triv Triv)
                                (Strong (Parrow (PredVar "P") (PredVar "Q")))],
             Consequence [PropVar "Gamma"] [Assert (Var "e2") Triv 
                                                   (PredVar "P")]]
            (Consequence [PropVar "P"]
                      [Assert (App Triv (Var "e1") (Var "e2")) Triv 
                                                   (PredVar "Q")]))

valid11 = validateRule   -- is False
  (Conclude [Consequence [PropVar "Gamma"] 
                         [Assert (Var "e1") (Arrow Triv Triv)
                                (Strong (Parrow (PredVar "P") (PredVar "Q")))],
             Consequence [PropVar "Gamma"] [Assert (Var "e2") Triv 
                                                   (PredVar "P")]]
            (Consequence [PropVar "Gamma"]
                      [Assert (App Triv (Var "e1") (Var "e2")) Triv 
                                                   (PredVar "Q")]))

valid11_ = validateRule  -- is True
  (Conclude [Consequence [PropVar "Gamma"] 
                         [Assert (Var "e1") (Arrow Triv Triv)
                                (Strong (Parrow (PredVar "P") (PredVar "Q")))]]
            (Consequence [PropVar "Gamma", Assert (Var "e2") Triv 
                                                  (PredVar "P")]
                      [Assert (App Triv (Var "e1") (Var "e2")) Triv 
                                                   (PredVar "Q")]))

valid11__ = validaterule -- is True in a pred env in which Q = Univ
  (Conclude [Consequence [PropVar "Gamma"] 
                         [Assert (Var "e1") (Arrow Triv Triv)
                                (Strong (Parrow (PredVar "P") (PredVar "Q")))]]
            (Consequence [PropVar "Gamma", Assert (Var "e2") Triv 
                                                  (PredVar "P")]
                      [Assert (App Triv (Var "e1") (Var "e2")) Triv 
                                                   (PredVar "Q")]))
  [("P",UnDef),("Q", Univ)]

valid11___ = validaterule -- is False in a pred env in which Q = $Univ
  (Conclude [Consequence [PropVar "Gamma"] 
                         [Assert (Var "e1") (Arrow Triv Triv)
                                (Strong (Parrow (PredVar "P") (PredVar "Q")))]]
            (Consequence [PropVar "Gamma",Assert (Var "e2") Triv (PredVar "P")]
                      [Assert (App Triv (Var "e1") (Var "e2")) Triv 
                                                   (PredVar "Q")]))
  [("P",UnDef),("Q",Strong Univ)]


valid12 = validateRule  -- True
  (Conclude [Consequence [Assert (Var "e") (Arrow Triv Triv) 
                                 (Parrow (PredVar "P") (Strong (PredVar "Q")))]
                         [PropVar "Delta"]]
            (Consequence [Assert (Var "x") Triv (PredVar "P"),
                          Assert (App Triv (Var "e") (Var "x")) Triv 
                                 (Strong (PredVar "Q"))]
                         [PropVar "Delta"]))

valid13F = not $ validateRule  -- True
  (Conclude [Consequence [PropVar "Gamma"] [Assert (Var "t") List Univ]]
            (Consequence [PropVar "Gamma"]
               [Assert (App Triv 
                          (App List 
                             (Constr "Cons" [(Lazy,Triv),(Lazy,List)])
                             Undefined)
                          (Var "t"))
                       List
                       (Strong (ConPred "Nil" [Strong Univ,(PredVar "Q")]))]))

valid13T = validateRule 
  (Conclude [Consequence [PropVar "Gamma"] 
            [Conj [Assert (Var "h") Triv (Strong Univ)
                  ,Assert (Var "t") List Univ]]]
            (Consequence [PropVar "Gamma"]
               [Assert (App List
                          (App Triv
                             (Constr "Cons" [(Lazy,Triv),(Lazy,List)])
                             (Var "h"))
                          (Var "t"))
                       List
                       (Strong (ConPred "Cons" [Strong Univ, Univ]))]))


valid13S = validateRule
  (Conclude [Consequence [PropVar "Gamma"] 
                         [Assert (Var "x") Triv (Strong Univ)]]
            (Consequence [PropVar "Gamma"] 
               [Assert (App Triv (Constr "Strict" [(Strict,Triv)]) (Var "x"))
                       StrictOpt (Strong (ConPred "Strict" [Strong Univ]))]))

valid13U = not $ validateRule
  (Conclude [Consequence [PropVar "Gamma"] 
                         [Assert (Var "x") Triv Univ]]
            (Consequence [PropVar "Gamma"] 
               [Assert (App Triv (Constr "Strict" [(Strict,Triv)]) (Var "x"))
                       StrictOpt (Strong (ConPred "Strict" [Univ]))]))

valid13L = validateRule
  (Conclude [Consequence [PropVar "Gamma"] 
                         [Assert (Var "x") Triv Univ]]
            (Consequence [PropVar "Gamma"] 
               [Assert (App Triv (Constr "Lazy" [(Lazy,Triv)]) (Var "x"))
                       StrictOpt (Strong (ConPred "Lazy" [Univ]))]))


valid14 = validateRule
  (Conclude [Consequence [PropVar "Gamma"] 
                         [Assert (Var "x") Triv (Strong Univ)]]
            (Consequence [PropVar "Gamma"] 
               [Not (Assert (App Triv (Constr "Strict" [(Strict,Triv)]) (Var "x"))
                       StrictOpt (ConPred "Lazy" [Univ]))]))

valid15T = validateRule
  (Conclude [Consequence [Assert (Var "x") Triv (PredVar "P")]
                         [PropVar "Delta"]]
            (Consequence 
               [Assert (App Triv (Constr "Lazy" [(Lazy,Triv)]) (Var "x"))
                       StrictOpt (Strong (ConPred "Lazy" [PredVar "P"]))]
               [PropVar "Delta"]))

valid15F = not $ validateRule
  (Conclude [Consequence [Assert (Var "x") Triv (Strong (PredVar "P"))]
                         [PropVar "Delta"]]
            (Consequence 
               [Assert (App Triv (Constr "Lazy" [(Lazy,Triv)]) (Var "x"))
                       StrictOpt (Strong (ConPred "Lazy" [PredVar "P"]))]
               [PropVar "Delta"]))

allValid =
  and [valid0,valid00,valid0',valid1,valid9,valid10,valid11,valid11_,
       valid11__,valid11___,valid12,valid13F,valid13T,valid13S,valid13U,
       valid13L,valid14,valid15T,valid15F]