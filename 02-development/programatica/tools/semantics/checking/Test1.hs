module Test1
where
import AST
import MyHaskell
import Valid
import Variables
import Rules
import Maybe
import List

-- Tests of rules for eliminating application on the left.

ruleAppLeft = 
  Conclude
    [Consequence [PropVar "Gamma"][Assert (Var "x") Triv (PredVar "P")]
    ,Consequence [Assert (App (Var "e") (Var "x")) Triv (PredVar "Q")]
                 [PropVar "Delta"]]
    (Consequence [PropVar "Gamma"
                 ,Assert (Var "e") (Arrow Triv Triv)
                         (Strong (Parrow (PredVar "P") (PredVar "Q")))]
                 [PropVar "Delta"])

ruleAppLeft1 = 
  Conclude
    [Consequence [PropVar "Gamma"][Assert (Var "x") Triv (PredVar "P")]
    ,Consequence [PropVar "Gamma"
                 ,Assert (App (Var "e") (Var "x")) Triv (PredVar "Q")]
                 [PropVar "Delta"]]
    (Consequence [PropVar "Gamma"
                 ,Assert (Var "e") (Arrow Triv Triv)
                         (Strong (Parrow (PredVar "P") (PredVar "Q")))]
                 [PropVar "Delta"])

testallPQ = allPreds ["P","Q"] ==
        [[("P",UnDef),("Q",UnDef)],[("P",UnDef),("Q",Strong Univ)]
        ,[("P",Strong Univ),("Q",UnDef)],[("P",Strong Univ),("Q",Strong Univ)]]

testAppLeft_ = validateRule_ ruleAppLeft

testAppLeft = and
 [validateRule ruleAppLeft

 ,validateRule ruleAppLeft1

 ,not $ validateRule $   -- assumption weakened in the conclusion
  Conclude
    [Consequence [PropVar "Gamma"][Assert (Var "x") Triv (PredVar "P")]
    ,Consequence [Assert (App (Var "e") (Var "x")) Triv (PredVar "Q")]
                 [PropVar "Delta"]]
    (Consequence [PropVar "Gamma"
                 ,Assert (Var "e") (Arrow Triv Triv)
                         (Parrow (PredVar "P") (PredVar "Q"))]
                 [PropVar "Delta"])
 ]

-- Delta records that App e x :: Q
-- To check this in the consequent, look for an "e" value, an "x" value,
-- and apply the "e" value to the "x" value, checking that it satisfies "Q".
