module Examples where

import AST

-----------------------------------------------------------
--			some examples
-----------------------------------------------------------

conapp n els =
  let annotations = map snd els
      exps        = map fst els
  in
      ConApp (n,annotations) exps

leftpat = \ x -> Pcondata "L" [Pvar x]
leftrightpat = \ x -> Pcondata "L" $ [Pcondata "R" [Pvar x]]
rpat = \ x -> Pcondata "R" [x]
spat = \ x -> Pcondata "S" [x]
bpat = \ x -> Pcondata "B" [x]
lpat = \x -> Pcondata "L" [x]
epat = \x -> Pcondata "E" [x]
tpat = \p1 p2 -> Pcondata "T" [p1,p2]
lexp = \t -> conapp "L" [(t,Lazy)]
eexp = \t -> conapp "E" [(t,Lazy)]
bexp = \t -> conapp "B" [(t,Lazy)]
rexp = \t -> conapp "R" [(t,Lazy)]
texp = \t1 t2 -> conapp "T" [(t1,Lazy),(t2,Lazy)]
leafexp = conapp "L" []
leftexp = \t -> conapp "L" [(t,Lazy)]
rightexp = \t -> conapp "R" [(t,Lazy)]
pairexp = \ t1 t2 -> TupleExp [t1,t2]
lambda p e = Abs [p] e
