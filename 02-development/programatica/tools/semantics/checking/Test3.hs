module Test3
where
import AST
import MyHaskell
import Valid
import Variables
import Rules
import Maybe
import List

vTrivs = [Bottom,Void]
vLists = [Bottom,Tagged "Nil" [],Tagged "Cons" [Bottom,Bottom]]

testTraces = (constTraces vTrivs vLists) ++ (strictTraces vTrivs vLists)

testmkFunctionSpace = mkFunctionSpace Triv List

ruleAbs = 
  Conclude
    [Consequence [PropVar "Gamma",Assert (Var "x") Triv (PredVar "P")]
                 [Assert (Var "e") List (PredVar "Q")]]
    (Consequence [PropVar "Gamma"]
                 [Assert (Abs [Pvar "x"] (Var "e"))
                         (Arrow Triv List)
                         (Parrow (PredVar "P") (PredVar "Q"))])

testRuleAbs = validateRule ruleAbs

testRuleAbs_ = validateRule_ ruleAbs

testdc =
  bindContext "Gamma" 
     (Consequence 
        [PropVar "Gamma", Assert (Var "x") Triv (PredVar "P")]
        [Truth])
--        [Assert (Var "e") List (PredVar "Q")])
     [("P",Strong Univ),("Q",Strong Univ)]

testdcuniv =
  bindContext "Gamma" 
     (Consequence 
        [PropVar "Gamma", Assert (Var "x") Triv (PredVar "P")]
        [Assert (Var "e") List (PredVar "Q")])
     [("P", Univ),("Q", Univ)]

testdcss =
  bindContext "Gamma" 
     (Consequence 
        [PropVar "Gamma", Assert (Var "x") Triv (PredVar "P")]
        [Assert (Var "e") List (PredVar "Q")])
     [("P",Strong Univ),("Q",Strong Univ)]

testdcsu =
  bindContext "Gamma" 
     (Consequence 
        [PropVar "Gamma", Assert (Var "x") Triv (PredVar "P")]
        [Assert (Var "e") List (PredVar "Q")])
     [("P",Strong Univ),("Q",UnDef)]

testdcus =
  bindContext "Gamma" 
     (Consequence 
        [PropVar "Gamma", Assert (Var "x") Triv (PredVar "P")]
        [Assert (Var "e") List (PredVar "Q")])
     [("P",UnDef),("Q",Strong Univ)]

testdcuu =
  bindContext "Gamma" 
     (Consequence 
        [PropVar "Gamma", Assert (Var "x") Triv (PredVar "P")]
        [Assert (Var "e") List (PredVar "Q")])
     [("P",UnDef),("Q",UnDef)]

testmkFTSpace = mkFTSpace Triv Triv
testmkFTSpace' = mkFTSpace  Triv List
testmkFTSpace'' = mkFTSpace  List List


-- constTraces (concat (map (map snd) (map reverse rhos))) vs

tstTraces rhos vs = 
   map FT (constTraces  (concat (map (map snd) (map reverse rhos))) vs ++
           strictTraces (concat (map (map snd) (map reverse rhos))) vs)

testbs =
  fst
   (sequentBindings "Gamma" "Delta" [("P",Strong Univ),("Q",Strong Univ)] 
       [(Consequence 
          [PropVar "Gamma", Assert (Var "x") Triv (PredVar "P")]
          [Assert (Var "e") List (PredVar "Q")])])
--  == fromJust testdcss, but we can't check equality of function values.

testBindings = let Conclude antecedents _ = ruleAbs in
               sequentBindings "Gamma" "Delta" 
                   [("P",Strong Univ),("Q",Strong Univ)]
                   antecedents

resultBndg = let Conclude antecedents _ = ruleAbs in
             unionEnvs $ 
                      map (\s -> bindConclusion "Delta" s 
                             [("P",Strong Univ),("Q",Strong Univ)]) antecedents

testconsequent = let Conclude _ consequents = ruleAbs 
                     (context,_) = testBindings
                 in checkSequent consequents context resultBndg
                                 [("P",Strong Univ),("Q",Strong Univ)]

testconsequent_ = let Conclude _ consequents = ruleAbs 
                      (context,_) = testBindings
                  in checkSequent_ consequents context resultBndg             
                                  [("P",Strong Univ),("Q",Strong Univ)]



testA' =
   map (\e -> truthV 
                 (Assert (Abs [Pvar "x"] (Var "e"))
                         (Arrow Triv List)
                         (Parrow (PredVar "P") (PredVar "Q")))
                 e
                 [("P",Strong Univ),("Q",Strong Univ)]
       ) (filter (\e -> truthV (Assert (Var "x") Triv (Strong Univ))
                        e []) testconsequent_)

env1 = case fromJust testdcsu of    -- const "Cons(_|_,_|_)"
         (e:_) -> e
         _ -> error "empty envs"

dcsuenvs = (fromJust testdcuniv)

testAbsVal = and
 [map 
  (\rho -> truthV (Assert (Abs [Pvar "x"] (Var "e"))
                  (Arrow Triv List)
                  (Parrow (Strong Univ) UnDef)) 
                  rho []) dcsuenvs
  == [False,False,False,False,True]
 ,map
  (\rho -> truthV (Assert (Abs [Pvar "x"] (Var "e")) 
                  (Arrow Triv List)
                  (Parrow (Strong Univ) (Strong Univ))) 
                  rho []) dcsuenvs
 == [True,True,True,True,False]
 ,map
  (\rho -> truthV (Assert (Abs [Pvar "x"] (Var "e")) 
                  (Arrow Triv List)
                  (Parrow UnDef (Strong Univ)))
                  rho []) dcsuenvs
 == [True,True,False,False,False]
 ,map
  (\rho -> truthV (Assert (Abs [Pvar "x"] (Var "e"))
                  (Arrow Triv List)
                  (Parrow (Strong Univ) (Strong (ConPred "Cons" [UnDef,UnDef])))) 
                  rho []) dcsuenvs
 == [True,False,True,False,False]
 ]
funProp :: Prop
funProp = Assert (Var "f") (Arrow Triv List)
                 (Parrow (PredVar "P") (PredVar "Q"))

testFnEnvs =
  let envs = allVs [("f",Arrow Triv List)] []
  in filter (\e -> truthV funProp e [("P",Strong Univ),("Q",Strong Univ)]) envs

testallDepV = allDepV Triv ("x",Triv) []

testFTruth = and
  [truthV (Assert (Var "e") (Arrow Triv List) (Parrow (PredVar "P") (PredVar "Q")))
       [("e",FT[(Bottom,Bottom),(Void,Tagged "Nil" [])])]
       [("P",Univ),("Q",Univ)]
  ,truthV (Assert (Var "e") (Arrow Triv List) (Parrow (PredVar "P") (PredVar "Q")))
       [("e",FT[(Bottom,Bottom),(Void,Tagged "Nil" [])])]
       [("P",Strong Univ),("Q",Strong Univ)]
  ,not $ truthV (Assert (Var "e") (Arrow Triv List) (Parrow (PredVar "P") (PredVar "Q")))
       [("e",FT[(Bottom,Bottom),(Void,Tagged "Nil" [])])]
       [("P",UnDef),("Q",Strong Univ)]
  ,not $ truthV (Assert (Var "e") (Arrow Triv List) (Parrow (PredVar "P") (PredVar "Q")))
       [("e",FT[(Bottom,Bottom),(Void,Tagged "Nil" [])])]
       [("P",Strong Univ),("Q",UnDef)]
  ]

