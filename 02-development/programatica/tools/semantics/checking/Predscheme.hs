module Predscheme
where
import AST
import MyHaskell
import Valid
import Variables
import Maybe
import List

-- bindPredScheme calculates bindings of a PredScheme, as PredAbstractions
-- including const UnDef, const $Univ, Id, and the constructed abstractions
-- derived from the constructors of the signature of the PredScheme's type

bindPredScheme :: Name -> T -> [PredEnv]
bindPredScheme n tp = [[(n,PredAbs (\pr -> constructPred tp pr))]
                      ,[(n,PredAbs (\pr -> pr))]
                      ,[(n,PredAbs (\pr -> Strong Univ))]
                      ,[(n,PredAbs (\pr -> UnDef))]]

    where constructPred :: T -> Pr -> Pr
          constructPred tp pr = 
             let prs = map (\(C cname tl) ->
                               ConPred cname (map (\(tp',ls) ->
                                                    let p = if tp==tp' then pr
                                                            else Univ
                                                    in if ls==Lazy then p
                                                       else Strong p) tl))
                           (filter (\e -> case e of
                                             C _ _ -> True
                                             _     -> False) (sig tp))
             in case prs of
                   [] -> error ("bindPredScheme finds no constructor of type "
                                ++ show tp)
                   [pr']      -> pr'
                   [pr',pr''] -> PredDisj pr' pr''
                   _  ->  error ("bindPredScheme finds more than two constructors of type "
                                ++ show tp)


testbindPredScheme = 
   map (map (\(_,PredAbs p) -> p (PredVar "X"))) $ bindPredScheme "H" List

instantiatePredScheme :: Pr -> PredEnv -> [PredEnv]
instantiatePredScheme (PredScheme n tp pr) pe =
   case pr of  -- THIS IS WRONG.  NEED TO USE BINDING OF PREDVAR FROM PREDENV
     PredVar x -> case lookup x pe of
                     Just pr' -> map ((pe ++) . applyPredAbs pr') 
                                     (bindPredScheme n tp)
                     Nothing -> error("instantiatePredScheme: unbound PredVar "
                                      ++ x)
     Strong (PredVar x) -> 
                  case lookup x pe of
                     Just pr' -> map ((pe ++) . applyPredAbs (Strong pr')) 
                                     (bindPredScheme n tp)
                     Nothing -> error("instantiatePredScheme: unbound PredVar "
                                      ++ x)
     _ -> map ((pe ++) . applyPredAbs pr)
              (bindPredScheme n tp)
   where
     applyPredAbs pr [(n,PredAbs p)] = [(n, p pr)]
     applyPredAbs _ _ = error "match failure in applyPredAbs"

     combinePredEnvs [] pes' = pes'
     combinePredEnvs ([]:pes) pes' = combinePredEnvs pes pes'
     combinePredEnvs (pe:pes) pes' = 
                  combinePredEnvs pes (map (\pe' -> pe ++ pe') pes')


eliminateDuplicates :: [PredEnv] -> [PredEnv]
eliminateDuplicates [] = []
eliminateDuplicates (pe:pes) =
   if occursIn pe pes then eliminateDuplicates pes
   else pe : eliminateDuplicates pes

   where occursIn pe [] = False
         occursIn pe (pe':pes) = isEquiv pe pe' || occursIn pe pes

         isEquiv :: PredEnv -> PredEnv -> Bool
         isEquiv [] [] = True
         isEquiv [] _  = False
         isEquiv _  [] = False
         isEquiv ((n,p):pe) pe' = 
            case lookup n pe' of
               Just r  -> p==r && isEquiv pe (filter (\(n',_) -> n/=n') pe')
               Nothing -> False

testeliminateDuplicates =
   eliminateDuplicates [[("P",UnDef),("Q",UnDef)]
                       ,[("P",UnDef),("Q",UnDef)]
                       ,[("P",UnDef),("Q",Univ)]
                       ]

testinstantiatePredScheme = eliminateDuplicates $
   instantiatePredScheme (PredScheme "H" List (PredVar "Xi")) 
       [("Xi",Strong Univ)]
