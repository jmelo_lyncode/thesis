module MyHaskell where

import AST 
import Examples
import Maybe

is_member _ [] = False
is_member x (y:ys) | x == y = True
is_member x (y:ys) | True   = is_member x ys

----------------------------------------------------------------------
-- Values
----------------------------------------------------------------------

data V 
  = Tagged Name [V]  --- algebraic structured data
  | FT [(V,V)]       --- trace representation of a function
  | DV [Name] V      --- dependent value, used only in a context binding
  | Void             --- the unique non-Bottom value of type Triv
  | Bottom


----------------------------------------------------------------------
-- Type signatures
----------------------------------------------------------------------

data Constr = C Name [(T,LS)]
            | B V
            | A Constr Constr
  deriving (Eq, Show)

sig :: T -> [Constr]
sig Triv = [B Void, B Bottom]
sig List = [C "Cons" [(Triv,Lazy),(List,Lazy)],
            C "Nil" [],
            B Bottom]
sig StrictOpt = [C "Strict" [(Triv,Strict)],
                 C "Lazy" [(Triv,Lazy)],
                 B Bottom]
sig (Arrow t1 t2) = (B Bottom) : [A t1' t2' | t1' <- sig t1, t2' <- sig t2 ]
-- [A t1 t2,  B Bottom]

arrowTypes :: [T] -> [(Name,T)] -> [(Name,T)]
arrowTypes ts nts = foldr mkArrow nts ts
   where mkArrow t nts = map (\(s,t') -> (s,Arrow t t')) nts

constrArgTypes :: Name -> [T]
constrArgTypes n = if n == "Cons" then [Triv,List] else []

-------------------------------------------------------------
-------------------------------------------------------------
type Env = [(Name, V)]              -- Association list, Maps Names to Values V

xEnv :: Env -> Name -> V -> Env
xEnv rho n phi = (n,phi):rho

zEnv :: Env -> Name -> V -> Env
zEnv rho n phi = case lookup n rho of
                   Just _  -> rho
                   Nothing -> xEnv rho n phi

extend :: Env -> [Name] -> [V] -> Env
extend rho xs vs = zip xs vs ++ rho

------------------------------------------------------------------
{-
Call these variables the "fringe" of p. (fringe p) returns the names of free 
variables of p in *order* of occurrence from left to right. -}

-- returns the fringe of p in order  from left to right.
fringe :: P -> [Name]
fringe (Pvar x) = [x]
fringe (Pcondata n ps) = foldr (++) [] (map fringe ps)
fringe (Ptilde p) = fringe p
fringe (Paspat n p) = n : (fringe p)
fringe Pwildcard = []

n_bvs :: P -> Int
n_bvs (Pvar x) = 1
n_bvs (Pcondata n ps) = sum(map n_bvs ps)
n_bvs (Ptilde p) = n_bvs p
n_bvs (Paspat n p) = n_bvs p
n_bvs Pwildcard = 0

tildefy :: P -> P
tildefy p = case p of 
                  (Ptilde p') -> p
                  (Pvar x)    -> p
                  Pwildcard   -> p
                  Paspat n p  -> Paspat n (tildefy p)
                  _           -> (Ptilde p)

-- eqHd :: Show a => Eq a => a -> [a] -> Bool
eqHd x (y:_) = x==y
eqHd x []    = error ("eqHd " ++ show x ++ " is applied to []")

hd :: [a] -> a
hd xs = case xs of
          (x:_) -> x
          []    -> error "hd of [] not defined"

tl :: [a] -> [a]
tl xs = case xs of
          (_:y) -> y
          []    -> error "tl of [] not defined"

showWithCommas :: Show a => [a] -> [Char]
showWithCommas [] = ""
showWithCommas [x] = show x
showWithCommas (x:xs) = show x ++ ',':showWithCommas xs

-------------------------------------------
--      Important Semantic Operators     --
-------------------------------------------

-- Composition operators (N.b., both diagrammatic order.)
(>>>) :: (a -> b) -> (b -> c) -> a -> c
f >>> g = g . f              -- Functional

(<>) :: (a -> Maybe b) -> (b -> Maybe c) -> a -> Maybe c
f <> g = \x -> f x >>= g     -- Kleisli 

purify :: Maybe V -> V                  -- "run" of the Maybe monad
purify (Just x) =  x
purify Nothing  = Bottom

semseq :: V -> V -> V
semseq x y = case x of 
                (Tagged _ _) -> y
                (FT _)       -> y
                Void         -> y
                Bottom       -> Bottom 

app :: V -> V -> V                      -- Application
app (FT tes) x = 
   case lookup x tes of
      Nothing -> Bottom
--         error ("in application of FT[" ++ showWithCommas tes ++ "] to "
--                    ++ show x)
      Just r  -> r
app Bottom x = Bottom

fatbar :: (a -> Maybe b) -> (a -> Maybe b) -> (a -> Maybe b)
fatbar f g x = let r = f x in
               case r of
                 Just y  -> r
                 Nothing -> g x
{-
-- The function match is used to construct the meaning of a Match.
match :: Env -> (P, B) -> V -> Maybe V
match rho (p,b) = mP p <> (\vs -> mwhere (extend rho (fringe p) vs) b)
-}
-- used in letbind and mE
lam :: P -> E -> T -> Env -> V -> V
lam p e t rho = (mP p <> ((\vs -> mE e t (extend rho (fringe p) vs)) >>> Just))
              >>> purify

{-
mcase :: Env -> [Match] -> V -> V
mcase rho ml = (fatbarL $ map (match rho) ml) >>> purify
                     where fatbarL :: [V -> Maybe V] -> V -> Maybe V
                           fatbarL  = foldr fatbar (\ _ -> (Just Bottom))
-}
			 	   
{-
Translation of function bindings from Haskell98 Report (p. 54):

             x p(1,1) ... p(1,k)  match1
                      ...
             x p(n,1) ... p(n,k)  matchn

 is equivalent to
            x = \x1 ... xn -> case (x1,...,xn) of
			                            (p(1,1),...,p(1,k)) ->  match1
                                            ...
                                        (p(n,1),...,p(n,k)) ->  matchn  
	        where x1,...,xn are fresh variables.
			
The function below, "mD", is constructed similarly, although without any
variable generation. 
-}
-------------------------------------------
---          semantic values of a type  ---
-------------------------------------------


value :: Constr -> V
value (B v) = v
value (C n ts) = Tagged n (map (const Bottom) ts)

values :: T -> [V]
values (Arrow t1 t2) = mkFTSpace t1 t2
values t = map value (sig t)

-----------------------------------------------------------------
-- Modeling a function space of monotonic functions
-----------------------------------------------------------------

constTraces :: [V] -> [V] -> [[(V,V)]]
constTraces aVs bVs = map (\b -> (map (\a -> (a,b)) aVs))
                          (filter (/= Bottom) bVs)

ex :: [V] -> V -> [(V,V)] -> [[(V,V)]]
ex bVs a tr = map (\b -> (a,b):tr) bVs

eX :: [V] -> V -> [[(V,V)]] -> [[(V,V)]]
eX bVs a = concat . map (ex bVs a)

strictTraces :: [V] -> [V] -> [[(V,V)]]
strictTraces aVs bVs = map ((Bottom,Bottom):) $
                       foldr (eX bVs) [[]] (filter (/= Bottom) aVs)
mkFTSpace :: T -> T -> [V]
mkFTSpace t1 t2 =
   let v1s = values t1
       v2s = values t2
   in map FT (constTraces v1s v2s ++ strictTraces v1s v2s)
   
eqlength [] = True
eqlength ss = eqvals (map length ss)
   where eqvals [_] = True
         eqvals (l1:ls@(l2:_)) = l1 == l2 && eqvals ls

--------------------------------------------
---          semantic functions          ---
--------------------------------------------

mE  :: E -> T -> Env -> V
mP  :: P -> V -> Maybe [V]
-- mB  :: B -> Env -> Maybe V
-- mE' :: E -> (Name -> Maybe V) -> Maybe V
-----------------------------------------------------------------
-- The meaning of expressions
-----------------------------------------------------------------

ifV :: V -> a -> a -> a
ifV (Tagged "True" []) x y = x
ifV (Tagged "False" []) x y = y

mE (Var n) _ rho        = fromJust (lookup n rho)

mE (Cond e0 e1 e2) t rho = ifV (mE e0 Bool rho) (mE e1 t rho) (mE e2 t rho)

mE (Constr n sts) t rho = 
     case sts of
       []          -> Tagged n []
       (s,t'):sts' -> FT $ map (\v -> curryarg v (Constr n sts')) 
                                   (filterBottomIfStrict s (values t'))
     where filterBottomIfStrict Lazy   = id
           filterBottomIfStrict Strict = filter (\v -> v /= Bottom)

           curryarg v constr =
             case mE constr t rho of
               Tagged n [] -> (v, Tagged n [v])
               ft          -> (v, appendToArgList v ft)
             where appendToArgList v (FT tcs) = FT $ map (append2arg v) tcs

                   append2arg v (x,Tagged n args) = (x,Tagged n (v:args))
                   append2arg v (x,ft) = (x,appendToArgList v ft)
                     
mE (App t' e1 e2) t rho    = app (mE e1 (Arrow t' t) rho) (mE e2 t' rho)
mE (Abs [Pvar x] e@(Var y)) (Arrow t1 t2) rho =
          case lookup y rho of
            Just (DV ns m) | x `is_member` ns -> 
                                 FT $ map (\v -> (v,m)) (values t1)
                           | True   -> error ("Variable " ++ show x ++
                                         " abstracted from value dependent on "
                                         ++ show y)
            _ ->  let vs = values t1 in
                  FT $ zip vs (map (lam (Pvar x) e t2 rho) vs)
mE (Abs [p] e) (Arrow t1 t2) rho    = let vs = values t1 in
                                      FT $ zip vs (map (lam p e t2 rho) vs)
mE Trivconst Triv _ = Void

mE Undefined _ _    = Bottom

{- 
A pattern p may be viewed as a function of type ::a -> Maybe (b1,...,bn).
The denotation of a pattern is represented here as a function ::V -> Maybe [V].
-}
mP (Pvar x) v                    = Just [v]

mP (Pcondata n ps) (Tagged t vs) = if n==t then
                                        stuple (map mP ps) vs
                                   else Nothing

mP (Ptilde p) v = Just(case mP p v of
                          Nothing -> replicate (n_bvs p) Bottom
                          Just z -> z) 
mP Pwildcard v = Just []

stuple :: [V -> Maybe [V]] -> [V] -> Maybe [V]
stuple [] []         = Just []
stuple (q:qs) (v:vs) = do v'  <- q v
                          vs' <- stuple qs vs
                          Just (v'++vs')

eqV :: V -> V -> Bool
eqV (Tagged n vs) (Tagged n' vs') = n==n' && vs==vs'
eqV Bottom Bottom = True
eqV Void Void = True
eqV _ _ = False

instance Eq V where
  (==) = eqV

showV :: V -> String
showV (Tagged n [])    = n
showV (Tagged n [v])   = "("++n++" ?)"
showV (Tagged n [v1,v2])   = "("++n++" "++show v1++" "++show v2++")"
showV Bottom = "_|_"
showV Void = "()"
showV (FT vprs) = "FT[" ++ separateWithCommas (map show vprs) ++ "]"
showV (DV names v) = 
          "DV " ++ separateWithCommas(map show names) ++ " " ++ showV v

instance Show V where
  show = showV

separateWithCommas [] = ""
separateWithCommas [s] = s
separateWithCommas (s:ss) = s ++ ',':separateWithCommas ss
			
