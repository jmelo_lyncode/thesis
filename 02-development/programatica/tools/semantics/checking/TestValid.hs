----------------------------------------------------------------------------
module TestValid
where
import List
import AST
import MyHaskell
import Variables
import Valid
import Rules
import Maybe

validAssertion = truthV  -- True
  (Assert (Abs [Pvar "x"] (Var "x")) (Arrow Triv Triv)
                        (Parrow (Strong Univ) (Strong Univ))) [] []

validseq0 = validSequent  -- False
  (Consequence [Truth]
               [Assert (Abs [Pvar "x"] (Var "x")) (Arrow Triv Triv)
                       (Strong (Parrow (Strong Univ) (Strong Univ)))])


validseq00 = not $ validSequent  -- False
  (Consequence [Assert (Abs [Pvar "x"] (Var "x")) (Arrow Triv Triv)
                       (Strong (Parrow (Strong Univ) (Strong Univ)))]
               [Falsity])

validseq1 = validSequent  -- True
  (Consequence [Assert (Abs [Pvar "x"] (Var "x")) (Arrow Triv Triv)
                       (Strong (Parrow (Strong Univ) (Strong Univ)))]
               [Assert (Abs [Pvar "x"] (Var "x")) (Arrow Triv Triv)
                       (Strong (Parrow (Strong Univ) (Strong Univ)))])

validseq2 = not $ validSequent -- False
  (Consequence [Assert (Abs [Pvar "x"] (Var "x")) (Arrow Triv Triv)
                       (Strong (Parrow (Strong Univ) (Strong Univ)))]
               [Assert (Abs [Pvar "x"] (Var "x")) (Arrow Triv Triv)
                       (Strong (Parrow Univ (Strong Univ)))])

consterm = mE (App List
                (App Triv
                   (Constr "Cons" [(Lazy,Triv),(Lazy,List)])
                   Trivconst)
                (Constr "Nil" [])) List []
--  prints as "(Cons () Nil)"

validconsequent = validSequent
  (Consequence [Truth]
    [Assert (App List
                (App Triv
                   (Constr "Cons" [(Lazy,Triv),(Lazy,List)])
                   Trivconst)
                (Constr "Nil" [])) List 
            (Strong (ConPred "Cons" [Strong Univ,Strong Univ]))])

strongConsAssert = truthV
    (Assert (App List
                (App Triv
                   (Constr "Cons" [(Lazy,Triv),(Lazy,List)])
                   Trivconst)
                (Constr "Nil" [])) List 
            (Strong (ConPred "Cons" [Strong Univ,Strong Univ])))
    [] []

strongNilAssert = truthV
    (Assert (Constr "Nil" []) List (Strong (ConPred "Nil" []))) [] []

bindNil = bindE (Constr "Nil" []) List []

valid9' = validateRule -- True, as PredVar "Q" is strengthened in antecedent
   (Conclude [Consequence [PropVar "Gamma", Assert (Var "x") Triv Univ]
                          [Assert (Var "e") Triv (Strong (PredVar "Q"))]]
             (Consequence [PropVar "Gamma"]
                          [Assert (Abs [Pvar "x"] (Var "e")) (Arrow Triv Triv)
                              (Parrow Univ (Strong (PredVar "Q")))]))


valid90 = validateRule --fails
   (Conclude [Consequence [PropVar "Gamma", Assert (Var "x") Triv Univ]
                          [Assert (Var "e") Triv (PredVar "Q")]]
             (Consequence [PropVar "Gamma"]
                          [Assert (Abs [Pvar "x"] (Var "e")) (Arrow Triv Triv)
                              (Parrow Univ (PredVar "Q"))]))

valid90_ = validaterule -- fails when PredVar "Q" is bound to UnDef
   (Conclude [Consequence [PropVar "Gamma", Assert (Var "x") Triv Univ]
                          [Assert (Var "e") Triv ( (PredVar "Q"))]]
             (Consequence [PropVar "Gamma"]
                          [Assert (Abs [Pvar "x"] (Var "e")) (Arrow Triv Triv)
                              (Parrow Univ ( (PredVar "Q")))]))
   [("Q",UnDef)]

valid9'__ = validateRule -- True, as Strong Univ is substituted for PredVar "Q"
   (Conclude [Consequence [PropVar "Gamma", Assert (Var "x") Triv Univ]
                          [Assert (Var "e") Triv (Strong Univ)]]
             (Consequence [PropVar "Gamma"]
                          [Assert (Abs [Pvar "x"] (Var "e")) (Arrow Triv Triv)
                              (Parrow Univ (Strong Univ))]))

valid90__ = validateRule -- fails, when UnDef is substituted for PredVar "Q"
   (Conclude [Consequence [PropVar "Gamma", Assert (Var "x") Triv Univ]
                          [Assert (Var "e") Triv UnDef]]
             (Consequence [PropVar "Gamma"]
                          [Assert (Abs [Pvar "x"] (Var "e")) (Arrow Triv Triv)
                              (Parrow Univ UnDef)]))

valid90___ = validateRule -- True
   (Conclude [Consequence [PropVar "Gamma"] [Assert (Var "e") Triv UnDef]]
             (Consequence [PropVar "Gamma"] [Assert (Var "e") Triv UnDef]))

valid90' = validSequent -- True
   (Consequence [Assert (Var "e") Triv UnDef]
                [Assert (Abs [Pvar "x"] (Var "e")) (Arrow Triv Triv)
                              (Parrow Univ UnDef)])

rule9'__ = Conclude [Consequence [PropVar "Gamma", Assert (Var "x") Triv Univ]
                          [Assert (Var "e") Triv (Strong Univ)]]
             (Consequence [PropVar "Gamma"]
                          [Assert (Abs [Pvar "x"] (Var "e")) (Arrow Triv Triv)
                              (Parrow Univ (Strong Univ))])


rule90__ = Conclude [Consequence [PropVar "Gamma", Assert (Var "x") Triv Univ]
                          [Assert (Var "e") Triv UnDef]]
             (Consequence [PropVar "Gamma"]
                          [Assert (Abs [Pvar "x"] (Var "e")) (Arrow Triv Triv)
                              (Parrow Univ UnDef)])

seq9'__ = Consequence [PropVar "Gamma", Assert (Var "x") Triv Univ]
                          [Assert (Var "e") Triv (Strong Univ)]

seq90__ = Consequence [PropVar "Gamma", Assert (Var "x") Triv Univ]
                          [Assert (Var "e") Triv UnDef]

consequent9'__ = Consequence [PropVar "Gamma"]
                          [Assert (Abs [Pvar "x"] (Var "e")) (Arrow Triv Triv)
                              (Parrow Univ (Strong Univ))]

consequent90__ = Consequence [PropVar "Gamma"]
                          [Assert (Abs [Pvar "x"] (Var "e")) (Arrow Triv Triv)
                              (Parrow Univ UnDef)]

envs9'__ = [("e",DV ["x"] Void)]
envs90__ = [("e",DV ["x"] Bottom)]


conclude9'__ = Assert (Abs [Pvar "x"] (Var "e")) (Arrow Triv Triv)
                              (Parrow Univ (Strong Univ))

explicitConc9'__ =  (\e -> truthV conclude9'__ e []) envs9'__   -- True

conclude90__ = Assert (Abs [Pvar "x"] (Var "e")) (Arrow Triv Triv)
                              (Parrow Univ UnDef)

explicitConc90__ =  (\e -> truthV conclude90__ e []) envs90__   -- True

val90__ = mE (Abs [Pvar "x"] (Var "e")) (Arrow Triv Triv)
               [("e",DV ["x"] Bottom)]

sat90__ = satisfies [] (Just val90__) (Parrow Univ UnDef)       -- True

val9'__ = mE (Abs [Pvar "x"] (Var "e")) (Arrow Triv Triv)
               [("e",DV ["x"] Void)]

sat9'__ = satisfies [] (Just val9'__) (Parrow Univ (Strong Univ))   -- True


antecedent9'__ = Consequence [PropVar "Gamma", Assert (Var "x") Triv Univ]
                          [Assert (Var "e") Triv (Strong Univ)]

ctxt9'__ = bindContext "Gamma" antecedent9'__ []
   -- Just [[("e",DV ["x"] Void)],[("e",DV ["x"] Bottom)]]

concV9'__ = map (\env -> truthV conclude9'__ env []) (fromJust ctxt9'__) 
   -- = [True,False]


antecedent90__ = Consequence [PropVar "Gamma", Assert (Var "x") Triv Univ]
                          [Assert (Var "e") Triv UnDef]

ctxt90__ = bindContext "Gamma" antecedent90__ []
   -- Just []

concV90__ = map (\env -> truthV conclude90__ env []) (fromJust ctxt90__)
   -- []

{- the notion of dependent variables only determines which variables in
   must be abstracted in a term of the conclusion, and 
   which (independent) variables must appear in the fringe of the
   pattern in the  abstraction.
   Perhaps the dependent environment, rhoD, should contain the list of
   names of independent variables on which the dependent variable is to
   be abstracted.
-}

-------------------------------------------------------------------------------
antecedents13T =
            [Consequence [PropVar "Gamma"] [Conj [Assert (Var "h") Triv (Strong Univ)
                                         ,Assert (Var "t") List (Strong Univ)]]]

consequent13T = 
            (Consequence [PropVar "Gamma"]
               [Assert (App List
                          (App Triv
                             (Constr "Cons" [(Lazy,Triv),(Lazy,List)])
                             (Var "h"))
                          (Var "t"))
                       List
                       (Strong (ConPred "Cons" [Strong Univ, Univ]))])
conclusions13T =
               [Assert (App List
                          (App Triv
                             (Constr "Cons" [(Lazy,Triv),(Lazy,List)])
                             (Var "h"))
                          (Var "t"))
                       List
                       (Strong (ConPred "Cons" [Strong Univ, Univ]))]

context = fst $ sequentBindings "Gamma" "Delta" [] antecedents13T

resultBndg = unionEnvs $  map (\s -> bindConclusion "Delta" s []) antecedents13T

checkSequent13T = checkSequent consequent13T context resultBndg []

extendedConclusions sequent@(Consequence hypotheses conclusions) = 
                                     regroup conclusions (negating hypotheses)

extendedConclusions13T = 
                   extendedConclusions $ head antecedents13T

-- bindContext' :: Name -> Sequent -> PredEnv -> Maybe [Aenv]
bindContext' context sequent@(Consequence hypotheses conclusions) pe =
    if not $ ismember (PropVar context) hypotheses then Nothing
    else
       let vars = allFreeVars sequent
           envs = allVs vars []
       in Just (map (\e -> or (map (\pr -> truthV pr e pe) 
                           (regroup conclusions (negating hypotheses))))
                       envs)

denvs13T = let vars = map fst (allFreeVarS antecedents13T)
           in  map (\s -> bindContext' "Gamma" s []) antecedents13T

ctxbndgs13T =
   let vars = map fst (allFreeVarS antecedents13T)
   in interEnvs vars $ filterJust $ map (\s -> bindContext "Gamma" s []) 
                                            antecedents13T

ctxbndgs' = bindContext "Gamma"
   (Consequence [PropVar "Gamma"] [Assert (Var "h") Triv (Strong Univ)
                                         ,Assert (Var "t") List (Strong Univ)])
   []

ctxbndgs'' =
                        filter (\rho -> 
                                     or (map (\pr -> truthV pr rho []) 
                                             (regroup conclusions13T [])))
                               (fromJust ctxbndgs')
