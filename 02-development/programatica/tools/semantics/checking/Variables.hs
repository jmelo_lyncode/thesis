module Variables
where
import AST
import MyHaskell

----------------------------------------------------------------------
 -- Predicates
----------------------------------------------------------------------

data Pr
  = Univ                    -- the Universal predicate
  | UnDef                   -- the Undefined predicate
  | ConPred Name [Pr]       -- a pattern predicate
  | Strong Pr               -- a strengthened predicate
  | PredVar Name            -- a predicate variable
  | Parrow Pr Pr            -- an arrow predicate
  | PredDisj Pr Pr          -- a predicate disjunction
  | PredNeg Pr
  | PredScheme Name T Pr -- a predicate scheme 
  | PredAbs (Pr -> Pr)      -- a predicate abstraction
  | LimitUnion Name T Pr -- an infinite union of predicates

instance Show Pr where 
      show Univ                 = "Univ"
      show UnDef                = "UnDef"
      show (Strong pr)          = "$" ++ show pr
      show (ConPred "List" prs) = sPL prs
         where sPL prs = "["++foldr sepByCommas [] (map show prs)++"]"
      show (ConPred n prs)      = rPL prs 
         where rPL prs = n++"("++foldr sepByCommas [] (map show prs)++")"
      show (PredVar n)          = n
      show (Parrow pr pr') = "(" ++ show pr ++ "->" ++ show pr' ++ ")"
      show (PredAbs _) = "PredAbs ()"
      show (PredScheme n t pr) = n ++ "[" ++ show pr ++ "]"
      show (PredDisj pr1 pr2) = "(" ++ show pr1 ++ " || " ++ show pr2 ++ ")"
      show (PredNeg pr) = "(Not " ++ show pr ++ ")"
      show (LimitUnion n t pr) = "Union " ++ n ++ "^i[" ++ show pr ++ "]"

instance Eq Pr where
      (==) Univ Univ = True
      (==) UnDef UnDef = True
      (==) (Strong m) (Strong n) = (==) (weaken m) (weaken n)
      (==) (ConPred n prs1) (ConPred m prs2) = m==n && (and (zipWith (==) prs1 prs2))
      (==) (PredVar n) (PredVar m) = n==m
      (==) (Parrow pr1 pr1') (Parrow pr2 pr2') = (==) pr1 pr2 && (==) pr1' pr2'
      (==) (PredAbs _) _ = False
      (==) (PredScheme n t1 pr1) (PredScheme m t2 pr2) =
           n==m && (==) t1 t2 && (==) pr1 pr2
      (==) (PredDisj pr1 pr2) (PredDisj pr1' pr2') = (==) pr1 pr2 && (==) pr1' pr2'
      (==) (PredNeg pr) (PredNeg pr') = (==) pr pr'
      (==) (LimitUnion n t1 pr1) (LimitUnion m t2 pr2) =
           n==m && (==) t1 t2 && (==) pr1 pr2
      (==) _ _ = False

weaken (Strong pr) = pr
weaken pr = pr

----------------------------------------------------------------------
-- Values
----------------------------------------------------------------------

--data V 
--  = Void             --- scalars
--  | Tagged Name [V]  --- algebraic structured data
--  --  | FV (V -> V)      --- functions
--  --  | TV [V]           --- tuple values 
--  | Bottom
--  deriving Eq

--showV :: V -> String
--showV Void = "()"
--showV (Tagged name vs) = name ++ catWith " " vs
--showV Bottom = "_|_"

catWith :: String -> [V] -> String
catWith s [] = ""
catWith s (v:vs) = showV v ++ s ++ catWith s vs

--instance Show V where
--  show = showV


----------------------------------------------------------------------
-- Propositions, Sequents and Rules
----------------------------------------------------------------------


data Prop = Assert E T Pr | All Name T Prop | Exist Name T Prop 
          | Truth | Falsity | PropVar Name | Not Prop | Equal T E E
          | Conj [Prop] | Disj [Prop] 
     deriving (Eq,Show)

data Sequent = Consequence [Prop] [Prop]

data Rule = Conclude [Sequent] Sequent

type PredEnv = [(Name,Pr)]   -- associates a predicate name with a predicate


----------------------------------------------------------------------
-- Free Variables
----------------------------------------------------------------------

-- argtypes looks up the types of the arguments of a constructor 
argtypes :: (Name,[LS]) -> T -> [T]
argtypes n t = findName (map fst) (fst n) (sig t)  -- ignore the LS field of n

findName :: ([(T,LS)] -> a) -> Name -> [Constr] -> a
findName g n' ((B _):sigs) = findName g n' sigs
findName g n' ((C n ts):sigs) = 
   if n' == n then g ts else findName g n' sigs
findName _ _ [] = undefined

freevars bvs (Var n) t ns = 
   case lookup n bvs of
      Nothing -> case lookup n ns of
                   Nothing -> (n,t):ns
                   _ -> ns
      _ -> ns
freevars bvs (App t1 e1 e2) t ns = 
   freevars bvs e1 (Arrow t1 t) (freevars bvs e2 t1 ns)

freevars bvs (Abs [Pvar n] e) (Arrow t1 t2) ns =
   freevars ((n,t1):bvs) e t2 ns

freevars bvs Trivconst Triv ns = ns

freevars bvs (Constr _ _) _ ns = ns

freeVars :: [(Name,T)] -> E -> T -> [(Name,T)]
freeVars bvs e t = freevars bvs e t []

freeVarS :: [(Name,T)] -> [(E,T)] -> [(Name,T)]
freeVarS bvs = foldr (\(e,t) -> freevars bvs e t) []

propFreeVars :: Prop -> [(Name,T)] -> [(Name,T)]
propFreeVars (Assert e t _) ns = freevars [] e t ns
propFreeVars (All n t p) ns = 
   filter1 n $ propFreeVars p ((n,t):ns)
propFreeVars (Exist n t p) ns = 
   filter1 n $ propFreeVars p ((n,t):ns)
propFreeVars (Not p) ns = propFreeVars p ns
propFreeVars (PropVar n) ns = ns
propFreeVars (Conj props) ns = foldr (\p ns' -> propFreeVars p ns') ns props
propFreeVars Truth ns = ns
propFreeVars Falsity ns = ns

filter1 n [] = []
filter1 n ((m@(n',_)):ns) = if n == n' then ns else m : filter1 n ns

allfreevars :: Sequent -> [(Name,T)] -> [(Name,T)]
allfreevars (Consequence assumptions conclusions) ns =
   let ns' = foldr propFreeVars ns conclusions in
   foldr propFreeVars ns' assumptions

allFreeVars :: Sequent -> [(Name,T)]
allFreeVars s = allfreevars s []

allFreeVarS :: [Sequent] -> [(Name,T)]
allFreeVarS ss = foldr allfreevars [] ss

filterJust :: [Maybe a] -> [a]
filterJust [] = []
filterJust (Just x:rest) = x:filterJust rest
filterJust (Nothing:rest) = filterJust rest

occurs_in :: Name -> E -> Bool
occurs_in n (Var n')          = n == n'
occurs_in n (App _ e1 e2)     = occurs_in n e1 || occurs_in n e2
occurs_in _ _                 = False

occursInProp :: (Name,T) -> Prop -> Bool
occursInProp (n,t) (Assert e _ _)       = occurs_in n e
occursInProp nt (Not pr)                = occursInProp nt pr
occursInProp nt@(n,_) (All n' _ pr)     = n /= n' && occursInProp nt pr
occursInProp nt@(n,_) (Exist n' _ pr)   = n /= n' && occursInProp nt pr
occursInProp _ _                        = False

hasPropVar :: [Prop] -> Bool
hasPropVar []              = False
hasPropVar (PropVar _ : _) = True
hasPropVar (_ : rest)      = hasPropVar rest
