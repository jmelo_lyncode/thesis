module Valid 
where

import List
import Maybe
import AST
import MyHaskell
import Variables

-- We need a representation for a property (P->Q) that is like traces,
-- consisting of args followed by a function value [a, FV f].
-- These traces represent pairs (a, f a) where a<-P and f a<- Q.  The
-- set of traces is the evidence for a function-typed term to satisfy (P->Q).
-- To check that a function value FV g has such a property, check it 
-- by running it against each of the first elements of traces.  It must yield
-- an identical result as does one of the trace fuctions on that argument.

--  This representation corresponds to that of
-- dependent expressions, where the expression's meaning depends on a
-- value from the context.  We can do such a property coercion when
-- the expression is an explicit abstraction.  Then the bindings of
-- abstracted variables can be "forgotten" in favor of the trace-element
-- notation.  A set of trace elements must be accepted as a property
-- characterization, bound to a name in lieu of a function value.

-- We use tuple values (constructor TV) to represent traces.

---------------------------------------------------------------------
-- Satisfaction of a predicate
---------------------------------------------------------------------

containsvars :: Prop -> [Name] -> Bool
containsvars (Assert e _ _) ns = eContainsvars e ns
containsvars (Not prop) ns = containsvars prop ns
containsvars (All n _ prop) ns = containsvars prop (n:ns)
containsvars (Exist n _ prop) ns = containsvars prop (n:ns)
containsvars _ _ = False

eContainsvars :: E -> [Name] -> Bool
eContainsvars (Var n) ns = n `notElem` ns
eContainsvars (ConApp _ args) ns =
   or $ map (\e -> eContainsvars e ns) args
eContainsvars _ _ = False

satisfies ::  PredEnv -> Maybe V -> Pr -> Bool

satisfies _ Nothing _ = False
satisfies pe (Just (Tagged s vs)) (ConPred s' ps) =
   if s == s' then and (zipWith (satisfies pe) (map Just vs) ps)
   else False
satisfies pe (Just (FT traces)) (Parrow p1 p2) =
                 and (map (\(v,r) ->   
                             not (satisfies pe (Just v) p1) ||
                             satisfies pe (Just r) p2) traces)
satisfies pe v (Strong p) = 
   case v of
      Just Bottom -> False
      _ -> satisfies pe v p
satisfies pe v (PredVar n) =
   case lookup n pe of
      Just p -> satisfies pe v p
      Nothing -> error ("unbound PredVar " ++ n)
satisfies pe v (PredDisj pr1 pr2) = satisfies pe v pr1 || satisfies pe v pr2
satisfies _ (Just Bottom) _ = True
satisfies _ _ Univ = True
satisfies _ _ (PredAbs _) = 
                  error "satisfies: called with a predicate abstraction"
satisfies _ _ _ = False

tests = and
  [satisfies []  (Just Bottom) UnDef
  ,satisfies [] (Just Bottom) (ConPred "Cons" [UnDef,UnDef])
  ,not $ satisfies [] (Just (Tagged "Nil" [])) UnDef
  ,not $ satisfies [] (Just Bottom) (Strong Univ)
  ,satisfies [] (Just (Tagged "Cons" [Bottom,Bottom]))
             (Strong (ConPred "Cons" [Univ,UnDef]))
  ,not $ satisfies [] (Just (Tagged "Nil" []))
             (ConPred "Cons" [Univ,UnDef])
  ,not $ satisfies [] (Just (Tagged "Cons" [Void,Tagged "Cons" [Bottom,Bottom]]))
             (Strong (ConPred "Cons" [Strong Univ,UnDef]))
  ,satisfies [] (Just (Tagged "Cons" [Void,Tagged "Cons" [Bottom,Bottom]]))
             (Strong (ConPred "Cons" [Strong Univ,ConPred "Cons" [Univ,UnDef]]))
  ]


---------------------------------------------------------------------
-- Assertions, Sequents and Rules
---------------------------------------------------------------------

-- allV calculates all valuations of a variable of name Name at type Type
-- and adds each to the current binding environment, creating a list of
-- such environments.

type Aenv = [(Name,V)]

type Eenv = Aenv -> Bool

allV :: (Name,Type) -> Aenv -> [Aenv]
allV (s,t) rho = 
   case lookup s rho of
      Nothing -> bindVals s t rho
      _       -> [rho]   -- s is already bound in rho

allVs :: [(Name,Type)] -> Aenv -> [Aenv]
allVs nts rho = foldr' allV rho nts

-- allDepV calculates all valuations of a dependent variable; that is,
-- a variable occurring on the r.h.s. of an asserted antecedent that
-- depends on bindings given to variables asserted in the l.h.s. 

allDepV :: Type ->(Name,Type) -> Aenv -> [Aenv]
allDepV t1 (s,t2) rhoD =
          case lookup s rhoD of
             Just _  -> [rhoD]  -- s is already bound as a dependent variable
             Nothing -> case t2 of
                           Arrow _ t2' -> 
                                map (\r ->(s,r):rhoD) (mkFTSpace t1 t2')
                           _ -> map (\r ->(s,r):rhoD) (mkFTSpace t1 t2)
                                -- do we need this line? (the default above)

allDepVals :: [(Name,Type)] -> [(Name,Type)] -> [Aenv]
allDepVals nt1s nt2s =
   foldr' (\nt2 rho ->
      foldr' (\(_,t1) -> allDepV t1 nt2) rho nt1s)
      [] nt2s

allEnvs :: [Aenv] -> [Aenv] -> [Aenv]
allEnvs [] envs = envs
allEnvs (rho:rho') envs = allEnvs rho' (map (\e -> rho ++ e) envs)

foldr' :: (a -> b -> [b]) -> b -> [a] -> [b]
foldr' f e [] = [e]
foldr' f e (x:xs) = concat (map (f x) (foldr' f e xs))


map2 :: (a -> b -> c) -> [a] -> [b] -> [c]
map2 f [] _ = []
map2 f (x:xs) (y:ys) = f x y : map2 f xs ys

bindV :: Name -> Constr -> Aenv -> Aenv
bindV s (B v) rho = (s,v):rho
bindV s (C n ts) rho = (s,Tagged n (map (const Bottom) ts)):rho

bindVals :: Name -> Type -> Aenv -> [Aenv]
bindVals s t rho =
   case t of
      Arrow t1 t2 -> ((s,Bottom):rho):
                     map (\f -> (s,f):rho) (mkFunctionSpace t1 t2)
--                     map (\f -> (s,f):rho) (mkFTSpace t1 t2)
      _ -> map (\c -> bindV s c rho) (sig t)

test1 :: [Aenv]
test1 = allV ("foo",List) []
test1' = allV ("foo",List) [("foo",Bottom)]
test2 = allV ("bar",Triv) []
test3 = map (allV ("bar",Triv)) test1

value :: Constr -> V
value (B v) = v
value (C n ts) = Tagged n (map (const Bottom) ts)

values :: Type -> [V]
values (Arrow t1 t2) = mkFTSpace t1 t2
values t = map value (sig t)

mkEnv :: Aenv -> Name -> V
mkEnv rho n = case lookup n rho of
                 Just v -> v
                 Nothing -> error ("No binding for " ++ n ++ " in env")

---------------------------------------------------------------------
-- Truth of assertions
---------------------------------------------------------------------

hasType Void (B Void:_) = True
hasType v@(Tagged n _) sigs = 
   case sigs of
      [] -> False
      (B _):constrs -> hasType v constrs
      (C n' _):constrs -> if n == n' then True
                          else hasType v constrs
hasType (FV _) (A _ _ : _) = True
hasType (FT trace) c = and $ map (\(_,v) -> hasType v c) trace
hasType Bottom _ = True
hasType _ _ = False

bindE :: E -> Type -> Aenv -> Maybe V
bindE (Var n) t rho = 
  case lookup n rho of
     Nothing -> Just Bottom
     u@(Just v)  -> if hasType v (sig t) then u
                    else  error ("type error found in bindE " ++ show v)
bindE ca@(ConApp _ _) t rho =
 Just $ mE ca (\n -> 
    case lookup n rho of
       Just v  -> v
       Nothing -> error ("lookup " ++ n ++ " fails to find a value"))
bindE Trivconst t _ = if t == Triv then Just Void
                      else error ("type error found in bindE: () : " ++ show t)
bindE _ _ _       = Just Bottom

truthVal :: Prop -> Aenv -> PredEnv -> Bool
truthVal assertion@(Assert e t pr) rho pe = 
   let nts = freeVars [] e t
       rhos = allVs nts rho in
   and (map (\e -> truthV assertion e pe) rhos)
truthVal (All n t pr) rho pe =
   let rho' = filter ((/= n) . fst) rho in
   let rhos = allV (n,t) rho' in
   and (map (\e -> truthVal pr e pe) rhos)
truthVal (Exist n t pr) rho pe =
   let rho' = filter ((/= n) . fst) rho in
   let rhos = allV (n,t) rho' in
   or (map (\e -> truthVal pr e pe) rhos)
truthVal (Not pr) rho pe = not (truthVal pr rho pe)
truthVal pr rho pe = truthV pr rho pe


-- truthV does not calculate variant rhos for an assertion
-- Since truthV and truthVal are mutually defined, the cases must
-- cover all Prop constructions or the functions may diverge

truthV :: Prop -> Aenv -> PredEnv -> Bool
truthV (Assert e t pr) rho pe = 
    case pr of
      PredVar n -> case lookup n pe of
            Just p -> truthV (Assert e t p) rho pe
            Nothing -> error ("unbound PredVar " ++ n)
      PredScheme n _ _ -> case lookup n pe of
            Just p -> truthV (Assert e t p) rho pe
            Nothing -> error ("unbound PredVar " ++ n)
      Strong pr' -> case bindE e t rho of
                       Just Bottom -> False
                       Just v -> 
                          truthV (Assert e t pr') rho pe
                       Nothing -> False
      Parrow predP predQ ->
         case e of
            Abs [Pvar n] expr ->
               satisfies pe (Just (mE expr (mkEnv rho))) pr
                                         
            _ -> satisfies pe (Just (mE e (\n -> 
                                             case (lookup n rho) of
                                                Just v  -> v
                                                Nothing -> 
                                                   error "no binding in rho")))
                           pr

      PredDisj pr1 pr2 -> truthV (Assert e t pr1) rho pe 
                       || truthV (Assert e t pr2) rho pe

      PredAbs _ -> error "truthV cannot evaluate a predicate abstraction"

      _ -> satisfies pe (if eContainsvars e (map fst rho) -- e contains
                         then Nothing                     -- unbound variables
                         else bindE e t rho) pr 
truthV (Not pr) rho pe = not (truthV pr rho pe)
truthV Truth _ _ = True
truthV Falsity _ _ = False
truthV pr rho pe = truthVal pr rho pe


testAssertion =
    not $ truthVal (Assert (ConApp ("Cons",[Lazy,Lazy]) 
                                   [Var "x", ConApp ("Nil",[]) []]) 
                          List
                          (ConPred "Cons" [UnDef,Univ])) [] []

testAssertions = and 
   [not $ truthVal (Assert (ConApp ("Cons",[Lazy,Lazy]) 
                                   [Var "x", ConApp ("Nil",[]) []]) 
                          List
                          (ConPred "Cons" [UnDef,Univ])) [] []
   ,truthVal (Assert (ConApp ("Cons",[Lazy,Lazy]) 
                             [Var "x", ConApp ("Nil",[]) []]) 
                     List
                     (ConPred "Cons" [Univ,Univ])) [] []
   ,not $ truthVal (Assert (Var "y") Triv UnDef) [("y",Void)] []
   ,truthVal (Assert (Var "y") Triv UnDef) [("y",Bottom)] []
   ,truthVal (Assert (Var "y") Triv (PredVar "P")) [("y",Bottom)] [("P",UnDef)]
   ,not $ truthVal (Assert (Var "y") Triv UnDef) [] []
   ,not $ truthVal (All "y" Triv (Assert (Var "y") Triv UnDef)) [] []
   ,truthVal (Exist "y" Triv (Assert (Var "y") Triv UnDef)) [] []
   ]

---------------------------------------------------------------------
-- Validity of a sequent
---------------------------------------------------------------------

validSequent sequent@(Consequence assumptions conclusions) =
   let vars = allFreeVars sequent
       rhos = allVs vars [] in
   and
    (map (\e -> if and (map (\pr -> truthV pr e []) assumptions) then
                   or (map (\pr -> truthV pr e []) conclusions)
                else True) rhos)


-- test validSequent, including some negative examples

testsequents = and
   [validSequent (Consequence [Falsity] [Falsity])
   ,validSequent (Consequence [Truth] [Truth])
   ,not $ validSequent (Consequence [] [Assert (Var "x") Triv (Strong Univ)])
   ,validSequent (Consequence
      [Assert (Var "x") List (ConPred "Nil" [])]
      [Assert (Var "x") List (ConPred "Nil" [])])
   ,not $ validSequent (Consequence
      [Assert (Var "x") List (ConPred "Nil" [])]
      [Assert (Var "x") List (Strong (ConPred "Nil" []))])
   ,validSequent (Consequence
      [Assert (Var "x") List (ConPred "Nil" []),Assert (Var "y") Triv Univ]
      [Assert (ConApp ("Cons",[Lazy,Lazy]) [(Var "y"),(Var "x")]) List 
              (ConPred "Cons" [Univ,ConPred "Nil" []])])
   ,not $ validSequent (Consequence
      [Assert (Var "x") List (ConPred "Nil" []),Assert (Var "y") Triv Univ]
      [Assert (ConApp ("Cons",[Lazy,Lazy]) [(Var "y"),(Var "x")]) List 
              (ConPred "Cons" [Strong Univ,ConPred "Nil" []])])
   ,validSequent (Consequence
      [Assert (ConApp ("Cons",[Lazy,Lazy]) [(Var "y"),(Var "x")]) List 
              (ConPred "Cons" [Univ,ConPred "Nil" []])]
              [Assert (Var "x") List (ConPred "Nil" [])])
   ,validSequent (Consequence
      [Assert (ConApp ("Cons",[Lazy,Lazy]) [(Var "y"),(Var "x")]) List 
              (ConPred "Cons" [Univ,ConPred "Nil" []])]
              [Assert (Var "y") Triv Univ])
   ]

-- Calculate a predicate constructor from a data constructor application

isSatisfiable (Strong UnDef) = False
isSatisfiable _ = True

sigma :: E -> [Pr] -> Pr
sigma (ConApp (n,ls) _) prs =
   if length ls /= length prs then Strong UnDef
   else let prls = zip prs ls
        in if or $ map (\(pr,l) -> isStrong pr || l /= Lazy) prls
           then let prs' = map (\(pr,l) -> 
                                    if isStrong pr || l==Lazy then pr
                                    else (Strong pr)) prls
                in Strong (ConPred n prs')
           else ConPred n prs       -- because all prs are lazy
   where isStrong (Strong _) = True
         isStrong _ = False
sigma e _ = error (show e ++ "is not a valid argument for sigma")


-----------------------------------------------------------------
-- Modeling a function space of monotonic functions
-----------------------------------------------------------------

constTraces :: [V] -> [V] -> [[(V,V)]]
constTraces aVs bVs = map (\b -> (map (\a -> (a,b)) aVs))
                          (filter (/= Bottom) bVs)

ex :: [V] -> V -> [(V,V)] -> [[(V,V)]]
ex bVs a tr = map (\b -> (a,b):tr) bVs

eX :: [V] -> V -> [[(V,V)]] -> [[(V,V)]]
eX bVs a = concat . map (ex bVs a)

strictTraces :: [V] -> [V] -> [[(V,V)]]
strictTraces aVs bVs = map ((Bottom,Bottom):) $
                       foldr (eX bVs) [[]] (filter (/= Bottom) aVs)

mkFunctionSpace :: Type -> Type -> [V]
mkFunctionSpace t1 t2 =
   let v1s = values t1
       v2s = values t2
       traces = (constTraces v1s v2s) ++ (strictTraces v1s v2s)
   in map (\trace -> FV (\v -> fromJust (lookup v trace))) traces

mkFTSpace :: Type -> Type -> [V]
mkFTSpace t1 t2 =
   let v1s = values t1
       v2s = values t2
   in map FT (constTraces v1s v2s ++ strictTraces v1s v2s)
   
eqlength [] = True
eqlength ss = eqvals (map length ss)
   where eqvals [_] = True
         eqvals (l1:ls@(l2:_)) = l1 == l2 && eqvals ls
