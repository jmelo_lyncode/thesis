module Tests
where
import List
import AST
import MyHaskell
import Variables
import Valid
import Rules

sequentbindings = sequentBindings "Gamma" "Delta" []

tests1 = sequentbindings
               [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "x") List (Strong Univ)])
               ,(Consequence [PropVar "Gamma"] [Assert (Var "y") List UnDef])] 

tests2 = sequentbindings
               [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "x") Triv (Strong Univ)])
               ,(Consequence [PropVar "Gamma"] [Assert (Var "y") List UnDef]
               )] 

tests3 = sequentbindings
               [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "x") Triv (Strong Univ)])
               ,(Consequence [PropVar "Gamma"] 
                             [Assert (Var "y") List (Strong (ConPred "Nil" []))])]

testvalidaterule_ =
   not $ validaterule            -- gives a match failure: type of "x" is mixed
     (Conclude [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "x") List (Strong Univ)])
               ,(Consequence [PropVar "Gamma"] [Assert (Var "y") List UnDef])] 
               (Consequence [PropVar "Gamma"] 
                            [Assert (Var "x") Triv (Strong Univ)
                            ,Assert (Var "y") List UnDef]))
     []

testvalidaterule = and
  [validaterule
     (Conclude [(Consequence [PropVar "Gamma"] [Assert (Var "x") Triv UnDef])
               ,(Consequence [PropVar "Gamma"] [Assert (Var "y") List (Strong Univ)]
               )] (Consequence [PropVar "Gamma"]
                      [Assert (ConApp ("Cons",[Lazy,Lazy]) [Var "x", Var "y"])
                         List (Strong (ConPred "Cons" [UnDef,Strong Univ]))]))
     []
  ,not $ validaterule 
     (Conclude [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "x") List (Strong Univ)])
               ,(Consequence [PropVar "Gamma"] [Assert (Var "y") List UnDef]
               )] 
                  (Consequence [Assert (Var "x") List UnDef]
                               [Assert (Var "y") List (Strong Univ)]))
     []

  ,not $ validaterule 
     (Conclude [(Consequence [PropVar "Gamma"] [Assert (Var "y") List UnDef]
               )] 
                  (Consequence [PropVar "Gamma"] 
                               [Assert (Var "y") List (Strong Univ)]))
     []
  ,not $ validaterule 
     (Conclude [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "x") Triv (Strong Univ)])
               ,(Consequence [PropVar "Gamma"] [Assert (Var "y") List UnDef]
               )] 
                  (Consequence [PropVar "Gamma"] 
                               [Assert (Var "y") List (Strong Univ)]))
     []
  ,validaterule 
     (Conclude [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "x") Triv (Strong Univ)])
               ,(Consequence [PropVar "Gamma"] [Assert (Var "y") List UnDef]
               )] 
                  (Consequence [PropVar "Gamma"] 
                               [Assert (Var "x") Triv Univ]))
     []

  ,validaterule 
     (Conclude [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "x") Triv (Strong Univ)])
               ,(Consequence [PropVar "Gamma"] 
                             [Assert (Var "y") List (Strong Univ)])]
                  (Consequence [PropVar "Gamma"] 
                               [Assert (Var "x") Triv Univ
                               ,Assert (Var "y") List Univ]))
     []

  ,not $ validaterule 
     (Conclude [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "x") Triv (Strong Univ)])
               ,(Consequence [PropVar "Gamma"] 
                             [Assert (Var "y") List (Strong (ConPred "Nil" []))])]
                  (Consequence [PropVar "Gamma"] 
                               [Assert (Var "y") List UnDef]))
     []

  ,not $ validaterule 
     (Conclude [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "x") Triv Univ])
               ,(Consequence [PropVar "Gamma"] 
                             [Assert (Var "y") List UnDef])]
                  (Consequence [PropVar "Gamma"] 
                               [Assert (Var "x") Triv (Strong Univ)
                               ,Assert (Var "y") List (Strong (ConPred "Nil" []))]))
     []

  ,not $ validaterule 
     (Conclude [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "y") Triv Univ])]
                  (Consequence [PropVar "Gamma"] 
                               [Assert (Var "y") Triv (Strong Univ)]))
     []

  ,not $ validaterule 
     (Conclude [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "y") Triv UnDef])]
                  (Consequence [PropVar "Gamma"] 
                               [Assert (Var "y") Triv (Strong Univ)]))
     []

  ,validaterule 
     (Conclude [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "y") Triv (Strong Univ)])]
                  (Consequence [PropVar "Gamma"] 
                               [Assert (Var "y") Triv Univ]))
     []
  ]


testU = and
        [validateSequent
                  (Consequence [Truth] [Assert (ConApp ("Nil",[]) []) List Univ])
                  [] [] []
        ,validateSequent
                  (Consequence [PropVar "Gamma"] 
                       [Assert (ConApp ("Nil",[]) []) List Univ])
                  [] [] []
        ,validateSequent
                  (Consequence [PropVar "Gamma"] 
                       [Assert (Var "x") List (Strong Univ)])
                  [[("x",Tagged "Nil" [])]] [] []
        ,not $ validateSequent
                  (Consequence [PropVar "Gamma"] 
                       [Assert (Var "x") List (Strong Univ)])
                  [[("x",Bottom)]] [] []
        ,not $ validateSequent
                  (Consequence [PropVar "Gamma"] 
                       [Assert (Var "x") List (Strong UnDef)])
                  [[("x",Tagged "Nil" [])]] [] []
        ,not $ validateSequent
                  (Consequence [PropVar "Gamma"] 
                       [Assert (Var "x") List UnDef]) 
                  [[("x",Tagged "Nil" [])]] [] []
        ,not $ validateSequent
                  (Consequence [PropVar "Gamma"] 
                       [Assert (ConApp ("Nil",[]) []) List UnDef])
                  [[("x",Tagged "Nil" [])]] [] []
        ]

testV = and
        [checkSequent 
                  (Consequence [Assert (Var "x") Triv (Strong Univ)]
                               [Assert (Var "x") Triv (Strong Univ)])
                  [[]] emptyBndg []

        ,not $ checkSequent 
                  (Consequence [Assert (Var "x") Triv Univ]
                               [Assert (Var "x") Triv (Strong Univ)])
                  [[]] emptyBndg []

        ,not $ checkSequent 
                  (Consequence [Assert (Var "x") Triv (Strong Univ)]
                               [Assert (Var "x") Triv (PredVar "P")])
                  [[]] emptyBndg [("P",UnDef)]

        ,checkSequent 
                  (Consequence [Assert (Var "x") Triv (Strong Univ)]
                               [Assert (Var "x") Triv (PredVar "P")])
                  [[]] emptyBndg [("P",Strong Univ)]
        ]

bindSequent sequent envs concs pe =
   let (Consequence hypotheses conclusions) = sequent in
   let envs' = 
            foldr (++) [] $
                  map (\e -> bindcontext "Gamma" sequent e pe) envs 
   in envs'

testbindSequent = and
                  [bindSequent 
                   (Consequence [Assert (Var "x") Triv (Strong Univ)]
                                [Assert (Var "x") Triv (Strong Univ)]) 
                   [[]] [] [] 
                   == []
                  ,bindSequent 
                     (Consequence [PropVar "Gamma"] 
                          [Assert (Var "x") List UnDef])
                     [[("x",Tagged "Nil" [])]] [] [] 
                   == [[("x",Tagged "Nil" [])]]

                  ,bindSequent 
                     (Consequence [PropVar "Gamma"] 
                       [Assert (Var "x") List UnDef]) 
                       [[]] [] []
                   == [[("x",Bottom)]]

                  ,bindSequent 
                     (Consequence [PropVar "Gamma"] 
                       [Assert (Var "x") List Univ]) 
                       [[]] [] []
                   == [[("x",Tagged "Cons" [Bottom,Bottom])],
                       [("x",Tagged "Nil" [])],[("x",Bottom)]]
                  ]


checkSequent' sequent envs concs pe =
   let (Consequence hypotheses conclusions) = sequent in
   let envs' = foldr (++) [] $
                  map (\e -> explicitcontext hypotheses e pe) envs in
   envs'

tv2 = bindSequent
       (Consequence [PropVar "Gamma"]
                    [Assert (Var "x") List UnDef,Assert (Var "y") Triv UnDef])
       [[]] [[]] []

tv = bindSequent 
       (Consequence [PropVar "Gamma"]
                    [Assert (Var "x") List UnDef])
       [[("x",Tagged "Nil" [])]] [] []

tv' = bindSequent 
       (Consequence [PropVar "Gamma"]
                    [Assert (Var "x") List (Strong Univ)])
       [[("x",Tagged "Nil" [])]] [] []

tv'' = bindSequent 
       (Consequence [PropVar "Gamma"]
                    [Assert (Var "x") List (Strong UnDef)])
       [[]] [[]] []

tv''' = bindSequent 
       (Consequence [PropVar "Gamma"]
                    [Assert Trivconst Triv (Strong Univ)])
       [[]] [] []

cv = checkSequent' 
       (Consequence [PropVar "Gamma"]
                    [Assert (Var "x") List UnDef])
       [[("x",Tagged "Nil" [])]] [] []

cv' = checkSequent' 
       (Consequence [PropVar "Gamma"]
                    [Assert (Var "x") List (Strong Univ)])
       [[("x",Tagged "Nil" [])]] [] []

cv'' = checkSequent' 
        (Consequence [PropVar "Gamma"]
                     [Assert (Var "x") List (Strong Univ)])
        [[("x",Tagged "Cons" [Bottom,Bottom])],
         [("x",Tagged "Nil" [])],[("x",Bottom)]] [] []

cV = not $ checkSequent 
       (Consequence [PropVar "Gamma"]
                    [Assert (Var "x") List UnDef])
       [[("x",Tagged "Nil" [])]] emptyBndg []

cV' = checkSequent 
       (Consequence [PropVar "Gamma"]
                    [Assert (Var "x") List (Strong Univ)])
       [[("x",Tagged "Nil" [])]] emptyBndg []

cV'' = not $ checkSequent 
        (Consequence [PropVar "Gamma"]
                     [Assert (Var "x") List (Strong Univ)])
        [[("x",Tagged "Cons" [Bottom,Bottom])],
         [("x",Tagged "Nil" [])],[("x",Bottom)]] emptyBndg []


testvalidateRule = and
  [validateRule $
      Conclude [(Consequence [PropVar "Gamma"] [Assert (Var "x") Triv UnDef])
               ,(Consequence [PropVar "Gamma"] [Assert (Var "y") List (Strong UnDef)]
               )] (Consequence [PropVar "Gamma"]
                      [Assert (ConApp ("Cons",[Lazy,Lazy]) [Var "x", Var "y"])
                         List (Strong (ConPred "Cons" [UnDef,Strong UnDef]))])
  ,validateRule $
      Conclude [(Consequence [PropVar "Gamma"] [Assert (Var "x") Triv UnDef])
               ,(Consequence [PropVar "Gamma"] [Assert (Var "y") List (Strong Univ)]
               )] (Consequence [PropVar "Gamma"]
                      [Assert (ConApp ("Cons",[Lazy,Lazy]) [Var "x", Var "y"])
                         List (Strong (ConPred "Cons" [UnDef,Strong Univ]))])
  ,not $ validateRule $
      Conclude [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "x") List (Strong Univ)])
               ,(Consequence [PropVar "Gamma"] [Assert (Var "y") List UnDef]
               )] 
                  (Consequence [PropVar "Gamma"]  
                               [Assert (Var "x") List UnDef
                               ,Assert (Var "y") Triv (Strong Univ)])

  ,validateRule $
      Conclude [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "x") List (Strong UnDef)])
               ,(Consequence [PropVar "Gamma"] [Assert (Var "y") List Univ]
               )] 
                 (Consequence [PropVar "Gamma"]
                     [Assert (ConApp ("Cons",[Lazy,Lazy]) [Var "x", Var "y"])
                        List (Strong (ConPred "Cons" [Univ,
                                                      Strong Univ]))])
-- Notice that one of the hypotheses of the above rule is unsatisfiable,
-- so the rule itself should be valid  (BUT FAILS IN TEST)

  ,not $ validateRule $
      Conclude [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "x") List (Strong (PredVar "P"))])
               ,(Consequence [PropVar "Gamma"] 
                             [Assert (Var "y") List (PredVar "Q")]
               )] 
                 (Consequence [PropVar "Gamma"]
                     [Assert (ConApp ("Cons",[Lazy,Lazy]) [Var "x", Var "y"])
                        List (Strong (ConPred "Cons" [PredVar "P",
                                                      Strong (PredVar "Q")]))])
  ,validateRule $
      Conclude [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "x") Triv (Strong (PredVar "P"))])
               ,(Consequence [PropVar "Gamma"] 
                             [Assert (Var "y") List (PredVar "Q")]
               )] 
                 (Consequence [PropVar "Gamma"]
                     [Assert (ConApp ("Cons",[Lazy,Lazy]) [Var "x", Var "y"])
                        List (Strong (ConPred "Cons" [(PredVar "P"),
                                                      PredVar "Q"]))])
  ,validateRule $
      Conclude [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "x") Triv (Strong (PredVar "P"))])
               ,(Consequence [PropVar "Gamma"] 
                             [Assert (Var "y") List (PredVar "Q")]
               )] 
                 (Consequence [PropVar "Gamma"]
                     [Assert (ConApp ("Cons",[Lazy,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Lazy,Lazy]) [Var "x", Var "y"])
                               [PredVar "P",PredVar "Q"])])
  ]

prsl = sigma (ConApp ("Con1",[Strict,Lazy]) [Trivconst,Trivconst])
                               [PredVar "P",PredVar "Q"]

testvalidateRules = and
 [not $ validateRule $
      Conclude [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "x") Triv (PredVar "P")])
               ,(Consequence [PropVar "Gamma"] [Assert (Var "y") List (PredVar "Q")]
               )] 
                 (Consequence [PropVar "Gamma"]
                     [Assert (ConApp ("Cons",[Strict,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Strict,Lazy]) [Trivconst,Trivconst])
                               [PredVar "P",PredVar "Q"])])
-- should be False as the consequent assertion is strong.
-- The predicate asserted in the consequent is $Cons($P,Q)

 ,validateRule $
      Conclude [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "x") Triv (PredVar "P")])
               ,(Consequence [PropVar "Gamma"] [Assert (Var "y") List (PredVar "Q")]
               )] 
                 (Consequence [PropVar "Gamma"]
                     [Assert (ConApp ("Cons",[Strict,Lazy]) [Var "x", Var "y"])
                        List (sigma
                                (ConApp ("Cons",[Lazy,Lazy]) [Trivconst,Trivconst])
                               [PredVar "P",PredVar "Q"])])

-- the predicate asserted in the consequent is  $Cons(P,Q)
-- which isn't satisfied by the strict contruction when "x" is bottom

 ,validateRule $
      Conclude [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "x") Triv (Strong (PredVar "P"))])
               ,(Consequence [PropVar "Gamma"] [Assert (Var "y") List (PredVar "Q")]
               )] 
                 (Consequence [PropVar "Gamma"]
                     [Assert (ConApp ("Cons",[Strict,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Strict,Lazy]) [Trivconst,Trivconst])
                               [Strong (PredVar "P"),PredVar "Q"])])

-- the predicate asserted in the consequent is  $Cons($P,Q).
 ]

pvs = allPreds (allPredVars 
                 (Consequence [PropVar "Gamma"]
                     [Assert (ConApp ("Cons",[Strict,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Strict,Lazy]) [Trivconst,Trivconst])
                               [Strong (PredVar "P"),PredVar "Q"])])
               )

bindings = map
   (\pe -> sequentBindings "Gamma" "Delta" pe 
             [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "x") Triv (Strong (PredVar "P"))])
               ,(Consequence [PropVar "Gamma"] [Assert (Var "y") List (PredVar "Q")]
               )]) pvs 
chkStrict_ = -- with two strict arguments, cannot conclude which is Bottom
       not $ checkSequent (Consequence
                     [Assert (ConApp ("Cons",[Strict,Strict]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Strict,Strict]) 
                                       [Trivconst,Trivconst])
                               [PredVar "P", PredVar "Q"])]
                     [Assert (Var "x") Triv UnDef])
       [] emptyBndg [("P",UnDef),("Q",UnDef)]


chkStrict = and
      [checkSequent (Consequence [PropVar "Gamma"] 
                     [Assert (ConApp ("Cons",[Strict,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Strict,Lazy]) 
                                       [Trivconst,Trivconst])
                               [ (PredVar "P"),PredVar "Q"])])
       [[("x",Bottom),("y",Bottom)]]
       emptyBndg [("P",UnDef),("Q",UnDef)]

      ,not $ checkSequent (Consequence [PropVar "Gamma"] 
                     [Assert (ConApp ("Cons",[Strict,Lazy]) [Var "x", Var "y"])
                        List (Strong
                              (sigma
                               (ConApp ("Cons",[Strict,Lazy]) 
                                       [Trivconst,Trivconst])
                               [ (PredVar "P"),PredVar "Q"]))])
       [[("x",Bottom),("y",Bottom)]]
       emptyBndg [("P",UnDef),("Q",UnDef)]

      ,checkSequent (Consequence
                     [Assert (ConApp ("Cons",[Strict,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Strict,Lazy]) 
                                       [Trivconst,Trivconst])
                               [PredVar "P", PredVar "Q"])]
                     [Assert (Var "x") Triv UnDef])
       [] emptyBndg [("P",UnDef),("Q",UnDef)]

      ,checkSequent (Consequence
                     [Assert (Var "x") Triv UnDef, Assert (Var "y") List UnDef]
                     [Assert (ConApp ("Cons",[Strict,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Strict,Lazy]) 
                                       [Trivconst,Trivconst])
                               [ (PredVar "P"),PredVar "Q"])])
       [] -- [[("x",Bottom),("y",Bottom)]] 
       emptyBndg [("P",UnDef),("Q",UnDef)]

      ,checkSequent (Consequence
                     [Assert (Var "x") Triv UnDef, Assert (Var "y") List UnDef]
                     [Assert (ConApp ("Cons",[Strict,Lazy]) [Undefined, Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Lazy,Lazy]) 
                                       [Trivconst,Trivconst])
                               [ (PredVar "P"),PredVar "Q"])])
       [] --[[("x",Bottom),("y",Bottom)]] 
       emptyBndg [("P",UnDef),("Q",UnDef)]

      ,checkSequent (Consequence
                     [Truth]
                     [Assert Undefined
                        List (sigma
                               (ConApp ("Cons",[Lazy,Lazy]) 
                                       [Trivconst,Trivconst])
                               [UnDef,UnDef])])
                    [] emptyBndg []

      ,checkSequent (Consequence
                     [Truth]
                     [Assert Undefined
                        List (sigma
                               (ConApp ("Cons",[Strict,Lazy]) 
                                       [Trivconst,Trivconst])
                               [UnDef,UnDef])])
                    [] emptyBndg []

      ,checkSequent (Consequence
                     [PropVar "Gamma"]
                     [Assert (ConApp ("Cons",[Strict,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Strict,Lazy]) 
                                       [Trivconst,Trivconst])
                               [ (PredVar "P"),PredVar "Q"])])
       [[("x",Bottom),("y",Bottom)]] emptyBndg [("P",UnDef),("Q",UnDef)]
      ]

pru = sigma (ConApp ("Cons",[Lazy,Lazy]) [Trivconst,Trivconst]) [UnDef,UnDef]
prv = sigma (ConApp ("Cons",[Strict,Lazy]) [Trivconst,Trivconst]) [UnDef,UnDef]
prw = sigma (ConApp ("Cons",[Strict,Lazy]) [Trivconst,Trivconst]) [Univ,UnDef]
prx = sigma (ConApp ("Cons",[Lazy,Lazy]) [Trivconst,Trivconst]) 
                    [Strong Univ,UnDef]
pry = sigma (ConApp ("Cons",[Strict,Lazy]) [Trivconst,Trivconst]) 
                    [PredVar "P",UnDef]
prz = sigma (ConApp ("Cons",[Strict,Lazy]) [Trivconst,Trivconst]) 
                    [Strong (PredVar "P"),UnDef]

conSat =
    satisfies [("P",UnDef),("Q",UnDef)]
       (bindE 
         (ConApp ("Cons",[Strict,Lazy]) [Var "x", Var "y"]) 
         List
         [("x",Bottom),("y",Tagged "Nil" [])])
       (sigma (ConApp ("Cons",[Strict,Lazy]) [Trivconst,Trivconst])
                      [PredVar "P", PredVar "Q"])

conSat' = satisfies [] (Just Bottom) (sigma (ConApp ("Cons",[Strict,Lazy])
                                      [Trivconst,Trivconst]) [UnDef,UnDef])

chkT = truthV (Assert (ConApp ("Cons",[Strict,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Strict,Lazy]) 
                                       [Trivconst,Trivconst])
                               [PredVar "P" ,PredVar "Q"]))
              [("x",Bottom),("y",Bottom)] [("P",UnDef),("Q",UnDef)]

chkUnSat = and
      [checkSequent (Consequence [Assert (Var "x") Triv (Strong UnDef)]
                                 [Assert (Var "x") Triv Univ])
                    [] emptyBndg []                                                                          
      ,not $ checkSequent (Consequence [Assert (Var "x") Triv Univ]
                                 [Assert (Var "x") Triv (Strong UnDef)])
                    [] emptyBndg []
      ]

testexplicitcontext' =
   explicitcontext [Assert (Var "x") List (Strong Univ)
                   ,Assert (Var "y") Triv (PredVar "P")
                   ] [] [("P",Univ),("Unsat",Strong UnDef)]


testexplicitcontext'' =
   explicitcontext [Assert (ConApp ("Cons",[Strict,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Strict,Lazy]) [Trivconst,Trivconst])
                               [ (PredVar "P"),PredVar "Q"])]

                   [("x",Bottom),("y",Tagged "Nil" [])] [("P",UnDef),("Q",Univ)]

chk = and $
      [checkSequent (Consequence [PropVar "Gamma"] 
                     [Assert (ConApp ("Cons",[Strict,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Strict,Lazy]) [Trivconst,Trivconst])
                               [Strong (PredVar "P"),PredVar "Q"])])
       [[("x",Tagged "Nil" []),("y",Bottom)]
       ,[("x",Tagged "Cons" [Bottom,Bottom]),("y",Bottom)]] 
       emptyBndg [("P",Strong Univ),("Q",UnDef)]

      ,checkSequent (Consequence [PropVar "Gamma"] 
                     [Assert (ConApp ("Cons",[Lazy,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Lazy,Lazy]) [Trivconst,Trivconst])
                               [Strong (PredVar "P"),PredVar "Q"])])
       [[("x",Tagged "Nil" []),("y",Bottom)]
       ,[("x",Tagged "Cons" [Bottom,Bottom]),("y",Bottom)]] 
       emptyBndg [("P",Strong Univ),("Q",UnDef)]

      ,checkSequent (Consequence [PropVar "Gamma"] 
                     [Assert (ConApp ("Cons",[Lazy,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Lazy,Lazy]) [Trivconst,Trivconst])
                               [(PredVar "P"),PredVar "Q"])])
       [[("x",Tagged "Nil" []),("y",Bottom)]
       ,[("x",Tagged "Cons" [Bottom,Bottom]),("y",Bottom)]] 
       emptyBndg [("P", Univ),("Q",UnDef)]

      ,checkSequent (Consequence [PropVar "Gamma"] 
                     [Assert (ConApp ("Cons",[Lazy,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Lazy,Lazy]) [Trivconst,Trivconst])
                               [Univ,UnDef])])
       [[("x",Tagged "Nil" []),("y",Bottom)]
       ,[("x",Tagged "Cons" [Bottom,Bottom]),("y",Bottom)]] 
       emptyBndg [("P", Univ),("Q",UnDef)]
      ]

chkTruthV = and $
               [truthV (Assert (Var "x") List (PredVar "P"))
                     [("x",Tagged "Nil" []),("y",Bottom)]
                     [("P", Univ),("Q",UnDef)]

               ,truthV (Assert (Var "x") List (PredVar "P"))
                     [("x",Tagged "Nil" []),("y",Bottom)]
                     [("P", ConPred "Nil" []),("Q",UnDef)]

               ,truthV (Assert (ConApp ("Cons",[Lazy,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Lazy,Lazy]) [Trivconst,Trivconst]) 
                               [PredVar "P",PredVar "Q"]))
                     [("x",Tagged "Nil" []),("y",Bottom)]
                     [("P", Univ),("Q",UnDef)]

               ,truthV (Assert (ConApp ("Cons",[Lazy,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Lazy,Lazy]) [Trivconst,Trivconst]) 
                               [PredVar "P",UnDef]))
                     [("x",Tagged "Nil" []),("y",Bottom)]
                     [("P", Univ),("Q",UnDef)]

               ,truthV (Assert (ConApp ("Cons",[Lazy,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Lazy,Lazy]) [Trivconst,Trivconst]) 
                               [Univ,PredVar "Q"]))
                     [("x",Tagged "Nil" []),("y",Bottom)]
                     [("P", Univ),("Q",UnDef)]
               ]

pred'' = sigma (ConApp ("Cons",[Lazy,Lazy]) [Trivconst,Trivconst])
               [(PredVar "P"),PredVar "Q"]

envsEq = 
    checkSequent' (Consequence [PropVar "Gamma"] 
                     [Assert (ConApp ("Cons",[Lazy,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Lazy,Lazy]) [Trivconst,Trivconst])
                               [(PredVar "P"),PredVar "Q"])])
       [[("x",Tagged "Nil" []),("y",Bottom)]
       ,[("x",Tagged "Cons" [Bottom,Bottom]),("y",Bottom)]] 
       [] [("P", Univ),("Q",UnDef)]

    == 
    checkSequent' (Consequence [PropVar "Gamma"] 
                     [Assert (ConApp ("Cons",[Lazy,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Lazy,Lazy]) [Trivconst,Trivconst])
                               [Univ,UnDef])])
       [[("x",Tagged "Nil" []),("y",Bottom)]
       ,[("x",Tagged "Cons" [Bottom,Bottom]),("y",Bottom)]] 
       [] [("P", Univ),("Q",UnDef)]
--  == [[("x",Nil),("y",_|_)],[("x",(Cons _|_ _|_)),("y",_|_)]]

chkGamma = checkSequent (Consequence [PropVar "Gamma"]
                           [Assert (Var "x") Triv (PredVar "P")])
             [[("x",Void)]] emptyBndg [("P",Strong Univ)]


tvss' =  
  validateRule $
      Conclude [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "x") List (Strong (PredVar "P"))])
               ,(Consequence [PropVar "Gamma"] [Assert (Var "y") List (PredVar "Q")]
               )] 
                 (Consequence [PropVar "Gamma"]
                     [Assert (ConApp ("Cons",[Strict,Lazy]) [Var "x", Var "y"])
                        List (Strong Univ)])

pV = allPredVars (Consequence [PropVar "Gamma"] 
                   [Assert (Var "x") List (Strong (PredVar "P"))])
pV' = allPredVars (Consequence [PropVar "Gamma"] 
                   [Assert (Var "x") List (PredVar "P")])
pV'' = allPredVars (Consequence [Assert (Var "y") List (PredVar "Q")]
                    [Assert (Var "x") List (PredVar "P")])
pB = allPreds pV
pB' = allPreds pV'
pB'' = allPreds pV''

testsequentBindings = and
      [sequentBindings "Gamma" "Delta" [("P",UnDef),("Q",Univ)]
            [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "x") List (Strong (PredVar "P"))])
               ,(Consequence [PropVar "Gamma"] [Assert (Var "y") List (PredVar "Q")])]
       == ([],[])

      ,sequentBindings "Gamma" "Delta" [("P",Univ),("Q",UnDef)]
            [(Consequence [PropVar "Gamma"] 
                             [Assert (Var "x") List (PredVar "P")])
               ,(Consequence [PropVar "Gamma"] [Assert (Var "y") List (PredVar "Q")])] 
       == ([[("x",Tagged "Cons" [Bottom,Bottom]),("y",Bottom)]
           ,[("x",Tagged "Nil" []),("y",Bottom)]
           ,[("x",Bottom),("y",Bottom)]],[])
      ]

vr0 = validaterule'
      (Conclude [Consequence [Assert (Var "x") Triv (Strong Univ)] 
                             [PropVar "Delta"]]
                (Consequence [Assert (Var "x") Triv Univ] [PropVar "Delta"]))
      []
vr1 = validaterule'
      (Conclude [Consequence [Assert (Var "x") Triv Univ] 
                             [PropVar "Delta"]]
                (Consequence [Assert (Var "x") Triv (Strong Univ)] 
                             [PropVar "Delta"]))
      []


testvalidateRuleC = and
  [validateRule $ -- (1)
      Conclude [Consequence [Assert (Var "x") Triv Univ] [PropVar "Delta"]]
               (Consequence [Assert (Var "x") Triv Univ] [PropVar "Delta"])
  ,not $ validateRule $  -- (2)
      Conclude [Consequence [Assert (Var "x") Triv (Strong Univ)] 
                            [PropVar "Delta"]]
               (Consequence [Assert (Var "x") Triv Univ] [PropVar "Delta"])
  ,validateRule $        -- (3)
      Conclude [Consequence [Assert (Var "x") Triv Univ] 
                            [PropVar "Delta"]]
               (Consequence [Assert (Var "x") Triv (Strong Univ)] 
                            [PropVar "Delta"])
  ,validateRule $        -- (4)
      Conclude [(Consequence [Assert (Var "x") Triv (Strong Univ)
                             ,Assert (Var "y") List Univ]
                             [PropVar "Delta"])] 
                 (Consequence 
                     [Assert (ConApp ("Cons",[Lazy,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Lazy,Lazy]) [Trivconst,Trivconst])
                               [Strong Univ,Univ])]
                     [PropVar "Delta"])
  ,not $ validateRule $  -- (5)
      Conclude [(Consequence [Assert (Var "x") Triv (Strong (PredVar "P"))
                             ,Assert (Var "y") List (PredVar "Q")]
                             [PropVar "Delta"])] 
                 (Consequence 
                     [Assert (ConApp ("Cons",[Lazy,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Lazy,Lazy]) [Trivconst,Trivconst])
                               [Strong Univ, Univ])]
                     [PropVar "Delta"])
  ,validateRule $        -- (6)
      Conclude [(Consequence [Assert (Var "x") Triv (Strong (PredVar "P"))
                             ,Assert (Var "y") List (PredVar "Q")]
                             [PropVar "Delta"])] 
                 (Consequence 
                     [Assert (ConApp ("Cons",[Lazy,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Lazy,Lazy]) [Trivconst,Trivconst])
                               [Strong (PredVar "P"),PredVar "Q"])]
                     [PropVar "Delta"])
  ,validateRule $        -- (7)
      Conclude [(Consequence [Assert (Var "x") Triv (Strong (PredVar "P"))
                             ,Assert (Var "y") List (PredVar "Q")]
                             [PropVar "Delta"])] 
                 (Consequence 
                     [Assert (ConApp ("Cons",[Strict,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Strict,Lazy]) [Trivconst,Trivconst])
                               [Strong (PredVar "P"),PredVar "Q"])]
                     [PropVar "Delta"])
  ,validateRule $        -- (8)
      Conclude [(Consequence [Assert (Var "x") Triv (Strong (PredVar "P"))
                             ,Assert (Var "y") List (PredVar "Q")]
                             [PropVar "Delta"])] 
                 (Consequence 
                     [Assert (ConApp ("Cons",[Strict,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Strict,Lazy]) [Trivconst,Trivconst])
                               [ (PredVar "P"),PredVar "Q"])]
                     [PropVar "Delta"])
  ,validateRule $        -- (9)
      Conclude [(Consequence [Assert (Var "x") Triv (PredVar "P")]
                             [PropVar "Delta"])] 
               (Consequence [Assert (Var "x") Triv (Strong (PredVar "P"))]
                             [PropVar "Delta"])
  ,not $ validateRule $  -- (10)
      Conclude [(Consequence [Assert (Var "x") Triv (Strong (PredVar "P"))]
                             [PropVar "Delta"])] 
               (Consequence [Assert (Var "x") Triv (PredVar "P")]
                             [PropVar "Delta"])
  ,validateRule $        -- (11)
      Conclude [(Consequence [Assert (Var "x") Triv (PredVar "P")
                             ,Assert (Var "y") List (PredVar "Q")]
                             [PropVar "Delta"])] 
                 (Consequence 
                     [Assert (ConApp ("Cons",[Strict,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Strict,Lazy]) [Trivconst,Trivconst])
                               [ (PredVar "P"),PredVar "Q"])]
                     [PropVar "Delta"])
  ,not $ validateRule $  -- (12)
      Conclude [Consequence [Assert (Var "x") Triv (Strong Univ)] 
                            [PropVar "Delta"]]
               (Consequence [Assert (Var "x") Triv Univ] [Falsity])
  ,not $ validateRule $  -- (13)
      Conclude [Consequence [Assert (Var "x") Triv (Strong Univ)] 
                            [PropVar "Delta"]]
               (Consequence [Assert (Var "x") Triv Univ] [])
  ,validateRule $        -- (14)
      Conclude [Consequence [Assert (Var "x") Triv UnDef] 
                            [PropVar "Delta"]]
               (Consequence [Assert (Var "x") Triv (Strong UnDef)] 
                            [PropVar "Delta"])
  ]



bindfalse = validaterule 
      (Conclude [Consequence [Assert (Var "x") Triv (Strong Univ)] 
                            [PropVar "Delta"]]
               (Consequence [Assert (Var "x") Triv Univ] [Falsity])) []
chkfalse = checkSequent 
               (Consequence [Assert (Var "x") Triv Univ] [Falsity])
               [[("x",Bottom)]] emptyBndg []

chkEmpty = checkSequent
               (Consequence [Assert (Var "x") Triv (Strong UnDef)]
                            [PropVar "Delta"])
               []
               (\envs -> case envs of
                            [] -> error "True"
                            (n,Bottom):_ -> n == "x"
                            _ -> error ("chkEmpty " ++ show envs))
               []  

testvalidateRuleC_ = and $
   validateRule_ $
      Conclude [(Consequence [Assert (Var "x") Triv (PredVar "P")
                             ,Assert (Var "y") List (PredVar "Q")]
                             [PropVar "Delta"])] 
                 (Consequence 
                     [Assert (ConApp ("Cons",[Strict,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Strict,Lazy]) [Trivconst,Trivconst])
                               [ (PredVar "P"),PredVar "Q"])]
                     [PropVar "Delta"])

prSL = sigma
                               (ConApp ("Cons",[Strict,Lazy]) 
                                       [Trivconst,Trivconst])
                               [UnDef,PredVar "Q"]
prSS = sigma
                               (ConApp ("Cons",[Strict,Strict]) 
                                       [Trivconst,Trivconst])
                               [UnDef,PredVar "Q"]

-- When x = Bottom, Cons x y = Bottom and no conclusion about "y" is
-- justified.  When Cons x y = Bottom, the assumptions of the consequent,
-- Cons x y ::: $Cons $P Q = False, thus the consequent (and hence, the
-- rule) is valid.

testvalidateRuleC__ = and $
   validateRule_ $ 
      Conclude [(Consequence [Assert (Var "x") Triv UnDef
                             ,Assert (Var "y") List (PredVar "Q")]
                             [PropVar "Delta"])] 
                 (Consequence 
                     [Assert (ConApp ("Cons",[Strict,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Strict,Lazy]) 
                                       [Trivconst,Trivconst])
                               [UnDef,PredVar "Q"])]
                     [PropVar "Delta"])

testvalidateRuleC___ = and $
   validateRule_ $ 
      Conclude [(Consequence [Assert (Var "x") Triv UnDef
                             ,Assert (Var "y") List (PredVar "Q")]
                             [PropVar "Delta"])] 
                 (Consequence 
                     [Assert (ConApp ("Cons",[Strict,Strict]) 
                                     [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Strict,Strict]) 
                                       [Trivconst,Trivconst])
                               [UnDef,PredVar "Q"])]
                     [PropVar "Delta"])


cs = checkSequent 
               (Consequence [Assert (Var "x") Triv Univ] 
                            [Assert (Var "x") Triv Univ]) 
               [] emptyBndg []

testCheckSequent_ =
   checkSequent_ (Consequence 
                     [Assert (ConApp ("Cons",[Strict,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Strict,Lazy]) [Trivconst,Trivconst])
                               [UnDef,PredVar "Q"])]
                     [PropVar "Delta"])
      [] [] [("Q",UnDef)]

showcontextresults =
  validaterule'
      (Conclude [Consequence [Assert (Var "x") Triv (Strong Univ)] 
                            [PropVar "Delta"]]
               (Consequence [Assert (Var "x") Triv Univ] [PropVar "Delta"]))
      []

showContextResults_ =
 map
  (validaterule'
     (Conclude [(Consequence [Assert (Var "x") Triv UnDef
                             ,Assert (Var "y") List (PredVar "Q")]
                             [PropVar "Delta"])] 
                 (Consequence 
                     [Assert (ConApp ("Cons",[Strict,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Strict,Lazy]) [Trivconst,Trivconst])
                               [UnDef,PredVar "Q"])]
                     [PropVar "Delta"])))
      [[("Q",Strong Univ)],[("Q",UnDef)]]

test_mE =
  mE (ConApp ("Cons",[Strict,Lazy]) [Var "x", Var "y"])
     (\n -> if n == "x" then Bottom else Tagged "Nil" [])

showPred = sigma
                               (ConApp ("Cons",[Strict,Lazy]) [Trivconst,Trivconst])
                               [UnDef,PredVar "Q"]

showContextResults' =
  validaterule'
     (Conclude [(Consequence [Assert (Var "x") Triv (Strong (PredVar "P"))
                             ,Assert (Var "y") List (PredVar "Q")]
                             [PropVar "Delta"])] 
                 (Consequence 
                     [Assert (ConApp ("Cons",[Lazy,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Lazy,Lazy]) [Trivconst,Trivconst])
--                               [Strong (PredVar "P"),PredVar "Q"])]
                               [Strong Univ,Univ])]
                     [PropVar "Delta"]))
     [("P",Strong Univ),("Q",Univ)]

showContextResults'' =
  validaterule'
     (Conclude [(Consequence [Assert (Var "x") Triv (Strong Univ)
                             ,Assert (Var "y") List Univ]
                             [PropVar "Delta"])] 
                 (Consequence 
                     [Assert (ConApp ("Cons",[Lazy,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Lazy,Lazy]) [Trivconst,Trivconst])
--                               [Strong (PredVar "P"),PredVar "Q"])]
                               [Strong Univ,Univ])]
                     [PropVar "Delta"]))
     [("P",Strong Univ),("Q",Univ)]


ruleContext r@(Conclude _ c) pe = 
  let (envs,res) = validaterule' r pe in
  checkSequent_ c envs res pe

showRuleContext =
   ruleContext
      (Conclude [(Consequence [Assert (Var "x") Triv (Strong (PredVar "P"))
                             ,Assert (Var "y") List (PredVar "Q")]
                             [PropVar "Delta"])] 
                 (Consequence 
                     [Assert (ConApp ("Cons",[Lazy,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Lazy,Lazy]) [Trivconst,Trivconst])
                               [Strong (PredVar "P"),PredVar "Q"])]
                     [PropVar "Delta"]))
      [("P",Strong Univ),("Q",Univ)]

testexplicitcontext_ =
   explicitcontext [Assert (Var "x") Triv (Strong Univ)]
                   [] []

testexplicitcontext__ =
   explicitcontext [Assert (Var "x") Triv (Strong UnDef)]
                   [] []


testconstructorcontext = 
   explicitcontext [Assert (ConApp ("Cons",[Lazy,Lazy]) [Var "x", Var "y"])
                        List (sigma
                               (ConApp ("Cons",[Lazy,Lazy]) [Trivconst,Trivconst])
                               [Strong (PredVar "P"),PredVar "Q"])]
                   [] [("P",Strong Univ),("Q",Univ)]

testBC = map (bindConclusion "Delta" (Consequence 
                [Assert (Var "x") Triv (PredVar "P")] [PropVar "Delta"]) 
                [("P", Strong Univ)]) 
             [[("x",Bottom)],[("x",Void)]]
         == [False,True]

testAllRules =
  testvalidateRule && testvalidateRules && testvalidateRuleC &&
      testvalidateRuleC_ && testvalidateRuleC__ && testvalidateRuleC___
