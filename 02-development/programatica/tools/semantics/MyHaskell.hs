module MyHaskell where

import AST 
import Examples

----------------------------------------------------------------------
-- Values
----------------------------------------------------------------------

data V 
  = Z Integer        --- scalars
  | FV (V -> V)      --- functions
  | Tagged Name [V]  --- algebraic structured data
  | TV [V]           --- tuple values 
  | FT [(V,V)]       --- trace representation of a function
  | Void
  | Bottom

-------------------------------------------------------------
-------------------------------------------------------------
type Env = Name -> V              -- Maps Names to Values V

xEnv :: Env -> Name -> V -> Env
xEnv rho n phi = tweek rho n phi
    where tweek f x y = \ z -> if x == z then y else f z

extend rho (x:xs) (b:bs) = (extend (xEnv rho x b) xs bs)
extend rho [] [] = rho

------------------------------------------------------------------
{-
There are variables in a pattern p with the appropriate types corresponding 
to the range of this function x1::b1, ..., xn::bn.

Call these variables the "fringe" of p. (fringe p) returns the free 
variables of p in *order* of occurrence from left to right. -}

-- returns the fringe of p in order  from left to right.
fringe :: P -> [Name]
fringe (Pconst i) = []
fringe (Pvar x) = [x]
fringe (Pcondata n ps) = foldr (++) [] (map fringe ps)
fringe (Ptilde p) = fringe p
fringe (Ptuple ps)= foldr (++) [] (map fringe ps)
fringe (Paspat n p) = n : (fringe p)
fringe Pwildcard = []

frD :: [D] -> [Name]
frD [] = []
frD ((Fun f _):ds) = f : (frD ds)
frD ((Val p _ _):ds) = fringe p ++ (frD ds)

--- This collects the function names & patterns from declarations
--- as a single tuple pattern
declared :: [D] -> P
declared ds = ptuple $ map getbinder ds
     where     
	     getbinder (Fun f _)   = Pvar f
	     getbinder (Val p _ _) = tildefy p


arity (Pvar x) = 1
arity (Ptuple ps) = sum(map arity ps)
arity (Pcondata n ps) = sum(map arity ps)
arity (Ptilde p) = arity p
		 
tuple :: [V] -> V
tuple [v] = v
tuple vs = TV vs

tagtuple :: [V] -> V
tagtuple [v] = v
tagtuple vs = Tagged "tuple" vs

ptuple :: [P] -> P
ptuple [p] = p
ptuple ps = Pcondata "tuple" ps

tildefy :: P -> P
tildefy p = case p of 
                  (Ptilde p') -> p
                  (Pvar x)    -> p
                  _           -> (Ptilde p)

-- eqHd :: Show a => Eq a => a -> [a] -> Bool
eqHd x (y:_) = x==y
eqHd x []    = error ("eqHd " ++ show x ++ " is applied to []")

hd :: [a] -> a
hd xs = case xs of
          (x:_) -> x
          []    -> error "hd of [] not defined"

tl :: [a] -> [a]
tl xs = case xs of
          (_:y) -> y
          []    -> error "tl of [] not defined"

showWithCommas :: Show a => [a] -> [Char]
showWithCommas [] = ""
showWithCommas [x] = show x
showWithCommas (x:xs) = show x ++ ',':showWithCommas xs

-------------------------------------------
--      Important Semantic Operators     --
-------------------------------------------

-- Composition operators (N.b., both diagrammatic order.)
(>>>) :: (a -> b) -> (b -> c) -> a -> c
f >>> g = g . f              -- Functional

(<>) :: (a -> Maybe b) -> (b -> Maybe c) -> a -> Maybe c
f <> g = \x -> f x >>= g     -- Kleisli 

bottom :: a                             -- domains are pointed
bottom = undefined

purify :: Maybe a -> a                  -- "run" of Maybe monad
purify (Just x) =  x
purify Nothing  = bottom

fix :: (a -> a) -> a                    -- least fixed point
fix f = f (fix f)

semseq :: V -> V -> V
semseq x y = case x of 
                (Z _)        -> y
                (FV _)       -> y
                (Tagged _ _) -> y
                (TV _)       -> y
                Void         -> y
                Bottom       -> Bottom 

sharp :: Int -> [V] -> (V -> V) -> V    -- Currying
sharp 0 vs beta = beta (tuple vs)
sharp n vs beta = FV $ \ v -> sharp (n-1) (vs++[v]) beta

app :: V -> V -> V                      -- Application
app (FV f) x = f x
app (FT tes) x = 
   case lookup x tes of
      Nothing -> error ("in application of FT[" ++ showWithCommas tes ++ "] "
                    ++ show x)
      Just r  -> r

fatbar :: (a -> Maybe b) -> (a -> Maybe b) -> (a -> Maybe b)
f `fatbar` g = \ x -> (f x) `fb` (g x)
     where 
          fb :: Maybe a -> Maybe a -> Maybe a
          Nothing `fb` y = y
          (Just v) `fb` y = (Just v)
		  


{- The function match is used to construct the meaning of a Match. -}
match :: Env -> (P, B, [D]) -> V -> Maybe V
match rho (p,b,ds) = mP p <> (\vs -> mwhere (extend rho xs vs) b ds)
     where xs = fringe p

-- used in letbind and mE
lam :: P -> E -> Env -> V -> V
lam p e rho = (mP p <> ((\vs -> mE e (extend rho xs vs)) >>> Just)) >>> purify
     where xs = fringe p

mcase :: Env -> [Match] -> V -> V
mcase rho ml = (fatbarL $ map (match rho) ml) >>> purify
                     where fatbarL :: [V -> Maybe V] -> V -> Maybe V
                           fatbarL ms = foldr fatbar (\ _ -> (Just bottom)) ms

{-
(letbind rho ds e) is the meaning of Haskell's mutually recursive
let construct. Its definition uses a standard technique for 
defining mutual recursion with explicit fix-points:

let p1 = e1    
      ...
    pn = en    === (\(p1,...,pn) -> e) (fix \(~p1,...,~pn) -> (e1,...,en))
in 
    e

In order for the fixpoint to be defined, each pattern pi in the argument 
to fix is "~"-ed, so that the function is lazy.
-}

letbind ::  Env -> [D] -> E -> V
letbind rho [] e = mE e rho
letbind rho ds e = (lam dp e rho) v
    where
       dp = tildefy $ declared ds
       xs = frD ds
       decls env = tagtuple $ map (\d -> mD d env) ds
       v = fix $ ((mP dp) <> 
                   ((\vs -> decls (extend rho xs vs)) >>> Just)) >>> purify

mwhere ::  Env -> B -> [D] -> Maybe V
mwhere rho b [] = mB b rho
mwhere rho b ds = (wherecls dp b rho) v
 where
   wherecls p b rho = (mP p <> (\vs -> mB b (extend rho xs vs)))
                         where xs = fringe p
   dp = tildefy $ declared ds
   xs = frD ds
   decls env = tagtuple $ map (\d -> mD d env) ds
   v = fix $ ((mP dp) <> 
               ((\vs -> decls (extend rho xs vs)) >>> Just)) >>> purify
			 	   
{-
Translation of function bindings from Haskell98 Report (p. 54):

             x p(1,1) ... p(1,k)  match1
                      ...
             x p(n,1) ... p(n,k)  matchn

 is equivalent to
            x = \x1 ... xn -> case (x1,...,xn) of
			                            (p(1,1),...,p(1,k)) ->  match1
                                            ...
                                        (p(n,1),...,p(n,k)) ->  matchn  
	        where x1,...,xn are fresh variables.
			
The function below, "mD", is constructed similarly, although without any
variable generation. 
-}



--------------------------------------------
---          semantic functions          ---
--------------------------------------------

mE  :: E -> Env -> V
mP  :: P -> V -> Maybe [V]
mB  :: B -> Env -> Maybe V
mD :: D -> Env -> V
mE' :: E -> (Name -> Maybe V) -> Maybe V
-----------------------------------------------------------------
-- The meaning of expressions
-----------------------------------------------------------------

ifV :: V -> a -> a -> a
ifV (Tagged "True" []) x y = x
ifV (Tagged "False" []) x y = y



mE (Var n) rho        = rho n
mE (Const i) rho      = (Z i)

mE (TupleExp es) rho  = TV $ map (\e-> mE e rho) es
mE (Cond e0 e1 e2) rho = ifV (mE e0 rho) (mE e1 rho) (mE e2 rho)

mE (App e1 e2) rho    = app (mE e1 rho) (mE e2 rho)
mE (Abs [p] e) rho    = FV $ lam p e rho
mE (Abs ps e) rho     = mE (foldr (\p -> \body -> Abs [p] body) e ps) rho

mE (Let ds e) rho     = letbind rho ds e 
mE (Case e ml) rho    = mcase rho ml (mE e rho)

mE (ConApp (n,ls) es) rho  = evalL (zip es ls) rho n [] 
   where evalL :: [(E,LS)] -> Env -> Name -> [V] -> V
         evalL [] rho n vs              = Tagged n vs
         evalL ((e,Strict):es) rho n vs = semseq (mE e rho) 
		                                         (evalL es rho n (vs ++ [mE e rho]))
         evalL ((e,Lazy):es) rho n vs   = evalL es rho n (vs ++ [mE e rho])

mE (NewApp n e) rho   = mE e rho

--mE Boom rho           = mE Boom rho
mE Undefined rho      = Bottom
--mE (Error string) rho = bottom

mE (Seq e1 e2) rho    = semseq (mE e1 rho) (mE e2 rho)
mE (Bin op e1 e2) rho = binOp op (mE e1 rho) (mE e2 rho)
           where binOp Plus (Z i) (Z j)    = Z $ i+j
                 binOp Mult (Z i) (Z j)    = Z $ i*j
                 binOp IntEq (Z i) (Z j)   = Tagged (equal i j) []
                 binOp IntLess (Z i) (Z j) = Tagged (less i j) []
                 equal i j = if i==j then "True" else "False"
                 less i j  = if i<j then "True" else "False"


mE' (Var n) rho        = rho n
mE' (Const i) rho      = Just (Z i)

mE' (App e1 e2) rho    = case mE' e1 rho of
                            Nothing -> Nothing
                            Just v1 -> case mE' e2 rho of
                                          Nothing -> Nothing
                                          Just v2 -> Just (app v1 v2)
-- mE (Abs [p] e) rho    = FV $ lam p e rho
-- mE (Abs ps e) rho     = mE (foldr (\p -> \body -> Abs [p] body) e ps) rho

-- mE (Let ds e) rho     = letbind rho ds e 
-- mE (Case e ml) rho    = mcase rho ml (mE e rho)

-- mE (ConApp (n,ls) es) rho  = evalL (zip es ls) rho n [] 
--    where evalL :: [(E,LS)] -> Env -> Name -> [V] -> V
--          evalL [] rho n vs              = Tagged n vs
--          evalL ((e,Strict):es) rho n vs = semseq (mE e rho) 
--                                             (evalL es rho n (vs ++ [mE e rho]))
--          evalL ((e,Lazy):es) rho n vs   = evalL es rho n (vs ++ [mE e rho])

-- mE (NewApp n e) rho   = mE e rho

mE' Undefined rho      = Nothing

-- mE' (Seq e1 e2) rho    = semseq (mE e1 rho) (mE e2 rho)


mB (Normal e) rho               = Just (mE e rho)
mB (Guarded gl) rho             = ite gl rho
   where ite [] rho         = Nothing   
         ite ((g,e):gs) rho = ifV (mE g rho) (Just (mE e rho)) (ite gs rho)

{- 
A pattern p may be viewed as a function of type ::a -> Maybe (b1,...,bn).
The denotation of a pattern is represented here as a function ::V -> Maybe [V].
-}
mP (Pvar x) v                    = Just [v]
mP (Pconst i) (Z j)              = if i==j then Just [] else Nothing
mP (Ptuple ps) (TV vs)           = stuple (map mP ps) vs
mP (Pcondata n ps) (Tagged t vs) = if n==t then
                                        stuple (map mP ps) vs
                                   else Nothing
mP (Pnewdata n p) v              = mP p v
mP (Ptilde p) v = Just(case mP p v of
                          Nothing -> replicate (arity p) bottom
                          Just z -> z) 
mP Pwildcard v = Just []

stuple :: [V -> Maybe [V]] -> [V] -> Maybe [V]
stuple [] []         = Just []
stuple (q:qs) (v:vs) = do v'  <- q v
                          vs' <- stuple qs vs
                          Just (v'++vs')

mD (Fun f cs) rho  = sharp lps [] body
     where 
       body = mcase rho (map (\(ps,b,ds) -> (ptuple ps, b,ds)) cs)
       lps  = length ((\(pl,_,_)->pl) (head cs))
	                              
mD (Val p b ds) rho = purify $ mwhere rho b ds 

eqV :: V -> V -> Bool
eqV (Z i) (Z j) = i==j
eqV (Tagged n vs) (Tagged n' vs') = n==n' && vs==vs'
eqV (TV tp) (TV tp') = tp==tp'
eqV Bottom Bottom = True
eqV Void Void = True
eqV _ _ = False

instance Eq V where
  (==) = eqV

showV :: V -> String
showV (Z i)            = show i
showV (FV _)         = "(function value)"
showV (Tagged n [])    = n
showV (Tagged n [v])   = "("++n++" ?)"
showV (Tagged n [v1,v2])   = "("++n++" "++show v1++" "++show v2++")"
showV (TV [])    = "()"
showV (TV [v])   = "("++show v++")"
showV (TV [v1,v2])    = "("++show v1++","++show v2 ++")"
showV Bottom = "_|_"
showV Void = "()"
showV (FT vprs) = "FT[" ++ separateWithCommas (map show vprs) ++ "]"

instance Show V where
  show = showV

separateWithCommas [] = ""
separateWithCommas [s] = s
separateWithCommas (s:ss) = s ++ ',':separateWithCommas ss

rho0 = (\msg -> error "hey - you're applying the empty env!")
go e = mE e rho0

omegafac n = let fac x = if x==0 then 1 else x * (fac (x-1)) in fac n

omega :: V
omega = case (fix (\ i -> i) (Z 9)) of 
                      FV f -> FV f

omega1 :: Integer -> (Integer -> Integer) -> Integer -> Integer
omega1 x f = if x==0 then f else (omega1 (x-1) f)
			
