module Monsters where

import AST
import Examples

{-
Some bugs I noticed:

	seq ((\(Just x) y -> x) Nothing) 3

This should evaluate to 3 (H98 Report 3.3), but gives match failure.
The arrow function (p11) should delay matching until all arguments
are present.
-}

justP p = Pcondata "Just" [p]
nothing = ConApp "Nothing" []

m1 = (App (Abs [justP $ varpat "x", varpat "y"] (Var "x")) nothing)
m2 = (App (Abs [justP $ varpat "x"] (Abs [varpat "y"] (Var "x"))) nothing)
m3 = (App (Abs [justP $ varpat "x"] (Var "x")) nothing)

foobar0 = ((\ (Just x) y -> x) Nothing)
foobar1 = ((\ (Just x) -> (\ y -> x)) Nothing)

e1 = seq ((\ (Just x) y -> x) Nothing) 3
e2 = seq ((\ (Just x) -> (\ y -> x)) Nothing) 3
e3 = (\ ~(x, Just y) -> x) (0, Nothing)
e4 = case 1 of 
                x | x==z -> (case 1 of w | False -> 33)
                      where z = 1
                y -> 101

e5 = case 1 of 
                x | x==z -> (case 1 of w | True -> 33)
                      where z = 2
                y -> 101

e6 = let  fac 0 = 1 
          fac n = n * (fac (n-1))
	 in fac 3

monster1 = Seq (App (Abs [justP $ varpat "x", varpat "y"] (Var "x")) nothing) 
               (Const 3)
			   
monster2 = Seq (App (Abs [justP $ varpat "x"] (Abs [varpat "y"] (Var "x"))) nothing) 
               (Const 3)  

monster3 = App (lambda p (Var "x")) b
      where 
          p = Ptilde $ Pcondata "tuple" [varpat "x", justP $ varpat "y"]
          b = lconapp "tuple" [Const 0, nothing]


constFalse = ConApp "False" []
monster4 = 
   let

        casebody    = Case (Const 1) [(Pvar "w",
                                       Guarded [(constFalse, (Const 33))],
                                      [])]
					 
        guardedbody =  ((Pvar "x") ,
		                    Guarded [(Bin IntEq (Var "x") (Var "z"), casebody)],
                    [Val (Pvar "z") (Normal (Const 1)) []])
					
        normalbody  = ((Pvar "y"),  Normal (Const 101), [])		
   in
      Case (Const 1) [guardedbody,normalbody]           


constTrue = ConApp "True" []
monster5 = 
   let
        casebody    = Case (Const 1) [(Pvar "w",
                                       Guarded [(constTrue, (Const 33))],
                                       [])]
					 
        guardedbody =  ((Pvar "x") ,
		                    Guarded [(Bin IntEq (Var "x") (Var "z"), casebody)],
                    [Val (Pvar "z") (Normal (Const 2)) []])

        normalbody  = ((Pvar "y"),  Normal (Const 101), [])		
   in
      Case (Const 1) [guardedbody,normalbody]          
faccaseDef  = Fun "fac" 
               [
				([Pconst 0], Normal (Const 1), []),
				([Pvar "x"],
                   Normal (Bin Mult (Var "x")   
                              (App (Var "fac") 
                                   (Bin Plus (Var "x") (Const $ -1)))),[])]

monster6 = Let [faccaseDef] (App (Var "fac") (Const 3))

{-
	(\ ~(x, Just y) -> x) (0, Nothing)

This should give match failure (H98 Report 3.17.3(d)), but succeeds.
The last paragraph of p14 is incorrect, and your treatment of irrefutable
patterns needs to be redone.
-}

lconapp tag es = ConApp tag (map (\e->(e,Lazy)) es)


--Works...

{-
	let { f (Just x) = x; f Nothing = 0 } in f (Just 1)

Evaluation fails because variable x isn't found in the environment.
mExpCls (p18) erroneously overrides the environment containing the
pattern variables with the outer environment.
-}



{-
	let { f (Just x) = x; f Nothing = 0 } in f Nothing

This should evaluate to 0, but gives match failure, because mExpCls (p18)
discards all clauses after the first one.
-}

-------------------------------------------------------------------
-------------------------------------------------------------------
--- g5 & g6 are canonical examples of guard-induced weirdness.
--- canon5 and canon6 are their translations into AST.


--- factorial
facDef  = Fun "fac" 
             (([Pvar "x"],
               Normal (Cond (Bin IntEq (Var "x") (Const 0)) 
                        (Const 1)
                        (Bin Mult (Var "x")   
                          (App (Var "fac") 
                               (Bin Plus (Var "x") (Const $ -1))))),
              []):[])



fac = Let [facDef] (App (Var "fac") (Const 3))


facomega = Let [facDef] (App (Var "fac") (Const $ -3))

--- oddeven
oddeven = Let [evenDef,oddDef]  (App (Var "even") (Const 1001))

evenDef = Fun "even" 
             (([Pvar "x"],
               Normal (Cond (Bin IntEq (Var "x") (Const 0))
                         Tconst
                         (App (Var "odd") (Bin Plus (Var "x") (Const $ -1)))),
              []):[])

oddDef  = Fun "odd" 
             (([Pvar "y"],
               Normal (Cond (Bin IntEq (Var "y") (Const 0)) 
                         Fconst
                         (App (Var "even") (Bin Plus (Var "y") (Const $ -1)))),
              []):[])	  
			  
--- factorial + oddeven in various combinations

fodd0 = Let [evenDef,oddDef]
           (Let [facDef] (App (Var "even") (App (Var "fac") (Const 3))))

fodd1 = Let [facDef] 
           (Let [evenDef,oddDef]  (App (Var "even") (App (Var "fac") (Const 3))))

fodd2 = Let [facDef,evenDef,oddDef]  (App (Var "even") (App (Var "fac") (Const 3)))

fodd3 = Let [evenDef,facDef,oddDef]  (App (Var "even") (App (Var "fac") (Const 3)))

fodd4 = Let [evenDef,oddDef,facDef]  (App (Var "even") (App (Var "fac") (Const 3)))
