
## Compute the sequent that results from cut elimination
cut() {
  leftpaths="$1"
  rightpath="$2"
  #rightmod="${rightpath%%/*}"

  pivots=""
  hyp=`listcertattr hyp "$rightpath"`
  for leftpath in $leftpaths ; do
    leftmod="${leftpath%%/*}"
    pivot="$leftmod.`getcertattr conc "$leftpath"`"
    pivots="$pivots $pivot"
    hyp=`echo "$hyp";listcertattr hyp "$leftpath"`
  done

  hyp=`echo "$hyp" | sort -u`
  for pivot in $pivots ; do
    hyp=`echo "$hyp" | fgrep -wv "$pivot"`
  done

  hyp=`echo $hyp` # newline -> space
  conc="`getcertattr conc "$rightpath"`"
  
  # Returns $pivots, $hyp and $conc
}
