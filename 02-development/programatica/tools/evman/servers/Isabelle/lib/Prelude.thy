theory Prelude
imports HOLCF
begin

text {* Remove syntax so we can use "o" as a variable! *}
no_syntax
  comp :: "['b => 'c, 'a => 'b, 'a] => 'c" (infixl "o" 55)
  div  :: "'a::Divides.div \<Rightarrow> 'a \<Rightarrow> 'a"    (infixl 70)

subsection {* Haskell Quotes *}

nonterminals hs_type hs_exp

syntax
  "_HsQ" :: "hs_type \<Rightarrow> type" ("{| _ |}")
  "_HsQ" :: "hs_exp \<Rightarrow> logic" ("{| _ |}")

syntax (xsymbols)
  "_HsQ" :: "hs_type \<Rightarrow> type" ("\<guillemotleft>_\<guillemotright>")
  "_HsQ" :: "hs_exp \<Rightarrow> logic" ("\<guillemotleft>_\<guillemotright>")


subsection {* Haskell Type Syntax *}

syntax
  "_HsTyFun" :: "[hs_type, hs_type] \<Rightarrow> hs_type" ("_ -> _" [1, 0] 0)
  "_HsTyApp" :: "[hs_type, hs_type] \<Rightarrow> hs_type" ("_ _" [1, 2] 1)
  "" :: "hs_type \<Rightarrow> hs_type" ("'(_')" 1000)
  "_HsTyInt" :: "hs_type" ("Int")
  "_HsTyCon" :: "id \<Rightarrow> hs_type" ("_")
  "_HsTyVar" :: "tid \<Rightarrow> hs_type" ("_")
  "_hs_ofsort" :: "[tid, sort] => hs_type"   ("_::_" [1000, 0] 1000)

syntax (xsymbols)
  "_HsTyFun" :: "[hs_type, hs_type] \<Rightarrow> hs_type" ("_ \<rightarrow> _" [1, 0] 0)

translations
  "_HsQ (_hs_ofsort t s)" \<rightharpoonup> "_ofsort t s"
  "_HsQ (_HsTyVar a)" \<rightharpoonup> "a"
  "_HsQ (_HsTyCon a)" \<rightharpoonup> "a"
  "_HsQ _HsTyInt" \<rightleftharpoons> (type) "int lift"
  "_HsQ (_HsTyApp a b)" \<rightleftharpoons> "(_HsQ a) (_HsQ b)"
  "_HsQ (_HsTyFun a b)" \<rightleftharpoons> (type) "(a _HsQ) -> (b _HsQ)"

subsection {* Haskell Expression Syntax *}

(*
Expression precedences:
exp    0      ("::")
exp0
exp0b  10-19  (infix operators 0-9)
exp10b 20   (case, do)
fexp   99   (function application)
aexp   100   (lazy pat, as pat)
aexp1  200   (record syntax)
aexp2  1000   (atomic stuff)
*)

nonterminals
  hs_expb
  hs_expa
  hs_pats  (* includes apats *)
  hs_pat   (* includes apat *)
  hs_quals
  hs_fbinds
  hs_qvar

syntax
  "_HsEqual" :: "[hs_exp, hs_exp] \<Rightarrow> hs_exp" ("_ = _")
  "_HsConstrain" :: "[hs_expb, hs_type] \<Rightarrow> hs_exp" ("_ :: _")
  "" :: "hs_expa \<Rightarrow> hs_exp"  ("_")
  "" :: "hs_expb \<Rightarrow> hs_exp"  ("_")
  "_HsVar"   :: "longid \<Rightarrow> hs_expb"                ("_")
  "_HsVar"   :: "id \<Rightarrow> hs_expb"                    ("_") (* should be qvar *)
  "_HsParen" :: "hs_exp \<Rightarrow> hs_expb"                ("'(_')")
  "_HsApp"  :: "[hs_expb, hs_expb] \<Rightarrow> hs_expb"     ("(1_/ _)" [99, 100] 99)
  "_HsLambda" :: "[hs_pats, hs_exp] \<Rightarrow> hs_expa"  ("(3\\_ ->/ _)" [100, 0] 10)
  "_HsIf"  :: "[hs_exp, hs_exp, hs_exp] \<Rightarrow> hs_expa" ("if _ then _ else _" 10)


  "_hs_pats" :: "[hs_pat, hs_pats] \<Rightarrow> hs_pats"  ("_ _" [101, 100] 100)
  ""         :: "hs_pat \<Rightarrow> hs_pats"             ("_")

  (* should be checkPattern *)
  "" :: "hs_expb \<Rightarrow> hs_pat"           ("_")

  "_HsAsPat"  :: "[id, hs_expb] \<Rightarrow> hs_expb"         ("_ @ _" [0, 100] 100)
  "_HsIrrPat" :: "hs_expb \<Rightarrow> hs_expb"               ("~_" [100] 100)
  "_HsWildCard" :: "hs_expb"                      ("'_")
(*
  "_hs_rec0"  :: "hs_expb \<Rightarrow> hs_expb"               ("_ { }" [200] 200)
  "_hs_rec1"  :: "hs_expb \<Rightarrow> hs_fbinds \<Rightarrow> hs_expb"  ("_ { _ }" [200,0] 200)
*)

syntax (xsymbols)
  "_HsLambda" :: "[hs_pats, hs_exp] \<Rightarrow> hs_expa"  ("(3\<lambda>_ \<rightarrow>/ _)" [100, 0] 10)

syntax
  "_var" :: "'a"
  "_free" :: "'a"
  "_bound" :: "'a"
  "_tfree" :: "'a"
  "_hs_var" :: "'a"
  "_hs_free" :: "'a"
  "_hs_bound" :: "'a"
  "_hs_tfree" :: "'a"

translations
  "_HsQ (_hs_var x)" \<leftharpoondown> "_var x"
  "_HsQ (_hs_free x)" \<leftharpoondown> "_free x"
  "_HsQ (_hs_bound x)" \<leftharpoondown> "_bound x"
  "_HsQ (_hs_tfree x)" \<leftharpoondown> "_tfree x"

print_ast_translation {*
  let
    fun HsQ_ast_tr' [Syntax.Appl [Syntax.Constant c, x]] =
      case c of
          "_hs_var" => Syntax.Appl [Syntax.Constant "_var", x]
        | "_hs_free" => Syntax.Appl [Syntax.Constant "_free", x]
        | "_hs_bound" => Syntax.Appl [Syntax.Constant "_bound", x]
        | "_hs_tfree" => Syntax.Appl [Syntax.Constant "_tfree", x]
        | _ => raise Match;
    fun hs_var_ast_tr' [x] = Syntax.Appl [Syntax.Constant "_var", x];
    fun hs_free_ast_tr' [x] = Syntax.Appl [Syntax.Constant "_free", x];
    fun hs_bound_ast_tr' [x] = Syntax.Appl [Syntax.Constant "_bound", x];
    fun hs_tfree_ast_tr' [x] = Syntax.Appl [Syntax.Constant "_tfree", x];
  in
    [("_HsQ", HsQ_ast_tr'),
     ("_hs_var", hs_var_ast_tr'),
     ("_hs_free", hs_free_ast_tr'),
     ("_hs_bound", hs_bound_ast_tr'),
     ("_hs_tfree", hs_tfree_ast_tr')]
  end;
*}

translations
  "_HsQ (_HsVar x)" \<rightharpoonup> "x"
  "_HsQ (_HsApp f x)" \<rightleftharpoons> "(_HsQ f)$(_HsQ x)"
  "_HsQ (_HsLambda (_hs_pats x xs) t)"
    \<rightleftharpoons> "_cabs (_HsQ x) (_HsQ (_HsLambda xs t))"
  "_HsQ (_HsLambda x e)" \<rightleftharpoons> "_cabs (_HsQ x) (_HsQ e)"
  "_HsQ (_HsConstrain x t)" \<rightleftharpoons> "_constrain (_HsQ x) (_HsQ t)"
  "_HsQ (_HsEqual x y)" \<rightleftharpoons> "(_HsQ x) = (_HsQ y)"
  "_HsQ (_HsIf p x y)" \<rightleftharpoons> "If (_HsQ p) then (_HsQ x) else (_HsQ y) fi"
  "_HsQ (_HsParen x)" \<rightharpoonup> "_HsQ x"
  "_HsQ (_HsAsPat x p)" \<rightleftharpoons> "_as_pat (_HsQ x) (_HsQ p)"
  "_HsQ (_HsIrrPat p)" \<rightleftharpoons> "_lazy_pat (_HsQ p)"
  "_HsQ _HsWildCard" \<rightharpoonup> "_"

translations
  "_HsQ (_HsLambda (_HsIrrPat p) t)" \<leftharpoondown>
    "run oo (_HsQ (_hs_alt (_HsIrrPat p) t))"


subsection {* Let bindings *}

nonterminals hs_decl hs_decls

syntax
  "_HsLet" :: "[hs_decls, hs_exp] \<Rightarrow> hs_expa"    ("let {(_)} in _" 10)
  "_hs_decls" :: "[hs_decl, hs_decls] \<Rightarrow> hs_decls" ("_;/ _")
  "" :: "hs_decl \<Rightarrow> hs_decls" ("_")
  "_hs_decl" :: "[hs_expb, hs_exp] \<Rightarrow> hs_decl" ("_ = _")

translations
  "_HsQ (_HsLet ds e)" \<rightleftharpoons> "_Letrec (_HsQ ds) (_HsQ e)"
  "_HsQ (_hs_decls d ds)" \<rightleftharpoons> "_recbindt (_HsQ d) (_HsQ ds)"
  "_HsQ (_hs_decl x e)" \<rightleftharpoons> "_recbind (_HsQ x) (_HsQ e)"


subsection {* Case expressions *}

nonterminals hs_alt hs_alts

syntax
  "_HsCase" :: "[hs_exp, hs_alts] \<Rightarrow> hs_expb"  ("case _ of {(_)}" 10)
  "_hs_alts" :: "[hs_alt, hs_alts] \<Rightarrow> hs_alts" ("_;/ _")
  "" :: "hs_alt \<Rightarrow> hs_alts" ("_")
  "_hs_alt" :: "[hs_pat, hs_exp] \<Rightarrow> hs_alt" ("_ -> _")

syntax (xsymbols)
  "_hs_alt" :: "[hs_pat, hs_exp] \<Rightarrow> hs_alt" ("_ \<rightarrow> _")

translations
  "_HsQ (_HsCase e bs)" \<rightleftharpoons> "_Case_syntax (_HsQ e) (_HsQ bs)"
  "_HsQ (_hs_alts b bs)" \<rightleftharpoons> "_Case2 (_HsQ b) (_HsQ bs)"
  "_HsQ (_hs_alt x e)" \<rightleftharpoons> "_Case1 (_HsQ x) (_HsQ e)"


subsection {* Infix operators *}

nonterminals hs_qop

syntax
  "_HsVarSym" :: "hs_qop \<Rightarrow> hs_expb" ("'(_')")
  "_HsLeftSection" :: "[hs_expb, hs_qop] \<Rightarrow> hs_expb" ("'(_ _')")
  "_HsRightSection" :: "[hs_qop, hs_exp] \<Rightarrow> hs_expb" ("'(_ _')")

(* Right section not legal for "-", must treat specially *)

nonterminals
  hs_qopr0 hs_qopl0 hs_qop0
  hs_qopr1 hs_qopl1 hs_qop1
  hs_qopr2 hs_qopl2 hs_qop2
  hs_qopr3 hs_qopl3 hs_qop3
  hs_qopr4 hs_qopl4 hs_qop4
  hs_qopr5 hs_qopl5 hs_qop5
  hs_qopr6 hs_qopl6 hs_qop6
  hs_qopr7 hs_qopl7 hs_qop7
  hs_qopr8 hs_qopl8 hs_qop8
  hs_qopr9 hs_qopl9 hs_qop9

syntax (* infix declarations from Hugs Prelude *)
  "_hs_i"  :: "hs_qopr9" (".")
  "_hs_nn" :: "hs_qopl9" ("!!")
  "_hs_c"  :: "hs_qopr8" ("^")
  "_hs_cc" :: "hs_qopr8" ("^^")
  "_hs_tt" :: "hs_qopr8" ("**")
  "_hs_t"  :: "hs_qopl7" ("*")
  "_hs_s"  :: "hs_qopl7" ("'/")
  "_hs_Cv" :: "hs_qopl7" (":%")
  "_hs_v"  :: "hs_qopl7" ("%")
  "_hs_p"  :: "hs_qopl6" ("+")
  "_hs_C"  :: "hs_qopr5" (":")
  "_hs_pp" :: "hs_qopr5" ("++")
  "_hs_ee" :: "hs_qop4" ("==")
  "_hs_se" :: "hs_qop4" ("'/=")
  "_hs_l"  :: "hs_qop4" ("<")
  "_hs_le" :: "hs_qop4" ("<=")
  "_hs_ge" :: "hs_qop4" (">=")
  "_hs_g"  :: "hs_qop4" (">")
  "_hs_aa" :: "hs_qopr3" ("&&")
  "_hs_bb" :: "hs_qopr2" ("||")
  "_hs_gg" :: "hs_qopl1" (">>")
  "_hs_gge" :: "hs_qopl1" (">>=")
  "_hs_ell" :: "hs_qopr1" ("=<<")
  "_hs_d"  :: "hs_qopr0" ("$")
  "_hs_dn" :: "hs_qopr0" ("$!")

syntax
  "" :: "hs_qopr0 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qopr1 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qopr2 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qopr3 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qopr4 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qopr5 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qopr6 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qopr7 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qopr8 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qopr9 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qopl0 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qopl1 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qopl2 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qopl3 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qopl4 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qopl5 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qopl6 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qopl7 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qopl8 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qopl9 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qop0 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qop1 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qop2 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qop3 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qop4 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qop5 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qop6 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qop7 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qop8 \<Rightarrow> hs_qop" ("_")
  "" :: "hs_qop9 \<Rightarrow> hs_qop" ("_")

syntax
  "_HsInfix" :: "[hs_expb,hs_qopl0,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [0,0,1] 0)
  "_HsInfix" :: "[hs_expb,hs_qopl1,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [1,0,2] 1)
  "_HsInfix" :: "[hs_expb,hs_qopl2,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [2,0,3] 2)
  "_HsInfix" :: "[hs_expb,hs_qopl3,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [3,0,4] 3)
  "_HsInfix" :: "[hs_expb,hs_qopl4,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [4,0,5] 4)
  "_HsInfix" :: "[hs_expb,hs_qopl5,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [5,0,6] 5)
  "_HsInfix" :: "[hs_expb,hs_qopl6,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [6,0,7] 6)
  "_HsInfix" :: "[hs_expb,hs_qopl7,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [7,0,8] 7)
  "_HsInfix" :: "[hs_expb,hs_qopl8,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [8,0,9] 8)
  "_HsInfix" :: "[hs_expb,hs_qopl9,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [9,0,10] 9)
  "_HsInfix" :: "[hs_expb,hs_qopr0,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [1,0,0] 0)
  "_HsInfix" :: "[hs_expb,hs_qopr1,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [2,0,1] 1)
  "_HsInfix" :: "[hs_expb,hs_qopr2,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [3,0,2] 2)
  "_HsInfix" :: "[hs_expb,hs_qopr3,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [4,0,3] 3)
  "_HsInfix" :: "[hs_expb,hs_qopr4,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [5,0,4] 4)
  "_HsInfix" :: "[hs_expb,hs_qopr5,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [6,0,5] 5)
  "_HsInfix" :: "[hs_expb,hs_qopr6,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [7,0,6] 6)
  "_HsInfix" :: "[hs_expb,hs_qopr7,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [8,0,7] 7)
  "_HsInfix" :: "[hs_expb,hs_qopr8,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [9,0,8] 8)
  "_HsInfix" :: "[hs_expb,hs_qopr9,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [10,0,9] 9)
  "_HsInfix" :: "[hs_expb,hs_qop0,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [1,0,1] 0)
  "_HsInfix" :: "[hs_expb,hs_qop1,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [2,0,2] 1)
  "_HsInfix" :: "[hs_expb,hs_qop2,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [3,0,3] 2)
  "_HsInfix" :: "[hs_expb,hs_qop3,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [4,0,4] 3)
  "_HsInfix" :: "[hs_expb,hs_qop4,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [5,0,5] 4)
  "_HsInfix" :: "[hs_expb,hs_qop5,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [6,0,6] 5)
  "_HsInfix" :: "[hs_expb,hs_qop6,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [7,0,7] 6)
  "_HsInfix" :: "[hs_expb,hs_qop7,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [8,0,8] 7)
  "_HsInfix" :: "[hs_expb,hs_qop8,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [9,0,9] 8)
  "_HsInfix" :: "[hs_expb,hs_qop9,hs_expb] \<Rightarrow> hs_expb" ("_ _ _" [10,0,10] 9)

syntax
  "_HsInfix" :: "[hs_expb,hs_qopr0,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [1,0,0] 0)
  "_HsInfix" :: "[hs_expb,hs_qopr1,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [2,0,1] 1)
  "_HsInfix" :: "[hs_expb,hs_qopr2,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [3,0,2] 2)
  "_HsInfix" :: "[hs_expb,hs_qopr3,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [4,0,3] 3)
  "_HsInfix" :: "[hs_expb,hs_qopr4,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [5,0,4] 4)
  "_HsInfix" :: "[hs_expb,hs_qopr5,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [6,0,5] 5)
  "_HsInfix" :: "[hs_expb,hs_qopr6,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [7,0,6] 6)
  "_HsInfix" :: "[hs_expb,hs_qopr7,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [8,0,7] 7)
  "_HsInfix" :: "[hs_expb,hs_qopr8,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [9,0,8] 8)
  "_HsInfix" :: "[hs_expb,hs_qopr9,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [10,0,9] 9)
  "_HsInfix" :: "[hs_expb,hs_qopl0,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [0,0,1] 0)
  "_HsInfix" :: "[hs_expb,hs_qopl1,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [1,0,2] 1)
  "_HsInfix" :: "[hs_expb,hs_qopl2,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [2,0,3] 2)
  "_HsInfix" :: "[hs_expb,hs_qopl3,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [3,0,4] 3)
  "_HsInfix" :: "[hs_expb,hs_qopl4,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [4,0,5] 4)
  "_HsInfix" :: "[hs_expb,hs_qopl5,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [5,0,6] 5)
  "_HsInfix" :: "[hs_expb,hs_qopl6,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [6,0,7] 6)
  "_HsInfix" :: "[hs_expb,hs_qopl7,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [7,0,8] 7)
  "_HsInfix" :: "[hs_expb,hs_qopl8,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [8,0,9] 8)
  "_HsInfix" :: "[hs_expb,hs_qopl9,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [9,0,10] 9)
  "_HsInfix" :: "[hs_expb,hs_qop0,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [1,0,1] 0)
  "_HsInfix" :: "[hs_expb,hs_qop1,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [2,0,2] 1)
  "_HsInfix" :: "[hs_expb,hs_qop2,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [3,0,3] 2)
  "_HsInfix" :: "[hs_expb,hs_qop3,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [4,0,4] 3)
  "_HsInfix" :: "[hs_expb,hs_qop4,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [5,0,5] 4)
  "_HsInfix" :: "[hs_expb,hs_qop5,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [6,0,6] 5)
  "_HsInfix" :: "[hs_expb,hs_qop6,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [7,0,7] 6)
  "_HsInfix" :: "[hs_expb,hs_qop7,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [8,0,8] 7)
  "_HsInfix" :: "[hs_expb,hs_qop8,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [9,0,9] 8)
  "_HsInfix" :: "[hs_expb,hs_qop9,hs_expa] \<Rightarrow> hs_expa" ("_ _ _" [10,0,10] 9)
(* non-associative infix 9 is last; this gets used for printing *)

syntax
  "_HsBackTicks" :: "id \<Rightarrow> hs_qopl9" ("`_`")

constdefs
  flip :: "\<guillemotleft>('a \<rightarrow> 'b \<rightarrow> 'c) \<rightarrow> 'b \<rightarrow> 'a \<rightarrow> 'c\<guillemotright>"
  "flip \<equiv> \<guillemotleft>\<lambda>f x y \<rightarrow> f y x\<guillemotright>"

translations
  "_HsVarSym (_HsBackticks f)" \<rightharpoonup> "_HsVar f"
  "_HsInfix x f y" \<rightleftharpoons> "_HsApp (_HsLeftSection x f) y"
  "_HsLeftSection x f" \<rightleftharpoons> "_HsApp (_HsVarSym f) x"
  "_HsRightSection f x" \<rightleftharpoons> "_HsApp (_HsApp (_HsVar flip) (_HsVarSym f)) x"


subsection {* Do-notation *}

nonterminals hs_stmts

syntax
  "_HsDo" :: "hs_stmts \<Rightarrow> hs_expb" ("do {(_)}" 10)
  "_HsLetStmt" :: "[hs_decls, hs_stmts] \<Rightarrow> hs_stmts" ("let {(_)};/ _")
  "_HsGenerator" :: "[hs_pat, hs_exp, hs_stmts] \<Rightarrow> hs_stmts" ("_ <- _;/ _")
  "_HsQualifier" :: "[hs_exp, hs_stmts] \<Rightarrow> hs_stmts" ("_;/ _")
  "" :: "hs_stmts \<Rightarrow> hs_stmts" (";/ _")
  "_HsLastStmt" :: "hs_exp \<Rightarrow> hs_stmts" ("_")

syntax (xsymbols)
  "_HsGenerator" :: "[hs_pat, hs_exp, hs_stmts] \<Rightarrow> hs_stmts" ("_ \<leftarrow> _; _")

translations
  "_HsDo (_HsLetStmt ds ss)" \<rightleftharpoons> "_HsLet ds (_HsDo ss)"
  "_HsDo (_HsGenerator x e ss)" \<rightharpoonup>
    "_HsInfix e _hs_gge (_HsLambda x (_HsDo ss))"
  "_HsDo (_HsQualifier e ss)" \<rightharpoonup> "_HsInfix e _hs_gg (_HsDo ss)"
  "_HsDo (_HsLastStmt e)" \<rightharpoonup> "e"

translations
  "_HsDo (_HsGenerator p e (_HsLastStmt e'))" \<leftharpoondown>
    "_HsInfix e _hs_gge (_HsLambda p e')"
  "ss" \<leftharpoondown> "_HsLastStmt (_HsDo ss)"


subsection {* Boolean type *}

types Bool = tr

translations
  "_HsQ Bool" \<rightleftharpoons> "tr"

syntax
  "_HsTrue"  :: "hs_expb" ("True")
  "_HsFalse" :: "hs_expb" ("False")

translations
  "_HsQ (_HsVarSym _hs_aa)" \<rightleftharpoons> "trand"
  "_HsQ (_HsVarSym _hs_bb)" \<rightleftharpoons> "tror"
  "_HsQ (_HsInfix x _hs_aa y)" \<leftharpoondown> "(_HsQ x) andalso (_HsQ y)"
  "_HsQ (_HsInfix x _hs_bb y)" \<leftharpoondown> "(_HsQ x) orelse (_HsQ y)"

syntax
  "_HsNot" :: "hs_expb" ("not")

translations
  "_HsQ _HsTrue" \<rightleftharpoons> "TT"
  "_HsQ _HsFalse" \<rightleftharpoons> "FF"
  "_HsQ _HsNot" \<rightleftharpoons> "Tr.neg"

ML {* Syntax.trace_ast := false *}

lemma
  "\<guillemotleft>False && x = False\<guillemotright>"
  "\<guillemotleft>True  && x = x\<guillemotright>"
  "\<guillemotleft>False || x = x\<guillemotright>"
  "\<guillemotleft>True  || x = True\<guillemotright>"
by simp_all

lemma
  "\<guillemotleft>not True = False\<guillemotright>"
  "\<guillemotleft>not False = True\<guillemotright>"
by simp_all


subsection {* Lists *}

domain 'a List
  = nil
  | cons (lazy head :: "'a") (lazy tail :: "'a List")

syntax
  "_HsTyList" :: "hs_type \<Rightarrow> hs_type" ("[_]")

translations
  "_HsQ (_HsTyList a)" \<rightleftharpoons> (type) "(a _HsQ) List"

nonterminals
  hs_list
  hs_lexps

syntax
  "_hs_MN" :: "hs_expb" ("[]")
  "_HsList"   :: "hs_lexps \<Rightarrow> hs_expb"             ("[(_)]")
  "_hs_lexps" :: "[hs_exp, hs_lexps] \<Rightarrow> hs_lexps"  ("_,/ _")
  ""          :: "hs_exp \<Rightarrow> hs_lexps"              ("_")

translations
  "_HsQ _hs_MN" \<rightleftharpoons> "nil"
  "_HsQ (_HsVarSym _hs_C)" \<rightleftharpoons> "cons"

translations
  "_HsList (_hs_lexps a b)" \<rightleftharpoons> "_HsInfix a _hs_C (_HsList b)"
  "_HsList a" \<rightleftharpoons> "_HsInfix a _hs_C _hs_MN"


subsection {* Strings *}

types Char = "char lift"
types String = "\<guillemotleft>[Char]\<guillemotright>"

translations
  (type) "\<guillemotleft>Char\<guillemotright>" \<rightleftharpoons> (type) "char lift"
  (hs_type) "String" \<rightleftharpoons> (hs_type) "[Char]"

consts Defs :: "'a::type list \<Rightarrow> 'a lift List"
primrec
  "Defs [] = nil"
  "Defs (c # cs) = cons\<cdot>(Def c)\<cdot>(Defs cs)"

syntax
  "_String" :: "xstr \<Rightarrow> hs_expb" ("_")
  "_Char" :: "xstr \<Rightarrow> hs_expb" ("CHR _")

ML {* Syntax.trace_ast := false *}

translations
  "_HsQ (_constrain Nil t)" \<rightleftharpoons> "Defs (_constrain Nil t)"
  "_HsQ (Cons x xs)" \<rightleftharpoons> "Defs (Cons x xs)"
  "_HsQ (Char x y)" \<rightleftharpoons> "Def (Char x y)"

axclass Show < pcpo

consts
  "show" :: "\<guillemotleft>'a::Show -> String\<guillemotright>"


subsection {* Numbers *}

syntax
  "_HsLit" :: "num_const \<Rightarrow> hs_expb" ("_")
  "_HsLit0" :: "hs_expb" ("0")
  "_HsLit1" :: "hs_expb" ("1")

translations
  "_HsQ (_HsLit x)" \<rightharpoonup> "_Numeral x"
  "_HsQ _HsLit0" \<rightharpoonup> "0"
  "_HsQ _HsLit1" \<rightharpoonup> "1"

axclass Bounded < pcpo
axclass Enum < pcpo
axclass Eq < pcpo
axclass Ord < Eq
axclass Num < Eq, Show
axclass Real < Num, Ord
axclass Integral < Real, Enum

types Integer = "int lift"

consts
  hs_ee :: "\<guillemotleft>'a::Eq \<rightarrow> 'a \<rightarrow> Bool\<guillemotright>"
  hs_se :: "\<guillemotleft>'a::Eq \<rightarrow> 'a \<rightarrow> Bool\<guillemotright>"
  hs_l :: "\<guillemotleft>'a::Ord \<rightarrow> 'a \<rightarrow> Bool\<guillemotright>"
  hs_le :: "\<guillemotleft>'a::Ord \<rightarrow> 'a \<rightarrow> Bool\<guillemotright>"
  hs_ge :: "\<guillemotleft>'a::Ord \<rightarrow> 'a \<rightarrow> Bool\<guillemotright>"
  hs_g :: "\<guillemotleft>'a::Ord \<rightarrow> 'a \<rightarrow> Bool\<guillemotright>"
  hs_p :: "\<guillemotleft>'a::Num \<rightarrow> 'a \<rightarrow> 'a\<guillemotright>"
  hs_m :: "\<guillemotleft>'a::Num \<rightarrow> 'a \<rightarrow> 'a\<guillemotright>"
  hs_t :: "\<guillemotleft>'a::Num \<rightarrow> 'a \<rightarrow> 'a\<guillemotright>"
  negate :: "\<guillemotleft>'a::Num \<rightarrow> 'a\<guillemotright>"
  fromIntegral :: "\<guillemotleft>'a::Integral \<rightarrow> 'b::Num\<guillemotright>"
  div :: "\<guillemotleft>'a::Integral -> 'a -> 'a\<guillemotright>"
  toInteger :: "\<guillemotleft>'a::Integral -> Integer\<guillemotright>"
  toInt :: "\<guillemotleft>'a::Integral -> Int\<guillemotright>"
  minBound :: "\<guillemotleft>'a::Bounded\<guillemotright>"
  maxBound :: "\<guillemotleft>'a::Bounded\<guillemotright>"

translations
  "_HsQ (_HsVarSym _hs_ee)" \<rightleftharpoons> "hs_ee"
  "_HsQ (_HsVarSym _hs_se)" \<rightleftharpoons> "hs_se"
  "_HsQ (_HsVarSym _hs_l)"  \<rightleftharpoons> "hs_l"
  "_HsQ (_HsVarSym _hs_le)" \<rightleftharpoons> "hs_le"
  "_HsQ (_HsVarSym _hs_ge)" \<rightleftharpoons> "hs_ge"
  "_HsQ (_HsVarSym _hs_g)"  \<rightleftharpoons> "hs_g"
  "_HsQ (_HsVarSym _hs_p)"  \<rightleftharpoons> "hs_p"
  "_HsQ (_HsVarSym _hs_t)"  \<rightleftharpoons> "hs_t"

syntax
  "_HsMinus" :: "[hs_expb, hs_expa] \<Rightarrow> hs_expa" ("_ - _" [6, 7] 6)
  "_HsMinus" :: "[hs_expb, hs_expb] \<Rightarrow> hs_expb" ("_ - _" [6, 7] 6)
  "_HsNegate" :: "hs_expb \<Rightarrow> hs_expb"        ("- _" [7] 6)

translations
  "_HsQ (_HsMinus x y)" \<rightleftharpoons> "hs_m\<cdot>(_HsQ x)\<cdot>(_HsQ y)"
  "_HsQ (_HsNegate x)" \<rightleftharpoons> "negate\<cdot>(_HsQ x)"


text {* instances for lift *}

instance lift :: (zero) zero ..
instance lift :: (one) one ..
instance lift :: (number) number ..
instance lift :: (type) Eq ..
instance lift :: (ord) Ord ..
instance lift :: (type) Show ..
instance lift :: (number_ring) Num ..
instance lift :: ("{number_ring,ord}") Integral ..
instance lift :: (type) Bounded ..

defs (overloaded)
  zero_lift_def: "0 \<equiv> Def 0"
  one_lift_def: "1 \<equiv> Def 1"
  number_of_lift_def: "number_of w \<equiv> Def (number_of w)"

defs (overloaded)
  hs_ee_lift_def: "hs_ee \<equiv> FLIFT x y. Def (x = y)"
  hs_se_lift_def: "hs_se \<equiv> FLIFT x y. Def (x \<noteq> y)"
  hs_l_lift_def: "hs_l \<equiv> FLIFT x y. Def (x < y)"
  hs_le_lift_def: "hs_le \<equiv> FLIFT x y. Def (x \<le> y)"
  hs_ge_lift_def: "hs_ge \<equiv> FLIFT x y. Def (x \<ge> y)"
  hs_g_lift_def: "hs_g \<equiv> FLIFT x y. Def (x > y)"
  hs_p_lift_def: "hs_p \<equiv> FLIFT x y. Def (x + y)"
  hs_m_lift_def: "hs_m \<equiv> FLIFT x y. Def (x - y)"
  hs_t_lift_def: "hs_t \<equiv> FLIFT x y. Def (x * y)"


subsection {* Tuple Types *}

domain ('a,'b) Tuple2 =
  TupleCon2
    (lazy fst :: "'a")
    (lazy snd :: "'b")

translations
  "_HsQ fst" \<leftharpoondown> "Prelude.fst"
  "_HsQ snd" \<leftharpoondown> "Prelude.snd"

domain ('a,'b,'c) Tuple3 =
   TupleCon3
    (lazy "'a")
    (lazy "'b")
    (lazy "'c")

domain ('a,'b,'c,'d) Tuple4 =
   TupleCon4
    (lazy "'a")
    (lazy "'b")
    (lazy "'c")
    (lazy "'d")

domain ('a,'b,'c,'d,'e) Tuple5 =
   TupleCon5
    (lazy "'a")
    (lazy "'b")
    (lazy "'c")
    (lazy "'d")
    (lazy "'e")

syntax
  "_HsTyUnit" :: "hs_type" ("'(')")
  "_HsTyTuple2" :: "[hs_type, hs_type] \<Rightarrow> hs_type" ("'(_,/ _')")
  "_HsTyTuple3" :: "[hs_type, hs_type, hs_type] \<Rightarrow> hs_type" ("'(_,/ _,/ _')")
  "_HsTyTuple4" :: "[hs_type, hs_type, hs_type, hs_type] \<Rightarrow> hs_type"
    ("'(_,/ _,/ _,/ _')")
  "_HsTyTuple5" :: "[hs_type, hs_type, hs_type, hs_type, hs_type] \<Rightarrow> hs_type"
    ("'(_,/ _,/ _,/ _,/ _')")

translations
  "_HsQ _HsTyUnit" \<rightleftharpoons> (type) "one"
  "_HsQ (_HsTyTuple2 a b)"
    \<rightleftharpoons> "Tuple2 (_HsQ a) (_HsQ b)"
  "_HsQ (_HsTyTuple3 a b c)"
    \<rightleftharpoons> "Tuple3 (_HsQ a) (_HsQ b) (_HsQ c)"
  "_HsQ (_HsTyTuple4 a b c d)"
    \<rightleftharpoons> "Tuple4 (_HsQ a) (_HsQ b) (_HsQ c) (_HsQ d)"
  "_HsQ (_HsTyTuple5 a b c d e)"
    \<rightleftharpoons> "Tuple5 (_HsQ a) (_HsQ b) (_HsQ c) (_HsQ d) (_HsQ e)"

syntax
  "_HsUnit"      :: "hs_expb" ("'(')")
  "_HsTupleCon2" :: "hs_expb" ("'(,')")
  "_HsTupleCon3" :: "hs_expb" ("'(,,')")
  "_HsTupleCon4" :: "hs_expb" ("'(,,,')")
  "_HsTupleCon5" :: "hs_expb" ("'(,,,,')")

syntax
  "_HsTuple2" :: "[hs_expb, hs_expb] \<Rightarrow> hs_expb" ("'(_,/ _')")
  "_HsTuple3" :: "[hs_expb, hs_expb, hs_expb] \<Rightarrow> hs_expb" ("'(_,/ _,/ _')")
  "_HsTuple4" :: "[hs_expb, hs_expb, hs_expb, hs_expb] \<Rightarrow> hs_expb"
    ("'(_,/ _,/ _,/ _')")
  "_HsTuple5" :: "[hs_expb, hs_expb, hs_expb, hs_expb, hs_expb] \<Rightarrow> hs_expb"
    ("'(_,/ _,/ _,/ _,/ _')")

translations
  "_HsQ _HsUnit" \<rightleftharpoons> "ONE"
  "_HsQ _HsTupleCon2" \<rightleftharpoons> "TupleCon2"
  "_HsQ _HsTupleCon3" \<rightleftharpoons> "TupleCon3"
  "_HsQ _HsTupleCon4" \<rightleftharpoons> "TupleCon4"
  "_HsQ _HsTupleCon5" \<rightleftharpoons> "TupleCon5"

translations
   "_HsTuple2 a b" \<rightleftharpoons> "_HsApp (_HsApp _HsTupleCon2 a) b"
   "_HsTuple3 a b c" \<rightleftharpoons> "_HsApp (_HsApp (_HsApp _HsTupleCon3 a) b) c"
   "_HsTuple4 a b c d" \<rightleftharpoons>
     "_HsApp (_HsApp (_HsApp (_HsApp _HsTupleCon4 a) b) c) d"
   "_HsTuple5 a b c d e" \<rightleftharpoons>
     "_HsApp (_HsApp (_HsApp (_HsApp (_HsApp _HsTupleCon5 a) b) c) d) e"

term "\<guillemotleft>fst ((), (a,b,c,d,e))\<guillemotright>"

subsubsection {* List enumeration *}

consts
  "enumFrom" :: "\<guillemotleft>'a::Enum \<rightarrow> ['a]\<guillemotright>"
  "enumFromThen" :: "\<guillemotleft>'a::Enum \<rightarrow> 'a \<rightarrow> ['a]\<guillemotright>"
  "enumFromTo" :: "\<guillemotleft>'a::Enum \<rightarrow> 'a \<rightarrow> ['a]\<guillemotright>"
  "enumFromThenTo" :: "\<guillemotleft>'a::Enum \<rightarrow> 'a \<rightarrow> 'a \<rightarrow> ['a]\<guillemotright>"

syntax
  "_HsEnumFrom"       :: "hs_exp \<Rightarrow> hs_expb"                   ("[_ ..]")
  "_HsEnumFromThen"   :: "[hs_exp, hs_exp] \<Rightarrow> hs_expb"         ("[_, _ ..]")
  "_HsEnumFromTo"     :: "[hs_exp, hs_exp] \<Rightarrow> hs_expb"         ("[_ .. _]")
  "_HsEnumFromThenTo" :: "[hs_exp, hs_exp, hs_exp] \<Rightarrow> hs_expb" ("[_, _ .. _]")

  "_HsListComp"       :: "[hs_exp, hs_quals] \<Rightarrow> hs_expb"       ("[_ | _]")

ML {* Syntax.trace_ast := false *}
term "\<guillemotleft>\<lambda>x \<rightarrow> fst (f x)\<guillemotright>"

translations
  "_HsQ (_HsEnumFrom x)"
    \<rightleftharpoons> "enumFrom\<cdot>(_HsQ x)"
  "_HsQ (_HsEnumFromThen x y)"
    \<rightleftharpoons> "enumFromThen\<cdot>(_HsQ x)\<cdot>(_HsQ y)"
  "_HsQ (_HsEnumFromTo x y)"
    \<rightleftharpoons> "enumFromTo\<cdot>(_HsQ x)\<cdot>(_HsQ y)"
  "_HsQ (_HsEnumFromThenTo x y z)"
    \<rightleftharpoons> "enumFromThenTo\<cdot>(_HsQ x)\<cdot>(_HsQ y)\<cdot>(_HsQ z)"

term "\<guillemotleft>[2,5..12]\<guillemotright>"

instance lift :: (number_ring) Enum ..
(* TODO: definitions! *)


subsection {* Either type *}

domain ('a,'b) Either = Left (lazy "'a") | Right (lazy "'b")

subsection {* Maybe type *}

domain 'a Maybe = Nothing | Just (lazy "'a")


subsection {* Miscellaneous functions *}

syntax "_HsUndef" :: hs_expb  ("undefined")
translations "_HsQ _HsUndef" \<rightleftharpoons> "\<bottom>"

consts
  error :: "\<guillemotleft>String \<rightarrow> 'a\<guillemotright>"
  hs_d :: "\<guillemotleft>('a \<rightarrow> 'b) \<rightarrow> 'a \<rightarrow> 'b\<guillemotright>"

defs
  error_def: "error \<equiv> \<guillemotleft>\<lambda>s \<rightarrow> undefined\<guillemotright>"
  hs_d_def: "hs_d \<equiv> ID"

translations
  "_HsQ (_HsVarSym _hs_d)" \<rightleftharpoons> "hs_d"
  "_HsQ (_HsVarSym _hs_i)" \<rightleftharpoons> "cfcomp"
  "_HsQ (_HsInfix x _hs_i y)" \<leftharpoondown> "(_HsQ x) oo (_HsQ y)"

text {* List functions *}

consts
  hs_length :: "\<guillemotleft>['a] \<rightarrow> Int\<guillemotright>"
  zipWith :: "\<guillemotleft>('a \<rightarrow> 'b \<rightarrow> 'c) \<rightarrow> ['a] \<rightarrow> ['b] \<rightarrow> ['c]\<guillemotright>"
  map :: "\<guillemotleft>('a \<rightarrow> 'b) \<rightarrow> ['a] \<rightarrow> ['b]\<guillemotright>"
  foldl :: "\<guillemotleft>('a -> 'b -> 'a) -> 'a -> ['b] -> 'a\<guillemotright>"
  reverse :: "\<guillemotleft>['a] \<rightarrow> ['a]\<guillemotright>"
  filter :: "\<guillemotleft>('a \<rightarrow> Bool) \<rightarrow> ['a] \<rightarrow> ['a]\<guillemotright>"
  elem :: "\<guillemotleft>'a::Eq -> ['a] -> Bool\<guillemotright>"
  hs_pp :: "\<guillemotleft>['a] -> ['a] -> ['a]\<guillemotright>"

syntax "_hs_length" :: hs_expb ("length")

translations
  "_HsQ map" \<leftharpoondown> "Prelude.map"
  "_HsQ filter" \<leftharpoondown> "Prelude.filter"
  "_HsQ _hs_length" \<rightleftharpoons> "hs_length"
  "_HsQ (_HsVarSym _hs_pp)" \<rightleftharpoons> "hs_pp"

fixrec
  "\<guillemotleft>length [] = 0\<guillemotright>"
  "\<guillemotleft>length (x : xs) = 1 + length xs\<guillemotright>"

fixrec (permissive)
  "\<guillemotleft>zipWith f (a:as) (b:bs) = f a b : zipWith f as bs\<guillemotright>"
  "\<guillemotleft>zipWith f as bs = []\<guillemotright>"

fixrec
  "\<guillemotleft>map f [] = []\<guillemotright>"
  "\<guillemotleft>map f (x : xs) = f x : map f xs\<guillemotright>"

fixrec
  "\<guillemotleft>foldl f z [] = z\<guillemotright>"
  "\<guillemotleft>foldl f z (x:xs) = foldl f (f z x) xs\<guillemotright>"

defs reverse_def: "reverse \<equiv> \<guillemotleft>foldl (\<lambda> xs x \<rightarrow> x : xs) []\<guillemotright>"

fixrec
  "\<guillemotleft>filter p [] = []\<guillemotright>"
  "\<guillemotleft>filter p (x : xs) = if p x then x : filter p xs else filter p xs\<guillemotright>"

fixrec
  "\<guillemotleft>x `elem` [] = False\<guillemotright>"
  "\<guillemotleft>x `elem` (y : ys) = x == y || x `elem` ys\<guillemotright>"

fixrec
  "\<guillemotleft>[] ++ ys = ys\<guillemotright>"
  "\<guillemotleft>(x:xs) ++ ys = x : (xs ++ ys)\<guillemotright>"

end
