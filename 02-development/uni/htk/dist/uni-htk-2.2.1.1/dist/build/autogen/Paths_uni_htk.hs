module Paths_uni_htk (
    version,
    getBinDir, getLibDir, getDataDir, getLibexecDir,
    getDataFileName
  ) where

import Data.Version (Version(..))
import System.Environment (getEnv)

version :: Version
version = Version {versionBranch = [2,2,1,1], versionTags = []}

bindir, libdir, datadir, libexecdir :: FilePath

bindir     = "/Library/Frameworks/GHC.framework/Versions/7.0.4-i386/usr/bin"
libdir     = "/Library/Frameworks/GHC.framework/Versions/7.0.4-i386/usr/lib/uni-htk-2.2.1.1/ghc-7.0.4"
datadir    = "/Library/Frameworks/GHC.framework/Versions/7.0.4-i386/usr/share/uni-htk-2.2.1.1"
libexecdir = "/Library/Frameworks/GHC.framework/Versions/7.0.4-i386/usr/libexec"

getBinDir, getLibDir, getDataDir, getLibexecDir :: IO FilePath
getBinDir = catch (getEnv "uni_htk_bindir") (\_ -> return bindir)
getLibDir = catch (getEnv "uni_htk_libdir") (\_ -> return libdir)
getDataDir = catch (getEnv "uni_htk_datadir") (\_ -> return datadir)
getLibexecDir = catch (getEnv "uni_htk_libexecdir") (\_ -> return libexecdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
