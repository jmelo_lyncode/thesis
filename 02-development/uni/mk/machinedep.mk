# This file, included by Make, contains variables you may need to
# change for different installations of Uni.
#
# HC is the location of the GHC command
# CC is the location of the C compiler
# GFIND is the location of GNU find, hopefully.
# TOP is the top directory of the installation
# GHCTOP is the top directory, in a form suitable for passing to GHC
#  (so it should be the DOS form of the pathname for Windows)
# GHCPKG is the location of GHC's ghc-pkg command.
HC = /usr/bin/ghc
CC = gcc
TOP = /Users/melo/Desktop/Hybridization/uni
GHCTOP = @GHCTOP@
GFIND = find
CP = /bin/cp
LN = /bin/ln
MKDIR = /bin/mkdir
LD = @LD@
GHCPKG = @GHCPKG@
SED = /usr/bin/sed
WINDOWS = @WINDOWS@
GhcVersion = 7.0.4
GhcMajVersion = 7
GhcMinVersion = 0
TAR = /sw/bin/gtar
ZIP = /usr/bin/zip
UNIVERSION = @UNIVERSION@
OSTITLE = @OSTITLE@
GNUCLIENT = /usr/bin/gnuclient
HADDOCK = @HADDOCK@
