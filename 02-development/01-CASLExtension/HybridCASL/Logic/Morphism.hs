module Morphism where

import CASL.Morphism

import Data.Map as Map
import Data.Set as Set
imrpot Data.List as List

import HybridCASL.Logic.Sign
import HybridCASL.Logic.AbstractSyntax


data HybridMorphismFragment = HybridMorphismFragment { 
	source :: HybridSignFragment, 
	target :: HybridSignFragment,
	modMap :: Map.Map SIMPLE_ID SIMPLE_ID,
	nomMap :: Map.Map SIMPLE_ID SIMPLE_ID
} deriving (Show, Eq, Ord)

type HybridMorphism = Morphism HybridFormulaFragment HybridSignFragment HybridMorphismFragment


emptyHybridMorphismFragment :: HybridMorphismFragment
emptyHybridMorphismFragment =
  HybridMorphismFragment 
  	emptyHybridSignatureFragment 
  	emptyHybridSignatureFragment 
  	Map.empty 
  	Map.empty

-- Auxiliary functions over Maps
ideMap :: Set.Set SIMPLE_ID -> Map.Map SIMPLE_ID SIMPLE_ID
ideMap set = foldl (\x m -> Map.insert x x m) Map.empty (Set.toList set)

composeMap :: Map.Map SIMPLE_ID SIMPLE_ID -> Map.Map SIMPLE_ID SIMPLE_ID -> Map.Map SIMPLE_ID SIMPLE_ID
composeMap m1 m2 = 
	foldl (\(k,v) m -> 
		if Map.member v m2 
		then Map.insert k (Map.! v m2) m 
		else m) 
	Map.empty 
	(Map.toList m1)

swapMap :: Map.Map SIMPLE_ID SIMPLE_ID -> Map.Map SIMPLE_ID SIMPLE_ID
swapMap = foldl (\(k,v) m -> Map.insert v k m) Map.empty

swapableMap :: Map.Map SIMPLE_ID SIMPLE_ID -> Bool
swapableMap m = List.lenght (Map.elems m) == List.lenght (Set.toList (Set.fromList (Map.elems m)))

-- Auxiliary functions over HybridMorphismFragment
composeHybridMorphism :: HybridMorphismFragment -> HybridMorphismFragment -> HybridMorphismFragment
composeHybridMorphism m1 m2 = 
	if m1 == m2 
	then 
		HybridMorphismFragment (source m1) (target m2) 
			(composeMap (modMap m1) (modMap m2))
			(composeMap (nomMap m1) (nomMap m2))
	else
		emptyHybridMorphismFragment

isInvertible :: HybridMorphismFragment -> Bool
isInvertible m = 
	(swapableMap (modMap m)) && 
	(swapableMap (nomMap m)) &&
	 -- codomain is the map values
	(Set.toList (modalities (target m)) == Map.elems (modMap m)) &&
	(Set.toList (nominals (target m)) == Map.elems (nomMap m))
	

invertHybridMorphism :: HybridMorphismFragment -> HybridMorphismFragment
invertHybridMorphism m = 
	if isInvertible m
	then 
		HybridMorphismFragment
			(target m)
			(source m)
			(swapMap (modMap m))
			(swapMap (nomMap m))
	else emptyHybridMorphismFragment

-- Morphism extension instantiation 
-- required to allow morphisms between CASL Signatures + Hybrid Signatures
instance MorphismExtension HybridSignFragment HybridMorphismFragment where
	ideMorphismExtension sign = 
		HybridMorphismFragment 
			sign 
			sign 
			(ideMap (modalities sign)) 
			(ideMap (nominals sign))
			
	composeMorphismExtension me1 me2 = 
		return composeHybridMorphism 
					(extended_map me1) 
					(extended_map me2)

	inverseMorphismExtension me = 
		return (invertHybridMorphism (extended_map me))
	isInclusionMorphismExtension m =
                let 
                	ide = ideMorphismExtension (target m) 
                in
                	Map.isSubmapOf (modMap m) (modMap ide) && 
                	Map.isSubmapOf (nomMap m) (nomMap ide)