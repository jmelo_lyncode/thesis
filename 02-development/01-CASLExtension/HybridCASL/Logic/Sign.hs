module Sign where

-- Re-use of the CASL signature
import CASL.Sign

import Common.Id

import Data.Set

import HybridCASL.Logic.AbstractSyntax

{- Here one will distinguish signatures within the abstract syntax. -}

data HybridSignFragment = HybridSignFragment {
	modalities : Set SIMPLE_ID,
	nominals : Set SIMPLE_ID
} deriving (Eq, Ord, Show)

type HybridSign = Sign HybridFormulaFragment HybridSignFragment

emptyHybridSignatureFragment :: HybridSignFragment
emptyHybridSignatureFragment = HybridSignFragment empty empty

isSubHybridSign :: HybridSignFragment -> HybridSignFragment -> Bool
isSubHybridSign s1 s2 =
       	Set.isSubsetOf (modalities s1) (modalities s2)
        && Set.isSubsetOf (nominals s1) (nominals s2)