module Parser where

import Text.ParserCombinators.Parsec

import Common.Id
import Common.Parsec
import Common.Token
import Common.Lexer
import Common.AnnoState
import Common.Keywords
import Common.AS_Annotation

import CASL.AS_Basic_CASL
import CASL.Formula

import HybridCASL.Logic.AbstractSyntax

import Data.Maybe


hybrid_reserved_words :: [String]
hybrid_reserved_words = [ "modality", "modalities", "nominal", "nominals", atS ]


boxParser :: AParser st (SIMPLE_ID, Range)
boxParser = 
	do
    	open <- oBracketT
    	modal <- simpleId
    	close <- cBracketT
    	return (modal, (toRange open [] close))
    <|>
    do
    	open <- oBracketT
    	close <- cBracketT
    	let p = tokPos open
    	return Box (Token emptyS p) (toRange open [] close)

atParser :: AParser st (SIMPLE_ID, Range)
atParser =
	do
		asK <- asKey atS
		nom <- simpleId
		let p = tokPos asK
		return (nom, p)
		
formulaParser :: AParser st HybridFormulaFragment
formulaParser =
	do
		(sid, range) <- boxParser
		formula <- genPrimFormula formulaParser hybrid_reserved_words
		return Box sid formula range
	<|>
	do
		(sid, range) <- atParser
		formula <- genPrimFormula formulaParser hybrid_reserved_words
		return At sid formula range

instance TermParser HybridFormulaFragment where
    termParser = aToTermParser formulaParser


modalityKey :: AParser st Token
modalityKey = asKey "modality" <|> asKey "modalities"

nominalKey :: AParser st Token
nominalKey = asKey "nominal" <|> asKey "nominals"

basicItemParser :: AParser st HybridBasicItemFragment
basicItemParser =
        do k <- modalityKey
           (annoId, ks) <- separatedBy (annoParser simpleId) anSemiOrComma
           return ModalityDeclaration annoId (catRange (k : ks))
        <|>
        do k <- nominalKey
           (annoId, ks) <- separatedBy (annoParser simpleId) anSemiOrComma
           return NominalDeclaration annoId (catRange (k : ks))

instance AParsable HybridBasicItemFragment where
        aparser = basicItemParser