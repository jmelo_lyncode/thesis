module ATC where

import ATerm.Lib
import CASL.AS_Basic_CASL
import CASL.ATC_CASL
import CASL.Morphism
import CASL.Sign
import Common.AS_Annotation
import Common.Doc
import Common.DocUtils
import Common.Id
import Data.Typeable
import HybridCASL.Logic.AbstractSyntax
import HybridCASL.Logic.Sign
import HybridCASL.Logic.Morphism

import qualified Common.Lib.MapSet as MapSet
import qualified Data.List as List
import qualified Data.Map as Map
import qualified Data.Set as Set


{-! for HybridCASL.Logic.AbstractSyntax.HybridBasicItemFragment derive: Typeable !-}
{-! for HybridCASL.Logic.AbstractSyntax.HybridFormulaFragment derive: Typeable !-}
{-! for HybridCASL.Logic.Sign.HybridSignFragment derive: Typeable !-}
{-! for HybridCASL.Logic.Morphism.HybridMorphismFragment derive: Typeable !-}

{-! for HybridCASL.Logic.AbstractSyntax.HybridBasicItemFragment derive: ShATermConvertible !-}
{-! for HybridCASL.Logic.AbstractSyntax.HybridFormulaFragment derive: ShATermConvertible !-}
{-! for HybridCASL.Logic.Sign.HybridSignFragment derive: ShATermConvertible !-}
{-! for HybridCASL.Logic.Morphism.HybridMorphismFragment derive: ShATermConvertible !-}