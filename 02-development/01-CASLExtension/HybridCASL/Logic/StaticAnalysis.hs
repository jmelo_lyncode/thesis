module StaticAnalysis where

import HybridCASL.Logic.AbstractSyntax
import HybridCASL.Logic.Sign

import CASL.Sign
import CASL.MixfixParser
import CASL.StaticAna
import CASL.AS_Basic_CASL
import CASL.ShowMixfix
import CASL.Overload
import CASL.Quantification

import Common.AS_Annotation
import Common.GlobalAnnotations
import Common.Keywords
import Common.Lib.State
import Common.Id
import Common.Result
import Common.ExtSign
import Common.Lib.MapSet as MapSet

import Data.List as List
import Data.Map as Map
import Data.Set as Set

instance TermExtension HybridFormulaFragment where
        freeVarsOfExt sign ( Box _ f _ ) = freeVars sign f
        freeVarsOfExt sign ( At _ f _ ) = freeVars sign f


basicHybridAnalysis
        :: (HybridBasicSpecification, HybridSign, GlobalAnnos)
        -> Result (HybridBasicSpecification, ExtSign HybridSign Symbol, [Named HybridFormula])
basicHybridAnalysis =
		basicAnalysis formulaTypeAnalysis basicItemStaticAnalysis signatureItemAnalysis mixfixAnalysis

-- Auxiliary functions

		
-- Here one will verify that the used IDs where already declared		
formulaTypeAnalysis :: HybridSign -> HybridFormulaFragment -> Result HybridFormulaFragment
formulaTypeAnalysis sign (Box md f pos) =
	if Map.member md (modalities (extendedInfo sign))
	then
		do
			newf <- minExpFORMULA formulaTypeAnalysis sign f
			return (Box md newf pos)
	else Result [mkDiag Error "unknown modality" md] (Just (Box md f pos))
formulaTypeAnalysis sign (At nm f pos) =
	if Map.member nm (nominals (extendedInfo sign))
	then
		do
			newf <- minExpFORMULA formulaTypeAnalysis sign f
			return (At nm newf pos)
	else Result [mkDiag Error "unknown nominal" nm] (Just (At nm f pos))


{-
 type Ana a b s f e = Mix b s f e -> a -> State (Sign f e) a
 data Mix b s f e = MixRecord
    { getBaseIds :: b -> IdSets -- ^ ids of extra basic items
    , getSigIds :: s -> IdSets  -- ^ ids of extra sig items
    , getExtIds :: e -> IdSets  -- ^ ids of signature extensions
    , mixRules :: (TokRules, Rules) -- ^ rules for Earley
    , putParen :: f -> f -- ^ parenthesize extended formula
    , mixResolve :: MixResolve f -- ^ resolve extended formula
    }
-}
basicItemAnalysis :: 
	Mix HybridBasicItemFragment () HybridFormulaFragment HybridSignFragment -> 
	HybridBasicItemFragment -> 
	State HybridSign HybridBasicItemFragment
basicItemAnalysis mix (ModalityDeclaration ids pos) = 
	do
		mapM_ (updateExtInfo addModality) ids
		return (ModalityDeclaration ids pos)
basicItemAnalysis mix (NominalDeclaration ids pos) = 
	do
		mapM_ (updateExtInfo addNominal) ids
		return (NominalDeclaration ids pos)
		
addModality :: Annoted SIMPLE_ID -> HybridSignFragment -> Result HybridSignFragment
addModality ansid sign = 
	if Set.member (item ansid) (modalities sign)
	then
		Result [mkDiag Hint "repeated modality" (item ansid)] (Just sign)
	else
		return sign { modalities = Set.insert (item ansid) (modalities sign) }

addNominal :: Annoted SIMPLE_ID -> HybridSignFragment -> Result HybridSignFragment
addNominal ansid sign = 
	if Set.member (item ansid) (nominals sign)
	then
		Result [mkDiag Hint "repeated nominal" (item ansid)] (Just sign)
	else
		return sign { nominals = Set.insert (item ansid) (nominals sign) }

{-
 type Ana a b s f e = Mix b s f e -> a -> State (Sign f e) a
 data Mix b s f e = MixRecord
    { getBaseIds :: b -> IdSets -- ^ ids of extra basic items
    , getSigIds :: s -> IdSets  -- ^ ids of extra sig items
    , getExtIds :: e -> IdSets  -- ^ ids of signature extensions
    , mixRules :: (TokRules, Rules) -- ^ rules for Earley
    , putParen :: f -> f -- ^ parenthesize extended formula
    , mixResolve :: MixResolve f -- ^ resolve extended formula
    }
-}
signatureItemAnalysis ::
	Mix HybridBasicItemFragment () HybridFormulaFragment HybridSignFragment -> 
	() -> 
	State HybridSign ()
signatureItemAnalysis = const return
-- not needed

{- type IdSets = ((Set.Set Id, Set.Set Id), Set.Set Id) -- ops are first component -}
mixfixAnalysis :: Mix HybridBasicItemFragment () HybridFormulaFragment HybridSignFragment
mixfixAnalysis = emptyMix {
	putParen = parenHybridFormula,
	mixResolve = resolveHybridFormula
}


parenHybridFormula :: HybridFormulaFragment -> HybridFormulaFragment
parenHybridFormula (Box md f pos) = Box md (mapFormula parenHybridFormula f) pos
parenHybridFormula (At nm f pos) = Box nm (mapFormula parenHybridFormula f) pos

-- type MixResolve f = GlobalAnnos -> (TokRules, Rules) -> f -> Result f
resolveHybridFormula :: MixResolve HybridFormulaFragment
resolveHybridFormula ga ids (Box md f pos) = do
	nf <- resolveMixFrm parenHybridFormula resolveHybridFormula ga ids f
	return (Box md nf pos)
resolveHybridFormula ga ids (At nm f pos) = do
	nf <- resolveMixFrm parenHybridFormula resolveHybridFormula ga ids f
	return (At nm nf pos)	

-- Auxiliary (Não acho necessário)
{-
mkSingletonId t = mkId [t]

analyzeHybridFormula :: 
	Mix HybridBasicItemFragment () HybridFormulaFragment HybridSignFragment -> 
	HybridFormula -> State HybridSign (HybridFormula, HybridFormula)
analyzeHybridFormula mix f =
	do
		let ps = map (mkSingletonId) (Set.toList (getFormulaPredicateTokens f))
		pm <- gets predMap
		mapM_ (addPred (emptyAnno ()) $ PredType []) ps
        newGa <- gets globAnnos
		let Result es m = resolveFormula parenHybridFormula resolveHybridFormula newGa (mixRules mix) f
		addDiags es
		e <- get
		phi <- case m of
			Nothing -> return (f, f)
			Just r -> do
				n <- resultToState (minExpFORMULA frmTypeAna e) r
				return (r, n)
		e2 <- get
		put e2 {predMap = pm}
		return phi

getFormulaPredicateTokens :: HybridFormula -> Set.Set Token
getFormulaPredicateTokens frm = case frm of
    Quantification _ _ f _ -> getFormulaPredicateTokens f
    Conjunction fs _ -> Set.unions $ map getFormulaPredicateTokens fs
    Disjunction fs _ -> Set.unions $ map getFormulaPredicateTokens fs
    Implication f1 f2 _ _ ->
        Set.union (getFormulaPredicateTokens f1) $ getFormulaPredicateTokens f2
    Equivalence f1 f2 _ ->
        Set.union (getFormulaPredicateTokens f1) $ getFormulaPredicateTokens f2
    Negation f _ -> getFormulaPredicateTokens f
    Mixfix_formula (Mixfix_token t) -> Set.singleton t
    Mixfix_formula t -> getTermPredicateTokens t
    ExtFORMULA (Box _ f _) -> getFormulaPredicateTokens f
    ExtFORMULA (At _ f _ ) -> getFormulaPredicateTokens f
    Predication _ ts _ -> Set.unions $ map getTermPredicateTokens ts
    Definedness t _ -> getTermPredicateTokens t
    Existl_equation t1 t2 _ ->
        Set.union (getTermPredicateTokens t1) $ getTermPredicateTokens t2
    Strong_equation t1 t2 _ ->
        Set.union (getTermPredicateTokens t1) $ getTermPredicateTokens t2
    Membership t _ _ -> getTermPredicateTokens t
    _ -> Set.empty
    
getTermPredicateTokens :: TERM HybridFormulaFragment -> Set.Set Token
getTermPredicateTokens trm = case trm of
    Application _ ts _ -> Set.unions $ map getTermPredicateTokens ts
    Sorted_term t _ _ -> getTermPredicateTokens t
    Cast t _ _ -> getTermPredicateTokens t
    Conditional t1 f t2 _ -> Set.union (getTermPredicateTokens t1) $
        Set.union (getFormPredToks f) $ getTermPredicateTokens t2
    Mixfix_term ts -> Set.unions $ map getTermPredicateTokens ts
    Mixfix_parenthesized ts _ -> Set.unions $ map getTermPredicateTokens ts
    Mixfix_bracketed ts _ -> Set.unions $ map getTermPredicateTokens ts
    Mixfix_braced ts _ -> Set.unions $ map getTermPredicateTokens ts
    _ -> Set.empt
-}
