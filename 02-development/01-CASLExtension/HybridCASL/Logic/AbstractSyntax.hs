module AbstractSyntax where

{-

Hybrid Abstract Syntax definition module.

-}

-- Importing Identifiers (Common concept in HETS) [4, I.2.6]
import Common.Id
-- Annotations are used to help HETS [4, II.5]
import Common.AS_Annotation
-- One will base our syntactical definition upon the already defined CASL Syntax.
import CASL.AS_Basic_CASL


type HybridBasicSpecification = BASIC_SPEC HybridBasicItemFragment HybridSignatureItemFragment HybridFormulaFragment

data HybridBasicItemFragment = ModalityDeclaration [Annoted SIMPLE_ID] Range
                             | NominalDeclaration [Annoted SIMPLE_ID] Range
                               deriving Show


type HybridFormula = FORMULA HybridFormulaFragment
data HybridFormulaFragment = Box SIMPLE_ID HybridFormula Range
                           | At SIMPLE_ID HybridFormula Range
                             deriving (Eq, Ord, Show)