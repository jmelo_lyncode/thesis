module Logic where

import Logic.Logic as AbstractLogic

import CASL.AS_Basic_CASL
import CASL.Parse_AS_Basic
import CASL.SymbolParser (symbItems, symbMapItems)

import HybridCASL.Logic.AbstractSyntax
import HybridCASL.Logic.Parser

-- data structure to be used for functional dependencies, determining specific Logic parameters.
data HybridCASL = HybridCASL deriving Show

instance Language HybridCASL where
	description _ = "Hybrid Logic"
	
instance Syntax HybridCASL HybridBasicSpecification SYMB_ITEMS SYMB_MAP_ITEMS where
        parse_basic_spec HybridCASL = Just (basicSpec hybrid_reserved_words)
        parse_symb_items HybridCASL = Just (symbItems hybrid_reserved_words)
        parse_symb_map_items HybridCASL = Just (symbMapItems hybrid_reserved_words)
        
