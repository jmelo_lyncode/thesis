module HybridCASL where

{-

References:

	[1] João Melo. Hybrid CASL. 2012
	[2] João Melo. Extending casl by comorphism : A more pratical approach. 2012.
	[3] Alexandre Madeira, José M. Faria, Manuel A. Martins, and Luís S. Barbosa. Hybrid specification of reactive systems: An institutional approach. 2011.
	[4] Peter Mosses, editor. CASL Reference Manual. Springer, 2004.

Within the package HybridCASL one could find the HybridCASL instantiation of the Logic class. Also an instantiation of a Comorphism from HybridCASL to CASL. 


-}

