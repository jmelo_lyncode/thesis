\documentclass[10pt,a4paper]{article}
\usepackage[utf8x]{inputenc}
\usepackage{ucs}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{a4wide}
\usepackage{float}
\usepackage{url}
\usepackage{fancybox}
\usepackage[sort&compress,square,numbers]{natbib}

% Some commands
\newcommand{\abs}[1]{\left| #1 \right|} % for absolute value
\newcommand{\avg}[1]{\left< #1 \right>} % for average
\newcommand{\mybox}[1]{\vspace{10px}\doublebox{\begin{minipage}{4.7in}#1\end{minipage}}\vspace{10px}}
\newcommand{\figura}[1]{Figure \ref{#1}}
\newcommand{\codigo}[1]{\footnotesize#1\normalsize}


\lstset{
	language=Haskell,
	frame=single,
	basicstyle=\footnotesize
}

\author{João Melo}
\title{Extending CASL by Comorphisms\\\normalsize{A more pratical approach}}
\begin{document}

\maketitle

\abstract{The purpose of this paper is to give a more pratical approach of how highly theoretical concepts (Institution Theory) are implemented in HETS, it does not provide a specific implementation, but tries to simplify some theoretical concepts, localizing that concepts within the HETS code. Also tries to simplify some core aspects of the HETS implementation. The main idea is to maximize the re-use the CASL Logic implementation, which means extending CASL (by Comorphism).}

\section{Introduction}

The importance of formal verification of automated systems (that is, verifying the logic beneath) and more properly the approach of developing correct systems by construction (before the system implementation, specifying a conceptual logical model of it) is growing everyday\cite{constructing, formalverification, softwareabs}. This importance is explained by the costs of having problems in automated systems, some of them unrecoverable.

The Common Algebraic Specification Language (CASL) defined by CoFI\footnote{Common Framework Initiative} allows one to write, at a basic level, theorems in First Order Logic (FOL)\cite{caslrm}.

\mybox{
A system specification is a theorem (that must be proved). Within this context, it's important to distinguish between theory and theorem. A theory could be seen as a logical tool (so one assume as a logical truth construct) used to prove theorems\cite{habil, hetspaper, hetspaper1, theory, foundationsmathlogic}.}

The special thing about CASL is his definition based upon Institution Theory\cite{institutions, institutions1}.
Although CASL has concepts like structured specifications, architectural specifications and also libraries, one will, for simplification purposes, ignore those concepts and focus only at the basic specifications level, further reading is advisable \cite{caslrm, hetspaper, habil, hetspaper1}.

Heterogeneous Tool Set (HETS) is a tool, implemented in Haskell, for proving CASL written theorems, but not only, HETS gives a real evidence of the power beneath the concept of Institutions, that is, the implementation design follows the Institution concepts. Within HETS is possible to, in a simplified way, extend the CASL logic\cite{habil, usermanual, caslrm, hetspaper1, hetspaper} to allow extending the basic expressiveness of CASL, the target is to explain and discuss the implementation of the CASL logic giving some guidelines of how one could extend it.

\paragraph{Why extend CASL?}

Extending CASL means using an already defined (and well structured) language that allows one to express theorems in FOL. From this point of view one could extend CASL with others logical constructs so then one could express theorems in another logic. For example, adding Modal constructs allows one to write theorems using Modal Logic\cite{modalhandbook}. \\

One assume that the reader have a good understanding of Haskell\cite{haskell}, also as a good understanding of the CASL abstract syntax\cite{caslrm}.

\section{Institutions}

Institutions are an abstraction over the concept of logic, using as background the theory of Categories\cite{categories, categories1}. One will give the formal definition and then an informal and simplified one.

\theoremstyle{definition}
\newtheorem{institution}{Definition}
\begin{institution}
An institution $I = (\textnormal{\textbf{Sign}}, \textnormal{\textbf{Sen}}, \textnormal{\textbf{Mod}}, \vDash)$ consists of
\begin{itemize}
	\item a Category $\textnormal{\textbf{Sign}}$ of signatures,
	\item a Functor $\textnormal{\textbf{Sen}} : \textnormal{\textbf{Sign}} \rightarrow \textnormal{\textbf{Set}}$ giving, for each signature $\Sigma$ the set of sentences $\textnormal{\textbf{Sen}} (\Sigma)$, and for each signature morphism $\sigma: \Sigma \rightarrow \Sigma'$, the sentence translation map $\textnormal{\textbf{Sen}} (\sigma) : \textnormal{\textbf{Sen}} (\Sigma) \rightarrow \textnormal{\textbf{Sen}} (\Sigma')$, where often $\textnormal{\textbf{Sen}} (\sigma)(\varphi)$ is written as $\sigma(\varphi)$,
	\item a Functor $\textnormal{\textbf{Mod}} : \textnormal{\textbf{Sign}}^{op} \rightarrow \textnormal{\textbf{CAT}}$ giving, for each signature $\Sigma$, the category of models $\textnormal{\textbf{Mod}}(\Sigma)$, and for each signature morphism $\sigma : \Sigma \rightarrow \Sigma'$, the reduct functor $\textnormal{\textbf{Mod}}(\sigma) : \textnormal{\textbf{Mod}}(\Sigma') \rightarrow \textnormal{\textbf{Mod}}(\Sigma)$, where often $\textnormal{\textbf{Mod}}(\sigma)(M')$ is written as $M'\mid_\sigma$,
	\item a satisfaction relation $\vDash_{\Sigma} \subseteq \abs{\textnormal{\textbf{Mod}} (\Sigma)} \times \textnormal{\textbf{Sen}} (\Sigma)$ for each $\Sigma \in \textnormal{\textbf{Sign}}$,
\end{itemize}

such that for each $\sigma : \Sigma \rightarrow \Sigma'$ in $\textnormal{\textbf{Sign}}$ the following satisfaction condition holds:
\begin{center}
	$M' \vDash_{\Sigma'} \sigma(\varphi) \Leftrightarrow M'\mid_{\sigma} \vDash_{\Sigma} \varphi$
\end{center}

for each $M' \in \textnormal{\textbf{Mod}}(\Sigma')$ and $\varphi \in \textnormal{\textbf{Sen}}(\Sigma)$. Within this context one could define an arbitrary logic as a Institution equipped with an entailment system\cite{whatislogic, logicproof,institutionmodeltheory}.
\end{institution}

Lets forget all the above formalism and, in an attempt of simplifying those concepts, define them using less abstract terms.
Note that, this informal explanation must be seen as an incomplete simplification, and so, not a correct way of reasoning about Institutions, the main reason is, there can be Institutions with a much different point of view of Signatures, Sentences and Models then the presented below.

\paragraph{Signatures}

Imagine Signatures as a set of Symbols. 
For example, in Haskell a signature could be a function name\footnote{In Haskell one only need the function name (and sometimes also the module name/alias to univocally identify a function, so for simplification purposes let's assume that one only need the function name)}, aswell as sort names (\textbf{Int})\cite{foundationsmathlogic, haskell}.

\begin{lstlisting}
mult :: Int -> Int -> Int
\end{lstlisting}

A signature morphism transform symbols into anothers, imagine that one have a new symbol.

\begin{lstlisting}
extMult :: Int -> Int -> Int
\end{lstlisting}

One could establish a signature morphism where \codigo{mult} symbol is replaced by \codigo{extMult} symbol.


\paragraph{Sentences}

Following the above simplification, Sentences could be seen as a tree of symbols, where each tree node is a symbol (a signature). A parent node and his childs in the tree evidences a sentence constructor.

A signature morphism, changes those trees by replacing the symbols (nodes) in them by other symbols. 
\paragraph{Model}

Models have to interpret symbols, giving them some meaning, allowing that interpretation to be calculable by some algebraic structure. For example, in Haskell the possible models would use the Integer mathematical set (\textit{Carrier Set}) to define each sort symbol \textbf{Int} and a specific function implementation to the function name ``mult'' and ``extMult''. 

\mybox{One could distinguish two big domains in the Institution definition. The syntactical domain (Signatures and Sentences) $-$ where one can find structures of symbols without meaning $-$ and the semantical domain (Models) where one could give meaning for the syntactical domain.}

Then HETS would evaluate the truth of the written theory using an semantical entailment system for that Models\citep{habil, whatislogic}.

\section{CASL Implementation Overview}

HETS implementation of CASL logic is open source, and is already prepared to be extended. One would try to map the theoretical concepts (within Institution) to concrete implementation pieces of code.

\subsection{Signatures}

In \citep[Part III]{caslrm} one have.

\begin{quotation}
``A many-sorted signature $\Sigma$ consists of: a set of sorts; separate families of sets of total and partial function symbols, indexed by function profile (a sequence of argument sorts and a result sort – constants are treated as functions with no arguments); and a family of sets of predicate symbols, indexed by predicate profile (a sequence of argument sorts). Constants and functions are also referred to as operations.''
\end{quotation}

A CASL $\textnormal{\textbf{Sign}}$ is a pair $(\textnormal{SignSymbols}, \textnormal{Morphism})$ and is also parametrized with extended sentences constructors (f) and extended signature symbols (e). So lets dive into de code.

\paragraph{Note} Reader must be aware that the listed pieces of code are retrieved from the HETS source code\footnote{The HETS source code could be found at \url{https://svn-agbkb.informatik.uni-bremen.de/Hets/trunk}} and modified (for presentation purposes).

\begin{lstlisting}
-- CASL/Sign.hs
data Sign f e = Sign
    { opMap :: OpMap -- operations map
    , predMap :: PredMap  -- predicates map
    , sortRel :: Rel.Rel SORT -- every sort is a subsort of itself
    , extendedInfo :: e  -- Extended information
    } deriving Show
\end{lstlisting}

It is important do distinguish between the $\textnormal{\textbf{Sign}}$ Category and the Sign data structure within HETS, the data structure must be seen only as a bag of all CASL Symbols (a Category is an abstract concept that also contains signatures morphisms). It's also possible to see that the listed code does not need for sentences constructor (f) parameter to be defined, remember that the fragment of code listed is not complete. 

A operation profile is a pair with a list of argument sorts and the returning sort. One should be also able to distinguish between total and partial operations. Operations (or functions) and Predicates are translated into the following Haskell data structures. 

It is important to understand that predicates are not operations returning boolean values, as one could see in \cite{usermanual}:

\begin{quotation}
``... predicates are syntactically different from boolean-valued operations, and are used to form atomic formulas rather than terms. In CASL, a declared predicate symbol is interpreted as a relation on (i.e., a subset of) the Cartesian product of the carrier sets of the argument sorts. An application of a predicate is said to hold when the tuple of arguments is in the relation.''
\end{quotation}

\begin{lstlisting}
-- CASL/Sign.hs
type OpMap = MapSet.MapSet OP_NAME OpType
data OpType = OpType { 
			opKind :: OpKind, -- Partial or Total
			opArgs :: [SORT], 
			opRes :: SORT
		} deriving (Show, Eq, Ord)
              
type PredMap = MapSet.MapSet PRED_NAME PredType
data PredType = PredType {predArgs :: [SORT]} 
		deriving (Show, Eq, Ord)

\end{lstlisting}

The set of sorts are given by a Map to Sets of sorts. A Sort could be seen as subsort or a supersort of other sort, and always is a subsort of itself.

\begin{lstlisting}
-- Common/Lib/Rel.hs
newtype Rel a = Rel { toMap :: Map.Map a (Set.Set a) } 
	deriving (Eq, Ord)
\end{lstlisting}

A Signature morphism \citep[Part III]{caslrm} is defined as:

\begin{quotation}
A many-sorted signature morphism $\sigma : \Sigma \rightarrow \Sigma'$ maps symbols in $\Sigma$ to symbols in $\Sigma'$. A partial function symbol may be mapped to a total function symbol, but not vice versa.
\end{quotation}

in HETS, a CASL morphism can be seen as a Map between symbols.

\begin{lstlisting}
-- CASL/Morphism.hs
data Morphism f e m = Morphism
  { sort_map :: Sort_map
  , op_map :: Op_map
  , pred_map :: Pred_map
  , extended_map :: m
  } deriving (Show, Eq, Ord)
  
type Sort_map = Map.Map SORT SORT
type Op_map = Map.Map (Id, OpType) (Id, OpKind)
type Pred_map = Map.Map (Id, PredType) Id
\end{lstlisting}


\subsection{Sentences}

A CASL Sentence is defined as\citep[Part III]{caslrm}:

\begin{quotation}
``The many-sorted sentences in $\textnormal{\textbf{Sen}}(\Sigma)$ are sort-generation constraints (...) and the usual closed many-sorted first-order logic formulas, built from atomic formulas (application of qualified predicate symbols to argument terms of appropriate sorts, assertions about the definedness of fully-qualified terms, and existential and strong equations between fully-qualified terms of the same sort) using quantification and logical connectives. Predicate application, existential equations, implication and universal quantification are taken as primitive, the other forms being regarded as derived.''
\end{quotation}

In CASL, all sentences are derived from atomic formulas, the CASL atomic formulas could be seen below.

\begin{lstlisting}
-- CASL/AS_Basic_CASL.hs
data FORMULA f = 
	Quantification QUANTIFIER [VAR_DECL] (FORMULA f) Range
	-- pos: QUANTIFIER, semi colons, dot
	| Conjunction [FORMULA f] Range
	-- pos: "/\"s
	| Disjunction [FORMULA f] Range
	-- pos: "\/"s
	| Implication (FORMULA f) (FORMULA f) Bool Range
	-- pos: "=>" or "if" (False -> "=>")
	| Negation (FORMULA f) Range
	-- pos: not
	| Predication PRED_SYMB [TERM f] Range
	-- pos: opt. "(",commas,")"
	| Existl_equation (TERM f) (TERM f) Range
	-- pos: =e=
	| Strong_equation (TERM f) (TERM f) Range
	-- pos: =
	| Membership (TERM f) SORT Range
	-- pos: in
	| ExtFORMULA f
	-- needed for CASL extensions
		deriving (Show, Eq, Ord)
\end{lstlisting}

These are the basic Sentence constructurs. So far from now, one have, the Signatures in CASL defined and also basic Sentence constructors.

\subsection{Models}

A CASL Model is\citep[Part III]{caslrm}:

\begin{quotation}
``For a many-sorted signature $\Sigma$, a many-sorted model $M \in \textnormal{\textbf{Mod}}(\Sigma)$ assigns a non-empty carrier set to each sort in $\Sigma$, a partial resp. total function to each partial resp. total function symbol, and a predicate to each predicate symbol...''
\end{quotation}

As stated before, Models assign meaning to Signatures and Sentences. Giving meaning is a conceptual operation, this step only matters when one needs to reasoning about the written theory. CASL operation definitions take place in the specification, so building a model represents associating to each operation symbol a specific specification of that operation. The CASL implementation of the Model builder is complex. For example,

\begin{lstlisting}
-- CASL/AS_Basic_CASL.hs
data VAR_DECL = Var_decl [VAR] SORT Range
                -- pos: commas, colon
                deriving (Show, Eq, Ord)
\end{lstlisting}

With this data structure is possible to associate to each variable a specific Sort, an so a \textit{carrier set} of possible values. At the same file (CASL/AS Basic CASL.hs) one could find data structures that will be used to build a CASL Model.

\section{Extending CASL}
Within this paper the main target is to explain how one could extend CASL by a Comorphism. Comorphism is a special case of an Institutions Morphism\cite{instmorph, differentarrows}. The taxonomy of possible Institution Morphisms could be found at \citep[p. 29]{habil}. One will focus only on the Comorphisms of the CASL Institution. Generically, defining a Comorphism, means defining the following arrows,

\begin{center}
\begin{tabular}{c c c}
$\textnormal{\textbf{Sign}}$ & $\longrightarrow$ & $\textnormal{\textbf{Sign}}'$ \\
$\textnormal{\textbf{Sen}}$ & $\longrightarrow$ & $\textnormal{\textbf{Sen}}' \circ \Phi$ \\
$\textnormal{\textbf{Mod}}$ & $\longleftarrow$ & $\textnormal{\textbf{Mod}}' \circ \Phi$
\end{tabular}

\end{center}

where $\Phi : \textnormal{\textbf{Sign}} \rightarrow \textnormal{\textbf{Sign}}'$. That is,

\begin{itemize}
	\item Starting at \textnormal{\textbf{Sign}} and producing a new \textnormal{\textbf{Sign}}';
	\item Defining a new Functor \textnormal{\textbf{Sen}}' applicable to Signatures and Morphisms in \textnormal{\textbf{Sign}}', that generates Sentences in the targeted Institution;
	\item Defining a new Functor \textnormal{\textbf{Mod}}' applicable to Signatures and Morphisms in \textnormal{\textbf{Sign}}', that generate Models in the Source Institution; 
\end{itemize}

\mybox{\paragraph{Why extending CASL by Comorphisms?} The Institution formalism distinguish two big domains when building a logic, the syntactical domain and the semantical domain of the logic. Using this and because nowadays developing software means code re-using, extending an Institution formalism based implemented Logic (CASL) allows one (with a Comorphism) to re-use all the implementation of the Model and Proof system, but not only, as mentioned, one could also use the already defined Signatures and Sentences, only adding more Signatures and Sentence constructs. }

\subsection{Theoretical in Practice}

Let's ignore for a moment the HETS workflow, and implementation specifics and focus at the theoretical concepts, giving a more pratical approach. \\
An Institution Comorphism $\rho^{\textnormal{\textbf{C2L}}}$\footnote{One will use $\textnormal{\textbf{C2L}}$ to express CASL to Logic, where Logic could be any possible Logic.} from CASL to another Logic is a tuple:

\begin{center}
$\rho^{\textnormal{\textbf{C2L}}} = (\Phi^{\textnormal{\textbf{C2L}}}, \alpha^{\textnormal{\textbf{C2L}}}, \beta^{\textnormal{\textbf{C2L}}})$
\end{center}

where:

\begin{itemize}
	\item $\Phi^{\textnormal{\textbf{C2L}}}$ represents a $\textnormal{\textbf{Sign}}$ functor;
	\item $\alpha^{\textnormal{\textbf{C2L}}}$ represents a transformation between sentences;
	\item $\beta^{\textnormal{\textbf{C2L}}}$ represents a transformation between models;
\end{itemize}

\subsubsection{Functor $\Phi^{\textnormal{\textbf{C2L}}}$}

Suppose one have already defined the CASL $\textnormal{\textbf{Sign}}$ as $\textnormal{\textbf{Sign}}^{CASL}$ and the targeted logic (the CASL extension) $\textnormal{\textbf{Sign}}$ as $\textnormal{\textbf{Sign}}^{Logic}$, the Functor $\Phi^{\textnormal{\textbf{C2L}}}$ could be defined as,

\begin{center}
$\Phi^{\textnormal{\textbf{C2L}}} : \textnormal{\textbf{Sign}}^{CASL} \rightarrow \textnormal{\textbf{Sign}}^{Logic} \Leftrightarrow$

$\{$ $\textnormal{\textbf{Sign}}^{Logic}$ extends of the $\textnormal{\textbf{Sign}}^{CASL}$ by a fragment ($\Delta^{Logic}$) $\}$   

$\Leftrightarrow \Phi^{\textnormal{\textbf{C2L}}} : \textnormal{\textbf{Sign}}^{CASL} \rightarrow \textnormal{\textbf{Sign}}^{CASL}  \cup \Delta^{Logic}$
\end{center}

Informally, creating this functor means, adding more symbols, but also define the extended morphisms. For example, imagine one want to add modalities - Modal Logic\cite{modalhandbook}. 

\begin{lstlisting}
import CASL/Sign

data SignFragment = SignFrament {
	modalities : Set Id
} deriving (Show, Eq, Ord)
type ModalSign = Sign () SignFragment

data ExtendedMorphism = ExtendedMorphism { 
	mod_map :: Map.Map Id Id
} deriving (Show, Eq, Ord)
        
type MorphismExtension = 
	MorphismExtension ModalSign ExtendedMorphism
\end{lstlisting}

\subsubsection{Transformation $\alpha^{\textnormal{\textbf{C2L}}}$}

The formal definition is:

\begin{center}
$\alpha^{\textnormal{\textbf{C2L}}} :
	 \textnormal{\textbf{Sen}}^{CASL} \rightarrow \textnormal{\textbf{Sen}}^{Logic} \circ \Phi^{\textnormal{\textbf{C2L}}}$
\end{center}

That is, one must define a new set of sentence constructs and sentence translators from the CASL logic to the Extended Logic, this sentence constructs use Signatures within the newly defined $\textnormal{\textbf{Sign}}^{Logic}$, one could also re-use the CASL sentence constructs. For example, for the Modal Logic, one must add the following Modal Sentence constructs,

\begin{lstlisting}
data ModalFORMULA = Box MODALITY (ExtendedFormula) Range 
		  | Diamond MODALITY (ExtendedFormula) Range 
			deriving (Eq, Ord, Show)

type ExtendedFormula = FORMULA ModalFORMULA
\end{lstlisting}

As one could see, this implementation takes use of CASL Formulas, that is, the CASL sentence constructs defining a new and extended sentence constructs.

\subsubsection{Transformation $\beta^{\textnormal{\textbf{C2L}}}$}

The model transformation is formally defined as:

\begin{center}
$\beta^{\textnormal{\textbf{C2L}}} : \textnormal{\textbf{Mod}}^{Logic} \rightarrow \textnormal{\textbf{Mod}}^{CASL} \circ \Phi^{\textnormal{\textbf{C2L}}}$
\end{center}

The final step in establishing a Institution Comorphism is building a model in the CASL logic. From here one are able to re-use the satisfaction and entailment system for the CASL Logic. Thus, one take the meaning of sentences in the Extended Institution and build a Model in the CASL Institution.

\subsection{HETS internals}

At this point, one have given an idea of what is, in practice, defining an Institution Comorphism in HETS. Now, one must glue the separate parts of implementation together to allow HETS use the newly defined Logic.

The point here is to understand how HETS internally works, mainly the abstract workflow for implementing a Logic. HETS is a tool for theory proving. Basically it receives as input a raw text file (expected to be written in a specific logic) and then produces a Development Graph\cite{habil, developmentgraphs} based on the generated proof obligations. Then take advantage of various automatic theorem provers to obtain proof, allowing (GUI interface) the user to choose what prover to apply to each proof obligation. 

\begin{figure}[H]
\centering
\includegraphics[scale=0.7]{images/image2.png}
\caption{HETS internal workflow}
\label{fig:workflow}
\end{figure}

As one could see in \figura{fig:workflow} the process starts with parsing the raw file into an intermediate data structure, that intermediate representation is then validated with a static analysis process. After this time one should have a theory, that is, a pair (Signatures, Axioms). After that, is possible to pass this pair as argument to the entailment system, to model checkers, consistency checkers, etc.

\newpage
\subsection{Implementation Overview}

At this point is important to have a good syntactical and semantical view of the new Logic\footnote{Often one refer Logic and Institution as the same concept, because it obvious that in the HETS context every defined Institution also defines an entailment system, to allow one to prove written theorems.}. Defining the language grammar is an important step, mainly when that definition is based on the CASL grammar, then one could use the CASL implementation and extend it. The CASL grammar is complex, it was developed under the target of defining a common way of writing specifications\cite{caslrm, usermanual}.
 
In \figura{fig:workflow} one can distinguish four major steps within a Logic implementation:

\begin{enumerate}
	\item Defining the Abstract Syntax
	\item Implementing the Parser
	\item Defining Signatures and Morphisms (between them)
	\item Implementing the Static Analysis verification
\end{enumerate}

Implementing a new Logic in CASL, within this paper context, means instantiating the Logic Class and also the Comorphism Class. Let's dive into the code.

\lstset{
	numbers=left
}

\begin{lstlisting}
-- Logic/Logic.hs
class (StaticAnalysis lid
        basic_spec sentence symb_items symb_map_items
        sign morphism symbol raw_symbol,
       SemiLatticeWithTop sublogics,
       MinSublogic sublogics sentence,
       ProjectSublogic sublogics basic_spec,
       ProjectSublogicM sublogics symb_items,
       ProjectSublogicM sublogics symb_map_items,
       ProjectSublogic sublogics sign,
       ProjectSublogic sublogics morphism,
       ProjectSublogicM sublogics symbol,
       Convertible sublogics,
       SublogicName sublogics,
       Ord proof_tree, 
       Show proof_tree,
       Convertible proof_tree)
    => Logic lid sublogics
        basic_spec sentence symb_items symb_map_items
        sign morphism symbol raw_symbol proof_tree
        | lid -> sublogics
                 basic_spec sentence symb_items symb_map_items
                 sign morphism symbol raw_symbol proof_tree
\end{lstlisting}

\lstset{
	numbers=none
}

Within the above Haskell code one can distinguish:

\begin{itemize}
	\item Logic Class parameters (lines 18-20)
	\item Logic Class constraints (lines 2-17)
	\item Logic Class functional dependencies (lines 21-23)
\end{itemize}

\begin{lstlisting}
-- Logic/Comorphism.hs
class (Language cid,
       Logic lid1 sublogics1
        basic_spec1 sentence1 symb_items1 symb_map_items1
        sign1 morphism1 symbol1 raw_symbol1 proof_tree1,
       Logic lid2 sublogics2
        basic_spec2 sentence2 symb_items2 symb_map_items2
        sign2 morphism2 symbol2 raw_symbol2 proof_tree2) =>
  Comorphism cid
            lid1 sublogics1 basic_spec1 
            	sentence1 symb_items1 
            	symb_map_items1 sign1 morphism1 
		symbol1 raw_symbol1 proof_tree1
            lid2 sublogics2 basic_spec2 
            	sentence2 symb_items2 symb_map_items2 sign2 
		morphism2 symbol2 raw_symbol2 proof_tree2
   | cid -> 
   	lid1 sublogics1 basic_spec1 sentence1 symb_items1 
   		symb_map_items1 sign1 morphism1 symbol1 
		raw_symbol1 proof_tree1
	lid2 sublogics2 basic_spec2 sentence2 symb_items2 
		symb_map_items2 sign2 morphism2 symbol2 
		raw_symbol2 proof_tree2
\end{lstlisting}

One must firstly define the two Logics, and then instantiate the Comorphism between them.

This evidences one that a Logic Class instantiation does not necessarily fulfill the theoretical definition of Logic\cite{whatislogic}. 

\mybox{In HETS, when only defining a Logic based on a comorphism to another logic (for example, CASL), the Logic Class could be seen as the definition of the syntactical part of an Institution, that is, Signatures and Sentences. Then is within the Comorphism Class instantiation that one are able to establish an interpretation of the syntactical part. Basically one interpret the syntactical part of the source within the semantical world of the destination Logic.}

\subsubsection{Logic Type Parameters}

First of all, one must understand what is each Logic Class parameter.

\begin{lstlisting}
-- Logic/Logic.hs
    => Logic lid sublogics
        basic_spec sentence symb_items symb_map_items
        sign morphism symbol raw_symbol proof_tree
\end{lstlisting}


\paragraph{lid} The Language Id. This parameter should be a singleton data structure. The need for this parameter only concerns to the need of establishing a functional dependency for the rest of the parameters (as explained latter). For example, it could be:

\begin{lstlisting}
data Modal = Modal
\end{lstlisting}

\paragraph{sublogics} Sublogics of the given logic. Basically, one can define sublogics (sub-Institutions) by restrictions over the Signatures or Sentences\cite{habil}. For example, if one disable one specific symbol of a defined language, then a sub-Institutions is defined by that restriction. This type might be a record of Boolean flags, indicating whether some feature is present in the sublogic or not. 

\paragraph{basic\_spec} Data structure representing an abstract syntax of basic specifications. Basic specifications are extensively explained at CASL Reference Manual\cite{caslrm}. Basically, a theorem could be written only using basic specifications. In \cite{caslrm} one could find what are basic specifications in CASL:

\begin{quotation}
``Basic specifications denote classes of partial first-order structures: algebras where the functions are partial or total, and where also predicates are allowed. Subsorts are interpreted as embeddings. Axioms are first-order formulas built from definedness assertions and both strong and existential equations. Sort generation constraints can be stated. Datatype declarations are provided for concise specification of sorts equipped with constructors and (optional) selectors, including enumerations and products.''
\end{quotation}

A common way of defining a basic specification structure is taking use of the already defined CASL structure.

\begin{lstlisting}
-- CASL/AS_Basic_CASL.hs
data BASIC_SPEC b s f = Basic_spec [Annoted (BASIC_ITEMS b s f)]
                  deriving Show
\end{lstlisting}

The above data structure is already prepared to be extended. One could see that is expecting three type parameters.

\begin{center}
\begin{tabular}{| c | p{5cm} |}
\hline
Parameter & Description \\
\hline
\codigo{b} & Extended basic specifications, where one could define others basic specification constructs. \\
\hline
\codigo{s} & Signature items, that is, type representing an abstract structure of others signature items. Signature items are a subset of possible CASL constructs. \\
\hline
\codigo{f} & Other formulas, or sentence constructors, for writing axioms (properties of the system). As Signature items, formulas are a subset of possible CASL constructs. \\
\hline
\end{tabular}
\end{center}

\paragraph{sentence} Logical Formulas or Sentence constructors. A part of the basic specification abstract syntax relative to logical formulas. A common definition use the CASL formulas (also extendable-aware structure).

\begin{lstlisting}
-- CASL/AS_Basic_CASL.hs
data FORMULA f = Quantification QUANTIFIER [VAR_DECL] (FORMULA f) Range
             -- pos: QUANTIFIER, semi colons, dot
             | Conjunction [FORMULA f] Range
             -- pos: "/\"s
             | Disjunction [FORMULA f] Range
             -- ...
             | ExtFORMULA f
             -- needed for CASL extensions
               deriving (Show, Eq, Ord)
\end{lstlisting}


As mentioned before, the \codigo{f} parameter (is the same type of the given \codigo{f} parameter to a BASIC\_SPEC)

\paragraph{symb\_items \& symb\_map\_items} Explaining this types parameters means exploring the CASL structured specification level, which is not in the scope of this paper. One will fix this parameter has de CASL already defined parameter.

\begin{lstlisting}
-- CASL/AS_Basic_CASL.hs
data SYMB_ITEMS = Symb_items SYMB_KIND [SYMB] Range
                  -- pos: SYMB_KIND, commas
                  deriving (Show, Eq)
                  
data SYMB_MAP_ITEMS = Symb_map_items SYMB_KIND [SYMB_OR_MAP] Range
                  -- pos: SYMB_KIND, commas
                  deriving (Show, Eq)
\end{lstlisting}

At \cite{caslrm} a full definition could be found.

\paragraph{sign} Signatures, that is, contexts, or non-logical vocabularies, typically consisting of a set of declared sorts, predicates, function symbols, propositional letters etc., together with their typing. A common implementation re-use the parametrized CASL Sign data structure.

\begin{lstlisting}
-- CASL/Sign.hs
data Sign f e = Sign
    { sortRel :: Rel.Rel SORT
    , emptySortSet :: Set.Set SORT
    , opMap :: OpMap
    , assocOps :: OpMap
    , predMap :: PredMap
    , varMap :: Map.Map SIMPLE_ID SORT
    , sentences :: [Named (FORMULA f)]
    , declaredSymbols :: Set.Set Symbol
    , envDiags :: [Diagnosis]
    , annoMap :: MapSet.MapSet Symbol Annotation
    , globAnnos :: GlobalAnnos
    , extendedInfo :: e
    } deriving Show
\end{lstlisting}

This data type is used for the static analysis to accumulate variables, sentences, symbols, diagnostics and annotations, that are removed or ignored when looking at signatures from outside, i.e. during logic-independent processing. For that reason it contain fields for accumulate values, like the current sentences in the parsed theorem. Because of this, the structure need a parameter \codigo{f} of extended formulas or sentence constructs. The extended extended info \codigo{e} must define the newly signature objects added.

\paragraph{morphism} A type for defining maps between signatures. Also one could re-use the CASL Morphism and extend it. Using this one basically only need to define a structure for preserving a morphism between the newly added fragments of the Signatures.

\begin{lstlisting}
-- CASL/Morphism.hs
data Morphism f e m = Morphism
  { msource :: Sign f e
  , mtarget :: Sign f e
  , sort_map :: Sort_map
  , op_map :: Op_map
  , pred_map :: Pred_map
  , extended_map :: m
  } deriving (Show, Eq, Ord)
\end{lstlisting}

The type parameters are:

\begin{center}
\begin{tabular}{| c | p{5cm} |}
\hline
Parameter & Description \\
\hline
\codigo{f} & Other formulas, or sentence constructors, for writing axioms (properties of the system). As Signature items, formulas are a subset of possible CASL constructs. \\
\hline
\codigo{e} & Newly added signature objects. \\
\hline
\codigo{m} & A data structure for representing morphisms between the extended info \codigo{e}. \\
\hline
\end{tabular}
\end{center}

\paragraph{symbol \& raw\_symbol} Symbols that may occur in a signature, fully (Symbol) or partially (Raw Symbol) qualified with their types or unqualified (Raw Symbol). Basically, this structure act as a enumeration of possible Symbols in a signature. This should be a way of typing all possible Symbols in a signature.

\paragraph{proof\_tree} The proof tree is expected to give at least the information about which axioms have been used in the proof. As the purpose of this paper is to establish a comorphism to the CASL Logic, this parameter could be fixed to the data type defined in Common/ProofTree.hs.

\subsubsection{Logic Class Constraints}

Another important part of implementing a new Logic is fulfill all the required Logic Class constraints. Logic Class type parameters are required to ``belong'' to other Classes (that is, should agree with a directed acyclic graph of constraints). 

\begin{figure}[H]
\centering
\includegraphics[scale=0.4]{images/dependencies.png}
\caption{Logic Class dependencies}
\label{fig:deps}
\end{figure}

In \figura{fig:deps} one could see all the required class constraints for defining a Logic Class instantiation. Every class presented defines also the type parameters received. One could also find two types of arrows, dashed arrows and solid arrows. Their existence only concerns for graphical simplification purposes. Dashed arrows means that is possible to match parameters by their name, for example, the StaticAnalysis type parameters could be mapped to the Logic type parameters by their name. However the is also dashed arrows between one type argument classes, within this cases is possible to easily understand that if the classes are related (in \figura{fig:deps}) that means that the type parameter of the source also need to fulfill the target.

One will describe the newly defined classes (in HETS).\\

\begin{center}
	\begin{tabular}{| c | c | p{5cm} |}
		\hline
		\textbf{Class Name} & \textbf{Location} & \textbf{Description} \\
		\hline
		GetRange & Common/Id.hs & Basically it allows one to know the range (list of positions) used by any specification construction.\\
		\hline
		Pretty & Common/DocUtils.hs & A Pretty printing system for HETS. \\
		\hline
		SublogicName & Logic/Logic.hs & Gives names to sublogics. \\
		\hline
		MinSublogic & Logic/Logic.hs & Class for providing a minimal sublogic based on a given specification item (Signatures or Sentence constructures). \\
		\hline
		ProjectSublogic & Logic/Logic.hs & Projecting the sublogic over an item means removing from the item the undefined item constructs relative to that sublogic\cite{habil}.\\
		\hline
		ProjectSublogicM & Logic/Logic.hs & Like ProjectSublogic, but providing a partial projection (it may not exists a projection). \\
		\hline
		Language & Logic/Logic.hs & Class for defining a Language in HETS. \\
		\hline
		Category & Logic/Logic.hs & Class for defining a set of functions needed for working with Categories in HETS.  \\
		\hline
		Sentences & Logic/Logic.hs & Class for defining functions to work with Sentences\cite{caslrm}. \\
		\hline
		Syntax & Logic/Logic.hs & Basically defines a set of function for parsing\cite{caslrm}.  \\
		\hline
		StaticAnalysis & Logic/Logic.hs & Set of functions for the static analysis verification process\cite{staticanalysis, caslrm, habil}. \\
		\hline
	\end{tabular}
\end{center}

\subsubsection{Logic Class Functional Dependencies}

The functional dependency of the Logic Class allows one to establish that the logic type parameters \codigo{sublogics
                 basic\_spec sentence symb\_items symb\_map\_items
                 sign morphism symbol raw\_symbol proof\_tree} are uniquely identified by the language id (\codigo{lid}). This is a implementation design decision for stating that the mentioned type parameters belongs to the newly defined Logic.
                 

\section{Conclusion}

Whiting this paper one had given a more practical approach of how to implement a Logic in HETS, also one had projected highly theoretical Institution definition into a simplified one, together with pieces of CASL implementation in HETS.

In HETS, one could look at CASL logic implementation as a framework that allows one to focus at the new logic specifics. HETS allows one to have architectural and also structured specifications\cite{caslrm, habil} of systems, extending CASL allows one to easily re-use those features.

The way of maximizing the CASL implementation re-use is defining a Logic throughout an Institution Comorphism to CASL. The CASL implementation is well prepared to be extended, and that allows one to define a Logic in HETS in a simplified way.

%\bibliographystyle{plainnat}
\bibliographystyle{unsrtnat}



\bibliography{database}

\end{document}