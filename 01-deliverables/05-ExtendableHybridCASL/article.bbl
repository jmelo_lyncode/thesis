\begin{thebibliography}{15}
\providecommand{\natexlab}[1]{#1}
\providecommand{\url}[1]{\texttt{#1}}
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi: #1}\else
  \providecommand{\doi}{doi: \begingroup \urlstyle{rm}\Url}\fi

\bibitem[Eilenberg and MacLane(1945)]{generaltheorycategory}
Samuel Eilenberg and Saunders MacLane.
\newblock \emph{General Theory of Natural Equivalences}.
\newblock American Mathematical Society, 1945.

\bibitem[Lane(1998)]{categoriessimple}
S.M. Lane.
\newblock \emph{Categories for the Working Mathematician}.
\newblock Graduate Texts in Mathematics. Springer, 1998.
\newblock ISBN 9780387984032.

\bibitem[Lawvere and Schanuel(1997)]{categories}
F.~William Lawvere and Stephen~H. Schanuel.
\newblock \emph{Conceptual Mathematics: A First Introduction to Categories}.
\newblock Cambridge University Press, November 1997.

\bibitem[Pierce(1991)]{categories1}
Benjamin~C. Pierce.
\newblock \emph{Basic Category Theory for Computer Scientists (Foundations of
  Computing)}.
\newblock The MIT Press, August 1991.

\bibitem[Jech(1978)]{settheory}
T.J. Jech.
\newblock \emph{Set Theory}.
\newblock Number vol. 79 in Pure and applied mathematics. Academic Press, 1978.
\newblock ISBN 9780123819505.
\newblock URL \url{http://books.google.pt/books?id=pLxq0myANiEC}.

\bibitem[Hodges(1993)]{modeltheory}
Wilfrid Hodges.
\newblock \emph{Model Theory (Encyclopedia of Mathematics and its
  Applications)}.
\newblock Cambridge University Press, 1993.

\bibitem[Manzano(1999)]{modeltheory1}
M.~Manzano.
\newblock \emph{Model Theory}.
\newblock Oxford Logic Guides. Clarendon Press, 1999.
\newblock ISBN 9780198538516.

\bibitem[Chang and Keisler(1990)]{modeltheory2}
C.C. Chang and H.J. Keisler.
\newblock \emph{Model Theory}.
\newblock Studies in Logic and the Foundations of Mathematics. North-Holland,
  1990.
\newblock ISBN 9780444880543.

\bibitem[Hodges(1997)]{modeltheory-intro}
W.~Hodges.
\newblock \emph{A Shorter Model Theory}.
\newblock Cambridge University Press, 1997.
\newblock ISBN 9780521587136.

\bibitem[Malik and Sen(2004)]{mathstructure}
D.S. Malik and K.~Sen.
\newblock \emph{Discrete Mathematical Structures: Theory and Applications}.
\newblock Course Technology, 2004.
\newblock ISBN 9780619212858.

\bibitem[Marker(2002)]{modeltheory-intro1}
D.~Marker.
\newblock \emph{Model Theory: An Introduction}.
\newblock Graduate Texts in Mathematics. Springer, 2002.
\newblock ISBN 9780387987606.

\bibitem[Mossakowski et~al.(2000)Mossakowski, Goguen, Diaconescu, and
  Tarlecki]{whatislogic}
Till Mossakowski, Joseph Goguen, Razvan Diaconescu, and Andrzej Tarlecki.
\newblock What is a logic?
\newblock 2000.

\bibitem[Stra{\ss}burger(2006)]{logicproof}
Lutz Stra{\ss}burger.
\newblock What is a logic, and what is a proof ?
\newblock \emph{Logica Universalis}, 2006.

\bibitem[Diaconescu(2008)]{institutionmodeltheory}
Razvan Diaconescu.
\newblock \emph{Institution-Independent Model Theory}.
\newblock Birkh{\"a}user, 2008.

\bibitem[Meseguer(FALTA)]{generalogics}
Jos{\'e} Meseguer.
\newblock \emph{General Logics*}.
\newblock FALTA.

\end{thebibliography}
