% Faz com que o ínicio do capítulo sempre seja uma página ímpar
\cleardoublepage
\pagestyle{empty}

\pagenumbering{arabic}

\chapter{Introdução}\label{cap1}

\cleardoublepage

% Inclui o cabeçalho definido no meta.tex
\pagestyle{normalstyle}


% Números das páginas em arábicos

Serve a presente introdução para apresentar a movitação do atual estudo, como também, os objetivos inicialmente propostos, terminando com o enquadramento teórico necessário para a realização do estudo em causa.

\section{Motivação}\label{intro:motivacao}

O acesso à tecnologia está num processo de constante banalização. O passado recente da humanidade é caracterizado pelos enormes e sucessivos avanços tecnológicos, não fazendo qualquer sentido, nos dias de hoje, pensar em futuro sem supôr uma (r)evolução tecnológica. A banalização do uso de dispositivos tecnológicos não só contribui para o aumento exponencial da sua utilização, como também para facilitar a aceitação de sistemas automatizados na resolução de, cada vez mais, problemas sensíveis, ou seja, problemas cuja solução assume grande responsabilidade.

A utilização de tecnologia incide, atualmente, na sua grande maioria, na resolução de tarefas de baixo risco, isto é, onde o mau funcionamento dos sistemas automatizados não tem efeitos negativos graves. Mas, conforme já referido, verifica-se um crescimento da utilização de sistemas automatizados onde a existência de deficiências pode resultar em danos irreparáveis. Exemplos disso, são os pilotos automáticos de aviões, ou mais recentemente, pilotos automáticos de carros, onde, no pior cenário, uma falha pode resultar em perdas humanas. 

Mas, não só as perdas humanas são irreparáveis, no meio empresarial há sistemas cujo mau funcionamento pode resultar em prezuizos incalculáveis, colocando mesmo em causa a sobrevivência das próprias empresas. Um exemplo disso são os serviços online que dependem da infra-estrutura que suporta esse mesmo serviço, onde a falha na disponibilidade do serviço hipoteca por completo o sucesso do mesmo.

É neste contexto tecnológico, muito sensível, onde os custos das falhas são muito elevados, por vezes, irrecuperáveis, que os métodos formais têm grande preponderância. Métodos formais\index{Métodos Formais} é o nome dado ao conjunto de técnicas matemáticas rigorosas (auxiliadas por ferramentas) que permitem especificar, desenvolver e verificar sistemas de software ou hardware \citep{whatisfm}. Tal importância é reconhecida pela crescente utilização dos métodos formais na indústria, com impacto bastante positivo no desenvolvimento de sub-sistemas críticos (partes fundamentais do sistema que requerem garantias de correto funcionamento) \citep{fmsurvey, industryfm, constructing, formalverification, softwareabs}.

No que diz respeito aos sistemas reativos, isto é, sistemas que interagem continuamente com o meio envolvente \citep{reactivesystems}, o sucesso no desenvolvimento e implementação de segurança crítica, partindo do conceito inicial do sistema, passando pelas fases de definição, até à implementação e validação, coloca uma série de desafios que os engenheiros têm de superar. 

Há duas abordagens básicas para capturar formalmente os requisitos para este tipo de software: uma enfatiza o comportamento e a sua evolução, enquanto outra foca-se nos dados e nas suas transformações. 
No que diz respeito ao primeiro paradigma, os sistemas reativos são normalmente especificados através de modelos com base em máquinas de estados\index{Máquinas de Estados}. Tais modelos capturam a evolução do sistema em termos de ocorrência de eventos, determinando o seu impacto no estado interno do sistema. A teoria de autómatos, fornece um formalismo adequado para a especificação e análise deste tipo de modelos\footnote{Modelo é o resultado da especificação, usando uma linguagem apropriada, da concepção de um sistema.} \citep{statemachine,automata,automata1,petrinets}.
Já a segunda abordagem é orientada aos dados, estabelecendo-se relações entre os dados de entrada e saída do sistema, relações estas denominadas de operações. Desta forma, a especificação de um sistema é um teorema expresso numa lógica concreta e ajustada, teorema que requer a obtenção de prova, através da utilização de uma teoria apropriada. Devido ao abuso de linguagem levado a cabo por alguma bibliografia, torna-se importante distinguir os conceitos de teorema e teoria.

\mybox{
A especificação de um sistema é um teorema\index{Teorema} (que tem que ser provado). Uma teoria\index{Teoria} pode ser vista como uma ferramenta lógica (assumida como uma construção lógica verdadeira) utilizada para provar teoremas \cite{habil, hetspaper, hetspaper1, theory, foundationsmathlogic}.}


No entanto, ambas as abordagens estão bastante interligadas, isto é, (olhando à segunda abordagem mencionada anteriormente) o comportamento do sistema, especificado sob a forma de operações, é variável e depende seu estado atual (teoria dos autómatos - primeira abordagem mencionada).

No que toca à primeira abordagem, tem-se que a semântica padrão das estruturas de Kripke\index{Estrutura de Kripke} (formalização da semântica para máquinas de estados \citep{fmalgorithms, petrinets, automata1, automata}), define o conceito de verdade tendo em conta o estado do sistema em causa. Mais concretamente, uma proposição lógica pode ter diferentes valores de verdade em diferentes estados desse sistema. Normalmente, esses estados representam os mundos possíveis, tempos, espaços, conhecimentos, estados de um computador. Isso permite formalizar declarações em linguagem natural cujos valores de verdade são relativos a um conjunto de estados concretos do universo de estados possíveis, por exemplo, a afirmação ``está a chover'' apenas é verdade em determinados períodos do tempo\citep{logicintro, statemachine, petrinets}.

A lógica modal\index{Lógica Modal} pode ser utilizada com objetivo de especificar sistemas reativos. A lógica modal é uma extensão à lógica proposicional que inclui operadores modais. Por exemplo, na afirmação ``habitualmente chove'', a palavra ``habitualmente'' pode ser vista como um operador modal\index{Modalidades}. A definição inicial da lógica modal (com origem nos anos sessenta) define duas modalidades elementares: a possibilidade (por exemplo, ``é possível que chova''), e a inevitabilidade (por exemplo, ``é inevitável que chova'') \citep{formalogic, modalhandbook, modalnotes}. Existem também outras modalidades formalizadas posteriormente, como é o exemplo das modalidades temporais \citep{templogic, temporalogic}, modalidades de conhecimento \citep{knowledgelogic} e modalidades de convicção \citep{belieflogic}.

Apesar da lógica modal possuir um poder expressivo que permite especificar alguns sistemas que podem ser modelados através de transições de estados e seus invariantes, esta não tem capacidade expressiva para determinados contextos. Por exemplo, a afirmação ``são exatamente cinco horas da tarde do dia 6 de Novembro de 2012'' é uma afirmação verdadeira apenas num determinado espaço temporal e falsa em todos os restantes, não sendo representável em lógica modal. É nesta falta de poder expressivo que surge a lógica híbrida\index{Lógica Híbrida} \citep{hybridLogic}, que permite associar especificações a estados concretos do universo de estados possíveis. Este incremento de poder expressivo é obtido com a introdução do
conceito de nominais. Um nominal\index{Nominal} pode ser visto como uma proposição apenas verdadeira num e só num estado, de todos os estados possíveis.


\section{Objetivos}\label{intro:objetivos}

Pretende-se com este estudo documentar a metodologia de implementação de extensões à ferramenta \textit{open source} Heterogeneous Tool Set (HETS\index{HETS}) \citep{habil, hetspaper1, hetspaper} (que permite especificar e verificar sistemas), desenvolver uma extensão, para a mesma, que permita verificar especificações que façam uso de operadores híbridos e, por fim, estudar metedologias de hibridização de lógicas, isto é, métodos que permitam munir outras extensões (lógicas) ao HETS de operadores híbridos.

\section{Enquadramento Teórico}\label{intro:enquadramento}

A ferramenta HETS\index{HETS} distingue-se de todas as restantes dado à sua arquitectura e desenvolvimento basear-se na teoria das instituições\citep{hetspaper, hetspaper1}. Tendo por objetivo perceber a implementação de extensões à ferramenta HETS, é necessário todo um \textit{background} sobre a teoria das instituições \citep{institutions}.

A definição formal de instituição lógica\index{Instituição Lógica} tem por base a teoria das categorias\index{Teria das Categorias} \citep{introcategories, categoriesawodey2010, categories, categories1}, como também, a teoria dos modelos\footnote{É importante distinguir o conceito de modelo enquanto resultado de uma especificação, de modelo no âmbito da teoria dos modelos}\index{Teoria dos Modelos} \citep{modeltheory, modeltheory1, modeltheory2}. Por sua vez, a definição formal de categoria faz uso de conceitos definidos no âmbito da teoria dos conjuntos\index{Teoria dos Conjuntos}.

Segue-se assim, uma breve apresentação das teorias mencionadas, com especial foco nos conceitos utilizados no contexto deste estudo. Terminando com a apresentação, informal e sucinta, da definição da linguagem \texttt{CASL}\index{CASL}, seguida de uma referência à lógica híbrida\index{Lógica Híbrida}.

\subsection{Teoria dos Conjuntos}\index{Teoria dos Conjuntos}

O estudo da teoria dos conjuntos, como ramo da matemática, foi iniciado por \textit{Georg Cantor} e \textit{Richard Dedekind} nos anos setenta do século \texttt{XIX}. Esta teoria, como o próprio nome indica dedica-se ao estudo dos conjuntos. No âmbito do presente estudo, é apenas importante reter o conceito de classe\index{Classe}, que pode ser definido, informalmente, como uma coleção de conjuntos \citep{settheory, settheory1}.

\subsection{Teoria das Categorias}\index{Teoria das Categorias}

A teoria das categorias foi inicialmente formalizada no livro \textit{General Theory of Natural Equivalences} publicado em 1945 \citep{generaltheorycategory}. Informalmente, uma categoria pode ser vista como uma estrutura que contém um conjunto de $objetos$ e também um conjunto de transformações (formalmente chamadas de $\mathbf{morfismos}$) entre $\mathbf{objetos}$. 

Em geral, a noção de categoria fornece uma forma abstrata para descrever matematicamente entidades e as suas relações.

$\mathbf{Morfismos}$ são, habitualmente, representados como setas. Em termos de notação, tem-se o seguinte.

\begin{notation}[Morfismo]\label{not:morphism}\index{Morfismo}
Seja $f$ um $\mathbf{morfismo}$ entre dois $\mathbf{objetos}$, $a$ e $b$, denota-se o $\mathbf{morfismo}$ $f$ como,

\begin{center}
	$f : a \rightarrow b$
\end{center}
\end{notation}

Usando morfismos é possivel estabelecer o conceito de composição, isto é, dados, abstratamente, dois morfismos $f$ e $g$, o morfismo $f$ aceita como objetos de entrada, os objetos resultantes do morfismo $g$, adoptando-se, por convenção a representação: $f \circ g$.

Formalmente, uma categoria é definida da seguinte forma \cite{settheory, settheory1}:


\begin{definition}[Categoria]\label{def:category}
Uma categoria (denotada como $\mathbf{C}$) é um par:

\begin{center}
$\mathbf{C} = (O, H)$
\end{center}

onde $O$ é a classe\index{Classe} de $\mathbf{objetos}$ e $H$ a classe\index{Classe} de $\mathbf{morfismos}$\index{Morfismo}, que respeita os seguintes axiomas:

\begin{description}
	\item[Associatividade] de $\mathbf{morfismos}$, $\forall f, g, h \in H : (f \circ g) \circ h : f \circ (g \circ h)$
	\item[Identidade] $\forall x \in O :: \exists (m : a \rightarrow b) \in H \wedge  a = b = x $ 
\end{description}
\end{definition}

É importante realçar que, um $\mathbf{objeto}$ representa um conceito completamente abstrato. Existe ainda uma categoria especial, denominada ``Categoria das categorias'', cujos $\mathbf{objetos}$ são eles mesmos categorias. Esta categoria especial é denotada como \textbf{CAT}\index{Categoria das Categorias (CAT)}.

Por via da teoria das categorias é ainda possível obter a definição de functor, um conceito fundamental para o estudo em causa.

\begin{definition}[Functor]\index{Functor}
Um \textnormal{\textbf{Functor}} é um \textbf{morfismo} na ``Categoria das categorias'' $\textnormal{\textbf{CAT}}$, uma transformação entre categorias.
\end{definition}

Uma outra base da teoria das instituições, é a teoria dos modelos, que será sucintamente apresentada de seguida.

\subsection{Teoria dos Modelos}\index{Teoria dos Modelos}

%The Model Theory examines semantical elements by means of syntactical elements of a corresponding language

A teoria dos modelos, resumidamente, atribui elementos semânticos a elementos sintaticos de uma determinada linguagem\citep{modeltheory, modeltheory1, modeltheory2}. Neste contexto, a definição mais importante a reter da teoria dos modelos, é a definição de estrutura\index{Estrutura} \citep{modeltheory-intro, mathstructure, modeltheory-intro1}.

\begin{definition}[Estrutura]
Dado uma estrutura (denotada por $\mathcal{M}$), esta é um tuplo $(\mathcal{D}, \mathcal{L}, \mathcal{I})$ onde:

\begin{itemize}
	\item $\mathcal{D}$, designado \textit{informação de $\mathcal{M}$} é um tuplo $(dom(\mathcal{M}), \mathbf{F}, \mathbf{R}, \mathbf{C})$, onde:
	\begin{itemize}
		\item $dom(\mathcal{M})$ é o domínio de $\mathcal{M}$;
		\item $\mathbf{F}$ é o conjunto das funções $n$-árias de $\mathcal{M}$ (cada denotada por $f_{\mathcal{M}}$);
		\item $\mathbf{R}$ é o conjunto das relações $n$-árias de $\mathcal{M}$ (cada denotada por $R_{\mathcal{M}}$);
		\item $\mathbf{C}$ é o conjunto de constantes de $\mathcal{M}$ (cada denotada por $c_{\mathcal{M}}$);
	\end{itemize}
	\item $\mathcal{L}$ é designado de \textit{Linguagem}\index{Linguagem} ou \textit{Assinaturas}\index{Assinatura} de $\mathcal{M}$, é um tuplo $(\mathcal{F}, \mathcal{R}, \mathcal{C})$, onde:
	\begin{itemize}
		\item $\mathcal{F}$ é o conjunto símbolos que representam funções (cada função $f_{\mathcal{M}} \in \mathbf{F}$ pode ser representada por mais de um símbolo), cada símbolo de função é denotado por $f$;
		\item $\mathcal{R}$ é o conjunto de símbolos que representam relações (cada relação $R_{\mathcal{M}} \in \mathbf{R}$ pode ser representada por mais de um símbolo), cada símbolo de relação é denotado por $R$;
		\item $\mathcal{C}$ é o conjunto de símbolos de constantes (cada constante $c_{\mathcal{M}} \in \mathbf{C}$ pode ser representada por mais de um símbolo), cada símbolo de constante é denotado por $c$;
	\end{itemize}
	\item $\mathcal{I}$ é uma função de interpretação, isto é, uma função que atribui a cada símbolo de constante $c$, a cada símbolo de relação $R$ e a cada símbolo de função $f$, uma constante $c_{\mathcal{M}}$, uma relação $R_{\mathcal{M}}$ e uma função $f_{\mathcal{M}}$, respetivamente.
\end{itemize}
\end{definition}

Informalmente, uma estrutura pode ser vista como a atribuição de significado (semântica) a uma linguagem (sintaxe), potencialmente, munida de constantes, relações e funções.

Outro conceito importante na teoria dos modelos é o conceito de frase (lógica)\index{Frase Lógica}, denotada por $\phi$, que pode ser visto como resultado da composição de símbolos atómicos, isto é, constantes, relações ou funções. 

\begin{definition}[Modelo]\index{Modelo}
Dado que $\phi$ usa símbolos de $\mathcal{M}$, diz que $\phi$ é uma afirmação sobre $\mathcal{M}$, essa afirmação pode ser verdadeira ou falsa. Se for verdadeira, diz-se que $\mathcal{M}$ é um modelo de $\phi$, denotado como:

\begin{center}
	$\mathcal{M} \vDash \phi$
\end{center}
\end{definition}

Informalmente, um determinado modelo, é modelo de uma frase $\phi$ se a interpretação dessa frase segundo esse mesmo modelo for verdadeira.

Dados estes conceitos base, é possível, então, partir para a definição de instituição lógica.

\subsection{Teoria das Instituições}\index{Teoria das Instituições}

Qualquer linguagem lógica pode ser formalizada usando a teoria institucional. A noção de instituição foi criada por \textit{Joseph Goguen} e \textit{Burstall Rod} no final de 1970, a fim de lidar com a explosão populacional entre os sistemas lógicos usados no âmbito da ciência da computação. A noção de instituição, resumidamente, tenta capturar a essência do conceito de sistema lógico. 

Instituições são uma abstração do conceito de lógica \citep{institutions}. Segue-se a definição formal de Instituição lógica e, posteriormente, uma interpretação informal do conceito.

\newpage
\begin{definition}[Instituição]\label{def:institution}\index{Instituição Lógica}
Uma instituição (representada por $I$) é um tuplo $I = (\textnormal{\textbf{Sign}}, \textnormal{\textbf{Sen}}, \textnormal{\textbf{Mod}}, \vDash)$ tal que:

\begin{itemize}
	\item $\textnormal{\textbf{Sign}}$ é a categoria\index{Categoria}\index{Assinatura} de assinaturas (cada assinatura é representada por $\Sigma$);
	\item $\textnormal{\textbf{Sen}}$ é um Functor\index{Functor} $\textnormal{\textbf{Sen}} : \textnormal{\textbf{Sign}} \rightarrow \textnormal{\textbf{Set}}$ que retorna, para cada assinatura $\Sigma$, o conjunto de frases\index{Frase Lógica} possiveis, representado por $\textnormal{\textbf{Sen}} (\Sigma)$, e para cada morfismo de assinatura (representado por $sigma: \Sigma \rightarrow \Sigma'$), a função tradução de uma frase, ou seja, $\textnormal{\textbf{Sen}} (\sigma) : \textnormal{\textbf{Sen}} (\Sigma) \rightarrow \textnormal{\textbf{Sen}} (\Sigma')$, onde normalmente $\textnormal{\textbf{Sen}} (\sigma)(\varphi)$ é denotado por $\sigma(\varphi)$;
	\item $\textnormal{\textbf{Mod}}$ é um Functor\index{Functor} $\textnormal{\textbf{Mod}} : \textnormal{\textbf{Sign}}^{op} \rightarrow \textnormal{\textbf{CAT}}$ que retorna, para cada assinatura $\Sigma$, a categoria de modelos\index{Modelo} (representada por $\textnormal{\textbf{Mod}}(\Sigma)$), e para cada morfismo de assinatura $\sigma : \Sigma \rightarrow \Sigma'$, o Functor redutor $\textnormal{\textbf{Mod}}(\sigma) : \textnormal{\textbf{Mod}}(\Sigma') \rightarrow \textnormal{\textbf{Mod}}(\Sigma)$, onde normalmente $\textnormal{\textbf{Mod}}(\sigma)(M')$ é denotado por $M'\mid_\sigma$;
	\item $\vDash$ é a relação de satisfação\index{Relação de Satisfação} $\vDash_{\Sigma} \subseteq \abs{\textnormal{\textbf{Mod}} (\Sigma)} \times \textnormal{\textbf{Sen}} (\Sigma)$ para cada $\Sigma \in \textnormal{\textbf{Sign}}$;
\end{itemize}

tal que, para cada $\sigma : \Sigma \rightarrow \Sigma'$ de $\textnormal{\textbf{Sign}}$ a seguinte condição de satisfação verifica-se:
\begin{center}
	$M' \vDash_{\Sigma'} \sigma(\varphi) \Leftrightarrow M'\mid_{\sigma} \vDash_{\Sigma} \varphi$
\end{center}

para cada $M' \in \textnormal{\textbf{Mod}}(\Sigma')$ e $\varphi \in \textnormal{\textbf{Sen}}(\Sigma)$.
\end{definition}

Neste contexto, podemos estabelecer que uma lógica\index{Lógica} pode ser vista como uma instituição equipada com um sistema de consequências lógicas\citep{generalogics}.

Para a definição de uma instituição é necessário definir três conceitos primitivos dessa instituição, isto é, as \textbf{Assinaturas}, as \textbf{Frases} e os \textbf{Modelos}. Numa tentativa de simplificar estas definições, apresenta-se de seguida uma explicação informal de cada.

\paragraph{Nota} É importante realçar que esta explicação informal deve ser vista como uma simplificação incompleta, usando conceitos mais comuns, e por conseguinte, não uma forma correta de tirar conclusões gerais sobre instituições, tudo porque o cariz abstrato das instituições permite ter perspetivas completamente distintas de assinaturas, frases e modelos das apresentadas seguidamente.

\paragraph{Assinaturas}\index{Assinatura}

Imagine-se assinaturas como conjuntos de símbolos. Por exemplo, em Haskell\footnote{Linguagem de programação funcional}\cite{foundationsmathlogic, haskell}, um símbolo (elemento da assinatura da instituição Haskell), pode ser o nome de uma função, como também nomes dos tipos.

\begin{lstlisting}
mult :: Int -> Int -> Int
\end{lstlisting}


\paragraph{Frases}\index{Frase Lógica}

Fazendo uso da simplificação anterior, as frases podem ser vistas como árvores de símbolos, onde cada nodo é um símbolo (pertencente à assinatura da instituição). Os nodos da árvore com descendentes são designados de construtores frásicos.

\paragraph{Modelos}\index{Modelo}


Os modelos têm de interpretar símbolos, dando significado aos mesmos, permitindo, dessa forma, seja determinado um resultado, por via de uma estrutura algebrica. Por exemplo, em Haskell\index{Haskell} os modelos possiveis teriam de usar o conjunto dos inteiros para definir o símbolo \textbf{Int} e associar as implementações das funções aos respetivos símbolos \codigo{mult} e \codigo{fastMult}.

% Sobre instutuições em geral

Com isto, é possível desenvolver conceitos de linguagens de especificação (como estruturação de especificações, parametrização, implementação, refinamento e desenvolvimento), prova e até mesmo ferramentas, de uma forma completamente independente do sistema lógico subjacente. Na teoria das instituições, os modelos e as especificações são objetos arbitrários, a única suposição feita é que há uma relação de satisfação entre os modelos e as especificações, dizendo-se que uma especificação tem um modelo (que verifica a especificação) ou não.

Uma característica fundamental das instituições\index{Instituição} é que os modelos, especificação e sua satisfação estão sempre associados a um vocabulário ou contexto (conforme já mencionado e designado de $\textnormal{\textbf{Sign}}$) que define os símbolos (não-lógicos) que podem ser usados nas especificações. Estes símbolos precisam ser interpretados em modelos concretos. Define-se também o conceito de morfismos de assinaturas, que permitem extender assinaturas, alterar a notação, entre outros. É importante realçar que, nada é assumido sobre assinaturas e morfismos de assinatura, excepto que, morfismos de assinaturas podem ser compostos, o que se traduz em definir o conceito de assinatura como uma categoria.

A abstração que caracteriza o conceito de instituição permite distinguir duas componentes bem distintas, a componente sintática\index{Sintaxe} (que envolve os conceitos de \textbf{Assinaturas} e \textbf{Frases}) e a componente semântica\index{Semântica} (\textbf{Modelos}). Tal separação permite definir vários tipos de morfismos entre instituições.

\subsubsection{Morfismos de Instituições}\index{Morfismo de Instituições}

Uma vez que a teoria das instituições não faz qualquer assunção sobre a natureza do sistema lógico, esta teoria permite estabelecer o conceito de morfismos de instituições, um conceito bastante poderoso, que permite relacionar e traduzir sistemas lógicos. Com isto é possível, a reutilização da estrutura lógica, especificação heterogénea\index{Especificação Heterogénea} e combinação de lógicas, verificando-se o seguinte corolário.

\mybox{Um morfismo de assinaturas\index{Morfismo de Assinaturas} preserva a relação de satisfação\citep{institutionmorphisms, hetspaper2}.}

Informalmente, um morfismo entre instituições permite transformar uma instituição noutra. Dado o cariz abstrato da definição de instituição, com a distinção clara das componentes semântica e sintática, é possível formular vários tipos de morfismos entre instituições, a taxonomia dos possiveis morfismos de instituições é descrita no estudo intitulado \textit{Heterogeneous specification and the heterogeneous tool set} \citep[p. 29]{habil}. 


Para o presente estudo, o tipo de morfismo mais importante a reter, é o morfismo denominado de \textbf{comorfismo}\index{Comorfismo}. Genericamente, a definição de um comorfismo significa definir várias ``setas'' (i.e. morfismos) entre uma instituição inicial e uma instituição alvo. Assim, dadas duas instituições $\textnormal{\textbf{I}} = (\textnormal{\textbf{Sign}}, \textnormal{\textbf{Sen}}, \textnormal{\textbf{Mod}}, \vDash)$ e $\textnormal{\textbf{I}}' = (\textnormal{\textbf{Sign}}', \textnormal{\textbf{Sen}}', \textnormal{\textbf{Mod}}', \vDash')$, define-se comorfismo como:


\begin{center}
\begin{tabular}{c c c}
$\textnormal{\textbf{Sign}}$ & $\longrightarrow$ & $\textnormal{\textbf{Sign}}'$ \\
$\textnormal{\textbf{Sen}}$ & $\longrightarrow$ & $\textnormal{\textbf{Sen}}' \circ \Phi$ \\
$\textnormal{\textbf{Mod}}$ & $\longleftarrow$ & $\textnormal{\textbf{Mod}}' \circ \Phi$
\end{tabular}

\end{center}

onde $\Phi : \textnormal{\textbf{Sign}} \rightarrow \textnormal{\textbf{Sign}}'$ é um morfismo de assinaturas. Ou seja, respetivamente,

\begin{itemize}
	\item Começando em \textnormal{\textbf{Sign}} e produzindo um novo \textnormal{\textbf{Sign}}', que traduz assinaturas da instituição inicial em símbolos da instituição alvo;
	\item Definir um Functor \textnormal{\textbf{Sen}}' aplicável a assinaturas e morfismos em \textnormal{\textbf{Sign}}', que gera frases da instituição alvo partindo das frases da instituição inicial;
	\item Definir um Functor \textnormal{\textbf{Mod}}' aplicável a assinaturas e morfismos em  \textnormal{\textbf{Sign}}', que gera modelos na instituição inicial, partindo dos modelos da instituição alvo; 
\end{itemize}

Exemplificando a definição das duas primeiras ``setas'', com base na Figura ~\ref{fig:signmorphism}, que apresenta um morfismo de assinaturas e duas frases distintas (representadas sob a forma de árvores sintáticas - de instituições distintas), pode-se observar que o morfismo de assinaturas apresentado permite re-escrever os símbolos atómicos, assim como os construtores frásicos, mantendo, no entanto, a estrutura da árvore sintática.

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.6]{images/morphism.pdf}
\caption{Aplicação de um morfismo de assinaturas}
\label{fig:signmorphism}
\end{center}
\end{figure}

Informalmente, a ideia de um comorfismo\index{Comorfismo} entre instituições é que uma instituição $\textnormal{\textbf{I}}'$ mais ``rica'' (por exemplo, com mais construtores frásicos/assinaturas) é construída sobre uma instituição $\textnormal{\textbf{I}}$ mais ``pobre''. Fazendo uso deste morfismo especifico é possível re-utilizar o modelo definido para a instituição inicial, por forma a obter provas na instituição alvo. Na prática, o uso desta técnica para definição de instituições garante a reutilização de modelos, facilitando, portanto, a definição de novas instituições.


Uma das aplicações da teoria institucional mais conhecidas, particularmente no que diz respeito à especificação heterogénea e re-utilização de estruturas lógicas é, justamente, a ferramenta de especificação e prova de modelos sob análise (HETS\index{HETS}). Toda a arquitetura do HETS foi pensada com base na teoria institucional, o seu nome (\textit{Heterogeneous Tool Set}) realça um conceito importante a reter, nomeadamente, o conceito de especificação heterogénea abordado de seguida.

\subsubsection{Especificação Heterogénea}\index{Especificação Heterogénea}

No que toca à especificação de sistemas, a procura de uma linguagem que permita especificar formalmente qualquer sistema, pode ser vista como o resultado da combinação de várias capacidades (construções lógicas) existentes em diferentes linguagens de especificação. Tal combinação atinge os seus limites aquando da co-existência de capacidades muitos dispares. Para um aprofundamento destes limites é recomendável a leitura de literatura auxiliar\citep{heterogeneousbook, hetspaper2}.

É no contexto de sistemas complexos que a especificação heterogénea ganha especial relevo. Informalmente, olhando a um sistema como uma composição de vários sub-sistemas, sub-sistemas esses cujas propriedades são melhor expressas em linguagens lógicas distintas, temos que a especificação heterogénea permite conciliar ambas as especificações (distintas)\citep{heterogeneousbook, heterogeneoussimp, heterogeneousSpec, heterogeneous, hetspaper, hetspaper1, hetspaper2, hetspaper3}.

Uma das formas de conciliar especificações em lógicas distintas é fazer uso de comorfismos, ou seja, dado que um comorfismo permite a reutilização dos modelos da instituição inicial, tal caracteristica permite, sem grande esforço, conciliar duas instituições com capacidades expressivas distintas.

A separação clara entre diferentes linguagens permite fazer uso das vantagens de cada linguagem especifica, como também das ferramentas associadas. O aproveitamento destas vantagens permite fazer da especificação heterogénea um modelo de especificação cada vez mais em voga.

O estudo em causa, requer também conhecimento da linguagem \texttt{CASL}\index{CASL} - \textit{Common Algebraic Specification Language}, definida pela iniciativa \texttt{CoFI}\footnote{Common Framework Initiative}, dado que é a linguagem de especificação mais desenvolvida da ferramenta HETS, servindo de base a, praticamente, todas as linguagens lógicas, posteriormente, implementadas nesta ferramenta.

\subsection{CASL}\index{CASL}

\texttt{CASL} distingue-se das restantes linguagens derivado à sua definição formal baseada na teoria das instituições \cite{habil}. Esta linguagem permite, sucintamente, a um nível básico escrever teoremas em lógica de Primeira Ordem\citep{caslrm, habil}.

Apesar da linguagem \texttt{CASL} ter conceitos como especificações estruturadas, especificações arquiteturais e bibliotecas, por motivos de simplificação, tais conceitos serão ignorados, focando-se o estudo apenas ao nível da especificação básica, dado que o estudo em causa apenas fará uso da noção de especificação básica\index{Especificação Básica}. Para o completo entendimento da linguagem \texttt{CASL} é aconselhável a leitura de literatura complementar \cite{caslrm, hetspaper, habil, hetspaper1}.

Apresenta-se, de seguida, as definições da linguagem \texttt{CASL}, mais precisamente, dos elementos fundamentais para a definição de uma instituição.


\paragraph{Assinaturas}\index{Assinatura}

Corforme especificado no manual de referência \citep[Part III]{caslrm}, as assinaturas são definidas da seguinte forma,

\begin{quotation}
``A many-sorted signature $\Sigma$ consists of: a set of sorts; separate families of sets of total and partial function symbols, indexed by function profile (a sequence of argument sorts and a result sort – constants are treated as functions with no arguments); and a family of sets of predicate symbols, indexed by predicate profile (a sequence of argument sorts). Constants and functions are also referred to as operations.''
\end{quotation}

Ou seja, uma assinatura em CASL é um tuplo, definido pelas seguintes parcelas:

\begin{itemize}
	\item Um conjunto de tipos;
	\item Um conjunto de funções totais\footnote{\label{footnote:func}Indexada pelo perfil da função, isto é, uma sequência de tipos dos argumentos e o tipo do resultado};
	\item Um conjunto de funções parciais\footnotemark[\value{footnote}];
	\item Um conjunto de predicados;
\end{itemize}

\paragraph{Frases}\index{Frase Lógica}

Uma frase em \texttt{CASL} é definida em \citep[Part III]{caslrm}, da seguinte forma:

\begin{quotation}
``The many-sorted sentences in $\textnormal{\textbf{Sen}}(\Sigma)$ are sort-generation constraints (...) and the usual closed many-sorted first-order logic formulas, built from atomic formulas (application of qualified predicate symbols to argument terms of appropriate sorts, assertions about the definedness of fully-qualified terms, and existential and strong equations between fully-qualified terms of the same sort) using quantification and logical connectives. Predicate application, existential equations, implication and universal quantification are taken as primitive, the other forms being regarded as derived.''
\end{quotation}

Ou seja, tal como referido anteriormente, a frases são resultado da composição de elementos atómicos. No contexto da linguagem \texttt{CASL}, dado ser uma linguagem que permite especificar sistemas em lógica de primeira ordem, verifica-se os construtores frásicos habituais, isto é:

\begin{itemize}
	\item Aplicação de predicados/operações
	\item Equações existenciais (atravez da igualdade)
	\item Implicação lógica
	\item Quantificação universal
\end{itemize}

Em \texttt{CASL} estes construtores frásicos são tidos como primitivos, sendo possível, a partir destes, derivar todos os restantes.

\paragraph{Modelos}\index{Modelo}

Um modelo em \texttt{CASL}, usando a base teórica das instituições, está definido em \citep[Part III]{caslrm}:

\begin{quotation}
``For a many-sorted signature $\Sigma$, a many-sorted model $M \in \textnormal{\textbf{Mod}}(\Sigma)$ assigns a non-empty carrier set to each sort in $\Sigma$, a partial resp. total function to each partial resp. total function symbol, and a predicate to each predicate symbol...''
\end{quotation}

Como já referido, os modelos atribuem um significado (interpretam) assinaturas e frases. Os modelos em \texttt{CASL}\index{CASL} resumem-se, por exemplo, no que diz respeito às operações, associar a especificação das operações à assinatura respectiva, seguindo a mesma filosofia para todas as restantes assinaturas e respetivas especificações.

Tendo em linha de conta que se pretende implementar uma extensão para HETS que implementa a lógica híbrida, torna-se essencial definir os conceitos básicos da mesma.

\subsection{Lógica Híbrida}\index{Lógica Híbrida}

A lógica híbrida começou a ser estudada por \textit{Arthur Prior} por volda dos anos sessenta. O termo lógica híbrida cobre um vasto número de lógicas obtidas por adicionar um maior poder expressivo à lógica modal\index{Lógica Modal}. Conforme já mencionado, a lógica híbrida mais básica é obtida por adição de nominais\index{Nominais}.

Em termos informais, como também já foi mencionado, um nominal pode ser visto como uma proposição lógica verdadeira apenas num estado de todos os estados existentes no sistema, permitindo dessa forma identificar univocamente (através dos nominais) um estado do sistema\citep{hybridLogic}.

A definição formal de lógica híbrida será abordada posteriormente aquando do estudo da sua implementação em HETS.
