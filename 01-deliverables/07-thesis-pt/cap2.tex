\pagestyle{empty}
\cleardoublepage

\chapter{Estado da Arte}\label{cap2}

\cleardoublepage

% Inclui o cabeçalho definido no meta.tex
\pagestyle{normalstyle}

Serve o presente capítulo para apresentar o estado da arte, mais concretamente, relatar o estado atual do desenvolvimento da ferramenta HETS\index{HETS}.

\section{HETS}

Conforme já referido anteriormente, a ferramenta HETS permite especificar e verificar sistemas. A sua implementação, baseada na teoria das instituições, dá especial significado ao conceito de \textbf{especificação heterogénea}\index{Especificação Heterogénea}.

De forma muito geral, o HETS pretende construir uma framework onde diferentes lógicas, suas ferramentas, bem como morfismos entre diferentes lógicas são combinados, tudo por base na definição instituicional\index{Instituições} das lógicas\citep{institutions}.

Existem já associados ao HETS vários \textit{provers} automáticos, tais como, \textit{Isabelle}, \textit{SPASS} entre outros. Permitindo que várias ferramentas de prova estejam ao dispor sem necessitar de, manualmente, realizar essa tarefa. O HETS suporta também várias linguagens, como por exemplo, \texttt{CASL}, \textit{Common Logic}, \textit{Maude} entre outras, sendo que, para a especificação heterogénea foi definida a linguagem \textit{heterogeneous CASL} (\texttt{HETCASL})\citep{habil}. Resumidamente, a linguagem \texttt{HETCASL} permite a combinação de espcificações heterogéneas.

\subsection{Arquitetura}


\begin{figure}[h]
\begin{center}
\includegraphics{images/hetsarch.png}
\caption{Arquitetura do HETS}
\label{fig:hetsarch}
\end{center}
\end{figure}

Uma visão geral da arquitetura do HETS\index{HETS} é ilustrada na Figura \ref{fig:hetsarch}. O HETS possui uma estrutura de interfaces\footnote{Em programação, a utilização de interfaces permite a composição de componentes de um software sem que a sua implementação seja conhecida\citep{computerArchitecture}.} para a definição de instituições (coluna da esquerda da Figura \ref{fig:hetsarch}), de forma a permitir que novas lógicas sejam integradas facilmente. Para tal, devem ser desenvolvidos três componentes essênciais da especificação básica das novas lógicas, nomeadamente:

\begin{itemize}
	\item Um \textit{parser}\index{Parser};
	\item Um \textit{static checker}\footnote{Um \textit{static checker} realisa a análise sintática, partindo da árvore sintática abstrata resultante do \textit{parsing}}\index{Análise Estática};
	\item Um \textit{prover}\index{Prover};
\end{itemize}

Tal lógica é implementada usando a linguagem Haskell\citep{haskell} cuja noção de interface é obtida por meio de classes em Haskell com vários parâmetros e dependências funcionais\citep{haskell, foundationsmathlogic}. Implementação análoga é usada para as transformações (entre lógicas - morfismos\index{Morfismo}).

Os módulos do HETS independentes das várias lógicas especificas, isto é, o \textit{core} do HETS, é visivel no lado direito da Figura \ref{fig:hetsarch}. O parser heterogéneo transforma o texto de entrada numa árvore sintática abstrata, usando para isso o \textit{parser} \texttt{Parsec}\index{Parsec}\citep{parsec, parsec1}. Este \textit{parser} mantém, em estado, a lógica na qual o \textit{parsing} deverá ser feito, permitindo devolver como resultado uma árvore sintática abstrata relativa a uma especificação básica de uma lógica concreta.

A análise estática (i.e. \textit{static analysis}) é baseada na análise à especificação básica\index{Especificação Básica}, permitindo transformar uma árvore sintática abstrata num \textit{development graph}\citep{developmentgraphs, habil}. Começando num nodo que corresponde a uma teoria ``vazia'', isto é, sem qualquer definição, sendo posteriormente, no decorrer da análise estática, extendida e/ou traduzida quer por via de simplificações (tradução intra-lógica), quer por via de morfismos de instituições (tradução inter-lógica) a teoria inicial, adicionando, simultaneamente, nodos e ligações ao \textit{development graph}\index{Development Graph}.

Para a obtenção de prova heterogénea são usados os \textit{development graphs}. No entanto, para obtenção de provas locais, isto é, relativas a um nodo do \textit{development graph}, são necessários provers especificos para a lógica específica desse nodo.

O HETS faz uso do formato, denominado, \textit{ATerm}\index{ATerm}\citep{aterms, aterm}, que é um formato utilizado como interface com outras ferramentas (por exemplo, \textit{provers} automáticos, permitindo obter provas de correção da especificação). Para mais detalhes é aconselhável a leitura de bibliografia auxiliar\citep{habil, hetspaper3}.

\subsection{Lógicas Suportadas}

A seguinte lista de lógicas (formalizadas usando a teoria das instutuições\index{Teoria das Instituições}\index{Instituições}) é atualmente suportada pelo HETS:

\begin{description}
	\item[CASL] extende a lógica de primeira ordem com funções parciais e \textit{subsorting}. Para mais detalhe é aconselhável a leitura de literatura auxiliar\citep{caslrm}. A implementação de \texttt{CASL} está feita de tal forma que é possível reutilizar a sua implementação. Tal feito é possível graças à utilização de variáveis polimórficas, isto é, as estruturas de dados da linguagem \texttt{CASL} são parametrizáveis permitindo extender todas elas, tais como, extender as assinaturas, os morfismos, a árvore sintática abstrata, etc. Desta forma é possível, com esforço moderado, integrar extensões à linguagem \texttt{CASL}.
	\item[CoCASL] \citep{cocasl} é uma extensão co-algébrica do \texttt{CASL} que serve para a especificação de tipos de processos e sistemas reativos.
	\item[ExtModal] é uma extensão à lógica modal, atualmente em estado experimental.
	\item[HasCASL] é uma extensão de segunda ordem do \texttt{CASL}, permitindo especificar tipos de dados polimórficos e funções. Está bastante relacionada com a linguagem de programação Haskell\index{Haskell}, uma visão geral desta lógica é dada em \citep{hascasl}.
	\item[Haskell] é uma linguagem de programação funcional, simultaneamente é a linguagem de implementação do HETS, para mais informações, é recomendável a leitura de \citep{haskell98, haskell}.
	\item[CspCASL] resulta da combinação de \texttt{CASL} com CSP\citep{caspcasl}.	
	\item[CommonLogic] \textit{Common logic} (CL) é uma \textit{framework} conceptual para uma família de linguagens lógicas baseadas em lógica de primeira ordem\citep{commonlogic}.
	\item[ConstraintCASL] é uma lógica em estado experimental para a especificação de calculo qualitativo de restrições (definição retirada do código fonte, não há literatura auxiliar associada).
	\item[OWL DL] é a linguagem de ontologia \textit{web} recomendada pela \textit{World Wide Web Consortium} (W3C, \url{http://www.w3c.org}), usada para representação de conhecimento e de \textit{web} semântica\citep{semanticweb}.
	\item[CASL-DL] é uma extensão ao \texttt{CASL} com restrições de cardinalidade para a especificação de tipos e predicados unários\citep{ontologies}.
	\item[Propositional] é a clássica lógica proposicional.
	\item[QBF] é a lógica que agrega os quantificadores booleanos.
	\item[RelScheme] é uma lógica para base de dados relacionais\citep{dbontology}.
	\item[SoftFOL] oferece várias ferramentas de prova automática para a lógica de primeira ordem com igualdade (existencial)\citep{softCASL}.
	\item[VSE] é um provador de teoremas interativo\citep{vse} usado no HETS.
	\item[DMU] é uma lógica para ler o resultado da ``Computer Aided Three-dimensional
Interactive Applicatio'' (Catia).
	\item[FreeCAD] é uma lógica para ler ficheiro de desenho CAD, mais precisamente, da ferramenta FreeCAD (\url{http://sourceforge.net/projects/free-cad}).
	\item[Maude] é um sistema de re-escrita para a lógica de primeira ordem (\url{http://maude.cs.uiuc.edu/}).
	\item[DFOL] é uma extensão à lógica de primeira ordem com tipos de dados dependentes\citep{dfol}.
	\item[LF] é a teoria de tipos dependentes para a ferramenta Twelf (\url{http://twelf.plparty.org/}). 
	\item[Framework] é uma lógica que permite especificar definições lógicas declarativas\citep{framework}.
	\item[Adl] ``A Description Language'' é uma linguagem de descrição baseada na algebra relacional, originalmente desenhada para definir regras de negócio\citep{businessRules}.
	\item[Fpl] é uma ``lógica para programas funcionais'', é uma extensão de uma restrição do \texttt{CASL} (restrição que retira os predicados das assinaturas).
	\item[EnCL] é uma ``linguagem de calculos de engenharia'' baseada na teoria de primeira ordem dos números reais\citep{encl}.
	\item[THF] é uma linguagem de intercâmbio para a lógica de segunda ordem\citep{thf}.

\end{description}


Várias lógicas são suportadas, algumas delas com ferramentas de prova automática associada. Assim, o suporte para obtenção de provas para uma lógica sem sistema de prova associado, pode ser obtido mediante a aplicação de morfismos entre instituições\index{Instituições}.

\subsection{Morfismos Suportados}

Em HETS existe já a implementação de morfismos entre várias lógicas (instituições\index{Instituições}\index{Morfismo de Instituições}), segue-se assim uma listagem dos morfismos atualmente possíveis.

\begin{center}
Adl $\rightarrow$ CASL \\
CASL $\rightarrow$ CoCASL \\
CASL $\rightarrow$  CspCASL \\
CASL $\rightarrow$ HasCASL \\
CASL $\rightarrow$ Isabelle \\
CASL $\rightarrow$  Modal \\
CASL $\rightarrow$ Propositional \\ 
CASL $\rightarrow$ SoftFOL \\
CASL $\rightarrow$ VSE \\
CoCASL $\rightarrow$ Isabelle \\
CommonLogic $\rightarrow$ CASL \\
CspCASL $\rightarrow$ Modal \\
DFOL $\rightarrow$ CASL \\
DMU $\rightarrow$ OWL \\
HasCASL $\rightarrow$ Haskell \\
Haskell $\rightarrow$ Isabelle \\
Maude $\rightarrow$ CASL \\
Modal $\rightarrow$ CASL \\
OWL $\rightarrow$ CASL \\
OWL $\rightarrow$ CommonLogic \\
Propositional $\rightarrow$ CASL \\ 
Propositional $\rightarrow$ QBF \\
QBF $\rightarrow$ Propositional \\
RelScheme $\rightarrow$ CASL \\
\end{center}

Conforme se pode observar, o HETS é uma ferramenta já bastante desenvolvida, com bastantes lógicas implementadas, assim como morfismos entre lógicas permitindo fazer do HETS uma ferramenta bastante poderosa, no que diz respeito à especificação e verificaçã.