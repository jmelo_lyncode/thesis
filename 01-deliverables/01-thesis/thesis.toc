\select@language {english}
\contentsline {chapter}{List of Figures}{vii}{section*.3}
\contentsline {chapter}{List of Tables}{ix}{section*.5}
\contentsline {chapter}{Glossary}{xi}{chapter*.6}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Formal Methods}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}Aims of the project}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Final aim}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Preliminary aims}{3}{section.2.2}
\contentsline {chapter}{\numberline {3}State of the Art}{5}{chapter.3}
\contentsline {chapter}{\numberline {4}Preliminars}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}Category Theory}{7}{section.4.1}
\contentsline {section}{\numberline {4.2}Model Theory}{8}{section.4.2}
\contentsline {section}{\numberline {4.3}Institutions}{9}{section.4.3}
\contentsline {chapter}{\numberline {5}Hybrid Logic}{11}{chapter.5}
\contentsline {chapter}{\numberline {6}Perspectives}{13}{chapter.6}
\contentsline {chapter}{\numberline {7}Heterogeneous Tool Set}{15}{chapter.7}
\contentsline {chapter}{\numberline {8}Hybridization}{17}{chapter.8}
\contentsline {section}{\numberline {8.1}Automated Hybridization}{17}{section.8.1}
\contentsline {chapter}{\numberline {9}A new practical approach}{19}{chapter.9}
\contentsline {chapter}{\numberline {10}Conclusion}{21}{chapter.10}
\contentsline {chapter}{Bibliography}{23}{section*.8}
