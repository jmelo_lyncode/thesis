% this file is called up by thesis.tex
% content in this file will be fed into the main document

%: ----------------------- name of chapter  -------------------------
\chapter{Preliminars} % top level followed by section, subsection


%: ----------------------- paths to graphics ------------------------

% change according to folder and file names
\ifpdf
    \graphicspath{{X/figures/PNG/}{X/figures/PDF/}{X/figures/}}
\else
    \graphicspath{{X/figures/EPS/}{X/figures/}}
\fi

%: ----------------------- contents from here ------------------------

Within this chapter we will give a brief over some used theoretical concepts, namely, Category Theory, Model Theory and Institution Theory.

\section{Category Theory}

Category theory first appeared in \citep{generaltheorycategory}. Informally, a category could be seen as structure that contains a set of objects and also a set of transformations (formally called $\mathbf{morphisms}$) between two $\mathbf{objects}$. Transformations could be composed, so one transformation could use as input, the output of another transformation. 

In general, the notion of category provides a fundamental and abstract way to describe mathematical entities and their relationships\citep{categoriessimple, generaltheorycategory, categories, categories1}. 

$\mathbf{Morphisms}$ are usually represented as arrows. 

\begin{notation}[Morphism]\label{not:morphism}
Let $f$ be a $\mathbf{morphism}$ between two $\mathbf{objects}$ a $a$ and $b$, one denote $f$ as,

\begin{center}
	$f : a \rightarrow b$
\end{center}
\end{notation}

Using morphisms one could establish the sense of composition, which, as explained before, the input for a morphism (for example, $f$), being the output of another (for example, $g$). Conventionally represented as $f \circ g$.

Formally, the definition of Category uses the concept of class from the Set Theory\cite{settheory}.


\begin{definition}[Category]\label{def:category}
A Category ($\mathbf{C}$) is defined as pair:

\begin{center}
$\mathbf{C} = (O, H)$
\end{center}

where $O$ is a class of $\mathbf{objects}$ and $H$ a class of $\mathbf{morphisms}$, with the following axioms:

\begin{description}
	\item[Associativity] of $\mathbf{morphisms}$, $\forall f, g, h \in H : (f \circ g) \circ h : f \circ (g \circ h)$
	\item[Identity] $\mathbf{morphism}$, $\forall x \in O :: \exists (m : a \rightarrow b) \in H \wedge  a = b = x $ 
\end{description}
\end{definition}

As stated before, $\mathbf{objects}$ could be anything. A special Category is the one where $\mathbf{objects}$ are specific Categories, usually called the ``Category of Categories'', and denoted as \textbf{CAT}. 

\begin{definition}[Functor]
A \textnormal{\textbf{Functor}} is a \textbf{morphism} within \textnormal{\textbf{CAT}}.
\end{definition}

% Forgetful functor \citep{categoriessimple}.


\section{Model Theory}

The Model Theory examines semantical elements by means of syntactical elements of a corresponding language\citep{modeltheory, modeltheory1, modeltheory2}, the most important definition within Model Theory is the definition of Structure\citep{modeltheory-intro, mathstructure}.

\begin{definition}[Structure]
A structure $\mathcal{M}$ is a tuple $(\mathcal{D}, \mathcal{L}, \mathcal{I})$\citep{modeltheory-intro1} where:

\begin{itemize}
	\item $\mathcal{D}$, usually called the data of $\mathcal{M}$ is a tuple $(dom(\mathcal{M}), \mathbf{F}, \mathbf{R}, \mathbf{C})$, where:
	\begin{itemize}
		\item $dom(\mathcal{M})$ is the domain of $\mathcal{M}$;
		\item $\mathbf{F}$ is the set of $n$-ary functions of $\mathcal{M}$, each denoted as $f_{\mathcal{M}}$;
		\item $\mathbf{R}$ is the set of $n$-ary relations of $\mathcal{M}$, each denoted as $R_{\mathcal{M}}$;
		\item $\mathbf{C}$ is the set of constants of $\mathcal{M}$, each denoted as $c_{\mathcal{M}}$;
	\end{itemize}
	\item $\mathcal{L}$ is called the Language or Signatures of $\mathcal{M}$, it's a tuple $(\mathcal{F}, \mathcal{R}, \mathcal{C})$, where:
	\begin{itemize}
		\item $\mathcal{F}$ is the set of functions symbols (each function $f_{\mathcal{M}} \in \mathbf{F}$ could be represented by more than one function symbol), each function symbols denoted as $f$;
		\item $\mathcal{R}$ is the set of relation symbols (each relation $R_{\mathcal{M}} \in \mathbf{R}$ could be represented by more than one relation symbol), each relation symbol denoted as $R$;
		\item $\mathcal{C}$ is the set of constant symbols (each constant $c_{\mathcal{M}} \in \mathbf{C}$ could be represented by more than one constant symbol), each constant symbol denoted as $c$;
	\end{itemize}
	\item $\mathcal{I}$ is an interpretation function, that is, a function that assigns to each constant symbol $c$, function symbol $f$ and relation symbol $R$, respectively, a constant $c_{\mathcal{M}}$, a function $f_{\mathcal{M}}$ and a relation $R_{\mathcal{M}}$.
\end{itemize}
\end{definition}

Another important concept within the Model Theory is the concept of Sentence - denoted as $\phi$ - (also called string of symbols), witch can be viewed as formulas that uses symbols (constant symbols, function symbols and relation symbols). 

\begin{definition}[Model]
As $\phi$ uses symbols of $\mathcal{M}$ then $\phi$ makes a statement about $\mathcal{M}$, that statement may be $\mathbf{true}$ or $\mathbf{false}$. If it is \textbf{true}, one say that $\mathcal{M}$ is a model of $\phi$, denoted as:

\begin{center}
	$\mathcal{M} \vDash \phi$
\end{center}
\end{definition}

\section{Institutions}

Institutions are an abstraction over the concept of logic, using as background the theory of Categories\cite{whatislogic,logicproof,institutionmodeltheory}. One will give the formal definition and then an informal and simplified one.

\begin{definition}[Institution]\label{def:institution}
An institution $I = (\textnormal{\textbf{Sign}}, \textnormal{\textbf{Sen}}, \textnormal{\textbf{Mod}}, \vDash)$ consists of
\begin{itemize}
	\item a Category of signatures (denoted as $\textnormal{\textbf{Sign}}$),
	\item a Functor $\textnormal{\textbf{Sen}} : \textnormal{\textbf{Sign}} \rightarrow \textnormal{\textbf{Set}}$ giving, for each signature $\Sigma$ the set of sentences $\textnormal{\textbf{Sen}} (\Sigma)$, and for each signature morphism $\sigma: \Sigma \rightarrow \Sigma'$, the sentence translation map $\textnormal{\textbf{Sen}} (\sigma) : \textnormal{\textbf{Sen}} (\Sigma) \rightarrow \textnormal{\textbf{Sen}} (\Sigma')$, where often $\textnormal{\textbf{Sen}} (\sigma)(\varphi)$ is written as $\sigma(\varphi)$,
	\item a Functor $\textnormal{\textbf{Mod}} : \textnormal{\textbf{Sign}}^{op} \rightarrow \textnormal{\textbf{CAT}}$ giving, for each signature $\Sigma$, the category of models $\textnormal{\textbf{Mod}}(\Sigma)$, and for each signature morphism $\sigma : \Sigma \rightarrow \Sigma'$, the reduct functor $\textnormal{\textbf{Mod}}(\sigma) : \textnormal{\textbf{Mod}}(\Sigma') \rightarrow \textnormal{\textbf{Mod}}(\Sigma)$, where often $\textnormal{\textbf{Mod}}(\sigma)(M')$ is written as $M'\mid_\sigma$,
	\item a satisfaction relation $\vDash_{\Sigma} \subseteq \abs{\textnormal{\textbf{Mod}} (\Sigma)} \times \textnormal{\textbf{Sen}} (\Sigma)$ for each $\Sigma \in \textnormal{\textbf{Sign}}$,
\end{itemize}

such that for each $\sigma : \Sigma \rightarrow \Sigma'$ in $\textnormal{\textbf{Sign}}$ the following satisfaction condition holds:
\begin{center}
	$M' \vDash_{\Sigma'} \sigma(\varphi) \Leftrightarrow M'\mid_{\sigma} \vDash_{\Sigma} \varphi$
\end{center}

for each $M' \in \textnormal{\textbf{Mod}}(\Sigma')$ and $\varphi \in \textnormal{\textbf{Sen}}(\Sigma)$. Within this context one could define an arbitrary logic as a Institution equipped with an entailment system\citep{generalogics}.
\end{definition}



% ---------------------------------------------------------------------------
%: ----------------------- end of thesis sub-document ------------------------
% ---------------------------------------------------------------------------

